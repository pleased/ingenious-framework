#!/bin/bash

RED='\033[0;31m'
GREEN='\033[1;32m'
BLUE='\033[1;34m'
NC='\033[0m'

CONTAINER_NAME=ingeniousframe
IMAGE_NAME=registry.gitlab.com/pleased/ingenious-framework
USE_NVIM=false

USE_GPU=false
USE_VIRTUAL_DISPLAY=false

while [ "$#" -gt 0 ]; do
    case $1 in
        --use-gpu) USE_GPU=true ;;
        --use-virtual-display) USE_VIRTUAL_DISPLAY=true ;;
        --nvim) IMAGE_NAME=ingenious-framework-nvim ; CONTAINER_NAME=ingeniousframe-nvim; USE_NVIM=true ;;
        *) printf "\n${RED}Unknown parameter passed: $1"
           printf "\n${RED}Usage: sh mountCode.sh [--use-gpu] [--use-virtual-display] [--nvim]${NC}\n"
           exit 1
    esac
    shift
done

if ! $USE_NVIM ; then
    printf "\n${BLUE}Creating default docker container; Add argument '--nvim' to include the nvim configuration. ${NC}\n"
fi

### Development mode
# Stop the current ingenious container if it is running
printf "\n${GREEN}Stopping current container...${NC}\n"
docker stop "${CONTAINER_NAME}"

# Remove the current container
printf "\n${GREEN}Removing current container...${NC}\n"
if ! docker container rm "${CONTAINER_NAME}"; then
    sleep 5
fi

# Remove .gradle directory
printf "\n${GREEN}Removing old .gradle files...${NC}\n"
rm -rf IngeniousFrame/.gradle

os="$(uname -s)"

printf "\n${GREEN}Running on ${os}...${NC}\n"

case "${os}" in
Linux*)
    printf "\n${GREEN}Attempting mount on Linux...${NC}\n"
    ## Linux
    xhost local:root

    # Build docker command:
    docker_command="docker run --rm -dit"
    if $USE_GPU; then
        docker_command+=" --gpus 0"
    fi
    if ! $USE_VIRTUAL_DISPLAY; then
        docker_command+=" --volume /tmp/.X11-unix:/tmp/.X11-unix"
    fi
    docker_command+=" --env DISPLAY=${DISPLAY}"
    docker_command+=" --volume $(pwd)/../IngeniousFrame:/home/ingenious/IngeniousFrame"
    docker_command+=" --network host"
    docker_command+=" --memory=300g"
    docker_command+=" --name ${CONTAINER_NAME}"
    docker_command+=" ${IMAGE_NAME}"

    echo "$docker_command"
    $docker_command

    if [ $? -eq 0 ]; then
        printf "\n${BLUE}Container mounted successfully!${NC}\n"
    else
        printf "\n${RED}Container failed to mount using Linux command.${NC}\n"
    fi
    ;;
MINGW*)
    printf "\n${RED}Windows is not supported... the linux mount option
    can be used through WSL.${NC}\n"

    ;;
CYGWIN*)
    printf "\n${RED}Cygwin mount is not implemented yet.${NC}\n"
    ;;
Darwin*)
    printf "\n${RED}Mac OS mount is not implemented yet.${NC}\n"
    ;;
*)
    printf "\n${RED}Unknown OS. Please execute mount code manually.${NC}\n"
    ;;
esac

docker exec -d "${CONTAINER_NAME}" /bin/bash -c "mkdir ~/tmp && touch ~/tmp/setup.sh"

if $USE_NVIM; then
    docker exec -it "${CONTAINER_NAME}" /bin/bash -c "echo 'exec zsh -l' >> ~/tmp/setup.sh"


    docker exec -it "${CONTAINER_NAME}" /bin/bash -c "echo 'source ~/tmp/permission-setup.sh' >> ~/.zlogin"
    docker exec -it "${CONTAINER_NAME}" /bin/bash -c "echo 'sudo chmod -R a+rwx ~/IngeniousFrame' >> ~/tmp/permission-setup.sh"

    # This command includes virtual display configuration for zsh shell session - if present.
    # ~/.zlogin is sourced during zsh start up.
    if $USE_VIRTUAL_DISPLAY; then
        docker exec -it "${CONTAINER_NAME}" /bin/bash -c "echo 'source ~/tmp/virtual-display.sh' >> ~/.zlogin"
    fi
fi

if $USE_VIRTUAL_DISPLAY; then
  docker exec -d "${CONTAINER_NAME}" /bin/bash -c "touch ~/tmp/virtual-display.sh && echo 'source ~/tmp/virtual-display.sh' >> ~/tmp/setup.sh"
  docker exec -d "${CONTAINER_NAME}" /bin/bash -c "echo 'Xvfb :2 &' >> ~/tmp/virtual-display.sh"
  docker exec -d "${CONTAINER_NAME}" /bin/bash -c "echo 'export DISPLAY=:2' >> ~/tmp/virtual-display.sh"
fi
