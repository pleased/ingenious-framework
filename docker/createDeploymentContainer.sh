GREEN='\033[1;32m'
NC='\033[0m'

# Stop the current ingenious container if it is running
printf "\n${GREEN}Stopping current container...${NC}\n"
docker stop ingeniousframe

# Remove the current container
printf "\n${GREEN}Removing current container...${NC}\n"
docker container rm ingeniousframe

# Build new docker image
printf "\n${GREEN}Building new docker image...${NC}\n"
docker build --no-cache -t ingenious-frame IngeniousFrame/

# Remove old docker image
printf "\n${GREEN}Removing old docker image...${NC}\n"
echo "y" | docker image prune
docker rmi $(docker images -f "dangling=true" -q)

# Create and run a new ingenious-frame container
printf "\n${GREEN}Creating new container...${NC}\n"
xhost local:root
docker run -dit \
    --env DISPLAY=${DISPLAY} \
    --volume /tmp/.X11-unix:/tmp/.X11-unix \
    --network host \
    --memory=300g \
    --name ingeniousframe \
    ingenious-frame
printf "\n${GREEN}Done.${NC}\n"