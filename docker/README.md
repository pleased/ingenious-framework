# Docker configuration for the Ingenious Framework

The Ingenious Framework can be deployed in a docker container.
To pull the main image, run:
```bash
docker pull registry.gitlab.com/pleased/ingenious-framework
```

The framework can either be setup in a _development_ or _deployment_ mode.

## Development mode

To set up the container in development mode, run:
```bash
bash createDevelopmentContainer.sh [--use-gpu] [--use-virtual-display] [--nvim]
```
where the tags are optional, defaulting to no gpu usage and a configuration
tied to the host display if the respective tags are not provided. Note
that this script removes the old `ingeniousframe` container and all dangling 
images.

The `createDevelopmentContainer.sh` script includes a setup file inside the container to 
configure the desired environment.

Execute 
```bash
docker attach ingeniousframe
```
to attach the container, and once inside the container, run
```bash
source ~/tmp/setup.sh
```

In development mode, the container mounts the [IngeniousFrame](../IngeniousFrame) directory to 
the workspace in the container. Changes in the container are synced to the local file system, 
maintaining any modifications after the container is exited.

The container has gpu support with the argument `--gpus all`/`--gpus 0,1`.
If you are using rootless docker, you may need to set the "no-cgroups" option
to "true" in `/etc/nvidia-container-runtime/config.toml`.

The docker container supports GUI applications when run from a linux system with X11 installed

`If developing from a non-linux OS, performance may be severely degraded` when using the development container. For
example, with MCTS, the playout-count in a Windows-mounted container is roughly a tenth of a linux-mounted container's
playout-count.

## Deployment mode

To build the image and set up the container in deployment mode, run:
```bash
bash createDeploymentContainer.sh
```

The deployment container makes a copy of the file system, i.e. local changes and container-based changes to files are
distinct. Note that this script removes the old ingeniousframe container and all dangling images.

## Docker commands

To attach the container's terminal to the local terminal run `docker attach ingeniousframe`. To detach the local
terminal from the container's terminal use the keyboard shortcut `CTRL + P, CTRL + Q`.

The docker container can be stopped or restarted with `docker stop ingeniousframe` or `docker restart ingeniousframe`.
To remove the volumes used by old containers, use the `docker volume prune` command.

## Docker image for continuous integration

A docker image has been implemented for use in the CI pipeline.
The image is significantly smaller, built upon ubuntu and containing only 
the necessary dependencies for the java test suite to execute.
The docker file for this image is named `Dockerfile-ci`.

## Updating docker images

To update the base docker images, run:
```bash
bash updateBaseImage.sh
```
You will be prompted for your gitlab username and password. 
The script will then build the image based on the dockerfile in this directory and push it to the gitlab registry.

A similar script exists for updating the CI image, named `updateCiImage.sh`.

## Neovim configured container

A separate dockerfile exists, building upon `registry.gitlab.com/pleased/ingenious-framework`, that includes the use of
`zsh` and a custom `nvim` configuration obtained [here](https://github.com/WizardStark/dotfiles)

To use this configuration, build the docker image by running:
```bash
bash createNeoVimImage.sh
```

To create the container include the '--nvim' flag when running the `createDevelopmentContainer.sh` script.

As stated for the vanilla docker container, the `createDevelopmentContainer.sh` script includes a setup file inside 
the container to configure the desired environment.

Execute
```bash
docker attach ingeniousframe-nvim
```
to attach the container, and once inside the container, execute
```bash
source ~/tmp/setup.sh
```

### Neovim image nuances

- `vi` is aliased to `vim`, and if you prefer vanilla interactions stick to this + bash.
- Otherwise, `vim` is aliased to a Neovim configuration which can be found [here](https://github.com/WizardStark/dotfiles).
  Upon first launch, many plugins will be installed, including a Java LSP. Files can be traversed with spacebar+e and the
  arrow keys (or hjkl); and a list of commands and their descriptions can be brought up with spacebar+spacebar.
- a couple of 'ease-of-use' terminal extensions will be installed upon first launching zsh, some of which include CTRL + T
  for file navigation and CTRL + R for command history search.