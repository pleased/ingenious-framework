package za.ac.sun.cs.ingenious.games.mdp;

import static org.junit.jupiter.api.Assertions.assertEquals;

import za.ac.sun.cs.ingenious.games.mdp.testData.MDPTestData;
import org.junit.jupiter.api.Test;
import za.ac.sun.cs.ingenious.core.exception.games.mdp.BadMDPSettingException;

public class MDPRewardEvaluatorTest {

  @Test
  public void getScore_givenState_ReturnsCorrectReward() throws BadMDPSettingException {
    // Set up
    MDPTestData.testMdpSetting01.ensureValid();
    MDPRewardEvaluator<Long> evaluator =
        MDPRewardEvaluator.fromSetting(MDPTestData.testMdpSetting01);

    MDPState<Long> state = new MDPState<>(0L);
    state.nextGameState(1L);

    // Act
    double[] result = evaluator.getReward(state);

    // Assert
    assertEquals(-5.0, result[0], 0.0);
  }
}
