package za.ac.sun.cs.ingenious.core.data.structures.dag;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;

public class DAGTest {

  @Test
  public void AddNodesTest() {
    DAG<TestGameState, Action> dag = getDAG();

    assertEquals(5, dag.size());
    assertEquals(9, dag.getNumEdges());
  }

  @Test
  public void SortedIteratorTest() {
    DAG<TestGameState, Action> dag = getDAG();

    int[] nodeOrder = {1, 4, 3, 0, 2};
    int i = 0;

    for (TestGameState node : dag.getSortedList()) {
      assertEquals(nodeOrder[i], node.nodeNumber);
      i++;
    }
  }

  private DAG<TestGameState, Action> getDAG() {
    DAG<TestGameState, Action> dag = new DAG<>();

    TestGameState state0 = new TestGameState(1, 0);
    TestGameState state1 = new TestGameState(1, 1);
    TestGameState state2 = new TestGameState(1, 2);
    TestGameState state3 = new TestGameState(1, 3);
    TestGameState state4 = new TestGameState(1, 4);

    dag.addNode(state0);
    dag.addNode(state1);
    dag.addNode(state2);
    dag.addNode(state3);
    dag.addNode(state4);

    dag.addEdge(state1, state2, null);
    dag.addEdge(state1, state3, null);
    dag.addEdge(state1, state0, null);
    dag.addEdge(state3, state2, null);
    dag.addEdge(state3, state0, null);
    dag.addEdge(state4, state3, null);
    dag.addEdge(state4, state0, null);
    dag.addEdge(state1, state4, null);
    dag.addEdge(state0, state2, null);

    return dag;
  }

  private class TestGameState extends GameState {

    private int nodeNumber;

    public TestGameState deepCopy() {
      // TODO: Replace this with a custom deep copy implementation, as the cloner
      // library is slow and may lead to segfaults. See issue #323
      return cloner.deepClone(this);
    }

    public TestGameState(int numPlayers, int nodeNumber) {
      super(numPlayers);

      this.nodeNumber = nodeNumber;
    }

    @Override
    public String toString() {
      return "Node : " + nodeNumber;
    }

    @Override
    public int hashCode() {
      return nodeNumber;
    }
  }
}
