package za.ac.sun.cs.ingenious.games.mdp.testData;

import java.util.Arrays;
import za.ac.sun.cs.ingenious.games.mdp.util.MDPSetting;

public class MDPTestData {
  public static final MDPSetting testMdpSetting01 =
      new MDPSetting(
          0,
          3,
          2,
          // Set of terminal states.
          Arrays.asList(1, 2),
          // R(S, S')
          Arrays.asList(
              Arrays.asList(0.0, -5.0, 5.0),
              Arrays.asList(0.0, 0.0, 0.0),
              Arrays.asList(0.0, 0.0, 0.0)),
          // Valid actions A(S)
          Arrays.asList(Arrays.asList(0, 1), Arrays.asList(), Arrays.asList()),
          // Dynamics P(S, A, S).
          Arrays.asList(
              Arrays.asList(
                  Arrays.asList(0.0, 1.0, 0.0), // P(S0, A0, *)
                  Arrays.asList(0.0, 0.0, 1.0) // P(S0, A1, *)
                  ),
              Arrays.asList(
                  Arrays.asList(0.0, 0.0, 0.0), // P(S1, A0, *)
                  Arrays.asList(0.0, 0.0, 0.0) // P(S1, A1, *)
                  ),
              Arrays.asList(
                  Arrays.asList(0.0, 0.0, 0.0), // P(S2, A0, *)
                  Arrays.asList(0.0, 0.0, 0.0) // P(S2, A1, *)
                  )));

  public static final MDPSetting testMdpSetting02 =
      new MDPSetting(
          0,
          4,
          2,
          // Set of terminal states.
          Arrays.asList(1, 2, 3),
          // R(S, S')
          Arrays.asList(
              Arrays.asList(0.0, 1.0, 2.0, 3.0),
              Arrays.asList(0.0, 0.0, 0.0, 0.0),
              Arrays.asList(0.0, 0.0, 0.0, 0.0),
              Arrays.asList(0.0, 0.0, 0.0, 0.0)),
          // Valid actions A(S)
          Arrays.asList(
              Arrays.asList(0, 1), Arrays.asList(0, 1), Arrays.asList(0, 1), Arrays.asList(0, 1)),
          // Dynamics P(S, A, S).
          Arrays.asList(
              Arrays.asList(
                  Arrays.asList(0.0, 0.3, 0.0, 0.7), // P(S0, A0, *)
                  Arrays.asList(0.0, 0.0, 1.0, 0.0) // P(S0, A1, *)
                  ),
              Arrays.asList(
                  Arrays.asList(0.0, 0.0, 0.0, 0.0), // P(S1, A0, *)
                  Arrays.asList(0.0, 0.0, 0.0, 0.0) // P(S1, A1, *)
                  ),
              Arrays.asList(
                  Arrays.asList(0.0, 0.0, 0.0, 0.0), // P(S2, A0, *)
                  Arrays.asList(0.0, 0.0, 0.0, 0.0) // P(S2, A1, *)
                  ),
              Arrays.asList(
                  Arrays.asList(0.0, 0.0, 0.0, 0.0), // P(S3, A0, *)
                  Arrays.asList(0.0, 0.0, 0.0, 0.0) // P(S3, A1, *)
                  )));

  public static final MDPSetting testMdpSetting03 =
      new MDPSetting(
          0,
          3,
          2,
          // Set of terminal states.
          Arrays.asList(2),
          // R(S, S')
          Arrays.asList(
              Arrays.asList(-1.0, -1.0, 5.0),
              Arrays.asList(-1.0, -1.0, 5.0),
              Arrays.asList(-1.0, -1.0, 5.0)),
          // Valid actions A(S)
          Arrays.asList(Arrays.asList(0, 1), Arrays.asList(0, 1), Arrays.asList(0, 1)),
          // Dynamics P(S, A, S).
          Arrays.asList(
              Arrays.asList(
                  Arrays.asList(1.0, 0.0, 0.0), // P(S0, A0, *)
                  Arrays.asList(0.0, 1.0, 0.0) // P(S0, A1, *)
                  ),
              Arrays.asList(
                  Arrays.asList(1.0, 0.0, 0.0), // P(S1, A0, *)
                  Arrays.asList(0.0, 0.0, 1.0) // P(S1, A1, *)
                  ),
              Arrays.asList(
                  Arrays.asList(0.0, 1.0, 0.0), // P(S2, A0, *)
                  Arrays.asList(0.0, 0.0, 1.0) // P(S2, A1, *)
                  )));
}
