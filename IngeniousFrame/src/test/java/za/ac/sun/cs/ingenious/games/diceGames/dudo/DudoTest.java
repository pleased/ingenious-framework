package za.ac.sun.cs.ingenious.games.diceGames.dudo;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static za.ac.sun.cs.ingenious.search.cfr.Helper.getNormalizedDistribution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.junit.jupiter.api.Test;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.exception.probability.IncorrectlyNormalizedDistributionException;
import za.ac.sun.cs.ingenious.core.util.ClassUtils;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.selector.ActionSelector;
import za.ac.sun.cs.ingenious.core.util.selector.UniformActionSelector;
import za.ac.sun.cs.ingenious.games.diceGames.actions.DiceRollAction;

public class DudoTest {
  int numPlayers = 2;
  int numSides = 6;
  int numDice = 2;

  DudoLogic logic = new DudoLogic();
  Random rand = new Random();

  /** Test whether the first player to act can be determined from an information set */
  @Test
  public void FirstPlayerTest() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    logic.makeMove(state, new DiceRollAction(0, 1));
    logic.makeMove(state, new DiceRollAction(0, 1));
    logic.makeMove(state, new DiceRollAction(1, 3));
    logic.makeMove(state, new DiceRollAction(1, 2));

    int firstPlayer = state.getFirstPlayer();

    assertEquals(
        logic.getCurrentPlayersToAct(logic.observeState(state, 0)).toArray()[0], firstPlayer);
    assertEquals(
        logic.getCurrentPlayersToAct(logic.observeState(state, 1)).toArray()[0], firstPlayer);
  }

  /** Test multiple samples are all the type DiceRollAction */
  @Test
  public void GenerateStochasticActionsTest() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    Map<Action, Double> actions = logic.generateStochasticActions(state);

    for (int i = 0; i < actions.size(); i++) {
      try {
        Action action = ClassUtils.chooseFromDistribution(actions, rand);

        assertTrue(action instanceof DiceRollAction);

        int roll = ((DiceRollAction) action).getRoll();
        assertTrue(roll > 0 && roll <= numSides);
      } catch (IncorrectlyNormalizedDistributionException e) {
        fail("Dice roll action sampled from a unnormalized distribution");
      }
    }
  }

  /**
   * Test whether the uniform action selector approximately samples uniformly from the possible
   * stochastic actions
   */
  @Test
  public void UniformActionSelectorTest() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    Map<Action, Double> actions = logic.generateStochasticActions(state);
    Map<Action, Double> actionCounts = new HashMap<>();

    for (Action action : actions.keySet()) {
      actionCounts.put(action, 0.0);
    }
    ActionSelector selector = UniformActionSelector.getInstance();

    for (int i = 0; i < 10000; i++) {
      Action action = selector.getAction(state, logic);
      actionCounts.put(action, actionCounts.get(action) + 1.0);
    }

    Map<Action, Double> normalizedCounts = getNormalizedDistribution(actionCounts);
  }

  /**
   * Test that dice roll actions are correctly being applied to a game state. Also tests whether
   * actions are being created for the correct player.
   */
  @Test
  public void ApplyDiceRollActionsTest() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);
    Move[][] playerRolls = state.getPlayerRolls();

    for (int i = 0; i < numPlayers; i++) {
      for (int j = 0; j < numDice; j++) {
        assertEquals(numPlayers, logic.getCurrentPlayersToAct(state).toArray()[0]);
        Map<Action, Double> actions = logic.generateStochasticActions(state);
        try {
          DiceRollAction diceRoll =
              (DiceRollAction) ClassUtils.chooseFromDistribution(actions, rand);

          logic.makeMove(state, diceRoll);

          assertSame(playerRolls[i][j], diceRoll);

        } catch (IncorrectlyNormalizedDistributionException e) {
          fail("Dice roll action sampled from a unnormalized distribution");
        }
      }
    }
  }

  /**
   * Test whether the stochastic player is chosen to play for the correct number of times, followed
   * by an actual player
   */
  @Test
  public void ChooseStochasticPlayerTest() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    for (int i = 0; i < numPlayers; i++) {
      Map<Action, Double> actions = logic.generateStochasticActions(state);
      for (int j = 0; j < numDice; j++) {
        Set<Integer> playerSet = logic.getCurrentPlayersToAct(state);
        int playerID = ((Integer) playerSet.toArray()[0]);
        assertEquals(playerID, numPlayers);

        try {
          logic.makeMove(state, ClassUtils.chooseFromDistribution(actions, rand));
        } catch (IncorrectlyNormalizedDistributionException e) {
          fail("Dice roll action sampled from a unnormalized distribution");
        }
      }
    }

    Set<Integer> playerSet = logic.getCurrentPlayersToAct(state);
    int playerID = (Integer) playerSet.toArray()[0];
    assertTrue(playerID >= 0 && playerID < numPlayers);
  }

  private void getDiceRolls(DudoGameState state) {
    for (int i = 0; i < numPlayers; i++) {
      Map<Action, Double> actions = logic.generateStochasticActions(state);
      for (int j = 0; j < state.getNumPlayerDice()[i]; j++) {
        try {
          logic.makeMove(state, ClassUtils.chooseFromDistribution(actions, rand));
        } catch (IncorrectlyNormalizedDistributionException e) {
          fail("Dice roll action sampled from a unnormalized distribution");
        }
      }
    }
  }

  /**
   * Test whether the first player is chosen after all dice rolls are made and is followed by
   * cycling through the players
   */
  @Test
  public void ChoosePlayersTest() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    getDiceRolls(state);

    int firstPlayer = state.getFirstPlayer();
    int i = 0;
    while (true) {
      Set<Integer> playerSet = logic.getCurrentPlayersToAct(state);

      assertEquals((firstPlayer + i) % numPlayers, playerSet.toArray()[0]);

      List<Action> actions = logic.generateActions(state, (firstPlayer + i) % numPlayers);

      Action action = actions.get((actions.size() == 1) ? 0 : 1);

      if (((DiscreteAction) action).getActionNumber() == state.dudo) {
        break;
      }

      logic.makeMove(state, action);

      i++;
    }
  }

  /**
   * Test whether the first player generates all moves but for dudo. Second player is tested to
   * generate all moves except for the played move.
   */
  @Test
  public void GeneratedActionsTest() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    getDiceRolls(state);

    int id = state.getFirstPlayer();
    List<Action> generatedActions = logic.generateActions(state, id);
    assertEquals(generatedActions.size(), numSides * numDice * numPlayers);
    for (int i = 0; i < numSides * numDice * numPlayers; i++) {
      assertEquals(new DiscreteAction(id, i), generatedActions.get(i));
    }

    logic.makeMove(state, generatedActions.get(1));

    id = (id + 1) % numPlayers;
    generatedActions = logic.generateActions(state, id);
    assertEquals(
        generatedActions.size(),
        numSides * numDice * numPlayers); // Played move removed. Dudo added
    // Action with id 0 is still valid since it has a higher strength than the
    // action with id 1
    for (int i = 0; i < numSides * numDice * numPlayers; i++) {
      if (i == 1) {
        continue;
      }
      assertTrue(generatedActions.contains(new DiscreteAction(id, i)));
    }
    assertTrue(generatedActions.contains(new DiscreteAction(id, state.dudo)));
  }

  /**
   * Test whether the challenger loses the correct number of dice when the actual count exceeds the
   * claimed count.
   *
   * <p>Assumes two player game.
   */
  @Test
  public void DiceLostTest1() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    logic.makeMove(state, new DiceRollAction(0, 1));
    logic.makeMove(state, new DiceRollAction(0, 1));
    logic.makeMove(state, new DiceRollAction(1, 3));
    logic.makeMove(state, new DiceRollAction(1, 2));

    logic.makeMove(state, new DiscreteAction(0, 0)); // Claim 1x1

    logic.makeMove(state, new DiscreteAction(1, state.dudo));

    assertFalse(logic.isTerminal(state));

    assertArrayEquals(new int[] {2, 1}, state.getNumPlayerDice());
  }

  /**
   * Test whether the challenged loses the correct number of dice when the actual count is less than
   * the claimed count.
   *
   * <p>Assumes two player game.
   */
  @Test
  public void DiceLostTest2() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    logic.makeMove(state, new DiceRollAction(0, 4));
    logic.makeMove(state, new DiceRollAction(0, 4));
    logic.makeMove(state, new DiceRollAction(1, 2));
    logic.makeMove(state, new DiceRollAction(1, 3));

    logic.makeMove(state, new DiscreteAction(0, 8)); // Claim 2x3
    logic.makeMove(state, new DiscreteAction(1, state.dudo));

    assertFalse(logic.isTerminal(state));

    assertArrayEquals(new int[] {1, 2}, state.getNumPlayerDice());
  }

  /**
   * Same test as DiceLostTest2 but testing for losing more than one die.
   *
   * <p>Assumes two player game
   */
  @Test
  public void DiceLostTest3() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    logic.makeMove(state, new DiceRollAction(0, 4));
    logic.makeMove(state, new DiceRollAction(0, 4));
    logic.makeMove(state, new DiceRollAction(1, 3));
    logic.makeMove(state, new DiceRollAction(1, 2));

    logic.makeMove(state, new DiscreteAction(0, 10)); // Claim 2x5
    logic.makeMove(state, new DiscreteAction(1, state.dudo));

    assertArrayEquals(new int[] {0, 2}, state.getNumPlayerDice());
    assertTrue(logic.isTerminal(state));

    double[] scores = (new DudoFinalEvaluator()).getScore(state);
    assertArrayEquals(scores, new double[] {-1.0, 1.0}, 0.00001);
  }

  private void setupState(DudoGameState state) {
    logic.makeMove(state, new DiceRollAction(0, 3));
    logic.makeMove(state, new DiceRollAction(0, 3));
    logic.makeMove(state, new DiceRollAction(1, 2));
    logic.makeMove(state, new DiceRollAction(1, 2));

    logic.makeMove(state, new DiscreteAction(0, 8)); // Claim 1x2
    logic.makeMove(state, new DiscreteAction(1, state.dudo)); // Incorrect claim
  }

  /**
   * Test whether the challenged loses when the actual count is less than the claimed count
   *
   * <p>Assumes two player game.
   */
  @Test
  public void DiceLostTest4() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    setupState(state);

    assertArrayEquals(new int[] {2, 1}, state.getNumPlayerDice());
  }

  /**
   * Test whether the end of a round leads to stochastic play and if the winner of the round plays
   * first for the next round.
   */
  @Test
  public void EndOfRoundTest() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    setupState(state);

    int nextPlayer = (int) logic.getCurrentPlayersToAct(state).toArray()[0];

    assertEquals(nextPlayer, state.getNumPlayers());

    getDiceRolls(state);

    assertEquals(0, state.getFirstPlayer());
  }

  /**
   * Test whether the winner of a round has the correct possible actions at the beginning of the
   * round
   */
  @Test
  public void NewRoundPossibleActions() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    setupState(state);

    getDiceRolls(state);

    int id = state.getFirstPlayer();
    List<Action> generatedActions = logic.generateActions(state, id);
    assertEquals(numSides * (numDice * numPlayers - 1), generatedActions.size());
    for (int i = 0; i < numSides * (numDice * numPlayers - 1); i++) {
      assertTrue(generatedActions.contains(new DiscreteAction(id, i)));
    }
  }

  /**
   * Test whether the game ends when a player loses all dice.
   *
   * <p>Assumes two player game.
   */
  @Test
  public void EndOfGameTest() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);
    setupState(state);

    logic.makeMove(state, new DiceRollAction(0, 2));
    logic.makeMove(state, new DiceRollAction(0, 4));
    logic.makeMove(state, new DiceRollAction(1, 3));

    logic.makeMove(state, new DiscreteAction(0, 1)); // Claim 1x2
    logic.makeMove(state, new DiscreteAction(1, state.dudo));

    assertTrue(logic.isTerminal(state));

    double[] scores = (new DudoFinalEvaluator()).getScore(state);
    assertArrayEquals(scores, new double[] {1.0, -1.0}, 0.00001);
  }

  @Test
  public void StaticGameElementsTest() {
    DudoGameState state = new DudoGameState(numPlayers, numSides, numDice);

    for (int id = 0; id < numPlayers; id++) {
      List<DiscreteAction> actions = DudoGameState.possibleActions.get(id);
      int i = 0;
      for (int n = 1; n <= numDice; n++) {
        for (int r = 1; r <= numSides; r++) {
          assertEquals(actions.get(i).getActionNumber(), i);
          i++;
        }
      }
    }
  }
}
