package za.ac.sun.cs.ingenious.games.ingenious;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

public class IngeniousTest {

  /** Test basics of creating a board. */
  @Test
  public void IngeniousBoardTest() {
    IngeniousBoard ib = new IngeniousBoard(3, 2);
    assertEquals(2, ib.getNumColours());
    assertNull(ib.lastMove());
  }
}
