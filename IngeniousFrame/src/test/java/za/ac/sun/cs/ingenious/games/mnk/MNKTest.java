package za.ac.sun.cs.ingenious.games.mnk;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;

public class MNKTest {

  /** Test basics of creating a game state. */
  @Test
  public void MNKStateTest() {
    MNKState mnks = new MNKState(9, 9, 4, false, 2);
    assertEquals(4, mnks.getK());
    assertFalse(mnks.isPerfectInformation());
    assertEquals(9, mnks.getWidth());
    assertEquals(9, mnks.getHeight());
  }

  public void MNKCopyTest() {
    MNKState mnks = new MNKState(9, 9, 4, false, 2);
    MNKState mnkc = mnks.deepCopy();
    assertEquals(mnks, mnkc);
  }
}
