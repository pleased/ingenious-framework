package za.ac.sun.cs.ingenious.core.util.misc;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;

import org.junit.jupiter.api.Test;

public class CompassDirectionsTest {

  @Test
  public void testCardinals() {
    Set<CompassDirection> c = CompassDirection.cardinalDirections();
    assertTrue(c.contains(CompassDirection.N));
    assertTrue(c.contains(CompassDirection.E));
    assertTrue(c.contains(CompassDirection.S));
    assertTrue(c.contains(CompassDirection.W));
    assertFalse(c.contains(CompassDirection.NE));
    assertFalse(c.contains(CompassDirection.NW));
    assertFalse(c.contains(CompassDirection.SE));
    assertFalse(c.contains(CompassDirection.SW));
    assertEquals(4, c.size());
  }

  @Test
  public void testOrdinals() {
    Set<CompassDirection> c = CompassDirection.ordinalDirections();
    assertFalse(c.contains(CompassDirection.N));
    assertFalse(c.contains(CompassDirection.E));
    assertFalse(c.contains(CompassDirection.S));
    assertFalse(c.contains(CompassDirection.W));
    assertTrue(c.contains(CompassDirection.NE));
    assertTrue(c.contains(CompassDirection.NW));
    assertTrue(c.contains(CompassDirection.SE));
    assertTrue(c.contains(CompassDirection.SW));
    assertEquals(4, c.size());
  }

  @Test
  public void testPrincipals() {
    Set<CompassDirection> c = CompassDirection.principalDirections();
    assertTrue(c.contains(CompassDirection.N));
    assertTrue(c.contains(CompassDirection.E));
    assertTrue(c.contains(CompassDirection.S));
    assertTrue(c.contains(CompassDirection.W));
    assertTrue(c.contains(CompassDirection.NE));
    assertTrue(c.contains(CompassDirection.NW));
    assertTrue(c.contains(CompassDirection.SE));
    assertTrue(c.contains(CompassDirection.SW));
    assertEquals(8, c.size());
  }
}
