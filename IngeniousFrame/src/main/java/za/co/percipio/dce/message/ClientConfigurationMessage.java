package za.co.percipio.dce.message;

import za.co.percipio.mpl.Message;

/** Created by Chris Coetzee on 2016/07/28. */
public class ClientConfigurationMessage implements Message {

  private static final long serialVersionUID = -9135783298673612225L;

  private String hostname;
  private int serverPort;
  private String gameName;
  private String lobbyName;
  private String username;

  public ClientConfigurationMessage(
      String hostname, int serverPort, String gameName, String lobbyName, String username) {
    this.hostname = hostname;
    this.serverPort = serverPort;
    this.gameName = gameName;
    this.lobbyName = lobbyName;
    this.username = username;
  }

  public String getGameName() {
    return gameName;
  }

  public String getHostname() {
    return hostname;
  }

  public String getLobbyName() {
    return lobbyName;
  }

  public int getServerPort() {
    return serverPort;
  }

  public String getUsername() {
    return username;
  }

  /**
   * Escapes and quotes the string representation of the given object. - Replace all ' with \' -
   * Enclose string in single quotes '[string]'
   *
   * @param o
   * @return
   */
  private static String escapeAndQuote(Object o) {
    return '\'' + o.toString().replaceAll("'", "\\'") + '\'';
  }

  public String getCompleteArguments() {
    StringBuilder builder = new StringBuilder();
    builder.append(" -port ");
    builder.append(escapeAndQuote(serverPort));
    builder.append(" -hostname ");
    builder.append(escapeAndQuote(hostname));
    builder.append(" -lobby ");
    builder.append(escapeAndQuote(lobbyName));
    builder.append(" -game ");
    builder.append(escapeAndQuote(gameName));
    builder.append(" -username ");
    builder.append(escapeAndQuote(username));
    return builder.toString();
  }
}
