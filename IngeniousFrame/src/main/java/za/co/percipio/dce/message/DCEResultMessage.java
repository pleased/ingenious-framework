package za.co.percipio.dce.message;

import java.util.ArrayList;
import za.co.percipio.mpl.Message;

public class DCEResultMessage implements Message {
  private ArrayList<String> players;
  private ArrayList<String> scores;

  public DCEResultMessage(ArrayList<String> players, ArrayList<String> scores) {
    this.players = players;
    this.scores = scores;
  }

  public ArrayList<String> getPlayers() {
    return players;
  }

  public ArrayList<String> getScores() {
    return scores;
  }
}
