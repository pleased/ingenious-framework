package za.ac.sun.cs.ingenious.games.ingenious;

import com.esotericsoftware.minlog.Log;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.games.ingenious.engines.IngeniousScoreKeeper;
import za.ac.sun.cs.ingenious.games.ingenious.engines.Score;

public class IngeniousController implements Runnable {

  public final int NUMBER_OF_TILES;
  public final int NUMBER_OF_COLOURS;
  public final int NUMBER_OF_PLAYERS;

  private final ServerSocket serverSocket;
  private final int port;
  private final IngeniousMatchSetting matchSettings;
  volatile int numberOfEngines = 0;
  private IngeniousEngineConnection engineConns[];
  private volatile boolean gameLoop = true;
  private ArrayList<IngeniousRack> playerRacks;
  private BagSequence gameBag;
  private IngeniousBoard gameBoard;
  private IngeniousScoreKeeper scoreKeeper;

  /*
   * Engines and variants are paired by key
   */
  private Variant[] variants;

  public IngeniousController(MatchSetting match) throws Exception {
    this.matchSettings = new IngeniousMatchSetting(match);
    this.NUMBER_OF_TILES = matchSettings.getBoardSize();
    this.NUMBER_OF_COLOURS = matchSettings.getNumColours();
    this.NUMBER_OF_PLAYERS = matchSettings.getNumPlayers();
    this.variants = matchSettings.getVariants();
    this.gameBoard = new IngeniousBoard(matchSettings.getBoardSize(), NUMBER_OF_COLOURS);
    this.scoreKeeper = new IngeniousScoreKeeper(NUMBER_OF_PLAYERS, NUMBER_OF_COLOURS);

    this.engineConns = new IngeniousEngineConnection[matchSettings.getNumPlayers()];
    this.playerRacks = new ArrayList<IngeniousRack>(NUMBER_OF_PLAYERS);
    this.gameBag = new DefaultBag(120, NUMBER_OF_COLOURS);

    try {
      serverSocket = new ServerSocket(0);
      this.port = serverSocket.getLocalPort();
      Log.info("Controller created on port" + this.port);

    } catch (IOException e) {
      throw new Exception("ServerSocket not created: Controller failed.");
    }
  }

  public int getPort() {
    return this.port;
  }

  public IngeniousMatchSetting getMatchSetting() {
    return this.matchSettings;
  }

  public synchronized void addEngine(IngeniousEngineConnection engineConn, int position) {
    if (engineConns[position] == null) {
      engineConns[position] = engineConn;
      numberOfEngines++;
      if (numberOfEngines == matchSettings.getNumPlayers()) {
        try {
          this.serverSocket.close();
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
      Log.info("The number of  engines has just been increased to : " + numberOfEngines);
    }
    for (int i = 0; i < engineConns.length; i++) {
      Log.info(engineConns[i]);
    }
  }

  private void acceptJoiningPlayers() {
    while (numberOfEngines < matchSettings.getNumPlayers()) {
      try {
        Socket client = this.serverSocket.accept();

        IngeniousEngineConnection engine = new IngeniousEngineConnection(this, client);
      } catch (IOException e) {
        Log.info("failed to connect new client");
      }
      Log.info("At end of while loop: " + numberOfEngines);
    }
  }

  private void initGameRacks() {
    for (int i = 0; i < engineConns.length; i++) {
      playerRacks.add(i, getRack(variants[i].getRackSize()));
    }

    distributeRacks();
  }

  private IngeniousRack getRack(int rackSize) {
    if (rackSize == 0) {
      return null;
    }
    IngeniousRack rack = new IngeniousRack(rackSize);
    for (int i = 0; i < rackSize; i++) {
      rack.add(gameBag.draw());
    }

    return rack;
  }

  private void distributeRacks() {
    for (IngeniousEngineConnection engine : engineConns) {
      for (int i = 0; i < this.NUMBER_OF_PLAYERS; i++) {
        if (variants[i].isOpponentsRackSizeVisible() || i == engine.id) {
          engine.setRack(playerRacks.get(i), i);
        }
      }
    }
  }

  private void distributeBags() {
    for (IngeniousEngineConnection engine : engineConns) {
      engine.setBag(gameBag.viewVisible(gameBag.size()));
    }
  }

  private void distributeDraw(Tile drawnTile, int playerId) {
    for (IngeniousEngineConnection engine : engineConns) {
      for (int i = 0; i < this.NUMBER_OF_PLAYERS; i++) {
        if (variants[i].isOpponentsRackSizeVisible() || i == playerId) {
          engine.draw(drawnTile, playerId);
        }
      }
    }
  }

  private void distributeAcceptedMove(int playerId, IngeniousAction move) {
    for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
      if (i != playerId) {
        engineConns[i].playMove(move);
      }
    }
  }

  @Override
  public void run() {
    acceptJoiningPlayers();

    Log.info("CONTROLLER CONNECTION CONCLUDED");

    initGameRacks();
    distributeBags();

    Log.info("CONTROLLER SETUP CONCLUDED");

    while (gameLoop) {
      for (IngeniousEngineConnection engineTurn : engineConns) {
        int playerId = engineTurn.id;
        Log.info("CURRENT PLAYER NUMBER : " + playerId);

        IngeniousAction generatedMove = engineTurn.genMove();

        if (generatedMove == null) {
          Log.info("generate move pass.");
          break;
        }

        if (this.gameBoard.makeMove(generatedMove)) {
          distributeAcceptedMove(playerId, generatedMove);
          Log.info(gameBoard);
        } else {
          Log.info(gameBoard.validMove(generatedMove));
          Log.info("Invalid move");
          break;
        }

        Log.info(generatedMove.getTile() + " tile is removed from rack");

        playerRacks.get(playerId).remove(generatedMove.getTile());

        int[] result = scoreKeeper.updateScore(playerId, gameBoard);

        Log.info(gameBoard);

        Score playerScore = scoreKeeper.getScore(playerId);

        if (playerScore.outrightWin(variants[playerId].getMaximumColourScore())) {
          Log.info("GAME WON BY PLAYER " + playerId);
          for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
            Log.info(scoreKeeper.getScore(i));
          }
          gameLoop = false;
          break;
        } else if (gameBoard.full()) {
          Log.info("GAME WON BY PLAYER " + scoreKeeper.getLeader());
          for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
            for (int j = 0; j < NUMBER_OF_COLOURS; j++) {
              Log.info(scoreKeeper.getScore(i).getColourScore(j) + " ");
            }
            Log.info();
          }
          gameLoop = false;
          break;
        }
        Tile drawnTile = gameBag.draw();
        distributeDraw(drawnTile, playerId);
        playerRacks.get(playerId).add(drawnTile);

        Log.info("Player Leading at end of round : " + scoreKeeper.getLeader());
      }
    }
  }
}
