package za.ac.sun.cs.ingenious.games.nim;

import java.util.ArrayList;
import java.util.List;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;

/**
 * Game logic for Nim.
 *
 * @author Alexander Mouton
 */
public class NimLogic implements TurnBasedGameLogic<NimBoard> {

  @Override
  public boolean validMove(NimBoard fromState, Move move) {
    XYAction xyMove = (XYAction) move;
    return fromState.board[fromState.xyToIndex(xyMove.getX(), xyMove.getY())] != 0;
  }

  /**
   * Applies the move by setting the value of the cell, and all non-zero cells to the right of it,
   * to 0.
   */
  @Override
  public boolean makeMove(NimBoard fromState, Move move) {
    XYAction xyMove = (XYAction) move;
    int row = xyMove.getY();
    int col = xyMove.getX();
    int index = fromState.xyToIndex(col, row);
    while (fromState.board[index] != 0 && col < fromState.getBoardWidth()) {
      fromState.board[index] = (byte) 0;
      index = fromState.xyToIndex(++col, row);
      if (index == fromState.getNumCells()) break;
    }
    nextTurn(fromState, move);
    return true;
  }

  @Override
  public void undoMove(NimBoard fromState, Move move) {
    throw new UnsupportedOperationException();
  }

  /** Generates all valid moves for the player. */
  @Override
  public List<Action> generateActions(NimBoard fromState, int forPlayerID) {
    ArrayList<Action> actions = new ArrayList<>();
    for (byte i = 0; i < fromState.board.length; i++) {
      if (fromState.board[i] == 1) {
        actions.add(
            new XYAction(
                (short) (i % fromState.getBoardWidth()),
                (short) (i / fromState.getBoardWidth()),
                forPlayerID));
      }
    }
    return actions;
  }

  /**
   * Generates all valid moves for the player. The previous action is redundant, but is included for
   * completeness.
   */
  public List<Action> generateActions(NimBoard fromState, int forPlayerID, Action action) {
    return generateActions(fromState, forPlayerID);
  }

  /**
   * Determines whether the game state is terminal. The game state is terminal if the board only
   * contains 0 values.
   */
  @Override
  public boolean isTerminal(NimBoard state) {
    for (int i = 0; i < state.getNumCells(); i++) {
      if (state.board[i] == 1) {
        return false;
      } else {
        i += state.getBoardWidth() - 1;
      }
    }
    return true;
  }
}
