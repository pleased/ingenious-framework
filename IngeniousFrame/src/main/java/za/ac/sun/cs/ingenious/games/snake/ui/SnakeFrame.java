package za.ac.sun.cs.ingenious.games.snake.ui;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import za.ac.sun.cs.ingenious.games.snake.SnakeState;

public class SnakeFrame extends JFrame {
  private final SnakePanel panel;

  public SnakeFrame() {
    Dimension dimension =
        new Dimension(
            SnakeState.WIDTH + SnakePanel.EDGE_INSET * 2,
            SnakeState.HEIGHT + SnakePanel.EDGE_INSET * 2);

    panel = new SnakePanel();
    panel.setPreferredSize(dimension);

    setSize(dimension);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setContentPane(panel);
    setVisible(true);
    pack();
  }

  public SnakePanel getPanel() {
    return panel;
  }
}
