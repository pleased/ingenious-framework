package za.ac.sun.cs.ingenious.games.diceGames.actions;

import za.ac.sun.cs.ingenious.core.Action;

public class DiceRollAction implements Action, Cloneable, Comparable<DiceRollAction> {
  private final int roll;
  private final int playerID;

  public DiceRollAction(int playerID, int roll) {
    this.playerID = playerID;
    this.roll = roll;
  }

  @Override
  public String toString() {
    return "Player " + playerID + " roll: " + roll;
  }

  @Override
  public int getPlayerID() {
    return playerID;
  }

  @Override
  public DiceRollAction clone() {
    return new DiceRollAction(this.getPlayerID(), this.roll);
  }

  @Override
  public DiceRollAction deepCopy() {
    return clone();
  }

  public int getRoll() {
    return this.roll;
  }

  @Override
  public int hashCode() {
    return Integer.hashCode(roll);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    DiceRollAction dra = (DiceRollAction) o;

    if (this.roll != dra.roll || this.playerID != dra.playerID) {
      return false;
    }

    return true;
  }

  @Override
  public int compareTo(DiceRollAction diceRollAction) {
    return Integer.compare(this.hashCode(), diceRollAction.hashCode());
  }
}
