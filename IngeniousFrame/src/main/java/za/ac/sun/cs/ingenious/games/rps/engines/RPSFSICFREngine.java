package za.ac.sun.cs.ingenious.games.rps.engines;

import com.esotericsoftware.minlog.Log;
import java.util.Map;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.exception.dag.CycleException;
import za.ac.sun.cs.ingenious.core.exception.dag.StateNotFoundException;
import za.ac.sun.cs.ingenious.core.exception.probability.IncorrectlyNormalizedDistributionException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.ClassUtils;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.games.rps.RPSGameState;
import za.ac.sun.cs.ingenious.games.rps.RPSLogic;
import za.ac.sun.cs.ingenious.search.cfr.FSICFR;

public class RPSFSICFREngine extends GeneralEngine {
  protected final Map<RPSGameState, Map<Action, Double>> aveStrategies;

  /**
   * @param toServer An established connection to the GameServer
   */
  public RPSFSICFREngine(EngineToServerConnection toServer) {
    super(toServer);

    FSICFR<RPSGameState> fsicfr = null;

    try {

      fsicfr = new FSICFR<>(currentState, logic, logic, logic, logic);

      fsicfr.solve(numIterations);
    } catch (CycleException | StateNotFoundException ex) {
      Log.error("RPFFSICFREngine", "Failed to train RPS with FSICFR");
      ex.printStackTrace();
    }

    aveStrategies = fsicfr.getNormalizedStrategy();
    Log.info(getPrettyStrategies());
  }

  @Override
  public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
    Map<Action, Double> strat = aveStrategies.get(currentState);

    Action action;
    try {
      action = ClassUtils.chooseFromDistribution(strat, rand);
    } catch (IncorrectlyNormalizedDistributionException ex) {
      action = (Action) strat.keySet().toArray()[0];
    }

    return new PlayActionMessage(action);
  }

  private String getPrettyStrategies() {
    StringBuilder stringBuilder = new StringBuilder();

    for (RPSGameState state : aveStrategies.keySet()) {
      if (aveStrategies.get(state).keySet().isEmpty()
          || (int) (logic.getCurrentPlayersToAct(state).toArray())[0] != this.playerID) {
        continue;
      }

      stringBuilder.append(state.getPretty());
      stringBuilder.append("=================\nStrategy Profile:\n");
      for (Action action : aveStrategies.get(state).keySet()) {
        stringBuilder
            .append(RPSLogic.getPrettyRPSAction((DiscreteAction) action))
            .append(" : ")
            .append(aveStrategies.get(state).get(action))
            .append("\n");
      }
      stringBuilder.append("=================\n\n");
    }

    return stringBuilder.toString();
  }
}
