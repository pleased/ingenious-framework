package za.ac.sun.cs.ingenious.games.cardGames.uno.engines;

import com.esotericsoftware.minlog.Log;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Random;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.DrawCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.HiddenDrawCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.PlayCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.deck.CardDeck;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;
import za.ac.sun.cs.ingenious.games.cardGames.uno.game.UnoGameLogic;
import za.ac.sun.cs.ingenious.games.cardGames.uno.game.UnoGameState;
import za.ac.sun.cs.ingenious.games.cardGames.uno.game.UnoInitGameMessage;
import za.ac.sun.cs.ingenious.games.cardGames.uno.game.UnoLocations;

/**
 * A random Uno engine.
 *
 * @author Joshua Wiebe
 */
public class UnoRandomEngine extends Engine {

  /** A UnoGameState for internal representation. */
  UnoGameState state;

  /** A UnoGameLogic for internal representation. */
  UnoGameLogic logic;

  /**
   * Instantiates a new Uno random engine.
   *
   * @param toServer the server connection
   */
  public UnoRandomEngine(EngineToServerConnection toServer) {
    super(toServer);
    logic = new UnoGameLogic();
  }

  @Override
  public void setZobrist(ZobristHashing zobristHashing) {}

  /**
   * @see za.ac.sun.cs.ingenious.core.Engine#engineName()
   * @return Returns it's name.
   */
  @Override
  public String engineName() {
    return "UnoRandomPlayer";
  }

  /**
   * @see
   *     za.ac.sun.cs.ingenious.core.Engine#receiveInitAction(za.ac.sun.cs.ingenious.core.network.game.actions.InitGameAction)
   *     <p>Initialize the game state and update it with the given CardRack.
   */
  @Override
  public void receiveInitGameMessage(InitGameMessage a) {

    // Cast InitGameAction to UnoInitGameAction
    UnoInitGameMessage uiga = (UnoInitGameMessage) a;

    // Retrieve CardRack
    CardRack<UnoSymbols, UnoSuits> rack = uiga.getRack();

    // Initialize the game state with a Uno deck.
    CardDeck<UnoSymbols, UnoSuits> deck =
        new CardDeck<>(EnumSet.allOf(UnoSymbols.class), EnumSet.allOf(UnoSuits.class), 2);
    state = new UnoGameState(deck, uiga.getNumPlayers());

    // Update game state according to given rack.
    for (Card<UnoSymbols, UnoSuits> card : rack) {
      try {
        state.changeLocation(card, UnoLocations.DRAWPILE, UnoLocations.values()[super.playerID]);
      } catch (KeyNotFoundException e) {
        e.printStackTrace();
      } catch (LocationNotFoundException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * @see
   *     za.ac.sun.cs.ingenious.core.Engine#receivePlayMove(za.ac.sun.cs.ingenious.core.network.game.actions.PlayMoveAction)
   *     <p>Update internal state according to received PlayMoveAction.
   *     <p>Cannot simply use makeMove() of UnoGameLogic because of imperfect information.
   */
  @Override
  public void receivePlayedMoveMessage(PlayedMoveMessage m) {
    Move move = m.getMove();

    // similar to logic.makemove() but not the same. So far cannot use logic.makemove() because of
    // imperfect information (validMove() would not approve).
    // TODO: Solve this problem and make use of makeMove() in UnoGameLogic. Maybe by updating the
    // game state with incoming information first.
    // But them the problem with validMove() is still present.

    // If DrawCardMove: Update from DRAWPILE to THIS! player.
    if (move instanceof DrawCardAction) {
      DrawCardAction<UnoLocations> dcMove = (DrawCardAction) move;
      try {
        // Update state
        state.changeLocation(
            dcMove.getCard(), UnoLocations.DRAWPILE, UnoLocations.values()[playerID]);
        state.incrementRackSize(playerID);
      } catch (KeyNotFoundException | LocationNotFoundException e) {
        e.printStackTrace();
      }
    }

    // If HiddenDrawCardMove: an other player drew a card. Update rackSize.
    else if (move instanceof HiddenDrawCardAction) {
      HiddenDrawCardAction hdcMove = (HiddenDrawCardAction) move;
      state.incrementRackSize(hdcMove.getPlayerID());
    }

    // If PlayCardMove: Update either from DRAWPILE to new location or from this player to new
    // location
    // (distinction necessary because of imperfect information)
    else if (move instanceof PlayCardAction) {
      PlayCardAction<UnoLocations> pcMove = (PlayCardAction) move;
      state.changeTop(pcMove.getCard());
      state.decrementRackSize(pcMove.getPlayerID());

      // This player
      if (pcMove.getPlayerID() == this.getPlayerID()) {
        try {
          // Update state
          state.changeLocation(pcMove.getCard(), pcMove.getOldLoc(), pcMove.getNewLoc());
        } catch (KeyNotFoundException | LocationNotFoundException e) {
          e.printStackTrace();
        }
      }

      // Other player
      else {
        try {
          // Update state
          state.changeLocation(pcMove.getCard(), UnoLocations.DRAWPILE, pcMove.getNewLoc());
        } catch (KeyNotFoundException | LocationNotFoundException e) {
          e.printStackTrace();
        }
      }
    }

    state.incrementRoundNR();
  }

  /**
   * @see
   *     za.ac.sun.cs.ingenious.core.Engine#receiveGenerateMove(za.ac.sun.cs.ingenious.core.network.game.actions.GenMoveAction)
   *     <p>Generate random move.
   */
  @Override
  public PlayActionMessage receiveGenActionMessage(GenActionMessage m) {
    ArrayList<Action> actions = (ArrayList<Action>) logic.generateActions(state, this.playerID);

    // If no possible moves
    if (actions.size() == 0) {
      return null;
    }

    // Else: chose random move.
    Random rand = new Random();
    Action randomAction = actions.get(rand.nextInt(actions.size()));
    return new PlayActionMessage(randomAction);
  }

  /**
   * @see
   *     za.ac.sun.cs.ingenious.core.Engine#receiveGameTerminatedAction(za.ac.sun.cs.ingenious.core.network.game.actions.GameTerminatedAction)
   */
  @Override
  public void receiveGameTerminatedMessage(GameTerminatedMessage m) {}

  /** Print the current rack sizes of the state. */
  public void printRackSizes() {
    StringBuilder s = new StringBuilder();
    s.append("Round: " + state.getRoundNR() + "\n");
    s.append("Racks: [");
    for (int i = 0; i < state.getNumPlayers(); i++) {
      if (i != 0) {
        s.append(", ");
      }
      s.append(state.getRackSizes()[i]);
    }
    s.append("] \n");

    Log.info(s);
  }
}
