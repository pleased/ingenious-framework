package za.ac.sun.cs.ingenious.games.domineering;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.util.referee.FullyObservableMovesReferee;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.games.domineering.game.DomineeringLogic;

public class DomineeringReferee
    extends FullyObservableMovesReferee<
        TurnBasedSquareBoard, DomineeringLogic, DomineeringFinalEvaluator> {

  public DomineeringReferee(MatchSetting match, PlayerRepresentation[] players)
      throws MissingSettingException, IncorrectSettingTypeException {
    super(
        match,
        players,
        new TurnBasedSquareBoard(match.getSettingAsInt("boardSize"), 0, 2),
        new DomineeringLogic(),
        new DomineeringFinalEvaluator());
  }

  @Override
  protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
    return new DomineeringInitGameMessage(currentState.getBoardWidth());
  }
}
