package za.ac.sun.cs.ingenious.core.network.game.messages;

/** This message must be sent by an engine to the server if the engine crashed. */
public class OpponentCrashedMessage extends PlayActionMessage {

  public OpponentCrashedMessage() {
    super(null);
  }

  private static final long serialVersionUID = 1L;
}
