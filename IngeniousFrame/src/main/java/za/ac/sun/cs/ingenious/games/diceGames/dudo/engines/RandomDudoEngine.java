package za.ac.sun.cs.ingenious.games.diceGames.dudo.engines;

import java.util.List;
import java.util.Random;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;

public class RandomDudoEngine extends DudoGeneralEngine {

  private final Random random;

  public RandomDudoEngine(EngineToServerConnection toServer) {
    super(toServer);

    random = new Random();
  }

  @Override
  public String engineName() {
    return "RandomDudoEngine";
  }

  @Override
  public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
    List<Action> actions = logic.generateActions(currentState, this.playerID);

    Action action = actions.get(random.nextInt(actions.size()));

    return new PlayActionMessage(action);
  }
}
