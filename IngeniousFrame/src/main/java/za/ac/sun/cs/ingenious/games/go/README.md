# Implementation of Go

## Built With
* [Gradle 4.4.1] (https://gradle.org/)
* [openjdk 11.0.11] (https://www.oracle.com/za/java/technologies/javase/jdk11-archive-downloads.html)

## How to compile the code
To compile the code run the following commands

```
$ gradle clean
$ gradle shadowJar
```
## How to run test cases
To run all the test cases use the following command

```
$ gradle build
```

## How to run the game

Two terminals are necessary for running the game. Both terminals should be in the folder 

> IngeniousFrame/scripts/go

In the first terminal run the command
```
$ ./start_server.sh
```
After running that command in the first terminal, run the following commands in the second terminal.
```
$ ./create_game.sh
$ ./connect_clients.sh
```
## Changing game settings

There is a Go.json file that defines the setting that will be used by the framework. This Go.json file can be found in the IngeniousFrame/scripts/go folder. 

The number of players is set in the "numPlayers" field.

> "numPlayers": 2

The board size is set in the "boardSize" field.

> "boardSize": 19

The number of threads available is defined in the "threadCount" field.

> "threadCount": 4

The turn length is defined in the "turnLength" field (the number shown is in milliseconds so 3000 is 3 seconds)

> "turnLength": 3000

The game engine is set by the "engine" field.

> "engine": "za.ac.sun.cs.ingenious.games.go.engines.GoMCTSLeafEngine"

There are three valid engines for the Leaf, Root, and Tree implementations of the MCTS algorithm. 
> "za.ac.sun.cs.ingenious.games.go.engines.GoMCTSLeafEngine"

> "za.ac.sun.cs.ingenious.games.go.engines.GoMCTSRootEngine"

> "za.ac.sun.cs.ingenious.games.go.engines.TreeEngine.GoMCTSTreeGenericEngine"

The enhancements used in the MCTS algorithm can be defined in the "enhancement" field. These enhancements can be further defined in the IngeniousFrame/scripts/MctsEnhancementConfigs folder in a .json format.

> "enhancement": "../MctsEnhancementConfigs/EnhancementChoiceVanilla.json"

## Framework structure

The framework contains a network module that allows a **GameServer** to play different games with various clients. The **GameServer** creates Lobbies, the Lobbies create **Referee**s once enough players have joined, the **Referee**s play games with **Engine**s(=Players). A detailed writedown of how the network components of the framework interact with each other can be found under "Classes related to network and communication" (most of the information in that section will be irrelevant to new developers).

### Most important classes

Finally, the framework contains three classes to structure a game implementation with (they are supposed to be extended for specific games):

* **GameState** represents a state in some game. There are helpful utility classes like **TurnBasedGameState** for games where only one player may play at a time or **TurnBased2DBoard** for games that are turn based and played on two dimensional boards (for example chess).

* **GameLogic**, which defines common operations for all games (Which players may play when? Which actions are allowed?). It operates on instances of game states.

* **GameFinalEvaluator**, which takes a terminal game state and returns the scores for each player in that state.

The search-package makes heavy use of these three classes to offer generic implementations of search algorithms like MiniMax, MCTS or CFR.

## Go structure

### GoBoard

The **GoBoard** extends the utility class **TurnBasedSquareBoard** which is a valid instance of a **GameState**. It is necessary to use a **TurnBasedSquareBoard** object as the MCTS algorithm implementation expects an Object of this type. 

#### Initialization 

Initialization of a **GoBoard** can be done with three arguments, an integer representing the board size, an integer representing the first player, and an integer representing the number of players.  
