package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

import com.esotericsoftware.minlog.Log;
import java.util.Arrays;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.cardGameState.CardGameState;
import za.ac.sun.cs.ingenious.games.cardGames.core.deck.CardDeck;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;

/**
 * The class UnoGameState.
 *
 * @author Joshua Wiebe
 */
public class UnoGameState extends CardGameState<UnoSymbols, UnoSuits, UnoLocations> {

  /** The constant DEFAULT_HAND_SIZE. */
  public static final int DEFAULT_HAND_SIZE = 7;

  /** The actual initial hand size */
  private int initHandSize;

  /** Top of the discard pile. */
  private Card<UnoSymbols, UnoSuits> top;

  /** Round NR. */
  private int roundNR;

  /** Remaining size of the draw pile. */
  private int drawPileSize;

  /** Number of cards players holding on their hand. */
  private int[] rackSizes;

  public UnoGameState deepCopy() {
    // TODO: Replace this with a custom deep copy implementation, as the cloner
    // library is slow and may lead to segfaults. See issue #323
    return cloner.deepClone(this);
  }

  /**
   * Instantiates a new Uno game state.
   *
   * @param deck The Uno deck for superclass
   * @param numberOfPlayers The number of players
   * @param initHandSize The initial number of cards each player is holding.
   */
  public UnoGameState(
      CardDeck<UnoSymbols, UnoSuits> deck, int numberOfPlayers, int initHandSize, int firstPlayer) {
    super(deck, UnoLocations.DRAWPILE, numberOfPlayers);
    super.setCurrentPlayer(firstPlayer);

    // Set initial values.
    roundNR = 1;
    top = null;
    this.initHandSize = initHandSize;
    drawPileSize = deck.getSize() - (numberOfPlayers * initHandSize);
    rackSizes = new int[numberOfPlayers];
    Arrays.fill(rackSizes, initHandSize);
  }

  /**
   * Instantiates a new Uno game state.
   *
   * @param deck The Uno deck for superclass
   * @param numberOfPlayers The number of players
   */
  public UnoGameState(CardDeck<UnoSymbols, UnoSuits> deck, int numberOfPlayers) {
    // If no further information on initial hand size is given, pass the constant
    // DEFAULT_HAND_SIZE to more general constructor.
    // Also the player with ID 0 starts by calling this constructor.
    this(deck, numberOfPlayers, DEFAULT_HAND_SIZE, 0);
  }

  /**
   * @see za.ac.sun.cs.ingenious.games.cardGames.core.cardGameState.CardGameState#printPretty()
   *     <p>Print the game state
   */
  @Override
  public void printPretty() {

    // StringBuilder for Log.info().
    StringBuilder s = new StringBuilder();

    s.append("\n#Uno - Round " + roundNR + " #\n");
    s.append("Number of players: " + this.getNumPlayers() + "\n");
    s.append("Rack sizes: [");
    for (int i = 0; i < rackSizes.length; i++) {
      if (i == 0) {
        s.append(rackSizes[i]);
      } else {
        s.append(", " + rackSizes[i]);
      }
    }
    s.append("] \n");
    s.append("It's Player" + this.getCurrentPlayer() + "'s turn\n");
    s.append("Top of the discard pile:");
    if (top == null) {
      s.append("empty\n");
    } else {
      s.append(top.toString() + "\n\n");
    }
    Log.info(s);

    super.printPretty();
  }

  /** Increments round NR. */
  public void incrementRoundNR() {
    roundNR++;
  }

  /** Decrements draw pile. */
  public void decrementDrawPile() {
    drawPileSize--;
  }

  /**
   * Gets the round NR.
   *
   * @return the round NR
   */
  public int getRoundNR() {
    return roundNR;
  }

  /**
   * Gets the size of draw pile.
   *
   * @return Size of draw pile
   */
  public int getSizeOfDrawPile() {
    return drawPileSize;
  }

  /**
   * Gets number of cards players holding on their hand.
   *
   * @return sizes of racks
   */
  // Deep Copy
  public int[] getRackSizes() {
    int[] copy = Arrays.copyOf(rackSizes, rackSizes.length);
    return copy;
  }

  /**
   * Change top.
   *
   * @param newTop The new top
   */
  public void changeTop(Card<UnoSymbols, UnoSuits> newTop) {
    this.top = newTop;
  }

  /**
   * Gets the top.
   *
   * @return The top
   */
  public Card<UnoSymbols, UnoSuits> getTop() {
    return top;
  }

  /**
   * Gets The initial number of cards on each players hand.
   *
   * @return Hand size
   */
  public int getInitHandSize() {
    return initHandSize;
  }

  /**
   * Increments rack size.
   *
   * @param player The player for which the rack size is incremented.
   */
  public void incrementRackSize(int player) {
    this.rackSizes[player]++;
  }

  /**
   * Decrement rack size.
   *
   * @param player The player for which the rack size is decremented.
   */
  public void decrementRackSize(int player) {
    this.rackSizes[player]--;
  }

  /** Print the current rack sizes of the state. */
  public void printRackSizes() {
    System.out.println("Round: " + getRoundNR());
    System.out.print("Racks: [");
    for (int i = 0; i < getNumPlayers(); i++) {
      if (i != 0) {
        System.out.print(", ");
      }
      System.out.print(getRackSizes()[i]);
    }
    System.out.println("]");
  }
}
