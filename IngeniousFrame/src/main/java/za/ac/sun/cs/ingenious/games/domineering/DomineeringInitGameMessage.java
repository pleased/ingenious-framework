package za.ac.sun.cs.ingenious.games.domineering;

import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;

/**
 * @author Michael Krause
 */
public class DomineeringInitGameMessage extends InitGameMessage {

  private static final long serialVersionUID = 1L;

  private int boardSize;

  public DomineeringInitGameMessage(int boardSize) {
    this.boardSize = boardSize;
  }

  public int getBoardSize() {
    return boardSize;
  }
}
