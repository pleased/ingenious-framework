package za.ac.sun.cs.ingenious.search.mcts.nodeComposition;

import java.util.List;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

public interface MctsNodeExtensionParallelInterface<
    T extends MctsNodeExtensionParallelInterface<T>> {

  String getID();

  void setUp(MctsNodeComposition node);

  void resetMemory();

  <S extends GameState> void addChild(MctsNodeTreeParallel<S> child);

  <S extends GameState> void addChildren(List<MctsNodeTreeParallel<S>> newChildren);

  <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> String getMoveLogString(
      N node);

  T getNewCopy();
}
