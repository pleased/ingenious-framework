package za.ac.sun.cs.ingenious.games.gridWorld.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Set;
import javax.swing.JPanel;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldLogic;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;

/**
 * JPanel for GridWorld containing code for painting the UI.
 *
 * @author Steffen Jacobs
 */
public class GridWorldPanel extends JPanel {
  public static final int BLOCK_WIDTH = 20;

  private GridWorldState state;
  private GridWorldLogic logic;

  public GridWorldPanel() {}

  public void updateState(GridWorldState state, GridWorldLogic logic) {
    this.state = state;
    this.logic = logic;
    repaint();
  }

  @Override
  protected void paintComponent(Graphics graphics) {
    super.paintComponent(graphics);

    Graphics2D graphics2d = (Graphics2D) graphics;

    Set<Coord> terminalPositions = logic.getTerminalPenalties();
    Set<Coord> terminalObjectives = logic.getTerminalObjectives();

    for (int i = 0; i < state.board.length; i++) {
      for (int j = 0; j < state.board[0].length; j++) {
        Color color;

        if (state.board[i][j] == GridWorldState.CELL_UNREACHABLE) {
          color = Color.BLACK;
        } else if (state.board[i][j] == GridWorldState.CELL_REACHABLE) {
          color = Color.WHITE;

          if (terminalPositions.contains(new Coord(j, i))) {
            color = Color.RED;
          } else if (terminalObjectives.contains(new Coord(j, i))) {
            color = Color.GREEN;
          }
        } else if (state.board[i][j] >= 0) {
          color = Color.BLUE;
        } else {
          color = Color.WHITE;
        }

        graphics2d.setColor(color);

        int x = j * BLOCK_WIDTH;
        int y = i * BLOCK_WIDTH;
        graphics2d.fillRect(x, y, BLOCK_WIDTH, BLOCK_WIDTH);
      }
    }
  }
}
