package za.ac.sun.cs.ingenious.search.mcts.selection;

import java.util.List;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

/**
 * A standard implementation of the TreeEngine selection strategy {@link TreeSelection}, that
 * calculates UCT values to make a decision of which child node to select. This strategy is
 * specifically for TreeEngine selection and makes use of read locks when viewing node information
 * that is used in the calculations.
 *
 * @author Karen Laubscher
 * @param <S> The game state type.
 * @param <N> The type of the mcts node.
 */
public class TreeSelectionUctTuned<
        S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, ?>>
    implements TreeSelection<N> {

  private boolean fpuEnabled;

  private double startPriority;
  private double c;
  public int player;

  /**
   * Constructor to create a TreeEngine UCT selection object with a specified value for the constant
   * C used in the UCT value calculation.
   *
   * @param player The player for which the UCT value is calculated
   * @param startPriority The reward value of each node before it is explored for the first time
   */
  private TreeSelectionUctTuned(int player, double c, boolean fpuEnabled, double startPriority) {
    this.player = player;
    this.c = c;
    this.fpuEnabled = fpuEnabled;
    this.startPriority = startPriority;
  }

  /**
   * The method that decides which child node to traverse to next, based on calculating the UCT
   * value for each child and then selecting the child with the highest UCT value. Since the
   * TreeEngine structure is shared in TreeEngine parallelisation, nodes are (read) locked when
   * information for the calculations are viewed.
   *
   * @param node The current node whose children nodes are considered for the next node to traverse
   *     to.
   * @return The selected child node with the highest UCT value.
   */
  public N select(N node) {

    // if there are still unexplored children and FPU is disabled, return
    // to selection phase and expand one of them. If fpu is enabled, we don't
    // necessarily want to expand a child node, so we continue with selection
    if (!fpuEnabled && !node.unexploredActionsEmpty()) {
      return null;
    }

    List<N> children = node.getChildren();
    double currentVisitCount = node.getVisitCount();
    N highestUctChild = null;
    double tempVal, avgChildScore, childValue, childVisits, V, logTerm;
    double highestUct = Double.NEGATIVE_INFINITY;
    for (N child : children) {

      childValue = child.getValue();
      childVisits = child.getVisitCount() + child.getVirtualLoss();
      avgChildScore = childValue / childVisits;

      logTerm = Math.log(currentVisitCount) / childVisits;

      // equation as proposed by Gelly and Wang (2006)
      V = avgChildScore - (avgChildScore * avgChildScore) + Math.sqrt((2 * logTerm));
      tempVal = avgChildScore + c * Math.sqrt(logTerm * Math.min(0.25, V));

      if (Double.compare(tempVal, highestUct) > 0) {
        highestUctChild = child;
        highestUct = tempVal;
      }
    }

    if (fpuEnabled) {
      // if there is no chosen child yet, or the highest scoring child is less than
      // the threshold value and there are still unexplored children, return null -
      // i.e. do not select a child already in the tree, but expand a new unexplored
      // child
      if (highestUctChild == null
          || (!node.unexploredActionsEmpty() && highestUct < startPriority)) {
        return null;
      }
    }

    return highestUctChild;
  }

  /**
   * A static factory method, which creates a TreeEngine UCT selection object that uses the
   * specified value for the constant C in the UCT value calculation.
   *
   * <p>This factory method is also a generic method, which uses Java type inferencing to
   * encapsulate the complex generic type capturing required by the TreeEngine UCT selection
   * constructor.
   *
   * @param logic The game logic
   * @param c The constant value for C (in the UCT calculation).
   */
  public static <SS extends GameState, NN extends MctsNodeTreeParallelInterface<SS, NN, ?>>
      TreeSelection<NN> newTreeSelectionUctTuned(
          int player, double c, boolean fpuEnabled, double startPriority) {
    return new TreeSelectionUctTuned<SS, NN>(player, c, fpuEnabled, startPriority);
  }
}
