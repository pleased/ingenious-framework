package za.ac.sun.cs.ingenious.games.go;

import java.util.Arrays;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.search.minimax.GameEvaluator;

public class GoFinalEvaluator implements GameEvaluator<GoBoard> {

  @Override
  public double[] getScore(GoBoard forState) {
    return forState.getScore();
  }

  private double[] areaScoring(GoBoard fromState, XYAction move, double[] owner) {
    int x = move.getX();
    int y = move.getY();
    boolean control = true;
    byte[] storeBoard = Arrays.copyOf(fromState.board, fromState.board.length);
    fromState.board[fromState.xyToIndex(x, y)] = 3;
    if (x + 1 < fromState.getBoardWidth()) {
      if (owner[0] == 0 && fromState.board[fromState.xyToIndex(x + 1, y)] != 3) {
        owner[0] = fromState.board[fromState.xyToIndex(x + 1, y)];
      }
      if (fromState.board[fromState.xyToIndex(x + 1, y)] == 0) {
        owner = areaScoring(fromState, new XYAction(x + 1, y, 0), owner);
      } else if (fromState.board[fromState.xyToIndex(x + 1, y)] != owner[0]
          && fromState.board[fromState.xyToIndex(x + 1, y)] != 3) {
        control = false;
      }
    }
    if (x - 1 >= 0) {
      if (owner[0] == 0 && fromState.board[fromState.xyToIndex(x - 1, y)] != 3) {
        owner[0] = fromState.board[fromState.xyToIndex(x - 1, y)];
      }
      if (fromState.board[fromState.xyToIndex(x - 1, y)] == 0) {
        owner = areaScoring(fromState, new XYAction(x - 1, y, 0), owner);
      } else if (fromState.board[fromState.xyToIndex(x - 1, y)] != owner[0]
          && fromState.board[fromState.xyToIndex(x - 1, y)] != 3) {
        control = false;
      }
    }
    if (y + 1 < fromState.getBoardHeight()) {
      if (owner[0] == 0 && fromState.board[fromState.xyToIndex(x, y + 1)] != 3) {
        owner[0] = fromState.board[fromState.xyToIndex(x, y + 1)];
      }
      if (fromState.board[fromState.xyToIndex(x, y + 1)] == 0) {
        owner = areaScoring(fromState, new XYAction(x, y + 1, 0), owner);
      } else if (fromState.board[fromState.xyToIndex(x, y + 1)] != owner[0]
          && fromState.board[fromState.xyToIndex(x, y + 1)] != 3) {
        control = false;
      }
    }
    if (y - 1 >= 0) {
      if (owner[0] == 0 && fromState.board[fromState.xyToIndex(x, y - 1)] != 3) {
        owner[0] = fromState.board[fromState.xyToIndex(x, y - 1)];
      }
      if (fromState.board[fromState.xyToIndex(x, y - 1)] == 0) {
        owner = areaScoring(fromState, new XYAction(x, y - 1, 0), owner);
      } else if (fromState.board[fromState.xyToIndex(x, y - 1)] != owner[0]
          && fromState.board[fromState.xyToIndex(x, y - 1)] != 3) {
        control = false;
      }
    }
    if (control) {
      owner[1]++;
    } else {
      owner[0] = -1;
    }
    fromState.board = Arrays.copyOf(storeBoard, fromState.board.length);
    return owner;
  }
}
