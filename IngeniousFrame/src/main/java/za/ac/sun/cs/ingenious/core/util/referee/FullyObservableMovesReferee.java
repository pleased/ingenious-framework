package za.ac.sun.cs.ingenious.core.util.referee;

import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;

/**
 * Abstract super class for referees for any game with fully observable moves. This referee is no
 * different from {@link GeneralReferee} except that the ActionSensor used is always a {@link
 * PerfectInformationActionSensor}
 */
public abstract class FullyObservableMovesReferee<
        S extends GameState, L extends GameLogic<S>, E extends GameFinalEvaluator<S>>
    extends GeneralReferee<S, L, E> {

  protected FullyObservableMovesReferee(
      MatchSetting match, PlayerRepresentation[] players, S currentState, L logic, E eval) {
    super(match, players, currentState, logic, eval, new PerfectInformationActionSensor<S>());
  }
}
