package za.ac.sun.cs.ingenious.games.go.engines;

import java.io.IOException;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.games.go.GoBoard;
import za.ac.sun.cs.ingenious.games.go.GoEngine;
import za.ac.sun.cs.ingenious.games.go.GoFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSDescender;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSNode;
import za.ac.sun.cs.ingenious.search.mcts.legacy.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.legacy.SimpleUpdater;
import za.ac.sun.cs.ingenious.search.mcts.legacy.UCT;

public class GoLegacyMCTSEngine extends GoEngine {

  private static final int TURN_TIME = 500;

  /**
   * @param toServer An established connection to the GameServer
   */
  public GoLegacyMCTSEngine(EngineToServerConnection toServer)
      throws MissingSettingException, IncorrectSettingTypeException, IOException {
    super(toServer);
  }

  @Override
  public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
    MCTSNode<GoBoard> root = new MCTSNode<GoBoard>(currentState, logic, null, null);
    return new PlayActionMessage(
        MCTS.generateAction(
            root,
            new RandomPolicy<GoBoard>(
                logic,
                new GoFinalEvaluator(),
                new PerfectInformationActionSensor<GoBoard>(),
                false),
            new MCTSDescender<GoBoard>(logic, new UCT<>(), new PerfectInformationActionSensor<>()),
            new SimpleUpdater<>(),
            TURN_TIME));
  }
}
