package za.ac.sun.cs.ingenious.search.mcts.expansion;

import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;

public class ExpansionSingle<
        S extends GameState, C, N extends MctsNodeCompositionInterface<S, C, ?>>
    implements ExpansionThreadSafe<C, N> {

  private GameLogic<S> logic;

  private ExpansionSingle(GameLogic<S> logic) {
    this.logic = logic;
  }

  /**
   * Create a new child node from the current node and add it to the search tree.
   *
   * @param node the current node for which a child must be added
   * @return
   */
  public C expand(N node) {
    // get a random unexplored child
    C expandedChild = node.popUnexploredChild(logic);
    // if there are no unexplored children, return self as child-type
    if (expandedChild == null) {
      return node.getSelfAsChildType();
    }
    // if an unexplored child was found, add the child to the list of visited
    // children
    node.addChild(expandedChild);
    return expandedChild;
  }

  public static <SS extends GameState, CC, NN extends MctsNodeCompositionInterface<SS, CC, ?>>
      ExpansionThreadSafe<CC, NN> newExpansionSingle(GameLogic<SS> logic) {
    return new ExpansionSingle<>(logic);
  }
}
