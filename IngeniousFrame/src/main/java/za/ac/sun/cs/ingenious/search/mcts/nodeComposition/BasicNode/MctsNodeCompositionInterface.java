package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode;

import java.util.List;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.search.TreeNode;

public interface MctsNodeCompositionInterface<S extends GameState, C, P> extends TreeNode<S, C, P> {

  double getVisitCount();

  void incVisitCount();

  void decVisitCount();

  double getChildValue(C child);

  boolean unexploredActionsEmpty();

  List<Action> getUnexploredActions();

  List<Action> getAllPossibleActions();

  C popUnexploredChild(GameLogic<S> logic);

  C popUnexploredChild(GameLogic<S> logic, int index);

  Action getPrevAction();

  C getSelfAsChildType();

  int getPlayerID();
}
