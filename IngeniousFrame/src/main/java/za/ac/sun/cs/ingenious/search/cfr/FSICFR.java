package za.ac.sun.cs.ingenious.search.cfr;

import static za.ac.sun.cs.ingenious.search.cfr.Helper.getNextPlayerID;

import com.esotericsoftware.minlog.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.data.structures.dag.GameStateDAG;
import za.ac.sun.cs.ingenious.core.exception.dag.CycleException;
import za.ac.sun.cs.ingenious.core.exception.dag.StateNotFoundException;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;

public class FSICFR<S extends GameState> {
  private final double exploitThreshold = 0.001;

  private S root;
  private InformationSetDeterminizer<S> det;
  private GameLogic<S> logic;
  private GameFinalEvaluator<S> eval;
  private ActionSensor<S> sensor;

  private Map<S, Integer> nVisits;
  private Map<S, Integer> nTotalVisits;
  private Map<S, Double> nValues;
  private Map<S, Map<Action, Double>> nRegrets;
  private Map<S, Map<Action, Double>> nStrategySums;
  private Map<S, Map<Action, Double>> nStrategies;
  private Map<S, Double> nSumOne;
  private Map<S, Double> nSumTwo;

  private Random rand;

  private int trainingIterations;

  private boolean collectExploitability;
  private BestResponse<S> bestResponse;
  private double[] exploitability;

  private boolean collectTrainingTimes;
  private double[] trainingTimes;
  private double totalTrainingTime;

  // TODO: BEFORE USE: Please see issue #300 and the accompanying TODO on line 333
  public FSICFR(
      S root,
      InformationSetDeterminizer<S> det,
      GameLogic<S> logic,
      GameFinalEvaluator<S> eval,
      ActionSensor<S> sensor)
      throws StateNotFoundException {
    this.root = root;
    this.det = det;
    this.logic = logic;
    this.eval = eval;
    this.sensor = sensor;

    nVisits = new HashMap<S, Integer>();
    nTotalVisits = new HashMap<>();
    nRegrets = new HashMap<S, Map<Action, Double>>();
    nValues = new HashMap<S, Double>();
    nStrategySums = new HashMap<S, Map<Action, Double>>();
    nStrategies = new HashMap<S, Map<Action, Double>>();
    nSumOne = new HashMap<S, Double>();
    nSumTwo = new HashMap<S, Double>();

    rand = new Random();

    Log.info("FSICFR", "Initializing data structures...");
    long startTime = System.nanoTime();
    initTables();
    Log.info(
        "FSICFR",
        "Initializing DONE. Time elapsed: "
            + TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - startTime)
            + "s. Number of info sets: "
            + nStrategies.size());
  }

  // TODO: See Issue #299
  /*
   * public FSICFR(S root, InformationSetDeterminizer<S> det, GameLogic<S> logic,
   * GameFinalEvaluator<S> eval, ActionSensor<S> sensor,
   * Map<S, Map<Action, Double>> nStrategySums) {
   *
   * this.root = root;
   * this.det = det;
   * this.logic = logic;
   * this.eval = eval;
   * this.sensor = sensor;
   *
   * nVisits = new HashMap<S, Integer>();
   * nTotalVisits = new HashMap<>();
   * nRegrets = new HashMap<S, Map<Action, Double>>();
   * nValues = new HashMap<S, Double>();
   * nStrategySums = nStrategySums;
   * nStrategies = new HashMap<S, Map<Action, Double>>();
   * nSumOne = new HashMap<S, Double>();
   * nSumTwo = new HashMap<S, Double>();
   *
   * rand = new Random();
   *
   * Log.info("FSICFR", "Loading data from saved cumulative strategies");
   * initTables(nStrategySums.keySet());
   * Log.info("FSICFR", "Loaded " + nStrategySums.size() + " information sets");
   * }
   */

  public void setCollectExploitability(boolean collectExploitability) {
    this.collectExploitability = collectExploitability;
  }

  public void setCollectTrainingTimes(boolean collectTrainingTimes) {
    this.collectTrainingTimes = collectTrainingTimes;
  }

  private void initPlayerNodeTables(S node, List<Action> actions) {
    Map<Action, Double> nRegret = new HashMap<>();
    Map<Action, Double> nStrategySum = new HashMap<>();
    Map<Action, Double> nStrategy = new HashMap<>();

    double initialActionProb = 1.0 / actions.size();
    for (Action action : actions) {
      nRegret.put(action, 0.0);
      nStrategySum.put(action, 0.0);
      nStrategy.put(action, initialActionProb);
    }

    nTotalVisits.put(node, 0);
    nRegrets.put(node, nRegret);
    nStrategySums.put(node, nStrategySum);
    nStrategies.put(node, nStrategy);

    initNodeTables(node);
  }

  // TODO Issue #299: Figure out how to save the cumulative strategies and load
  // all nodes for all tables.
  private void initNodeTables(S node) {
    nValues.put(node, 0.0);
    nVisits.put(node, 0);
    nSumOne.put(node, 0.0);
    nSumTwo.put(node, 0.0);
  }

  private void initTables() {
    List<S> queue = new LinkedList<S>();
    queue.add(root);

    while (!queue.isEmpty()) {
      S currentState = queue.remove(0);

      List<Action> actions;
      int currentPlayerID = Helper.getNextPlayerID(currentState, logic);

      S imperfectInfoForActivePlayer = det.observeState(currentState, currentPlayerID);
      if (currentPlayerID == currentState.getNumPlayers()) { // If chance node
        actions = new ArrayList<>(logic.generateStochasticActions(currentState).keySet());

        initNodeTables(imperfectInfoForActivePlayer);

      } else {
        actions = logic.generateActions(currentState, currentPlayerID);

        if (!nRegrets.containsKey(imperfectInfoForActivePlayer)) {
          if (logic.isTerminal(currentState)) {
            initNodeTables(imperfectInfoForActivePlayer);
          } else {
            initPlayerNodeTables(imperfectInfoForActivePlayer, actions);
          }
        }
      }

      for (Action a : actions) {
        @SuppressWarnings("unchecked")
        S newState = (S) currentState.deepCopy();
        logic.makeMove(newState, sensor.fromPointOfView(a, newState, -1));

        queue.add(newState);
      }
    }
  }

  private void fsicfr(GameStateDAG<S> reachableSubset) throws StateNotFoundException {
    // Line 2
    List<S> sortedSubset = reachableSubset.getSortedList();

    if (!reachableSubset.validate()) {
      // TODO: Throw exception
    }

    for (S node : sortedSubset) {

      // Line 3-6
      if (node.equals(root)) {
        nVisits.put(node, 1);
        nSumOne.put(node, 1.0);
        nSumTwo.put(node, 1.0);
      }

      int nodePlayer = reachableSubset.getActivePlayers(node).get(0);
      // Line 7
      if (reachableSubset.getType(node) == GameStateDAG.NodeType.PLAYER) {

        // Line 8-12
        Map<Action, Double> nodeStrategy = nStrategies.get(node);
        double sumCumulativeRegrets = 0;
        List<Action> actionsAtNode = reachableSubset.getPossibleActions(node);
        for (Action a : actionsAtNode) {
          sumCumulativeRegrets += Math.max(nRegrets.get(node).get(a), 0.0);
        }
        double uniformProb = 1.0 / actionsAtNode.size();
        for (Action a : actionsAtNode) {
          if (sumCumulativeRegrets > 0) {
            nodeStrategy.put(a, Math.max(nRegrets.get(node).get(a), 0.0) / sumCumulativeRegrets);
          } else {
            nodeStrategy.put(a, uniformProb);
          }
          // Line 12
          nStrategySums
              .get(node)
              .put(
                  a,
                  nStrategySums.get(node).get(a)
                      + ((nodePlayer == 0)
                          ? nodeStrategy.get(a) * nSumOne.get(node)
                          : nodeStrategy.get(a) * nSumTwo.get(node)));
        }

        // Lines 13-18
        for (Action a : actionsAtNode) {
          S c = reachableSubset.getChildren(node, a).get(0);

          nVisits.put(c, nVisits.get(c) + nVisits.get(node));
          nSumOne.put(
              c,
              nSumOne.get(c)
                  + ((nodePlayer == 0)
                      ? nodeStrategy.get(a) * nSumOne.get(node)
                      : nSumOne.get(node)));
          nSumTwo.put(
              c,
              nSumTwo.get(c)
                  + ((nodePlayer == 0)
                      ? nSumTwo.get(node)
                      : nodeStrategy.get(a) * nSumTwo.get(node)));
        }
        // Lines 19-24
      } else if (reachableSubset.getType(node) == GameStateDAG.NodeType.CHANCE) {
        S c = reachableSubset.getChildren(node).get(0);
        nVisits.put(c, nVisits.get(c) + nVisits.get(node));
        nSumOne.put(c, nSumOne.get(c) + nSumOne.get(node));
        nSumTwo.put(c, nSumTwo.get(c) + nSumTwo.get(node));
      }
    }

    // Line 26
    for (ListIterator<S> it = sortedSubset.listIterator(sortedSubset.size()); it.hasPrevious(); ) {
      S node = it.previous();

      int nodePlayer = reachableSubset.getActivePlayers(node).get(0);

      if (reachableSubset.getType(node) == GameStateDAG.NodeType.PLAYER) {
        nValues.put(node, 0.0);
        List<Action> actionsAtNode = reachableSubset.getPossibleActions(node);
        Map<Action, Double> actionsValue = new HashMap<>();

        // Lines 29-33
        for (Action action : actionsAtNode) {
          S c = reachableSubset.getChildren(node, action).get(0);

          actionsValue.put(
              action,
              ((nodePlayer == reachableSubset.getActivePlayers(c).get(0))
                  ? nValues.get(c)
                  : -nValues.get(c)));
          nValues.put(
              node,
              nValues.get(node) + nStrategies.get(node).get(action) * actionsValue.get(action));
        }
        // Line 34
        double cfp = (nodePlayer == 0) ? nSumTwo.get(node) : nSumOne.get(node);

        // Lines 35-37
        for (Action action : actionsAtNode) {
          nRegrets
              .get(node)
              .put(
                  action,
                  (1.0 / (nTotalVisits.get(node) + nVisits.get(node)))
                      * (nTotalVisits.get(node) * nRegrets.get(node).get(action)
                          + nVisits.get(node)
                              * cfp
                              * (actionsValue.get(action) - nValues.get(node))));
        }
        // Line 38
        nTotalVisits.put(node, nTotalVisits.get(node) + nVisits.get(node));
        // Line 39
      } else if (reachableSubset.getType(node) == GameStateDAG.NodeType.CHANCE) {
        assert reachableSubset.getChildren(node).size() == 1;

        S c = reachableSubset.getChildren(node).get(0);
        nValues.put(node, nValues.get(c));
        // Line 41
      } else if (reachableSubset.getType(node) == GameStateDAG.NodeType.TERMINAL) {
        nValues.put(node, eval.getScore(node)[nodePlayer]);
      }

      // Line 44
      nVisits.put(node, 0);
      nSumOne.put(node, 0.0);
      nSumTwo.put(node, 0.0);
    }
  }

  public void solve(int numIterations) throws StateNotFoundException, CycleException {
    trainingIterations = numIterations;

    if (collectTrainingTimes) {
      exploitability = new double[numIterations];
      bestResponse = new BestResponse<>(root, det, logic, eval, sensor);
    }

    if (collectTrainingTimes) {
      totalTrainingTime = 0.0;
      trainingTimes = new double[numIterations];
    }

    double trainingTime = 0.0;
    for (int t = 0; t < numIterations; t++) {
      long iterStart = System.nanoTime();

      GameStateDAG<S> reachableSubset = determineChanceNodes();
      fsicfr(reachableSubset);

      double iterTime = System.nanoTime() - iterStart;
      trainingTime += iterTime;
      Log.info(
          "FSICFR",
          "Iteration " + t + " took " + TimeUnit.NANOSECONDS.toSeconds((long) iterTime) + "s.");

      if (collectTrainingTimes) {
        trainingTimes[t] = iterTime;
        totalTrainingTime += iterTime;
      }

      if (this.collectExploitability) {
        Map<S, Map<Action, Double>> normStrat = getNormalizedStrategy();
        // TODO Issue #300: BestResponse class is incompatible. Need a means of
        // expanding
        // game tree to allow for best response calculation.
        double brvP1 = bestResponse.getBestResponseValue(normStrat, 0);
        double brvP2 = bestResponse.getBestResponseValue(normStrat, 1);

        exploitability[t] = (brvP1 + brvP2) / 2.0;

        if (exploitability[t] < exploitThreshold) {
          trainingIterations = t + 1;
          break;
        }
      }
    }

    Log.info(
        "FSICFR",
        "Total training took " + TimeUnit.NANOSECONDS.toSeconds((long) trainingTime) + "s.");
  }

  public Map<S, Map<Action, Double>> getCumulativeStrategyProfile() {
    return nStrategySums;
  }

  public Map<S, Map<Action, Double>> getNormalizedStrategy() {
    Map<S, Map<Action, Double>> normStrategy = new HashMap<>();

    for (S state : nStrategySums.keySet()) {
      normStrategy.put(state, Helper.getNormalizedDistribution(nStrategySums.get(state)));
    }

    return normStrategy;
  }

  private S addNode(GameStateDAG<S> DAG, S h, Action a, double prob) throws StateNotFoundException {

    int activePlayerID = Helper.getNextPlayerID(h, logic);
    S I = det.observeState(h, activePlayerID);

    @SuppressWarnings("unchecked")
    S ha = (S) h.deepCopy();
    logic.makeMove(ha, sensor.fromPointOfView(a, ha, -1));

    int nextPlayerID = Helper.getNextPlayerID(ha, logic);
    S Ia = det.observeState(ha, nextPlayerID);

    if (!DAG.contains(Ia)) {
      if (nextPlayerID == ha.getNumPlayers()) {
        DAG.addNode(
            Ia, new ArrayList<>(logic.getCurrentPlayersToAct(ha)), GameStateDAG.NodeType.CHANCE);
      } else if (logic.isTerminal(ha)) {
        // Active player at terminal node is the player that made the last action
        // Allows FSICFR algorithm to identify correct score to propagate up the tree
        DAG.addNode(
            Ia, new ArrayList<>(logic.getCurrentPlayersToAct(h)), GameStateDAG.NodeType.TERMINAL);
      } else {
        DAG.addNode(
            Ia, new ArrayList<>(logic.getCurrentPlayersToAct(ha)), GameStateDAG.NodeType.PLAYER);
      }

      DAG.addEdge(I, Ia, a, prob);
    }

    return ha;
  }

  private void _determineChanceNodes(GameStateDAG<S> infosetDAG, S h)
      throws StateNotFoundException {
    int activePlayerID = Helper.getNextPlayerID(h, logic);

    if (logic.isTerminal(h)) {
      return;
    } else if (activePlayerID == h.getNumPlayers()) {
      Map<Action, Double> actionsProbs = logic.generateStochasticActions(h);
      Action a = Helper.sampleFromStrategy(actionsProbs);

      S ha = addNode(infosetDAG, h, a, 1.0);

      _determineChanceNodes(infosetDAG, ha);

    } else {
      S I = det.observeState(h, activePlayerID);

      Map<Action, Double> actionsMap = nStrategies.get(I);

      for (Action a : actionsMap.keySet()) {
        S ha = addNode(infosetDAG, h, a, actionsMap.get(a));

        _determineChanceNodes(infosetDAG, ha);
      }
    }
  }

  private GameStateDAG<S> determineChanceNodes()
      throws StateNotFoundException, IllegalStateException {
    GameStateDAG<S> reachableDAG = new GameStateDAG<>();

    int rootPlayerID = getNextPlayerID(root, logic);
    if (rootPlayerID == root.getNumPlayers()) {
      reachableDAG.addNode(
          root, new ArrayList<>(logic.getCurrentPlayersToAct(root)), GameStateDAG.NodeType.CHANCE);
    } else { // Assume root cannot be terminal
      reachableDAG.addNode(
          root, new ArrayList<>(logic.getCurrentPlayersToAct(root)), GameStateDAG.NodeType.PLAYER);
    }

    _determineChanceNodes(reachableDAG, root);

    return reachableDAG;
  }

  public double[] getExploitability() {
    if (exploitability.length > trainingIterations) {
      exploitability = Arrays.copyOfRange(exploitability, 0, trainingIterations);
    }

    return exploitability;
  }

  public double[] getTrainingTimes() {
    if (trainingTimes.length > trainingIterations) {
      trainingTimes = Arrays.copyOfRange(trainingTimes, 0, trainingIterations);
    }

    return trainingTimes;
  }
}
