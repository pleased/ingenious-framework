package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;
import za.ac.sun.cs.ingenious.core.Action;

/**
 * A stored action which holds all actions relating to the single action index in the hashmap. This
 * holds the 'many' part of the one-to-many correspondence.
 */
public class CMCStoredActions {

  private final HashMap<Action, CMCStoredAction> storedActions;
  private final ReentrantLock storedActionsLock = new ReentrantLock();

  /**
   * Constructor initialising values according to the win boolean
   *
   * @param action
   * @param win
   */
  public CMCStoredActions() {
    storedActions = new HashMap<Action, CMCStoredAction>();
  }

  public void addResult(Action action, double score) {
    // lock to prevent two threads from adding the same action at the same time
    storedActionsLock.lock();
    CMCStoredAction storedAction = storedActions.get(action);
    if (storedAction == null) {
      storedAction = new CMCStoredAction(score);
      storedActions.put(action, storedAction);
      // new action added, release lock
      storedActionsLock.unlock();
    } else {
      // action already exists, release lock
      storedActionsLock.unlock();
      // update the stored action
      storedAction.addResult(score);
    }
  }

  /**
   * Returns the winrate of the action if it exists. If it does not exist, it returns 1.0 to
   * maximise exploration of not-yet-explored move-pairs
   *
   * @return
   */
  public double getCMCValue(Action action) {
    CMCStoredAction storedAction = storedActions.get(action);
    return (storedAction != null) ? storedAction.getCMCValue() : 1.0;
  }

  /**
   * Returns the cumulative wins of the action if it exists. If it does not exist, it returns -1.0
   */
  public double getCMCWins(Action action) {
    CMCStoredAction storedAction = storedActions.get(action);
    return (storedAction != null) ? storedAction.getCMCWins() : -1.0;
  }

  /**
   * Returns the number of visits of the action if it exists. If it does not exist, it returns -1
   */
  public int getCMCVisits(Action action) {
    CMCStoredAction storedAction = storedActions.get(action);
    return (storedAction != null) ? storedAction.getCMCVisits() : -1;
  }

  /**
   * Returns true if the action is contained in the stored actions
   *
   * @param action
   */
  public boolean contains(Action action) {
    return storedActions.containsKey(action);
  }

  /**
   * @return returns the list of actions relating to the 'many' part of the one-to-many
   *     correspondence
   */
  public List<Action> getActions() {
    return storedActions.keySet().stream().collect(Collectors.toList());
  }

  class CMCStoredAction {

    private final ReadWriteLock actionLock = new ReentrantReadWriteLock();
    private final Lock actionWriteLock = actionLock.writeLock();
    private final Lock actionReadLock = actionLock.readLock();

    private double score;
    private int visitCount;

    /**
     * Constructor initialising values according to the win boolean
     *
     * @param win
     */
    public CMCStoredAction(double initalScore) {
      score = initalScore;
      visitCount = 1;
    }

    /**
     * Adds the given score to the current score and increments the visit count
     *
     * @param score
     */
    public void addResult(double delta) {
      actionWriteLock.lock();
      score += delta;
      visitCount++;
      actionWriteLock.unlock();
    }

    /**
     * Returns the winrate of the action
     *
     * @return
     */
    private double getCMCValue() {
      actionReadLock.lock();
      try {
        return score / visitCount;
      } finally {
        actionReadLock.unlock();
      }
    }

    /**
     * Returns the number of wins of the action
     *
     * @return
     */
    private double getCMCWins() {
      actionReadLock.lock();
      try {
        return score;
      } finally {
        actionReadLock.unlock();
      }
    }

    /**
     * Returns the number of visits of the action
     *
     * @return
     */
    private int getCMCVisits() {
      actionReadLock.lock();
      try {
        return visitCount;
      } finally {
        actionReadLock.unlock();
      }
    }
  }
}
