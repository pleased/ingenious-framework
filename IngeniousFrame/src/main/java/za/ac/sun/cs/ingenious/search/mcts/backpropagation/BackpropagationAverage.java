package za.ac.sun.cs.ingenious.search.mcts.backpropagation;

import java.util.List;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;

public class BackpropagationAverage<N extends MctsNodeCompositionInterface<?, ?, ?>>
    implements BackpropagationThreadSafe<N> {

  public BackpropagationAverage() {}

  /**
   * Update the fields relating to this backpropagation strategy for the current node.
   *
   * @param node
   * @param results the win/loss or draw outcome for the simulation relating to the current playout.
   */
  public void propagate(N node, double[] results, List<Action> moves) {
    // TODO : The mcts selection classes all divide by the number of visits... so
    // this class does not seem to serve any purpose and won't perform as expected
    // in conjunction with the current selection classes. See issue #328
    double oldValue = node.getValue();
    double oldVisitCount = node.getVisitCount();
    double tempTotal = (oldValue * oldVisitCount) + results[((N) node.getParent()).getPlayerID()];
    node.incVisitCount();
    node.addValue(tempTotal / node.getVisitCount() - oldValue);
  }

  public static <NN extends MctsNodeCompositionInterface<?, ?, ?>>
      BackpropagationAverage<NN> newBackpropagationAverage() {
    return new BackpropagationAverage();
  }
}
