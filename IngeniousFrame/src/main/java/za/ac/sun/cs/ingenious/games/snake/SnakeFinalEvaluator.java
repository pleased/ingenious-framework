package za.ac.sun.cs.ingenious.games.snake;

import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;

/**
 * Final evaluator for Snake that derives the player score from game state.
 *
 * @author Steffen Jacobs
 */
public class SnakeFinalEvaluator implements GameFinalEvaluator<SnakeState> {
  @Override
  public double[] getScore(SnakeState forState) {
    return new double[] {forState.getScore()};
  }
}
