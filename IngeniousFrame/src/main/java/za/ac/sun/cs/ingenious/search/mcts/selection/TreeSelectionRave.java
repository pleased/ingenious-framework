package za.ac.sun.cs.ingenious.search.mcts.selection;

import com.esotericsoftware.minlog.Log;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE.MctsRaveNodeExtensionParallel;

/**
 * A standard implementation of the TreeEngine selection strategy {@link TreeSelection}, that
 * calculates UCT values to make a decision of which child node to select. This strategy is
 * specifically for TreeEngine selection and makes use of read locks when viewing node information
 * that is used in the calculations.
 *
 * @author Karen Laubscher
 * @param <S> The game state type.
 * @param <N> The type of the mcts node.
 */
public class TreeSelectionRave<
        S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, ?>>
    implements TreeSelection<N> {

  private boolean fpuEnabled;
  private double startPriority;
  protected double c;
  protected double V;
  public int player;

  /**
   * Constructor to create a TreeEngine UCT selection object with a specified value for the constant
   * C used in the UCT value calculation.
   *
   * @param logic The game logic
   * @param c The constant value for C (in the UCT calculation).
   */
  protected TreeSelectionRave(
      int player, double c, double V, boolean fpuEnabled, double startPriority) {
    this.player = player;
    this.c = c;
    this.V = V;
    this.fpuEnabled = fpuEnabled;
    this.startPriority = startPriority;
  }

  /**
   * The method that decides which child node to traverse to next, based on calculating the UCT
   * value for each child and then selecting the child with the highest UCT value. Since the
   * TreeEngine structure is shared in TreeEngine parallelisation, nodes are (read) locked when
   * information for the calculations are viewed.
   *
   * @param node The current node whose children nodes are considered for the next node to traverse
   *     to.
   * @return The selected child node with the highest UCT value.
   */
  public N select(N node) {

    // if there are still unexplored children and FPU is disabled, return
    // to selection phase and expand one of them. If fpu is enabled, we don't
    // necessarily want to expand a child node, so we continue with selection
    if (!fpuEnabled && !node.unexploredActionsEmpty()) {
      return null;
    }

    // Get the current node's visit count
    double currentVisitCount = node.getVisitCount();

    Hashtable<String, MctsNodeExtensionParallelInterface> enhancements =
        node.getEnhancementClasses();

    node.readLockEnhancementClassesArrayList();
    MctsRaveNodeExtensionParallel raveEnhancementExtensionParallel =
        (MctsRaveNodeExtensionParallel) enhancements.get("Rave");
    node.readUnlockEnhancementClassesArrayList();

    if (raveEnhancementExtensionParallel == null) {
      Log.error("No rave enhancement to select from in Rave selection class");
    }

    ConcurrentHashMap<Action, MctsNodeTreeParallelInterface<?, ?, ?>> actionToNodeMapping =
        raveEnhancementExtensionParallel.getActionToNodeMapping();

    double uctScore,
        raveVisitCount,
        raveWinsCount,
        monteCarloVisitCount,
        monteCarloWinCount,
        childAverage,
        childRaveAverage;

    N highestUctChild = null;
    double highestUct = Double.NEGATIVE_INFINITY;

    raveEnhancementExtensionParallel.setReadLockChild();
    for (HashMap.Entry<Action, MctsNodeTreeParallelInterface<?, ?, ?>> entry :
        actionToNodeMapping.entrySet()) {
      Action action = entry.getKey();
      N child = (N) entry.getValue();

      // calculate the UCB score for the child
      monteCarloVisitCount = child.getVisitCount() + child.getVirtualLoss();
      monteCarloWinCount = child.getValue();
      childAverage =
          (monteCarloWinCount / monteCarloVisitCount)
              + c * Math.sqrt(Math.log(currentVisitCount) / monteCarloVisitCount);

      // calculate the rave score for the child
      raveVisitCount =
          raveEnhancementExtensionParallel.getRaveVisits(action) + child.getVirtualLoss();
      raveWinsCount = raveEnhancementExtensionParallel.getRaveWins(action);
      if (raveVisitCount == 0.0) {
        childRaveAverage = 0.0;
      } else {
        childRaveAverage = raveWinsCount / raveVisitCount;
      }

      // calculate the weighted uct score
      uctScore = calculateUct(monteCarloVisitCount, childAverage, childRaveAverage);

      if (Double.compare(uctScore, highestUct) > 0) {
        highestUctChild = child;
        highestUct = uctScore;
      }
    }
    raveEnhancementExtensionParallel.unsetReadLockChild();

    if (fpuEnabled) {
      // if there is no chosen child yet, or the highest scoring child is less than
      // the threshold value and there are still unexplored children, return null -
      // i.e. do not select a child already in the tree, but expand a new unexplored
      // child
      if (highestUctChild == null
          || (!node.unexploredActionsEmpty() && highestUct < startPriority)) {
        return null;
      }
    }

    return highestUctChild;
  }

  protected double calculateUct(
      double monteCarloVisitCount, double childAverage, double childRaveAverage) {
    double beta = Math.max(0.0, (this.V - monteCarloVisitCount) / V);
    return (1 - beta) * childAverage + beta * childRaveAverage;
  }

  /**
   * A static factory method, which creates a TreeEngine UCT selection object that uses the
   * specified value for the constant C in the UCT value calculation.
   *
   * <p>This factory method is also a generic method, which uses Java type inferencing to
   * encapsulate the complex generic type capturing required by the TreeEngine UCT selection
   * constructor.
   *
   * @param logic The game logic
   * @param c The constant value for C (in the UCT calculation).
   */
  public static <SS extends GameState, NN extends MctsNodeTreeParallelInterface<SS, NN, ?>>
      TreeSelection<NN> newTreeSelectionRave(
          int player, double c, double V, boolean fpuEnabled, double startPriority) {
    return new TreeSelectionRave<SS, NN>(player, c, V, fpuEnabled, startPriority);
  }
}
