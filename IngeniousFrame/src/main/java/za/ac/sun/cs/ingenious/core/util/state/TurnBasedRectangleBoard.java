package za.ac.sun.cs.ingenious.core.util.state;

import com.esotericsoftware.minlog.Log;
import java.util.Arrays;
import java.util.Objects;

/**
 * Represents the state of a game that is played on a rectangle board with players taking turns.
 * Adapted from @ Michael Krause's TurnBasedSquareBoard.java.
 *
 * @author Alexander Mouton
 */
public class TurnBasedRectangleBoard extends TurnBasedGameState {

  public byte[] board;
  private final int boardWidth;
  private final int boardHeight;
  private final int numCells;

  /** Initializes an empty board. */
  public TurnBasedRectangleBoard(int boardWidth, int boardHeight, int firstPlayer, int numPlayers) {
    super(firstPlayer, numPlayers);
    this.boardWidth = boardWidth;
    this.boardHeight = boardHeight;
    this.numCells = boardWidth * boardHeight;
    this.board = new byte[this.numCells];
  }

  /** Copy constructor. Duplicates the given board. */
  public TurnBasedRectangleBoard(TurnBasedRectangleBoard toCopy) {
    super(toCopy.nextMovePlayerID, toCopy.numPlayers);
    this.boardWidth = toCopy.getBoardWidth();
    this.boardHeight = toCopy.getBoardHeight();
    this.numCells = toCopy.getNumCells();
    this.board = new byte[this.numCells];
    System.arraycopy(toCopy.board, 0, this.board, 0, this.numCells);
  }

  /**
   * @return The width of the board.
   */
  public int getBoardWidth() {
    return boardWidth;
  }

  /**
   * @return The height of the board.
   */
  public int getBoardHeight() {
    return boardHeight;
  }

  /**
   * @return The size of the board, i.e. width * height.
   */
  public int getNumCells() {
    return numCells;
  }

  @Override
  public TurnBasedRectangleBoard deepCopy() {
    return new TurnBasedRectangleBoard(this);
  }

  /**
   * @return The index into the board array for the given x and y coordinates.
   */
  public int xyToIndex(int x, int y) {
    return x + y * this.boardWidth;
  }

  @Override
  public void printPretty() {
    Log.info(this.toString());
  }

  @Override
  public String getPretty() {
    return this.toString();
  }

  public String toString() {
    StringBuilder s = new StringBuilder();
    s.append("\n+");
    for (short i = 0; i < boardWidth; i++) {
      s.append("-");
    }
    s.append("+\n");

    for (short y = 0; y < boardHeight; y++) {
      s.append("|");
      for (short x = 0; x < boardWidth; x++) {
        s.append(board[xyToIndex(x, y)]);
      }
      s.append("|");
      s.append("\n");
    }

    s.append("+");
    for (short i = 0; i < boardWidth; i++) {
      s.append("-");
    }
    s.append("+");
    return s.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(Arrays.hashCode(board), numCells, numPlayers, nextMovePlayerID);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!super.equals(obj)) return false;
    if (getClass() != obj.getClass()) return false;
    TurnBasedRectangleBoard other = (TurnBasedRectangleBoard) obj;
    if (boardWidth != other.boardWidth || boardHeight != other.boardHeight) return false;
    if (!Arrays.equals(board, other.board)) return false;
    return true;
  }
}
