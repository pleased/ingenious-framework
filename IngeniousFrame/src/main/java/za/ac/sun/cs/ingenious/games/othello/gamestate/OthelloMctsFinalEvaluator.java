package za.ac.sun.cs.ingenious.games.othello.gamestate;

import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.search.minimax.GameEvaluator;

/**
 * Class for evaluating terminal states for Othello, as well as evalutations that return result
 * values that can be used in MCTS searches.
 *
 * @author Karen Laubscher
 */
public class OthelloMctsFinalEvaluator
    implements GameEvaluator<OthelloBoard>, MctsGameFinalEvaluator<OthelloBoard> {

  // Othello is a zero-sum 2-player game
  private static final double WIN_VALUE = 1.0;
  private static final double LOSE_VALUE = 0.0;
  private static final double DRAW_VALUE = 0.5;

  /**
   * Returns the scores for each player if the game is terminal. If the state is not terminal, then
   * null is returned (scores from the normal evaluation function, which returns null in the case of
   * a non terminal state).
   *
   * @return The scores (used for MCTS results)
   */
  public double[] getMctsScore(OthelloBoard forState) {
    double[] scores = getScore(forState);
    if (scores[0] > scores[1]) {
      scores[0] = WIN_VALUE;
      scores[1] = LOSE_VALUE;
    } else if (scores[0] < scores[1]) {
      scores[0] = LOSE_VALUE;
      scores[1] = WIN_VALUE;
    } else {
      scores[0] = DRAW_VALUE;
      scores[1] = DRAW_VALUE;
    }
    return scores;
  }

  /**
   * Getter for the value of a win (For the MCTS result)
   *
   * @return The win value used in MCTS.
   */
  public double getWinValue() {
    return (double) WIN_VALUE;
  }

  /**
   * Getter for the value of a loss (For the MCTS result).
   *
   * @return The loss value used in MCTS.
   */
  public double getLossValue() {
    return (double) LOSE_VALUE;
  }

  /**
   * Getter for the value of a draw (For the MCTS result).
   *
   * @return The draw value used in MCTS.
   */
  public double getDrawValue() {
    return (double) DRAW_VALUE;
  }

  /**
   * We normalize the players' scores here, so 0 is a complete loss, 1 a perfect win and 0.5 a draw.
   */
  public double[] getScore(OthelloBoard forState) {
    double[] scores = new double[2];
    scores[0] = forState.blackScore;
    scores[1] = forState.whiteScore;
    return scores;
  }
}
