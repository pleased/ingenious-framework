package za.ac.sun.cs.ingenious.games.nim;

import za.ac.sun.cs.ingenious.core.util.state.TurnBasedRectangleBoard;

public class NimBoard extends TurnBasedRectangleBoard {

  public NimBoard(int rows, int player, int numPlayers) {
    super(2 * rows - 1, rows, player, numPlayers);
    // initialise the board
    for (int j = 0; j < getBoardHeight(); j++) {
      int r = j * getBoardWidth();
      for (int i = 0; i < 2 * j + 1; i++) {
        board[r + i] = 1;
      }
    }
  }

  public NimBoard(NimBoard toCopy) {
    super(toCopy);
  }

  @Override
  public NimBoard deepCopy() {
    return new NimBoard(this);
  }
}
