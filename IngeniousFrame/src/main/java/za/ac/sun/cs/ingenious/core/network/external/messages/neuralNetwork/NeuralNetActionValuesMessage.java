package za.ac.sun.cs.ingenious.core.network.external.messages.neuralNetwork;

import java.util.Map;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

/**
 * Used to receive approximated action values for a state using the DQN neural network.
 *
 * @author Steffen Jacobs
 */
public class NeuralNetActionValuesMessage extends ExternalMessage {
  Map<Integer, Double> payload;

  public NeuralNetActionValuesMessage(Map<Integer, Double> payload) {
    super(ExternalMessageType.NEURAL_NET_ACTION_VALUES);

    this.payload = payload;
  }

  public Map<Integer, Double> getPayload() {
    return payload;
  }

  public void setPayload(Map<Integer, Double> payload) {
    this.payload = payload;
  }
}
