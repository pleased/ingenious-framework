package za.ac.sun.cs.ingenious.games.cardGames.hearts.game;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSuits;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSymbols;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;

/**
 * The class HeartsFinalEvaluator.
 *
 * <p>Evaluates a given game state. Normally used when a game state is terminal.
 *
 * @author Joshua Wiebe
 */
public class HeartsFinalEvaluator implements GameFinalEvaluator<HeartsGameState> {

  /**
   * @see
   *     za.ac.sun.cs.ingenious.core.GameFinalEvaluator#getWinner(za.ac.sun.cs.ingenious.core.model.GameState)
   *     <p>This method checks which player has collected least points and returns this player.
   *     <p>Note: this method cannot deal with multiple winners. If more than one player have the
   *     same score the winner with the lowest id is preferred.
   * @return Returns the winner ID of a given state.
   */
  public int getWinner(HeartsGameState forState) {

    // The winner.
    int winner = -1;

    // Array with scores.
    double[] scores = getScore(forState);

    // Find which player collected least points. Exception: one player gathered all points.
    double minScore = 100;
    for (int i = 0; i < forState.getNumPlayers(); i++) {

      // If one player gathered all points: he becomes the winner and all other players get 26
      // points.
      if (scores[i] == 26) {
        winner = i;
        break;
      }

      // Else minimize.
      else if (scores[i] < minScore) {
        minScore = scores[i];
        winner = i;
      }
    }

    return winner;
  }

  /**
   * @see
   *     za.ac.sun.cs.ingenious.core.GameFinalEvaluator#getScore(za.ac.sun.cs.ingenious.core.model.GameState)
   * @param forState is the state for which this method calculates the scores of each player.
   * @return Returns the scorings array.
   */
  @Override
  public double[] getScore(HeartsGameState forState) {

    double[] scores = new double[forState.getNumPlayers()];

    // For each players trick pile:
    CardRack<ClassicalSymbols, ClassicalSuits>[] trickPileRacks = forState.getPlayersTrickPiles();
    for (int i = 0; i < trickPileRacks.length; i++) {

      // For each card of the trick pile:  count score.
      for (Card<ClassicalSymbols, ClassicalSuits> card : trickPileRacks[i]) {

        // Each card of the hearts suit is worth one point.
        if (card.getf2().equals(ClassicalSuits.HEARTS)) {
          scores[i]++;
        }

        // The queen of spades is worth 13 points.
        else if (card.getf1().equals(ClassicalSymbols.QUEEN)
            && card.getf2().equals(ClassicalSuits.SPADES)) {
          scores[i] += 13;
        }
      }
    }

    return scores;
  }

  /**
   * Prints players score for a given state.
   *
   * @param forState the HeartsGameState
   */
  public void printScore(HeartsGameState forState) {

    double[] scores = getScore(forState);
    StringBuilder s = new StringBuilder();

    for (int i = 0; i < scores.length; i++) {
      s.append(scores[i] + " ");
    }

    Log.info(s);
  }
}
