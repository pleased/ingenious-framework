package za.ac.sun.cs.ingenious.games.rps.engines;

import com.esotericsoftware.minlog.Log;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.move.UnobservedMove;
import za.ac.sun.cs.ingenious.games.rps.RPSGameState;
import za.ac.sun.cs.ingenious.games.rps.RPSLogic;
import za.ac.sun.cs.ingenious.search.cfr.CFR;

public abstract class GeneralEngine extends Engine {
  protected final int numIterations = 10000;

  protected final RPSLogic logic;
  protected final RPSGameState currentState;
  protected final Random rand;

  /**
   * @param toServer An established connection to the GameServer
   */
  public GeneralEngine(EngineToServerConnection toServer) {
    super(toServer);

    logic = new RPSLogic();
    currentState = new RPSGameState(2);
    rand = new Random();
  }

  @Override
  public void setZobrist(ZobristHashing zobristHashing) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String engineName() {
    return "RPS CFR Engine";
  }

  @Override
  public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
    Log.info("Game over");
  }

  @Override
  public void receivePlayedMoveMessage(PlayedMoveMessage a) {
    Log.info(
        "Player "
            + a.getMove().getPlayerID()
            + " played "
            + ((a.getMove() instanceof UnobservedMove)
                ? "a hidden move"
                : RPSLogic.getPrettyRPSAction(((DiscreteAction) a.getMove()))));

    logic.makeMove(currentState, a.getMove());
  }

  // /**
  // * Creates a strategy profile with an initial cumulative strategy table. For
  // demonstrating the
  // * convergence of the CFR algorithm.
  // * @return
  // */
  // private Map<RPSGameState, Map<Action, Double>> getBeginningStrategy() {
  // double[] cumStrat = new double[]{2, 20, 30};
  //
  // Map<RPSGameState, Map<Action, Double>> strategy = new HashMap<>();
  //
  // List<RPSGameState> queue = new LinkedList<>();
  // queue.add(currentState);
  //
  // while (!queue.isEmpty()) {
  // RPSGameState firstState = queue.remove(0);
  //
  // initTable(cumStrat, firstState, strategy);
  // List<Action> actions = logic.generateActions(firstState,
  // (int)logic.getCurrentPlayersToAct(firstState).toArray()[0]);
  //
  // for (Action a: actions) {
  // RPSGameState newState = firstState.deepCopy();
  // logic.makeMove(newState, logic.fromPointOfView(a, newState, -1));
  // queue.add(newState);
  // }
  // }
  // return strategy;
  // }

  /** Initializes the strategy table for an information set. Copied logic from {@link CFR} */
  private void initTable(
      double[] cumStrat, RPSGameState state, Map<RPSGameState, Map<Action, Double>> strategy) {
    int activePlayerID = (int) logic.getCurrentPlayersToAct(state).toArray()[0];
    RPSGameState infoSet = logic.observeState(state, activePlayerID);

    if (strategy.containsKey(infoSet) || logic.isTerminal(state)) {
      return;
    }

    List<Action> actions = logic.generateActions(infoSet, activePlayerID);
    Map<Action, Double> strategyTable = new HashMap<>();
    for (int i = 0; i < actions.size(); i++) {
      strategyTable.put(actions.get(i), cumStrat[i]);
    }

    strategy.put(infoSet, strategyTable);
  }
}
