package za.ac.sun.cs.ingenious.core.util.search;

import za.ac.sun.cs.ingenious.core.GameState;

public interface SearchNode<S extends GameState> {

  public S getState(boolean getDefensiveCopy);

  public double getValue();

  public void addValue(double add);

  public String toString();
}
