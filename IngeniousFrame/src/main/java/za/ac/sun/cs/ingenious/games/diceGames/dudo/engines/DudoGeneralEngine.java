package za.ac.sun.cs.ingenious.games.diceGames.dudo.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.message.MatchSettingsInitGameMessage;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.move.UnobservedMove;
import za.ac.sun.cs.ingenious.games.diceGames.actions.DiceRollAction;
import za.ac.sun.cs.ingenious.games.diceGames.actions.DudoClaimMove;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoGameState;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoLogic;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.network.DudoMatchResetMessage;

public abstract class DudoGeneralEngine extends Engine {

  protected DudoLogic logic;
  protected DudoGameState currentState;

  public DudoGeneralEngine(EngineToServerConnection toServer) {
    super(toServer);

    this.logic = new DudoLogic();

    Log.info("Playing with ID: " + this.playerID);
  }

  @Override
  public void setZobrist(ZobristHashing ZobristHashing) {}

  @Override
  public void receiveGameTerminatedMessage(GameTerminatedMessage a) {}

  @Override
  public void receiveMatchResetMessage(MatchResetMessage a) {
    DudoMatchResetMessage msg = (DudoMatchResetMessage) a;

    Log.info("Final scores:");
    double[] score = msg.getScores();
    for (int i = 0; i < score.length; i++) {
      Log.info("Player " + i + " : " + score[i]);
    }
  }

  @Override
  public void receiveInitGameMessage(InitGameMessage a) {
    MatchSetting ms = ((MatchSettingsInitGameMessage) a).getSettings();

    int s, d;
    s = ms.getSettingAsInt("num_sides", 6);
    d = ms.getSettingAsInt("num_dice", 2);

    this.currentState = new DudoGameState(ms.getNumPlayers(), s, d);
  }

  @Override
  public void receivePlayedMoveMessage(PlayedMoveMessage a) {
    if (a.getMove() instanceof UnobservedMove) {
      Log.info("Received unobserved move from player: " + a.getMove().getPlayerID());

    } else if (a.getMove() instanceof DiceRollAction) {
      Log.info("Received a dice roll: " + ((DiceRollAction) a.getMove()).getRoll());

    } else if (a.getMove() instanceof DiscreteAction) {
      Log.info(
          "Player "
              + (a.getMove().getPlayerID() + 1)
              + " claimed: "
              + currentState.getPrettyClaim(((DiscreteAction) a.getMove()).getActionNumber()));

      if (a.getMove() instanceof DudoClaimMove) {
        this.currentState.setNumPlayerDice(((DudoClaimMove) a.getMove()).getNumPlayerDice());
        currentState.resetRound();
        currentState.printPretty();
        return;
      }

    } else {
      Log.info("Received move: " + a.getMove());
    }

    logic.makeMove(currentState, a.getMove());

    currentState.printPretty();
  }
}
