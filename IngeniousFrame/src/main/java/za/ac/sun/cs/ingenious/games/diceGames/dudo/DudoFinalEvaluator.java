package za.ac.sun.cs.ingenious.games.diceGames.dudo;

import java.util.Arrays;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;

public class DudoFinalEvaluator implements GameFinalEvaluator<DudoGameState> {
  @Override
  public double[] getScore(DudoGameState forState) {
    double[] scores = new double[forState.getNumPlayers()];

    Arrays.fill(scores, -1.0);

    int winner = getWinner(forState);

    if (winner != -1) {
      scores[winner] = 1;
    }

    return scores;
  }

  private int getWinner(DudoGameState forState) {
    int[] numPlayerDice = forState.getNumPlayerDice();
    for (int i = 0; i < numPlayerDice.length; i++) {
      if (numPlayerDice[i] > 0) {
        return i;
      }
    }

    return -1;
  }
}
