package za.ac.sun.cs.ingenious.games.go.engines;

import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage.newBackpropagationAverage;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle.newExpansionSingle;
import static za.ac.sun.cs.ingenious.search.mcts.selection.SelectionUct.newSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom.newSimulationRandom;

import java.io.IOException;
import java.util.ArrayList;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsNodeSimple;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.games.go.GoBoard;
import za.ac.sun.cs.ingenious.games.go.GoEngine;
import za.ac.sun.cs.ingenious.games.go.GoMctsFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.MctsAlgorithm.MctsLeaf;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.selection.SelectionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;

public class GoMCTSLeafEngine extends GoEngine {
  protected int turnLength; // in milliseconds
  protected int threadCount;
  MctsLeaf<GoBoard, MctsNodeSimple<GoBoard>> mcts;

  /**
   * @param toServer An established connection to the GameServer
   */
  public GoMCTSLeafEngine(
      EngineToServerConnection toServer, String enhancementConfig, int threadCount, int turnLength)
      throws IOException, MissingSettingException, IncorrectSettingTypeException {
    super(toServer);
    this.threadCount = threadCount;
    this.turnLength = turnLength;

    MatchSetting m = new MatchSetting(enhancementConfig);

    SelectionThreadSafe<MctsNodeSimple<GoBoard>, MctsNodeSimple<GoBoard>> selection =
        newSelectionUct(logic);

    ExpansionThreadSafe<MctsNodeSimple<GoBoard>, MctsNodeSimple<GoBoard>> expansion =
        newExpansionSingle(logic);

    boolean recordMoves = false;
    if (selection.equals("Rave")) {
      recordMoves = true;
    }
    SimulationThreadSafe<GoBoard> simulation =
        newSimulationRandom(
            logic,
            new GoMctsFinalEvaluator(),
            new PerfectInformationActionSensor<GoBoard>(),
            recordMoves);

    BackpropagationThreadSafe<MctsNodeSimple<GoBoard>> backprop = newBackpropagationAverage();

    this.mcts =
        new MctsLeaf<>(selection, expansion, simulation, backprop, logic, this.threadCount, null);
  }

  @Override
  public void setZobrist(ZobristHashing zobristHashing) {}

  public String engineName() {
    return "GoMCTSLeafEngine";
  }

  @Override
  public PlayActionMessage receiveGenActionMessage(GenActionMessage message) {
    if (output) {
      currentState.printPretty();
    }
    // currentState.setSimulation();
    MctsNodeSimple<GoBoard> root =
        new MctsNodeSimple<GoBoard>(
            currentState, null, null, new ArrayList<MctsNodeSimple<GoBoard>>(), logic, playerID);
    Action action =
        mcts.doSearch(root, this.turnLength); // turnlength? Shouldn't this be from the clock
    // currentState.setSimulation();
    if (action == null) {
      return new PlayActionMessage(new IdleAction(playerID));
    }
    return new PlayActionMessage(action);
  }
}
