package za.ac.sun.cs.ingenious.games.go;

import com.esotericsoftware.minlog.Log;
import java.io.IOException;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;

public abstract class GoEngine extends Engine {

  protected GoBoard currentState;

  protected GoLogic logic;

  protected ZobristHashing zobrist;
  protected long currentBoardHash;

  protected boolean output = true;

  /**
   * @param toServer An established connection to the GameServer
   */
  public GoEngine(EngineToServerConnection toServer)
      throws IncorrectSettingTypeException, MissingSettingException, IOException {
    super(toServer);
    this.logic = new GoLogic();
  }

  @Override
  public void setZobrist(ZobristHashing zobristHashing) {
    zobrist = zobristHashing;
  }

  @Override
  public String engineName() {
    return "GoEngine";
  }

  @Override
  public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
    toServer.closeConnection();
    System.exit(0);
  }

  @Override
  public void receiveMatchResetMessage(MatchResetMessage a) {
    GoFinalEvaluator eval = new GoFinalEvaluator();
    Log.info("Game has terminated");
    Log.info("Final scores:");
    double[] score = eval.getScore(currentState);
    for (int i = 0; i < score.length; i++) {
      Log.info("Player " + (i + 1) + ": " + score[i] + "");
    }
    Log.info("Final state:");
    currentState.printPretty();
  }

  @Override
  public void receiveInitGameMessage(InitGameMessage a) {
    try {
      // get board size
      int boardSize = ((GoInitGameMessage) a).getBoardWidth();
      // create new board
      this.currentState = new GoBoard(boardSize, 0, 2);
      // create new zobrist hashing
      zobrist = new ZobristHashing(boardSize);
      currentBoardHash = zobrist.hashBoard(this.currentState);
    } catch (Exception ex) {
      Log.error("GoEngine error:Problem creating board - " + ex.getLocalizedMessage());
      throw new RuntimeException(ex);
    }
  }

  @Override
  public void receivePlayedMoveMessage(PlayedMoveMessage a) {
    logic.makeMove(currentState, a.getMove());
    if (a.getMove() instanceof XYAction) {
      // TODO: Add incremental zobrist hashing support. Issue #321.
      // Although the zobrist hashing is not incremental, we still use it here to
      // prevent game loops from occurring when the same board state is reached.
      long hash = zobrist.hashBoard(currentState);
      zobrist.addBoardToHashtable(hash, currentState);
    }
  }

  public void setCurrentBoardHash(long hash) {
    currentBoardHash = hash;
  }
}
