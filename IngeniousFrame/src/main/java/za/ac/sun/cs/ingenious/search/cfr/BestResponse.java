package za.ac.sun.cs.ingenious.search.cfr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.data.structures.dag.GameStateDAG;
import za.ac.sun.cs.ingenious.core.exception.dag.StateNotFoundException;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;

/**
 * Best response strategy is the optimal strategy player i can use against an opponent playing to a
 * known and fixed strategy. The best response value can be thought of as how much utility an
 * optimal opponent is able to receive if player i plays to a given strategy. This class was
 * developed for the purpose of calculating the exploitability of a strategy profile in a two player
 * game.
 */
public class BestResponse<S extends GameState> {

  private S root;
  private InformationSetDeterminizer<S> det;
  private GameLogic<S> logic;
  private GameFinalEvaluator<S> eval;
  private ActionSensor<S> sensor;

  public BestResponse(
      S root,
      InformationSetDeterminizer<S> det,
      GameLogic<S> logic,
      GameFinalEvaluator<S> eval,
      ActionSensor<S> sensor) {
    this.root = root;
    this.det = det;
    this.logic = logic;
    this.eval = eval;
    this.sensor = sensor;
  }

  private double setValues(
      Map<S, Map<Action, Double>> strategy,
      S h,
      GameStateDAG<S> infoSetDAG,
      Map<S, Double> valuesMap,
      int i,
      double pi)
      throws StateNotFoundException {
    int activePlayerID = Helper.getNextPlayerID(h, logic);
    S I = det.observeState(h, activePlayerID);

    double out = 0.0;
    if (logic.isTerminal(h)) {
      out = eval.getScore(h)[i] * pi;
    } else if (activePlayerID == h.getNumPlayers()) { // Chance node
      Map<Action, Double> actions = logic.generateStochasticActions(h);

      for (Action a : actions.keySet()) {
        @SuppressWarnings("unchecked")
        S ha = (S) h.deepCopy();
        logic.makeMove(ha, sensor.fromPointOfView(a, ha, -1));

        S Ia = det.observeState(ha, i);
        checkNode(infoSetDAG, det.observeState(h, i), Ia, valuesMap);

        double next_pi = actions.get(a) * pi;
        double v = setValues(strategy, ha, infoSetDAG, valuesMap, i, next_pi);
        valuesMap.put(Ia, valuesMap.get(Ia) + v);

        out += v;
      }
    } else if (activePlayerID != i) { // Opponent node
      List<Action> actions = logic.generateActions(I, activePlayerID);

      for (Action a : actions) {
        @SuppressWarnings("unchecked")
        S ha = (S) h.deepCopy();
        logic.makeMove(ha, sensor.fromPointOfView(a, ha, -1));

        S Ia = det.observeState(ha, i); // Nodes of info set tree are from perspective of i
        checkNode(infoSetDAG, det.observeState(h, i), Ia, valuesMap);

        double next_pi = strategy.get(I).get(a) * pi;
        double v = setValues(strategy, ha, infoSetDAG, valuesMap, i, next_pi);
        valuesMap.put(Ia, valuesMap.get(Ia) + v);

        out += v;
      }
    } else { // Best response node
      List<Action> actions = logic.generateActions(I, activePlayerID);

      for (Action a : actions) {
        @SuppressWarnings("unchecked")
        S ha = (S) h.deepCopy();
        logic.makeMove(ha, sensor.fromPointOfView(a, ha, -1));

        S Ia = det.observeState(ha, i);
        checkNode(infoSetDAG, I, Ia, valuesMap);

        double v = setValues(strategy, ha, infoSetDAG, valuesMap, i, pi);
        valuesMap.put(Ia, valuesMap.get(Ia) + v);

        out += v;
      }
    }

    return out;
  }

  private double getValue(GameStateDAG<S> infoSetDAG, S root, Map<S, Double> valuesMap, int i) {
    double out = 0.0;
    if (infoSetDAG.getChildren(root).size() == 0) {

      out = valuesMap.get(root);
    } else if (infoSetDAG.getActivePlayers(root).get(0) == i) {
      out = Double.NEGATIVE_INFINITY;
      for (S child : infoSetDAG.getChildren(root)) {
        double v = getValue(infoSetDAG, child, valuesMap, i);

        if (v > out) {
          out = v;
        }
      }
    } else {

      for (S child : infoSetDAG.getChildren(root)) {
        out += getValue(infoSetDAG, child, valuesMap, i);
      }
    }

    return out;
  }

  private void checkNode(GameStateDAG<S> dag, S parent, S child, Map<S, Double> valuesMap)
      throws StateNotFoundException {
    if (!dag.contains(child)) {
      dag.addNode(child, new ArrayList<>(logic.getCurrentPlayersToAct(child)));
      dag.addEdge(parent, child, null, 0); // Action and probability aren't used in this context
      valuesMap.put(child, 0.0);
    }
  }

  public double getBestResponseValue(Map<S, Map<Action, Double>> strategy, int i)
      throws StateNotFoundException {

    GameStateDAG<S> infoSetDAG = new GameStateDAG<>();
    infoSetDAG.addNode(root, new ArrayList<>(logic.getCurrentPlayersToAct(root)));

    Map<S, Double> valuesMap = new HashMap<>();

    setValues(strategy, root, infoSetDAG, valuesMap, i, 1);

    return getValue(infoSetDAG, root, valuesMap, i);
  }
}
