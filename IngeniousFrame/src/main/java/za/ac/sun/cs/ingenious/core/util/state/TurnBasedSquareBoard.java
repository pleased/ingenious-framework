package za.ac.sun.cs.ingenious.core.util.state;

/**
 * Represents the state of a game that is played on a square board with players taking turns. Useful
 * for games like TicTacToe.
 *
 * @author Michael Krause
 */
public class TurnBasedSquareBoard extends TurnBasedRectangleBoard {

  /** Initializes an empty board. */
  public TurnBasedSquareBoard(int boardWidth, int firstPlayer, int numPlayers) {
    super(boardWidth, boardWidth, firstPlayer, numPlayers);
  }

  /** Copy constructor. Duplicates the given board. */
  public TurnBasedSquareBoard(TurnBasedSquareBoard toCopy) {
    super(toCopy);
  }

  @Override
  public TurnBasedSquareBoard deepCopy() {
    return new TurnBasedSquareBoard(this);
  }
}
