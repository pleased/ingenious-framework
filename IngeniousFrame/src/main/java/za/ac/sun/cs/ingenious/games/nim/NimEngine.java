package za.ac.sun.cs.ingenious.games.nim;

import com.esotericsoftware.minlog.Log;
import java.io.IOException;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;

/** This is an abstract superclass for all the different engines we implement for Nim. */
public abstract class NimEngine extends Engine {

  protected ZobristHashing zobrist;
  protected long currentBoardHash;

  protected boolean output = true;

  protected NimBoard currentState;
  protected NimLogic logic;

  /**
   * The required constructor for the TCP back-end to work
   *
   * @param toServer Object representing the connection to the server. We just need to pass this on,
   *     the rest is all handled at the level of the superclass.
   */
  public NimEngine(EngineToServerConnection toServer)
      throws IncorrectSettingTypeException, MissingSettingException, IOException {
    super(toServer);
    this.logic = new NimLogic();
  }

  @Override
  public void setZobrist(ZobristHashing zobristHashing) {
    zobrist = zobristHashing;
  }

  /** Returns the name of the engine. */
  @Override
  public String engineName() {
    return "NimEngine";
  }

  /** Terminates the game and its connection to the server. */
  @Override
  public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
    toServer.closeConnection();
    System.exit(0);
  }

  /** Prints the final gamestate. */
  @Override
  public void receiveMatchResetMessage(MatchResetMessage a) {
    NimMctsFinalEvaluator eval = new NimMctsFinalEvaluator();
    Log.info("Game has terminated!");
    Log.info("Final scores:");
    double[] score = eval.getScore(currentState);
    for (int i = 0; i < score.length; i++) {
      Log.info("Player " + i + ": " + score[i]);
    }
    Log.info("Final state:");
    currentState.printPretty();
  }

  /** Resets the game state to the initial state. */
  @Override
  public void receiveInitGameMessage(InitGameMessage a) {
    try {
      // get board size
      int rows = ((NimInitGameMessage) a).getNumRows();
      // create new board
      this.currentState = new NimBoard(rows, 0, 2);
      // create new zobrist hashing
      zobrist = new ZobristHashing(currentState.getBoardWidth(), currentState.getBoardHeight());
      currentBoardHash = zobrist.hashBoard(currentState);
    } catch (Exception ex) {
      Log.error("NimEngine error:Problem creating board - " + ex.getLocalizedMessage());
      throw new RuntimeException(ex);
    }
  }

  /** Applies the given move to the game state. */
  @Override
  public void receivePlayedMoveMessage(PlayedMoveMessage a) {
    logic.makeMove(currentState, a.getMove());
    if (a.getMove() instanceof XYAction) {
      // TODO: Add incremental zobrist hashing support. Issue #321.
      // long hash = zobrist.hashBoard(currentState);
      zobrist.addBoardToHashtable(currentState.hashCode(), currentState);
    }
  }

  public void setCurrentBoardHash(long hash) {
    currentBoardHash = hash;
  }
}
