package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;
import java.util.function.Supplier;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.search.TreeNode;
import za.ac.sun.cs.ingenious.core.util.search.TreeNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;

public class MctsNodeComposition<S extends GameState, C, P>
    implements MctsNodeCompositionInterface<S, C, P> {

  private TreeNode<S, C, P> treeNode;
  private double visitCount;
  protected List<Action> unexploredActions;
  private final List<Action> allPossibleActions;
  private Action prevAction;
  private Function<GameLogic<S>, C> unexploredChildMethod;
  private Function<C, Double> childValueMethod;
  private Supplier<C> getAsChildTypeMethod;
  private final int playerID;

  private Hashtable<String, MctsNodeExtensionParallelInterface> enhancementClasses;

  public MctsNodeComposition(
      S state,
      Action prevAction,
      P parent,
      List<C> children,
      GameLogic<S> logic,
      Function<GameLogic<S>, C> unexploredChildMethod,
      Function<C, Double> childValueMethod,
      Supplier<C> getAsChildTypeMethod,
      boolean makeListsParallel,
      Hashtable<String, MctsNodeExtensionParallelInterface> enhancementClasses,
      int playerID) {

    this.playerID = playerID;

    treeNode =
        (makeListsParallel)
            ? new TreeNodeComposition<S, C, P>(state, parent, new CopyOnWriteArrayList<C>(children))
            : new TreeNodeComposition<S, C, P>(state, parent, children);
    this.prevAction = prevAction;
    this.visitCount = 0;

    allPossibleActions = Collections.unmodifiableList(logic.generateActions(state, playerID));
    unexploredActions =
        (makeListsParallel)
            ? new CopyOnWriteArrayList(allPossibleActions)
            : new ArrayList<>(allPossibleActions);

    this.unexploredChildMethod = unexploredChildMethod;
    this.childValueMethod = childValueMethod;
    this.getAsChildTypeMethod = getAsChildTypeMethod;
    this.enhancementClasses = enhancementClasses;
  }

  // TODO: change?
  public String toString() {
    getState(false).printPretty();
    return ("TreeNodeNode:\n\tState = "
        + getState(false).toString()
        + "\n\tvalue = "
        + getValue()
        + "\n\tParent (state) = "
        + this.getParent()
        + "\n\tChildren = "
        + getChildren()
        + "\n\tvisitCount = "
        + getVisitCount()
        + "\n\tUnexploredActions = "
        + unexploredActions);
  }

  public Hashtable<String, MctsNodeExtensionParallelInterface> getEnhancementClasses() {
    return enhancementClasses;
  }

  /**
   * @return the state relating to the current node.
   */
  public S getState(boolean getDefensiveCopy) {
    return treeNode.getState(getDefensiveCopy);
  }

  /**
   * @return the number of winning playouts played through the current node.
   */
  public double getValue() {
    return treeNode.getValue();
  }

  /**
   * Add the inputted value to the win count for the current node.
   *
   * @param add
   */
  public void addValue(double add) {
    treeNode.addValue(add);
  }

  /**
   * @return the player relating to this current node, i.e. the player that must play next from the
   *     current game state.
   */
  public int getPlayerID() {
    return playerID;
  }

  /**
   * @return the current node's parent.
   */
  public P getParent() {
    return treeNode.getParent();
  }

  /**
   * Set the current node's parent to the inputted node.
   *
   * @param parent
   */
  public void setParent(P parent) {
    treeNode.setParent(parent);
  }

  /**
   * @return the current node's set of children
   */
  public List<C> getChildren() {
    return treeNode.getChildren();
  }

  /**
   * Set the current node's list of children to the inputted set of children.
   *
   * @param children
   */
  public void setChildren(List<C> children) {
    treeNode.setChildren(children);
  }

  /**
   * Add the child to the current node's set of children
   *
   * @param child
   */
  public void addChild(C child) {
    treeNode.addChild(child);
  }

  /**
   * Add the list of children to the current node's set of children
   *
   * @param newChildren
   */
  public void addChildren(List<C> newChildren) {
    treeNode.addChildren(newChildren);
  }

  /**
   * return the visit count for this node
   *
   * @return
   */
  public double getVisitCount() {
    return this.visitCount;
  }

  /** increment the visit count of the current node by 1 */
  public void incVisitCount() {
    visitCount++;
  }

  /** decrement the visit count of the current node by 1 */
  public void decVisitCount() {
    visitCount--;
  }

  /**
   * @param child
   * @return the number of times the child relating to the current node has had a winning playout.
   */
  public double getChildValue(C child) {
    return this.childValueMethod.apply(child);
  }

  /**
   * @return the array of unexplored actions relating to children that can be expanded from this
   *     current node
   */
  public List<Action> getUnexploredActions() {
    return Collections.unmodifiableList(unexploredActions);
  }

  public List<Action> getAllPossibleActions() {
    return allPossibleActions;
  }

  /**
   * @return true if there are no more children to expland, false otherwise
   */
  public boolean unexploredActionsEmpty() {
    return unexploredActions.isEmpty();
  }

  /**
   * @return the action most recently made on the current node's board state
   */
  public Action getPrevAction() {
    return this.prevAction;
  }

  /**
   * @return the current node case as a child type object
   */
  public C getSelfAsChildType() {
    return this.getAsChildTypeMethod.get();
  }

  /**
   * Removes an unexplored action and returns the resulting child node. Note that this child node
   * will have to be added to the tree manually.
   *
   * @param logic
   * @return
   */
  public C popUnexploredChild(GameLogic<S> logic) {
    return this.unexploredChildMethod.apply(logic);
  }

  /**
   * Removes an unexplored action and returns the resulting child node. Note that this child node
   * will have to be added to the tree manually.
   *
   * @param logic
   * @param index the index into the array of unexplored actions for which the child node must be
   *     created.
   * @return
   */
  public C popUnexploredChild(GameLogic<S> logic, int index) {
    return this.unexploredChildMethod.apply(logic);
  }
}
