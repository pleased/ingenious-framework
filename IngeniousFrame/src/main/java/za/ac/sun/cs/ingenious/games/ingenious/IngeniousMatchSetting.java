package za.ac.sun.cs.ingenious.games.ingenious;

import com.esotericsoftware.minlog.Log;
import java.util.List;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;

public class IngeniousMatchSetting {

  private final MatchSetting settings;

  public IngeniousMatchSetting(MatchSetting settings) {
    this.settings = settings;
  }

  public String getLobbyName() {
    return settings.getLobbyName();
  }

  public int getNumPlayers() {
    return settings.getNumPlayers();
  }

  public int getNumColours() {
    try {
      return settings.getSettingAsInt("numColours");
    } catch (MissingSettingException | IncorrectSettingTypeException e) {
      Log.error(e.getMessage());
      return 0;
    }
  }

  public boolean getPerfectInformation() {
    try {
      return settings.getSettingAsBoolean("perfectInformation");
    } catch (MissingSettingException | IncorrectSettingTypeException e) {
      Log.error(e.getMessage());
      return false;
    }
  }

  public boolean getAlternating() {
    try {
      return settings.getSettingAsBoolean("alternatingPlay");
    } catch (MissingSettingException | IncorrectSettingTypeException e) {
      Log.error(e.getMessage());
      return false;
    }
  }

  public Variant[] getVariants() {
    List<Object> variants;
    try {
      variants = settings.getSettingAsList("variants");
      return variants.toArray(new Variant[0]); // need conversion
    } catch (MissingSettingException | IncorrectSettingTypeException e) {
      Log.error(e.getMessage());
      return new Variant[0];
    }
  }

  public int getBoardSize() {
    try {
      return settings.getSettingAsInt("boardSize");
    } catch (MissingSettingException | IncorrectSettingTypeException e) {
      Log.error(e.getMessage());
      return 0;
    }
  }
}
