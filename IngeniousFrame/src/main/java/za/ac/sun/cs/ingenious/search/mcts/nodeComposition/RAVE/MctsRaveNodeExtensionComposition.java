package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

public class MctsRaveNodeExtensionComposition<S extends GameState, C, P>
    implements MctsRaveNodeExtensionCompositionInterface {

  public static String id = "Rave";

  private List<Action> possibleActions;
  private ConcurrentHashMap<Action, Integer> raveActionVisitCount;
  private ConcurrentHashMap<Action, Double> raveActionWinCount;
  private ConcurrentHashMap<Action, MctsNodeTreeParallelInterface<?, ?, ?>> actionToNodeMapping;

  private int vValue;

  /** empty constructor */
  public MctsRaveNodeExtensionComposition(int vValue) {
    raveActionVisitCount = new ConcurrentHashMap<>();
    raveActionWinCount = new ConcurrentHashMap<>();
    actionToNodeMapping = new ConcurrentHashMap<>();
    this.vValue = vValue;
  }

  /**
   * set up to initialise the relevant lists with all possible actions relating to children. This is
   * used because reward values of the children are stored at the parent node.
   *
   * @param node
   */
  public void setUp(MctsNodeComposition node) {
    this.possibleActions = node.getAllPossibleActions();
    // Add each possible action from this game state to a hashtable with entries
    // corresponding to the Rave wins/game plays for the TreeEngine under this node
    for (Action actionToAdd : possibleActions) {
      raveActionVisitCount.put(actionToAdd, 0);
      raveActionWinCount.put(actionToAdd, 0.0);
    }
  }

  /**
   * @return the id of the next player to play for the node object for which this extension relates
   */
  public String getID() {
    return id;
  }

  /**
   * stores the node relating to each action for the moves on path used during backpropagation to
   * update rave fields
   *
   * @param action
   * @param node
   */
  public void storeActionToNodeMapping(Action action, MctsNodeTreeParallelInterface<?, ?, ?> node) {
    actionToNodeMapping.put(action, node);
  }

  /**
   * Update the action to node mapping for use in the selection phase of the rave implementation
   *
   * @param child
   */
  public void addChild(C child) {
    MctsNodeTreeParallel childToAdd = (MctsNodeTreeParallel) child;
    storeActionToNodeMapping(childToAdd.getPrevAction(), childToAdd);
  }

  /**
   * Update the action to node mapping for use in the selection phase of the rave implementation
   *
   * @param newChildren
   */
  public void addChildren(List<C> newChildren) {
    for (C child : newChildren) {
      MctsNodeTreeParallel childToAdd = (MctsNodeTreeParallel) child;
      storeActionToNodeMapping(childToAdd.getPrevAction(), childToAdd);
    }
  }

  /**
   * @return action to node mapping for use in the selection phase of the rave implementation
   */
  public ConcurrentHashMap<Action, MctsNodeTreeParallelInterface<?, ?, ?>>
      getActionToNodeMapping() {
    return actionToNodeMapping;
  }

  /**
   * @param action
   * @return the number of winning playouts made through the child node relating to the action given
   *     as a parameter
   */
  public double getRaveWins(Action action) {
    return raveActionWinCount.get(action);
  }

  /**
   * @param action
   * @return the number of playouts made through the child node relating to the action given as a
   *     parameter
   */
  public int getRaveVisits(Action action) {
    return raveActionVisitCount.get(action);
  }

  /**
   * updates the rave win and visit count fields for children of the current nodes
   *
   * @param win
   * @param movesOnPath
   */
  public void updateRaveValues(double score, List<Action> movesOnPath, int startIndex) {
    for (Action action : possibleActions) {
      for (int i = startIndex; i < movesOnPath.size(); i++) {
        if (movesOnPath.get(i).equals(action)) {
          // increment rave visits
          raveActionVisitCount.compute(action, (k, v) -> v == null ? 1 : v + 1);
          // increment rave win count (not necessary if it was a loss)
          if (score != 0.0) {
            raveActionWinCount.compute(action, (k, v) -> v == null ? score : v + score);
          }
        }
      }
    }
  }
}
