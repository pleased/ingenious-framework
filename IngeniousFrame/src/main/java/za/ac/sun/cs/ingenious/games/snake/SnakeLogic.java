package za.ac.sun.cs.ingenious.games.snake;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;
import za.ac.sun.cs.ingenious.games.snake.moves.SnakeSyncAction;

/**
 * Game logic for Snake.
 *
 * @author Steffen Jacobs
 */
public class SnakeLogic implements GameLogic<SnakeState> {
  public static final int BODY_SIZE = 20;
  public static final int STEP_SIZE = BODY_SIZE;
  public static final int FOOD_SIZE = 20;

  @Override
  public boolean validMove(SnakeState fromState, Move move) {
    if (move instanceof IdleAction) {
      // An idle action is always allowed.
      return true;
    } else if (move instanceof CompassDirectionAction) {
      CompassDirectionAction action = (CompassDirectionAction) move;

      // Only cardinal directions are allowed.
      if (!action.getDir().isCardinal()) return false;

      // Only allow directions that are perpendicular to the current direction.
      return !action.getDir().equals(fromState.getCurrentDirection())
          && !action.getDir().equals(fromState.getCurrentDirection().getOppositeDirection());
    } else if (move instanceof SnakeSyncAction) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public boolean makeMove(SnakeState fromState, Move move) {
    if (!validMove(fromState, move)) return false;

    boolean moveApplied = false;

    if (move instanceof CompassDirectionAction) {
      CompassDirectionAction action = (CompassDirectionAction) move;
      fromState.setCurrentDirection(action.getDir());

      moveApplied = true;
    } else if (move instanceof IdleAction) {
      moveApplied = true;
    } else if (move instanceof SnakeSyncAction) {
      SnakeSyncAction snakeSyncAction = (SnakeSyncAction) move;
      fromState.setFood(snakeSyncAction.getFood());

      moveApplied = false;
    }

    if (moveApplied) {
      // Let the game update a single step.
      fromState.tick();

      /*
       * Check if food was eaten to handle score update / food spawning. It is important that this occurs after
       * tick is invoked.
       */
      eatCheck(fromState);
    }

    return moveApplied;
  }

  public void eatCheck(SnakeState fromState) {
    LinkedList<Coord> body = fromState.getBody();

    if (body.getFirst().equals(fromState.getFood())) {
      fromState.incrementFoodEaten();

      if (!isTerminal(fromState)) {
        fromState.spawnFood();
      }
    }
  }

  @Override
  public void undoMove(SnakeState fromState, Move move) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<Action> generateActions(SnakeState fromState, int forPlayerID) {
    List<Action> validActions = new ArrayList<>();
    validActions.add(new IdleAction(forPlayerID));

    Set<CompassDirection> cardinalDirections = CompassDirection.cardinalDirections();

    for (CompassDirection direction : cardinalDirections) {
      CompassDirectionAction action = new CompassDirectionAction(forPlayerID, direction);

      if (validMove(fromState, action)) validActions.add(action);
    }

    return validActions;
  }

  @Override
  public boolean isTerminal(SnakeState state) {
    LinkedList<Coord> body = state.getBody();
    Coord headCoord = body.getFirst();

    // Vertical border hit detection
    if (headCoord.getX() < 0 || headCoord.getX() >= SnakeState.WIDTH) {
      return true;
    }

    // Horizontal border hit detection
    if (headCoord.getY() < 0 || headCoord.getY() >= SnakeState.HEIGHT) {
      return true;
    }

    // Self hit detection
    Iterator<Coord> iterator = body.iterator();
    iterator.next();

    while (iterator.hasNext()) {
      Coord segmentCoord = iterator.next();

      if (segmentCoord.equals(headCoord)) return true;
    }

    return state.hasMaximumBodyLength();
  }

  @Override
  public Set<Integer> getCurrentPlayersToAct(SnakeState fromState) {
    Set<Integer> playersToAct = new HashSet<>();
    playersToAct.add(SnakeState.DEFAULT_FIRST_PLAYER);

    return playersToAct;
  }
}
