package za.ac.sun.cs.ingenious.games.ingenious.engines;

import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousEngine;

public interface IMoveController {
  public IngeniousAction generateMove(IngeniousEngine engine, IngeniousEvaluator evaluator);
}
