package za.ac.sun.cs.ingenious.core.util.move;

import za.ac.sun.cs.ingenious.core.Action;

/**
 * An action that consists of x and y coordinates and the player that acted.
 *
 * @author Michael Krause
 */
public class XYAction implements Action {

  private static final long serialVersionUID = 1L;

  private final int x, y, player;
  private int hash;

  public XYAction(int x, int y, int player) {
    this.x = x;
    this.y = y;
    this.player = player;
    this.hash = -1;
  }

  public XYAction(int x, int y, int player, int hash) {
    this.x = x;
    this.y = y;
    this.player = player;
    this.hash = hash;
  }

  @Override
  public int getPlayerID() {
    return player;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  @Override
  public String toString() {
    return "(" + x + "|" + y + ") --> " + player;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj != null && this.getClass() == obj.getClass())) {
      return false;
    } else {
      return (this.hashCode() == obj.hashCode());
    }
  }

  @Override
  public int hashCode() {
    if (this.hash == -1) {
      // allocate 13 bits for x, 13 bits for y and 5 bits for the player (allowing up to 32 players)
      // the first bit is reserved so that it doesn't clash with hashes from the "IdleAction" class
      this.hash = (x << 18) ^ (y << 5) ^ player;
    }
    return this.hash;
  }

  @Override
  public XYAction deepCopy() {
    return new XYAction(x, y, player, hash);
  }
}
