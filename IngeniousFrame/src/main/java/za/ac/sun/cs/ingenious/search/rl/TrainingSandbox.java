package za.ac.sun.cs.ingenious.search.rl;

import com.esotericsoftware.minlog.Log;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.Random;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.exception.dag.CycleException;
import za.ac.sun.cs.ingenious.core.exception.dag.StateNotFoundException;
import za.ac.sun.cs.ingenious.core.network.game.DummyEngineToServerConnection;
import za.ac.sun.cs.ingenious.core.util.misc.GameSpeed;
import za.ac.sun.cs.ingenious.core.util.misc.LocalPlayer;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoActionSensor;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoFinalEvaluator;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoGameState;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoImperfectInfoDeterminizer;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoLogic;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.engines.DudoCFREngine;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldReferee;
import za.ac.sun.cs.ingenious.games.gridWorld.engines.*;
import za.ac.sun.cs.ingenious.games.mnk.MNKReferee;
import za.ac.sun.cs.ingenious.games.mnk.engines.MNKExternalDQNEngine;
import za.ac.sun.cs.ingenious.games.mnk.engines.MNKRandomEngine;
import za.ac.sun.cs.ingenious.games.mnk.engines.MNKTabularQLearningEngine;
import za.ac.sun.cs.ingenious.games.rps.RPSGameState;
import za.ac.sun.cs.ingenious.games.rps.RPSLogic;
import za.ac.sun.cs.ingenious.games.rps.engines.RPSCFREngine;
import za.ac.sun.cs.ingenious.games.snake.SnakeReferee;
import za.ac.sun.cs.ingenious.games.snake.engines.SnakeExternalDQNEngine;
import za.ac.sun.cs.ingenious.search.cfr.CFR;
import za.ac.sun.cs.ingenious.search.cfr.FSICFR;

/**
 * Class with a main method used to train reinforcement learning agents. While using the
 * client-server flow does work for RL agents, it is very slow. This provides a place to train
 * agents a bit more quickly.
 *
 * <p>The structure of this class is very informal, and can be made much more modular in the future,
 * if needed.
 *
 * @author Steffen Jacobs
 */
public class TrainingSandbox {
  public static void main(String[] args)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {
    if (args.length < 2) {
      System.out.println("Match count and scenario must be provided as args.");
      return;
    }

    String scenario = args[0];
    int matchCount = Integer.parseInt(args[1]);
    String optionalArg = null;

    if (args.length > 2) {
      StringBuilder argString = new StringBuilder();

      for (int i = 2; i < args.length; i++) {
        argString.append(args[i]);
        if (i + 1 < args.length) {
          argString.append(",");
        }
      }

      optionalArg = argString.toString();
    }

    System.out.println("Running `" + scenario + "` for " + matchCount + " matches.");

    switch (scenario) {
      case "RPSCFR":
        RPSCFR(matchCount);
        break;
      case "DudoCFR":
        DudoCFR(matchCount, optionalArg);
        break;
      case "RPSFSICFR":
        RPSFSICFR(matchCount);
        break;
      case "DudoFSICFR":
        DudoFSICFR(matchCount, optionalArg);
        break;
      case "snakeDQN":
        snakeDQN(matchCount);
        break;
      case "gridWorldExternalTable":
        gridWorldExternalTable(matchCount, optionalArg);
        break;
      case "gridWorldExternalFunctionApproximation":
        gridWorldExternalFunctionApproximation(matchCount, optionalArg);
        break;
      case "gridWorldDQN":
        gridWorldDQN(matchCount, optionalArg);
        break;
      case "gridWorldPolicyIteration":
        gridWorldPolicyIteration(matchCount, optionalArg);
        break;
      case "gridWorldValueIteration":
        gridWorldValueIteration(matchCount, optionalArg);
        break;
      case "gridWorldMonteCarloOnPolicy":
        gridWorldMonteCarloOnPolicy(matchCount, optionalArg);
        break;
      case "gridWorldMonteCarloOffPolicy":
        gridWorldMonteCarloOffPolicy(matchCount, optionalArg);
        break;
      case "gridWorldSarsa":
        gridWorldSarsa(matchCount, optionalArg);
        break;
      case "gridWorldTabularQLearning":
        gridWorldTabularQLearning(matchCount, optionalArg);
        break;
      case "gridWorldLFAQLearning":
        gridWorldLFAQLearning(matchCount, optionalArg);
        break;
      case "mnkTabularQLearning":
        mnkTabularQLearning(matchCount);
        break;
      case "mnkDQN":
        mnkDQN(matchCount);
        break;
      case "mnkDQNSelfPlay":
        mnkDQNSelfPlay(matchCount);
        break;
      default:
        System.out.println("Specified scenario not valid.");
        break;
    }

    System.exit(0);
  }

  private static void RPSFSICFR(int iterations) {
    RPSLogic logic = new RPSLogic();
    RPSGameState state = new RPSGameState(2);

    FSICFR<RPSGameState> fsicfr = null;

    try {
      fsicfr = new FSICFR<RPSGameState>(state, logic, logic, logic, logic);

      fsicfr.solve(iterations);
    } catch (CycleException | StateNotFoundException ex) {
      Log.error("RPSFSICFR", "Failed to train RPS FSICFR");
      ex.printStackTrace();
      System.exit(0);
    }

    Map<RPSGameState, Map<Action, Double>> aveStrategies = fsicfr.getNormalizedStrategy();

    System.out.println(RPSCFREngine.getPrettyStrategies(aveStrategies, logic, 1));
  }

  private static void DudoFSICFR(int iterations, String gameSettings) {

    int numSides = 6;
    int numDice = 1;
    int memory = 10;

    boolean exploit = false;
    String exploitFilePath = null;

    boolean trainingTimes = false;
    String timeFilePath = null;

    boolean loadStrategy = false;
    String loadStrategyFile = null;

    boolean saveStrategy = false;
    String saveStrategyFile = null;

    boolean printStrategiesP1 = false;
    boolean printStrategiesP2 = false;

    if (gameSettings != null) {

      int setting_i = 0;
      String[] strSettings = gameSettings.split(",");
      int[] intSettings = new int[3];
      for (int i = 0; i < 3; i++) {
        intSettings[i] = Integer.parseInt(strSettings[i]);
      }
      setting_i += 3;

      numSides = intSettings[0];
      numDice = intSettings[1];
      memory = intSettings[2];

      exploit = strSettings[setting_i++].equals("true");
      trainingTimes = strSettings[setting_i++].equals("true");
      loadStrategy = strSettings[setting_i++].equals("true");
      saveStrategy = strSettings[setting_i++].equals("true");

      if (exploit) {
        exploitFilePath = strSettings[setting_i++];
      }

      if (trainingTimes) {
        timeFilePath = strSettings[setting_i++];
      }

      if (loadStrategy) {
        loadStrategyFile = strSettings[setting_i++];
      }

      if (saveStrategy) {
        saveStrategyFile = strSettings[setting_i++];
      }

      printStrategiesP1 = strSettings[setting_i++].equals("true");
      printStrategiesP2 = strSettings[setting_i++].equals("true");
    }

    DudoLogic logic = new DudoLogic();
    DudoGameState state = new DudoGameState(2, numSides, numDice);
    DudoImperfectInfoDeterminizer det = new DudoImperfectInfoDeterminizer(memory, logic);
    DudoFinalEvaluator eval = new DudoFinalEvaluator();
    DudoActionSensor sensor = new DudoActionSensor();

    try {
      FSICFR<DudoGameState> fsicfr = null;

      Map<DudoGameState, Map<Action, Double>> loadedStrategy = null;

      if (loadStrategy) {
        try {
          FileInputStream cumulativeStrategyStream = new FileInputStream(loadStrategyFile);
          ObjectInputStream in = new ObjectInputStream(cumulativeStrategyStream);
          loadedStrategy = (Map<DudoGameState, Map<Action, Double>>) in.readObject();
          in.close();
          cumulativeStrategyStream.close();
          Log.info(
              "DudoFSICFR Sandbox", "Loaded cumulative strategy profile from: " + loadStrategyFile);

          // fsicfr = new FSICFR<>(
          //
          // )
        } catch (IOException | ClassNotFoundException e) {
          Log.error("DudoFSICFR Sandbox", "Failed to load FSICFR tables");
          e.printStackTrace();
          System.exit(0);
        }
      } else {
        fsicfr = new FSICFR<>(state, det, logic, eval, sensor);
      }

      fsicfr.setCollectExploitability(exploit);
      fsicfr.setCollectTrainingTimes(trainingTimes);

      Log.info(
          "DudoFSICFR Sandbox",
          "Training Dudo with FSICFR: {numSides: "
              + numSides
              + ", numDice: "
              + numDice
              + ", memory: "
              + memory
              + "}");

      fsicfr.solve(iterations);

      if (saveStrategy) {
        try {
          FileOutputStream fileOut = new FileOutputStream(saveStrategyFile);
          ObjectOutputStream out = new ObjectOutputStream(fileOut);
          out.writeObject(fsicfr.getCumulativeStrategyProfile());
          out.close();
          fileOut.close();
          Log.info("DudoFSICFR Sandbox", "Serialized data saved to " + saveStrategyFile);
        } catch (IOException e) {
          Log.error("DudoFSICFR Sandbox", "Unable to save trained strategy");
          e.printStackTrace();
          System.exit(0);
        }
      }

      if (printStrategiesP1 || printStrategiesP2) {
        Map<DudoGameState, Map<Action, Double>> aveStrategies = fsicfr.getNormalizedStrategy();

        if (printStrategiesP1) {
          System.out.println(DudoCFREngine.getPrettyStrategies(aveStrategies, logic, state, 0));
        }

        if (printStrategiesP2) {
          System.out.println(DudoCFREngine.getPrettyStrategies(aveStrategies, logic, state, 1));
        }
      }

      if (exploit) {
        saveTrainingData(exploitFilePath, fsicfr.getExploitability());
      }

      if (trainingTimes) {
        saveTrainingData(timeFilePath, fsicfr.getTrainingTimes());
      }
    } catch (StateNotFoundException | CycleException e) {
      Log.info("DudoFSICFR Sandbox", "Failed to create FSICFR object");
      e.printStackTrace();
      System.exit(0);
    }
  }

  private static void saveTrainingData(String filePath, double[] data) {
    StringBuilder str = new StringBuilder();
    for (int t = 0; t < data.length; t++) {
      str.append(t + 1).append("\t").append(data[t]).append("\n");
    }
    try {
      FileWriter fileWriter = new FileWriter(filePath);
      fileWriter.write(str.toString());

      fileWriter.close();
    } catch (IOException e) {
      Log.error("CFR Sandbox", "Unable to save to:\t" + filePath);
    }
  }

  private static void RPSCFR(int iterations) {
    RPSLogic logic = new RPSLogic();
    RPSGameState state = new RPSGameState(2);

    Map<RPSGameState, Map<Action, Double>> loadedStrategy =
        RPSCFREngine.getRandomStrategy(logic, state, new Random());

    CFR<RPSGameState> cfr = new CFR<>(state, logic, logic, logic, logic, loadedStrategy);

    try {
      cfr.solve(iterations);
    } catch (StateNotFoundException ex) {
      Log.error("RPSCFR Sandbox", "Failed to train CFR\n" + ex);
    }

    Map<RPSGameState, Map<Action, Double>> aveStrategies = cfr.getNormalizedStrategy();

    Log.info(RPSCFREngine.getPrettyStrategies(aveStrategies, logic, 0));
    Log.info(RPSCFREngine.getPrettyStrategies(aveStrategies, logic, 1));

    StringBuilder str = new StringBuilder();
    double[] exploitability = cfr.getExploitability();
    for (int t = 0; t < exploitability.length; t++) {
      str.append(t + 1).append("\t").append(exploitability[t]).append("\n");
    }
    // System.out.println(str);

    String exploitabilitiesFilePath = null;
    try {
      exploitabilitiesFilePath = "exploitabilities.txt";
      FileWriter exploitFile = new FileWriter(exploitabilitiesFilePath);
      exploitFile.write(str.toString());

      exploitFile.close();
    } catch (IOException e) {
      Log.error("CFR Sandbox", "Unable to save exploitabilities:\t" + exploitabilitiesFilePath);
    }
  }

  private static void DudoCFR(int iterations, String gameSettings) {

    int numSides = 6;
    int numDice = 1;

    boolean exploit = false;
    String exploitFilePath = null;

    boolean trainingTimes = false;
    String timeFilePath = null;

    boolean loadStrategy = false;
    String loadStrategyFilePath = null;

    boolean saveStrategy = false;
    String saveStrategyFilePath = null;

    boolean printStrategiesP1 = false;
    boolean printStrategiesP2 = false;

    int[] samplingOptions = null;
    if (gameSettings != null) {

      int setting_i = 0;
      String[] strSettings = gameSettings.split(",");
      int[] settings = new int[3];

      for (int i = 0; i < 3; i++) {
        settings[i] = Integer.parseInt(strSettings[i]);
      }
      setting_i += 3;

      numSides = settings[0];
      numDice = settings[1];

      // TODO: Extend to outcome and external sampling
      samplingOptions = new int[1];
      samplingOptions[0] = settings[2];

      exploit = strSettings[setting_i++].equals("true");
      trainingTimes = strSettings[setting_i++].equals("true");
      loadStrategy = strSettings[setting_i++].equals("true");
      saveStrategy = strSettings[setting_i++].equals("true");

      if (exploit) {
        exploitFilePath = strSettings[setting_i++];
      }

      if (trainingTimes) {
        timeFilePath = strSettings[setting_i++];
      }

      if (loadStrategy) {
        loadStrategyFilePath = strSettings[setting_i++];
      }

      if (saveStrategy) {
        saveStrategyFilePath = strSettings[setting_i++];
      }

      printStrategiesP1 = strSettings[setting_i++].equals("true");
      printStrategiesP2 = strSettings[setting_i++].equals("true");
    }

    DudoLogic logic = new DudoLogic();
    DudoGameState state = new DudoGameState(2, numSides, numDice);
    DudoFinalEvaluator eval = new DudoFinalEvaluator();
    DudoActionSensor sensor = new DudoActionSensor();

    CFR<DudoGameState> cfr = null;

    Map<DudoGameState, Map<Action, Double>> loadedStrategy = null;
    if (loadStrategy) {
      try {
        FileInputStream cumulativeStrategyStream = new FileInputStream(loadStrategyFilePath);
        ObjectInputStream in = new ObjectInputStream(cumulativeStrategyStream);
        loadedStrategy = (Map<DudoGameState, Map<Action, Double>>) in.readObject();
        in.close();
        cumulativeStrategyStream.close();
        Log.info("CFREngine", "Loaded cumulative strategy profile from: " + loadStrategyFilePath);

        cfr = new CFR<>(state, logic, logic, eval, sensor, loadedStrategy, samplingOptions);
      } catch (IOException | ClassNotFoundException e) {
        Log.error("DudoCFR Sandbox", "Failed to load CFR tables");
        e.printStackTrace();
        System.exit(0);
      }
    } else {
      cfr = new CFR<>(state, logic, logic, eval, sensor, samplingOptions);
    }

    cfr.setCollectExploitability(exploit);
    cfr.setCollectTrainingTimes(trainingTimes);

    Log.info(
        "DudoCFR",
        "Training Dudo with CFR: {numSides: " + numSides + ", numDice: " + numDice + "}");
    try {
      cfr.solve(iterations);
    } catch (StateNotFoundException ex) {
      Log.error("DudoCFR Sandbox", "Failed to train CFR\n" + ex);
      System.exit(0);
    }

    if (saveStrategy) {
      try {
        FileOutputStream fileOut = new FileOutputStream(saveStrategyFilePath);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(cfr.getCumulativeStrategyProfile());
        out.close();
        fileOut.close();
        Log.info("CFR Sandbox", "Serialized data saved to " + saveStrategyFilePath);
      } catch (IOException e) {
        Log.error("CFR Sandbox", "Unable to save trained strategy:");
        e.printStackTrace();
        System.exit(0);
      }
    }

    if (printStrategiesP1 || printStrategiesP2) {
      Map<DudoGameState, Map<Action, Double>> aveStrategies = cfr.getNormalizedStrategy();

      if (printStrategiesP1)
        System.out.println(DudoCFREngine.getPrettyStrategies(aveStrategies, logic, state, 0));
      if (printStrategiesP2)
        System.out.println(DudoCFREngine.getPrettyStrategies(aveStrategies, logic, state, 1));
    }

    // Save exploitability
    if (exploit) {
      Log.info("CFR Sandbox", "Saving exploitabilities to:\t" + exploitFilePath);
      saveTrainingData(exploitFilePath, cfr.getExploitability());
    }

    if (trainingTimes) {
      Log.info("CFR Sandbox", "Saving training times to:\t" + timeFilePath);
      saveTrainingData(timeFilePath, cfr.getTrainingTimes());
    }
  }

  private static void snakeDQN(int matchCount)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {
    SnakeExternalDQNEngine engine =
        new SnakeExternalDQNEngine(new DummyEngineToServerConnection(0));

    engine.setTraining();

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(engine),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);

    SnakeReferee referee = new SnakeReferee(matchSetting, players);
    referee.setGameSpeed(GameSpeed.REALTIME);
    referee.run();
    // rlEngine.getAlg().printQTable();
  }

  private static void gridWorldPolicyIteration(int matchCount, String level)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {
    GridWorldPolicyIterationEngine engine =
        new GridWorldPolicyIterationEngine(new DummyEngineToServerConnection(0));

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(engine),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("levelFile", level);

    GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

    referee.setGameSpeed(GameSpeed.REALTIME);
    referee.run();
  }

  private static void gridWorldValueIteration(int matchCount, String level)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {
    GridWorldValueIterationEngine engine =
        new GridWorldValueIterationEngine(new DummyEngineToServerConnection(0));

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(engine),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("levelFile", level);

    GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

    referee.setGameSpeed(GameSpeed.REALTIME);
    referee.run();
  }

  private static void gridWorldMonteCarloOnPolicy(int matchCount, String level)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {
    GridWorldMonteCarloOnPolicyEngine engine =
        new GridWorldMonteCarloOnPolicyEngine(new DummyEngineToServerConnection(0));

    engine.getAlg().withEpsilon(0.3);

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(engine),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("levelFile", level);

    GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

    referee.setGameSpeed(GameSpeed.REALTIME);
    referee.run();
  }

  private static void gridWorldMonteCarloOffPolicy(int matchCount, String level)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {
    GridWorldMonteCarloOffPolicyEngine engine =
        new GridWorldMonteCarloOffPolicyEngine(new DummyEngineToServerConnection(0));

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(engine),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("levelFile", level);

    GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

    referee.setGameSpeed(GameSpeed.REALTIME);
    referee.run();
  }

  private static void gridWorldSarsa(int matchCount, String level)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {
    GridWorldTabularSarsaEngine engine =
        new GridWorldTabularSarsaEngine(new DummyEngineToServerConnection(0));

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(engine),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("levelFile", level);

    GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

    referee.setGameSpeed(GameSpeed.REALTIME);
    referee.run();
  }

  private static void gridWorldTabularQLearning(int matchCount, String level)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {
    GridWorldTabularQLearningEngine engine =
        new GridWorldTabularQLearningEngine(new DummyEngineToServerConnection(0));

    engine.setTraining();

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(engine),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("levelFile", level);

    GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

    referee.setGameSpeed(GameSpeed.REALTIME);
    referee.run();
  }

  private static void gridWorldLFAQLearning(int matchCount, String level)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {
    GridWorldLinearFunctionApproxQLearningEngine rlEngine =
        new GridWorldLinearFunctionApproxQLearningEngine(new DummyEngineToServerConnection(0));

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(rlEngine),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("levelFile", level);

    GridWorldReferee referee = new GridWorldReferee(matchSetting, players);
    referee.showUI();
    referee.setGameSpeed(GameSpeed.REALTIME);
    referee.run();
  }

  private static void gridWorldDQN(int matchCount, String level)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {
    GridWorldExternalDQNEngine rlEngine =
        new GridWorldExternalDQNEngine(new DummyEngineToServerConnection(0));

    rlEngine.setTraining();

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(rlEngine),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("levelFile", level);

    GridWorldReferee referee = new GridWorldReferee(matchSetting, players);
    referee.showUI();
    referee.setGameSpeed(GameSpeed.REALTIME);
    referee.run();
  }

  private static void mnkTabularQLearning(int matchCount)
      throws IOException,
          ClassNotFoundException,
          MissingSettingException,
          IncorrectSettingTypeException {
    int playerID = 0;

    MNKTabularQLearningEngine player0 =
        new MNKTabularQLearningEngine(new DummyEngineToServerConnection(playerID++));

    player0.setTraining();

    MNKTabularQLearningEngine player1 =
        new MNKTabularQLearningEngine(new DummyEngineToServerConnection(playerID++));

    player1.setTraining();

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(player0), new LocalPlayer(player1),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("mnk_height", 3.0);
    matchSetting.setSetting("mnk_width", 3.0);
    matchSetting.setSetting("mnk_k", 3.0);
    matchSetting.setSetting("perfectInformation", true);

    // Instantiate and run the referee.
    MNKReferee referee = new MNKReferee(matchSetting, players);

    referee.run();
  }

  private static void mnkDQN(int matchCount)
      throws IOException,
          ClassNotFoundException,
          MissingSettingException,
          IncorrectSettingTypeException {
    int playerID = 0;

    MNKExternalDQNEngine player0 =
        new MNKExternalDQNEngine(new DummyEngineToServerConnection(playerID++));

    player0.setTraining();

    MNKRandomEngine player1 = new MNKRandomEngine(new DummyEngineToServerConnection(playerID++));

    // player1.setTraining();

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(player0), new LocalPlayer(player1),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("mnk_height", 3.0);
    matchSetting.setSetting("mnk_width", 3.0);
    matchSetting.setSetting("mnk_k", 3.0);
    matchSetting.setSetting("perfectInformation", true);

    // Instantiate and run the referee.
    MNKReferee referee = new MNKReferee(matchSetting, players);

    referee.run();
  }

  private static void mnkDQNSelfPlay(int matchCount)
      throws IOException,
          ClassNotFoundException,
          MissingSettingException,
          IncorrectSettingTypeException {
    int playerID = 0;

    MNKExternalDQNEngine player0 =
        new MNKExternalDQNEngine(new DummyEngineToServerConnection(playerID++));

    player0.setTraining();

    MNKExternalDQNEngine player1 =
        new MNKExternalDQNEngine(new DummyEngineToServerConnection(playerID++));

    player1.setTraining();

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(player0), new LocalPlayer(player1),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("mnk_height", 3.0);
    matchSetting.setSetting("mnk_width", 3.0);
    matchSetting.setSetting("mnk_k", 3.0);
    matchSetting.setSetting("perfectInformation", true);

    // Instantiate and run the referee.
    MNKReferee referee = new MNKReferee(matchSetting, players);

    referee.run();
  }

  private static void gridWorldExternalTable(int matchCount, String level)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {

    GridWorldExternalTabularQLearningEngine rlEngine =
        new GridWorldExternalTabularQLearningEngine(new DummyEngineToServerConnection(0));

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(rlEngine),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("levelFile", level);

    GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

    referee.run();

    // rlEngine.getAlg().printQTable();
  }

  private static void gridWorldExternalFunctionApproximation(int matchCount, String level)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {
    GridWorldLinearFunctionApproxQLearningEngine rlEngine =
        new GridWorldLinearFunctionApproxQLearningEngine(new DummyEngineToServerConnection(0));

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(rlEngine),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("levelFile", level);

    GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

    referee.run();

    // rlEngine.getAlg().printQTable();
  }

  private static void gridWorldExternalDQN(int matchCount)
      throws MissingSettingException,
          IncorrectSettingTypeException,
          IOException,
          ClassNotFoundException {
    GridWorldExternalDQNEngine rlEngine =
        new GridWorldExternalDQNEngine(new DummyEngineToServerConnection(0));

    PlayerRepresentation[] players =
        new PlayerRepresentation[] {
          new LocalPlayer(rlEngine),
        };

    MatchSetting matchSetting = new MatchSetting();
    matchSetting.setNumPlayers(players.length);
    matchSetting.setNumMatches(matchCount);
    matchSetting.setSetting("levelFile", "./scripts/gridWorld/large.gridworld");

    GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

    referee.run();
  }
}
