package za.ac.sun.cs.ingenious.core.util.selector;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.ClassUtils;

/**
 * This is used to select a move according to a probability distribution of the possible moves.
 * Intended to be used to select a stochastic move after calling the function:
 * generateStochasticActions from {@link GameLogic}. Should use {@link
 * ClassUtils#chooseFromDistribution} to sample from the made distribution.
 */
public interface ActionSelector {
  /**
   * @param fromGameState Game state from which actions are generated for
   * @param logic Game logic used to determine possible actions and PDF
   * @return Chosen action from implemented PDF
   */
  public <S extends GameState, L extends GameLogic> Action getAction(S fromGameState, L logic);
}
