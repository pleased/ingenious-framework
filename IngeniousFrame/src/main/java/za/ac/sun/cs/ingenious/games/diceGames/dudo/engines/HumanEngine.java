package za.ac.sun.cs.ingenious.games.diceGames.dudo.engines;

import com.esotericsoftware.minlog.Log;
import java.util.List;
import java.util.Scanner;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;

public class HumanEngine extends DudoGeneralEngine {

  private static String actionRequest =
      " ---------------------------------\n"
          + "  Please choose an action number:\n"
          + " ---------------------------------\n\n";

  public HumanEngine(EngineToServerConnection toServer) {
    super(toServer);
  }

  @Override
  public String engineName() {
    return "DudoHumanEngine";
  }

  @Override
  public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
    List<Action> actions = logic.generateActions(currentState, this.playerID);

    StringBuilder stringBuilder = new StringBuilder();

    stringBuilder.append("\n");
    stringBuilder.append(actionRequest);

    for (int i = 0; i < actions.size(); i++) {
      stringBuilder
          .append(" ")
          .append(i + 1)
          .append(". ")
          .append(currentState.getPrettyClaim(((DiscreteAction) actions.get(i)).getActionNumber()))
          .append("\n");
    }

    boolean valid = false;
    int option = -1;

    do {
      Log.info("DudoHumanEngine", stringBuilder.toString());

      Scanner in = new Scanner(System.in);

      try {
        option = Integer.parseInt(in.nextLine()) - 1;

        if (option >= 0 && option < actions.size()) {
          valid = true;
        }
      } catch (NumberFormatException ex) {
        valid = false;
      }

      if (!valid) {
        Log.error("DudoHumanEngine", "Invalid option.");
      }

    } while (!valid);

    return new PlayActionMessage(actions.get(option));
  }
}
