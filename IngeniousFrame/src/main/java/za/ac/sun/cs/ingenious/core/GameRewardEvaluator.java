package za.ac.sun.cs.ingenious.core;

/**
 * Used for determining the rewards to be issued to each player at the end of a round / step of a
 * match. Games using rewards issued to reinforcement learning agents should contain an
 * implementation of this class.
 *
 * @author Steffen Jacobs
 * @param <S> The type of GameState that this reward evaluator can determine.
 */
public interface GameRewardEvaluator<S extends GameState> {
  /**
   * Determines the rewards that should be issued to players for a given state.
   *
   * @param state Game state from which rewards are evaluated.
   * @return The rewards indexed by player ID.
   */
  public double[] getReward(S state);
}
