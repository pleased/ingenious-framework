package za.ac.sun.cs.ingenious.search.mcts.backpropagation;

import java.util.List;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.LGR.LGRTable;

public class BackpropagationLgrf<N extends MctsNodeTreeParallelInterface<?, ?, ?>>
    implements BackpropagationThreadSafe<N> {

  private LGRTable lgrTable;

  public BackpropagationLgrf(LGRTable lgrTable) {
    this.lgrTable = lgrTable;
  }

  /**
   * Update the fields relating to this backpropagation strategy for the current node.
   *
   * @param node
   * @param results the win/loss or draw outcome for the simulation relating to the current playout.
   */
  public void propagate(N node, double[] results, List<Action> moves) {
    N parent = (N) node.getParent();
    double result = results[parent.getPlayerID()];
    if (result == 1.0) {
      Action currentLgr;
      Action parentPreviousAction = parent.getPrevAction();
      if (parentPreviousAction != null) {
        currentLgr = lgrTable.moveCombinationScores.get(parentPreviousAction);
        if (currentLgr == null) {
          // TODO : Check if IdleActions should be ignored. See issue #329
          if (!(parentPreviousAction instanceof IdleAction)
              && !(node.getPrevAction() instanceof IdleAction)) {
            lgrTable.moveCombinationScores.put(parentPreviousAction, node.getPrevAction());
          }
        } else {
          lgrTable.moveCombinationScores.replace(parentPreviousAction, node.getPrevAction());
        }
      }
    } else {
      Action currentLgr;
      Action parentPreviousAction = parent.getPrevAction();
      if (parentPreviousAction != null) {
        currentLgr = lgrTable.moveCombinationScores.get(parentPreviousAction);
        if (currentLgr != null && currentLgr.equals(node.getPrevAction())) {
          lgrTable.moveCombinationScores.remove(parentPreviousAction);
        }
      }
    }
  }

  public static <NN extends MctsNodeTreeParallelInterface<?, ?, ?>>
      BackpropagationLgrf<NN> newBackpropagationLgrf(LGRTable lgrTable) {
    return new BackpropagationLgrf<>(lgrTable);
  }
}
