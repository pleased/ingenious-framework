package za.ac.sun.cs.ingenious.games.rps;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.move.UnobservedMove;

public class RPSGameState extends GameState {
  protected Move[] playerActions;

  public RPSGameState(int numPlayers) {
    super(numPlayers);

    playerActions = new Move[2];
  }

  @Override
  public String toString() {
    return getPretty();
  }

  @Override
  public String getPretty() {
    StringBuilder str = new StringBuilder();

    for (int i = 0; i < 2; i++) {
      str.append("Player ").append(i + 1).append(": ");
      if (playerActions[i] == null) {
        str.append("NULL");
      } else if (playerActions[i] instanceof UnobservedMove) {
        str.append("Unobserved");
      } else {
        str.append(RPSLogic.getPrettyRPSAction((DiscreteAction) playerActions[i]));
      }
      str.append("\n");
    }

    return str.toString();
  }

  @Override
  public RPSGameState deepCopy() {
    RPSGameState state = new RPSGameState(2);

    for (int i = 0; i < 2; i++) {
      if (playerActions[i] instanceof UnobservedMove) {
        state.playerActions[i] = playerActions[i];
      } else if (playerActions[i] instanceof DiscreteAction) {
        state.playerActions[i] = ((DiscreteAction) playerActions[i]).deepCopy();
      }
    }

    return state;
  }

  @Override
  public int hashCode() {
    int hash = 7;

    for (int i = 0; i < 2; i++) {
      int playerHash = 0;

      if (playerActions[i] instanceof UnobservedMove) {
        playerHash = playerActions[i].hashCode() + 1;
      } else if (playerActions[i] instanceof DiscreteAction) {
        playerHash = playerActions[i].hashCode();
      }

      hash = 31 * hash + playerHash;
    }

    return hash;
  }
}
