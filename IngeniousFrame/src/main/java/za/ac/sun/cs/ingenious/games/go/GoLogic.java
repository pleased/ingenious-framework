package za.ac.sun.cs.ingenious.games.go;

import java.util.List;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;

public class GoLogic implements TurnBasedGameLogic<GoBoard> {
  public byte[] previousState = null;

  private MatchSetting matchSettings = null;

  public GoLogic() {}

  public MatchSetting getMatchSettings() {
    return matchSettings;
  }

  @Override
  public boolean validMove(GoBoard fromState, Move move) {
    return fromState.validMove(move);
  }

  @Override
  public boolean makeMove(GoBoard fromState, Move move) {
    boolean good = fromState.makeMove(move);
    if (good) {
      nextTurn(fromState, move);
    }
    return good;
  }

  @Override
  public void undoMove(GoBoard fromState, Move move) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<Action> generateActions(GoBoard fromState, int forPlayerID) {
    return fromState.getFullList(forPlayerID);
  }

  public List<Action> generateActions(
      GoBoard fromState, int forPlayerID, Action previousActionByThisPlayer) {
    return fromState.getFullList(forPlayerID);
  }

  public Move generateAction(GoBoard fromState, int forPlayerID) {
    return fromState.getSingleMove(forPlayerID);
  }

  @Override
  public boolean isTerminal(GoBoard fromState) {
    return fromState.isTerminal();
  }
}
