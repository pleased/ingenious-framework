package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

/**
 * Simple enum for all possible Uno locations.
 *
 * @author Joshua Wiebe
 */
public enum UnoLocations {

  // List players first in order access player locations with index. E. g. for player 0:
  // UnoLocations.values()[index]
  PLAYER0,
  PLAYER1,
  PLAYER2,
  PLAYER3,
  PLAYER4,
  PLAYER5,
  PLAYER6,
  PLAYER7,

  // Other locations
  DRAWPILE,
  DISCARDPILE;
}
