package za.ac.sun.cs.ingenious.games.mdp;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.message.MatchSettingsInitGameMessage;
import za.ac.sun.cs.ingenious.games.mdp.util.MDPSetting;

/**
 * Base engine for MDP that handles game initialization and applying moves received from the
 * referee.
 *
 * @author Steffen Jacobs
 */
public abstract class MDPEngine extends Engine {

  protected MDPState<Long> state;
  protected MDPLogic<Long> logic;
  protected MDPFinalEvaluator<Long> eval;
  protected MDPRewardEvaluator<Long> rewardEval;

  public MDPEngine(EngineToServerConnection toServer) {
    super(toServer);
    state = null;
    logic = null;
    eval = null;
  }

  @Override
  public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
    Log.info("Final scores for my terminal position:");
    double[] score = eval.getScore(state);
    for (int i = 0; i < score.length; i++) {
      Log.info("Player " + i + ": " + score[i]);
    }
    Log.info("Final state as seen by player " + this.playerID + ":");
    state.printPretty();
    toServer.closeConnection();
  }

  @Override
  public void receiveInitGameMessage(InitGameMessage a) {
    MatchSetting ms = ((MatchSettingsInitGameMessage) a).getSettings();

    try {
      String mdpFilePath = ms.getSettingAsString("mdpGameFileName");
      MDPSetting mdpSetting = MDPSetting.fromFile(mdpFilePath);

      logic = MDPLogic.fromSetting(mdpSetting);
      state = MDPState.fromSetting(mdpSetting);
      eval = MDPFinalEvaluator.fromSetting(mdpSetting);
      rewardEval = MDPRewardEvaluator.fromSetting(mdpSetting);
    } catch (MissingSettingException | IncorrectSettingTypeException e) {
      Log.error(
          "Could not read settings from received MatchSetting object, message: " + e.getMessage());

      System.exit(-1);
    }
  }

  @Override
  public void receivePlayedMoveMessage(PlayedMoveMessage a) {
    logic.makeMove(state, a.getMove());
  }
}
