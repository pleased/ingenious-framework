package za.ac.sun.cs.ingenious.search.mcts.legacy;

import com.esotericsoftware.minlog.Log;
import java.util.List;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;

/**
 * Implementation of TreeDescender for MCTS. Always searches for the node with the best search value
 * until an expandable node is reached. This will not work well with imperfect information games.
 *
 * @author steve
 * @author Michael Krause
 */
public class MCTSDescender<S extends TurnBasedGameState> implements TreeDescender<S, MCTSNode<S>> {

  protected GameLogic<S> logic;
  protected SearchValue<S, MCTSNode<S>> searchValue;
  protected ActionSensor<S> sensor;

  /**
   * @param logic Logic object.
   * @param searchValue Object with which to generate search values for nodes. Most common is {@link
   *     UCT}
   * @param sensor ActionSensor for actions that are being applied to states. For perfect
   *     information games, use {@link PerfectInformationActionSensor}.
   */
  public MCTSDescender(
      GameLogic<S> logic, SearchValue<S, MCTSNode<S>> searchValue, ActionSensor<S> sensor) {
    this.logic = logic;
    this.searchValue = searchValue;
    this.sensor = sensor;
  }

  @Override
  public MCTSNode<S> expand(MCTSNode<S> parent) {
    // The simplest implementation just chooses a random move
    List<Move> unexpandedMoves = parent.getUnexpandedMoves();
    if (unexpandedMoves.isEmpty()) {
      return null;
    }
    int index = (int) (Math.random() * unexpandedMoves.size());
    Move toExpand = unexpandedMoves.remove(index);

    MCTSNode<S> child = createNodeForMove(toExpand, parent);
    parent.addChild(child);
    return child;
  }

  /**
   * @param move Move that leads to the new node.
   * @param parent Parent of the new node.
   * @return New child node of parent.
   */
  private MCTSNode<S> createNodeForMove(Move move, MCTSNode<S> parent) {
    if (!(move instanceof Action)) {
      String errorMsg = "Expanding move that is not an action.";
      Log.error("MCTSDescender", errorMsg);
      throw new RuntimeException(errorMsg);
    }
    Action a = (Action) move;

    @SuppressWarnings("unchecked")
    S newState = (S) parent.getGameState().deepCopy();
    this.logic.makeMove(newState, sensor.fromPointOfView(a, newState, -1));
    return new MCTSNode<S>(newState, logic, move, parent);
  }

  @Override
  public MCTSNode<S> descend(MCTSNode<S> root, int totalNofPlayouts) {
    // Takes a greedy path trough the TreeEngine by always picking
    // the node with the highest searchValue until it hits an expandable or terminal node.
    MCTSNode<S> currentNode = root;
    while (currentNode != null && !currentNode.isTerminal()) {
      if (!currentNode.getUnexpandedMoves().isEmpty()
          || currentNode.getExpandedChildren().isEmpty()) {
        // Node can be expanded OR no further descent possible, therefore descent ends here.
        break;
      }

      double highestValue = Double.NEGATIVE_INFINITY;
      MCTSNode<S> bestChild = null;
      for (MCTSNode<S> child : currentNode.getExpandedChildren()) {
        double childValue =
            this.searchValue.get(child, currentNode.getCurrentPlayer(), totalNofPlayouts);
        if (childValue > highestValue) {
          bestChild = child;
          highestValue = childValue;
        }
      }

      if (bestChild == null) {
        break;
      } else {
        currentNode = bestChild;
      }
    }
    return currentNode;
  }

  @Override
  public S getStateForPlayout(MCTSNode<S> fromNode) {
    return fromNode.getGameState();
  }
}
