package za.ac.sun.cs.ingenious.core;

import com.esotericsoftware.minlog.Log;
import com.rits.cloning.Cloner;
import java.io.Serializable;
import java.util.Objects;
import java.util.Observable;

/**
 * Represents one state of some game. Must be extended with specific information for each game. Note
 * that for both perfect and imperfect information games, the GameState contains <b>all</b>
 * information that constitutes a state of the game. In games of imperfect information, a GameState
 * object may represent an information set that a player of the game sees. But it must also support
 * representing the complete state of the game as seen e.g. by the referee.
 *
 * @author Michael Krause
 */
public abstract class GameState extends Observable implements Serializable {

  protected static final Cloner cloner = new Cloner();

  protected int numPlayers;

  public GameState(int numPlayers) {
    this.numPlayers = numPlayers;
  }

  public int getNumPlayers() {
    return numPlayers;
  }

  public void setNumPlayers(int numPlayers) {
    this.numPlayers = numPlayers;
  }

  /**
   * Print a representation of this state. This is used e.g. to display the GameState after a player
   * made their move.
   */
  // TODO - should take a logging level as a parameter - see issue 147
  public void printPretty() {
    Log.info(getPretty());
  }

  public String getPretty() {
    return toString();
  }

  public abstract GameState deepCopy();

  @Override
  public int hashCode() {
    return Objects.hashCode(numPlayers);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GameState other = (GameState) obj;
    if (numPlayers != other.numPlayers) return false;
    return true;
  }
}
