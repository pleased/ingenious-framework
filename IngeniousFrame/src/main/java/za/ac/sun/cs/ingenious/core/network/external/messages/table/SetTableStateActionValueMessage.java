package za.ac.sun.cs.ingenious.core.network.external.messages.table;

import java.util.HashMap;
import java.util.Map;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalState;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

/**
 * Used to communicate an action value to the Python application's table.
 *
 * @author Steffen Jacobs
 */
public class SetTableStateActionValueMessage extends ExternalMessage {
  Map<String, Object> payload;

  public SetTableStateActionValueMessage(GameState state, int action, double value) {
    super(ExternalMessageType.SET_TABLE_STATE_ACTION_VALUE);

    payload = new HashMap<>();
    payload.put("state", new ExternalState(state));
    payload.put("action", action);
    payload.put("value", value);
  }
}
