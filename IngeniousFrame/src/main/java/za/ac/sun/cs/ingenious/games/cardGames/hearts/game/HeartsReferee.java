package za.ac.sun.cs.ingenious.games.cardGames.hearts.game;

import com.esotericsoftware.minlog.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.referee.FullyObservableMovesReferee;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSuits;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSymbols;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.PlayCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.EmptyDrawPileException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;

/**
 * The Class HeartsReferee.
 *
 * @author Joshua Wiebe
 */
public class HeartsReferee
    extends FullyObservableMovesReferee<HeartsGameState, HeartsGameLogic, HeartsFinalEvaluator> {

  public static final int MAXNUMBEROFPLAYERS = 6;

  public static final int MINNUMBEROFPLAYERS = 3;

  /**
   * Instantiates a new Hearts referee.
   *
   * @param match The match settings
   * @param players The player representations
   */
  public HeartsReferee(MatchSetting match, PlayerRepresentation[] players) {
    super(
        match,
        players,
        new HeartsGameState(initAndReturnNumPlayers(match)),
        new HeartsGameLogic(),
        new HeartsFinalEvaluator());
  }

  /**
   * @see
   *     za.ac.sun.cs.ingenious.core.util.referee.PerfectInformationAlternatingPlayReferee#beforeGameStarts()
   *     <p>Before the game starts.
   */
  @Override
  protected void beforeGameStarts() {
    Log.info();
    Log.info("Variant: Hearts with " + currentState.getNumPlayers() + " players.");
  }

  /**
   * @see
   *     za.ac.sun.cs.ingenious.core.util.referee.PerfectInformationAlternatingPlayReferee#createInitGameAction(za.ac.sun.cs.ingenious.core.PlayerRepresentation)
   *     <p>Creates the initGameAction with a new generated rack.
   * @return Returns the initGameAction
   */
  @Override
  protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {

    // Create a new rack for given player
    CardRack<ClassicalSymbols, ClassicalSuits> rack = createInitRack(player.getID(), currentState);

    // Create the InitGameAction
    HeartsInitGameMessage iga =
        new HeartsInitGameMessage(rack, player.getID(), currentState.getNumPlayers());

    return iga;
  }

  /**
   * @see
   *     za.ac.sun.cs.ingenious.core.util.referee.PerfectInformationAlternatingPlayReferee#reactToValidMove(int,
   *     za.ac.sun.cs.ingenious.core.network.game.actions.PlayMoveAction)
   *     <p>This method will be run if a given move was valid. Logs the most relevant changes made
   *     within this turn.
   * @param player Current player
   * @param pma valid Move
   */
  @Override
  protected void reactToValidAction(int player, PlayActionMessage pam) {
    // Check again if move is null-move.
    if (pam.getAction() != null) {
      logRelevantChanges(player, pam);
    }
  }

  /**
   * @see
   *     za.ac.sun.cs.ingenious.core.util.referee.PerfectInformationAlternatingPlayReferee#reactToInvalidMove(int,
   *     za.ac.sun.cs.ingenious.core.network.game.actions.PlayMoveAction)
   *     <p>This method will be run if a player sent in a null-move or an invalid move. It chooses
   *     randomly a move for the player. After this, reactToValidMove() will be called with the
   *     random move.
   * @param player Current player
   * @param m Invalid move or null move (reactToNullMove just calls this method)
   */
  @Override
  protected void reactToInvalidAction(int player, PlayActionMessage m) {

    // Set up random draw from possible moves.
    Random rand = new Random();

    // Generate all possible moves for the given player.
    List<Action> moves = logic.generateActions(currentState, player);

    // Create a new PlayCardMove by randomly drawing one of the generated moves.
    PlayCardAction<HeartsLocations> pca = (PlayCardAction) moves.get(rand.nextInt(moves.size()));

    // Update current state.
    logic.makeMove(currentState, pca);

    // Distribute to players.
    for (PlayerRepresentation playerRepr : players) {
      PlayedMoveMessage pam = new PlayedMoveMessage(pca);
      players[playerRepr.getID()].playMove(pam);
    }

    // Now the move is valid and can be processed to reactToValidMove.
    reactToValidAction(players[player].getID(), new PlayActionMessage(pca));
  }

  /**
   * @see
   *     za.ac.sun.cs.ingenious.core.util.referee.PerfectInformationAlternatingPlayReferee#reactToNullMove(int,
   *     za.ac.sun.cs.ingenious.core.network.game.actions.PlayMoveAction)
   *     <p>Call reactToInvalidMove
   * @param player Current player
   * @param m Null move (of course also invalid)
   */
  @Override
  protected void reactToNullAction(int player, PlayActionMessage m) {
    reactToInvalidAction(player, m);
  }

  @Override
  protected void afterRound(Map<Integer, PlayActionMessage> messages) {
    // Do nothing - printing the state creates excessive output
  }

  /**
   * Creates the initial rack for a given player.
   *
   * @param player current player
   * @return the generated card rack
   */
  protected CardRack<ClassicalSymbols, ClassicalSuits> createInitRack(
      int player, HeartsGameState heartsGameState) {
    CardRack<ClassicalSymbols, ClassicalSuits> rack = new CardRack<>();

    // While rack is not full
    while (rack.size() < heartsGameState.getRoundsThatWillBePlayed()) {

      // draw new card by calling drawFromDrawPile(). Then add it to rack.
      try {
        Card<ClassicalSymbols, ClassicalSuits> card = drawFromDrawPile(player);
        rack.addCard(card);
        currentState.changeLocation(
            card, HeartsLocations.DRAWPILE, HeartsLocations.values()[player]);
      } catch (EmptyDrawPileException | KeyNotFoundException | LocationNotFoundException e) {
        e.printStackTrace();
      }
    }

    // After the rack for a given player is generated successfully, print it.
    StringBuilder s = new StringBuilder();
    int width = 20;
    s.append("\n+");
    for (int i = 0; i < width - 1; i++) {
      s.append("-");
    }
    s.append("+\n");
    s.append("|Rack of Player " + player + "   |\n");
    s.append("+");
    for (int i = 0; i < width - 1; i++) {
      s.append("-");
    }
    s.append("+\n");
    for (Card<ClassicalSymbols, ClassicalSuits> card : rack) {
      String tmpString = "|" + card.toString();
      s.append(tmpString);
      for (int i = 0; i < width - tmpString.length(); i++) {
        s.append(" ");
      }
      s.append("|\n");
    }
    s.append("+");
    for (int i = 0; i < width - 1; i++) {
      s.append("-");
    }
    s.append("+\n\n");
    Log.info(s);

    return rack;
  }

  /**
   * Draw from draw pile.
   *
   * @param player The drawing player.
   * @return The drawn card
   */
  protected Card<ClassicalSymbols, ClassicalSuits> drawFromDrawPile(int player)
      throws EmptyDrawPileException {

    // Fetch card-location-map.
    TreeMap<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> map =
        currentState.getMap();

    Random randomizer = new Random();
    Card<ClassicalSymbols, ClassicalSuits> card;

    // Get all potential cards
    ArrayList<Card<ClassicalSymbols, ClassicalSuits>> potentialCards = new ArrayList<>();
    // For each card type
    for (Map.Entry<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> entry :
        map.entrySet()) {
      // For each location of that cardType.
      for (HeartsLocations location : entry.getValue()) {
        // add if it is on draw pile.
        if (location == HeartsLocations.DRAWPILE) {
          potentialCards.add(entry.getKey());
        }
      }
    }

    // Select a card randomly
    int rand = randomizer.nextInt(potentialCards.size());
    card = potentialCards.get(rand);

    // Return card.
    return card;
  }

  @Override
  protected void afterGameTerminated() {
    Log.info();
    Log.info();
    Log.info("Game has terminated!");
    double[] scores = eval.getScore(currentState);
    Log.info();
    Log.info("Scores:");
    for (int i = 0; i < currentState.getNumPlayers(); i++) {
      StringBuilder s = new StringBuilder();
      s.append("Player" + i + ": " + scores[i]);
      Log.info(s);
    }
    Log.info();

    StringBuilder s = new StringBuilder();
    Log.info("Winner(s):");
    ArrayList<Double> winners = logic.multyArgMin(scores);
    for (double winner : winners) {
      Log.info("Player " + (int) winner);
    }

    Log.info(s);
  }

  /**
   * This method checks if the given number of players if valid. Must be static because it's called
   * before the referee is instantiated (see constructor)
   *
   * @param match A MatchSetting object.
   * @return The number of players
   */
  private static int initAndReturnNumPlayers(MatchSetting match) {
    int numPlayers = match.getNumPlayers();
    if (numPlayers > MAXNUMBEROFPLAYERS || numPlayers < MINNUMBEROFPLAYERS) {
      throw new IllegalArgumentException("===Invalid number of players)===");
    }
    return numPlayers;
  }

  /**
   * Given a PlayActionMessage this method prints all relevant changes that happened.
   *
   * @param player Player which did the move.
   * @param pam Message
   */
  private void logRelevantChanges(int player, PlayActionMessage pam) {

    StringBuilder s = new StringBuilder();
    int outputWidth = 62;

    // Parameters for logging.
    int printRoundNR;
    int printStartingPlayer;
    ClassicalSuits printSuit;
    int printTrickPileSize;

    // If round is not complete. ('normal turn')
    if (!currentState.getCurrentTrick().isEmpty()) {
      printRoundNR = currentState.getRoundNR();
      printStartingPlayer = currentState.getBeginningPlayer();
      printSuit = currentState.getCurrentTrickSuit();
      printTrickPileSize = currentState.getCurrentTrick().size();
    }

    // If round is complete.
    else {
      printRoundNR = currentState.getRoundNR() - 1;
      printStartingPlayer = currentState.getWinnerOfLastRound();
      printSuit = currentState.getTrickSuitOfLastRound();
      printTrickPileSize = currentState.getNumPlayers();
    }

    // Print round NR.
    s.append("\n+-------------------------------------------------------------+\n");

    String tmpString = "|                           Round " + printRoundNR;
    s.append(tmpString);
    for (int i = 0; i < outputWidth - tmpString.length(); i++) {
      s.append(" ");
    }
    s.append("|\n");

    // Print empty line.
    s.append("|                                                             |\n");

    // Print player who has started the current round
    tmpString = "| Player " + printStartingPlayer + " started this round.";
    s.append(tmpString);
    for (int i = 0; i < outputWidth - tmpString.length(); i++) {
      s.append(" ");
    }
    s.append("|\n");

    // Print player who has started the current round
    tmpString = "| Suit of the round: " + printSuit;
    s.append(tmpString);
    for (int i = 0; i < outputWidth - tmpString.length(); i++) {
      s.append(" ");
    }
    s.append("|\n");

    // Print players move (PlayCardMove).
    tmpString = "| Player " + player + ": " + pam.getAction().toString();
    s.append(tmpString);
    for (int i = 0; i < outputWidth - tmpString.length(); i++) {
      s.append(" ");
    }
    s.append("|\n");

    // Print current size trick pile.

    tmpString = "| Size of trick pile: " + printTrickPileSize;
    s.append(tmpString);
    for (int i = 0; i < outputWidth - tmpString.length(); i++) {
      s.append(" ");
    }
    s.append("|\n");

    // Print collected trick piles.
    StringBuilder tmpStringBuilder = new StringBuilder();
    int[] tricks = currentState.getCopyOfNrTricksOfPlayers();
    tmpStringBuilder.append("| Collected tricks: [");
    for (int i = 0; i < tricks.length; i++) {
      if (i == 0) {
        tmpStringBuilder.append(tricks[i]);
      } else {
        tmpStringBuilder.append(", " + tricks[i]);
      }
    }
    tmpStringBuilder.append("]");
    s.append(tmpStringBuilder.toString());
    for (int i = 0; i < outputWidth - tmpStringBuilder.length(); i++) {
      s.append(" ");
    }
    s.append("|\n");

    // Print current points.
    tmpStringBuilder = new StringBuilder();
    double[] points = eval.getScore(currentState);
    tmpStringBuilder.append("| Collected points: [");
    for (int i = 0; i < points.length; i++) {
      if (i == 0) {
        tmpStringBuilder.append((int) points[i]);
      } else {
        tmpStringBuilder.append(", " + (int) points[i]);
      }
    }
    tmpStringBuilder.append("]");
    s.append(tmpStringBuilder.toString());
    for (int i = 0; i < outputWidth - tmpStringBuilder.length(); i++) {
      s.append(" ");
    }
    s.append("|\n");

    // Print last line.
    s.append("+-------------------------------------------------------------+\n");

    // Log.
    Log.info(s);
  }
}
