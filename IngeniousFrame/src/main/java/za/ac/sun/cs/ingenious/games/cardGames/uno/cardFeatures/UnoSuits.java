package za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature;

/**
 * The enum for Uno suits.
 *
 * @author Joshua Wiebe
 */
public enum UnoSuits implements CardFeature {

  /** Uno suits. */
  RED(4),
  BLUE(3),
  GREEN(2),
  YELLOW(1);

  /**
   * Values for hierarchical ordering. Even though Uno has no hierarchy. (Just for sorting purposes)
   */
  int value;

  /**
   * Constructor for suits.
   *
   * @param Value of suit.
   */
  UnoSuits(int value) {
    this.value = value;
  }

  /**
   * @see za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature#getValue()
   */
  @Override
  public int getValue() {
    return this.value;
  }

  /**
   * @see za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature#setValue(int)
   */
  @Override
  public void setValue(int value) {
    this.value = value;
  }
}
