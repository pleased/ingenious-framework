package za.ac.sun.cs.ingenious.core.commandline.dte.client;

import com.esotericsoftware.minlog.Log;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;

/**
 * Created by Chris Coetzee on 2016/07/30. Very similar to the original client driver, but instead
 * takes it's engine name from a configuration file.
 */
public class Driver {

  public static void main(String[] args) throws URISyntaxException, IOException {
    Config config = Config.loadFromConfigFile();
    String[] argsUpdated = new String[args.length + 2];
    System.arraycopy(args, 0, argsUpdated, 0, args.length);

    /* add the engine flag */
    argsUpdated[args.length] = "-engine";
    argsUpdated[args.length + 1] = config.getEngineName();

    Log.info(Arrays.toString(argsUpdated));

    za.ac.sun.cs.ingenious.core.commandline.client.Driver.main(argsUpdated);
  }
}
