package za.ac.sun.cs.ingenious.games.rps;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.referee.GeneralReferee;

public class RPSReferee extends GeneralReferee<RPSGameState, RPSLogic, RPSLogic> {
  public RPSReferee(MatchSetting match, PlayerRepresentation[] players) {
    super(match, players, new RPSGameState(2), new RPSLogic(), new RPSLogic(), new RPSLogic());
  }

  @Override
  protected void updateServerState(PlayActionMessage m) {
    logic.makeMove(currentState, sensor.fromPointOfView(m.getAction(), currentState, -1));
  }
}
