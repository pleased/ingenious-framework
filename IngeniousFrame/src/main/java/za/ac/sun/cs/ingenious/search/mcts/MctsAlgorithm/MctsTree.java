package za.ac.sun.cs.ingenious.search.mcts.MctsAlgorithm;

import com.esotericsoftware.minlog.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelection;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionFinal;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationTuple;

/**
 * Implements a general-purpose TreeEngine parallelism version of the MCTS algorithm. This
 * implementation uses Threads and is for multicore systems with shared memory. Due to the shared
 * memory nature of this algorithm, the MCTS node type that can be used for this algorithm should
 * store the child and parent of the node as references to the node structure.
 *
 * @author Karen Laubscher
 * @param <S> The game state type
 * @param <N> The MCTS node type
 */
public class MctsTree<S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> {

  private TreeSelection<N> selection;
  private ExpansionThreadSafe<N, N> expansion;
  private SimulationThreadSafe<S> simulation;
  private ArrayList<BackpropagationThreadSafe<N>> backpropagationEnhancements;
  private TreeSelectionFinal<N> finalSelection;
  private GameLogic<S> logic;
  private int threadCount;
  int[] playoutCounts;
  private int playerId;
  private long totalPlayoutsOverGame = 0;
  private int numberTurns = 0;

  /**
   * Constructor that takes in the 4 basic strategies as well as a final selection strategy, that
   * may differ from the selection strategy used when traversing the TreeEngine during the MCTS
   * algorithm.
   *
   * @param selection The selection strategy. A common strategy to use is {@link TreeSelection},
   *     which calculates UCT values.
   * @param expansion The expansion strategy. A common strategy to use is {@link ExpansionSingle},
   *     which adds one unexplored node
   * @param simulation The simulation strategy. A basic play out policy is to use {@link
   *     SimulationRandom}, which do random playout.
   * @param backpropagationEnhancements The backpropagation strategy. A common strategy to use is
   *     {@link BackpropagationAverage}, just adding the result
   * @param finalSelection The selection strategy used to make the final selection to determine the
   *     best move to be returned.
   * @param logic The game logic, used to check for ternimal gamestates.
   * @param threadCount The number of TreeEngine threads that should be created.
   */
  public MctsTree(
      TreeSelection<N> selection,
      ExpansionThreadSafe<N, N> expansion,
      SimulationThreadSafe<S> simulation,
      ArrayList<BackpropagationThreadSafe<N>> backpropagationEnhancements,
      TreeSelectionFinal<N> finalSelection,
      GameLogic<S> logic,
      int threadCount,
      int playerId) {
    this.selection = selection;
    this.expansion = expansion;
    this.simulation = simulation;
    this.backpropagationEnhancements = backpropagationEnhancements;
    this.finalSelection = finalSelection;
    this.logic = logic;
    this.threadCount = threadCount;
    this.playerId = playerId;
    playoutCounts = new int[this.threadCount];
  }

  /**
   * This method generates an action that represents the best move choice found. The method forms
   * part of the template operation that drives the TreeEngine parallelisation MCTS algorithm, in
   * particular, it is the main thread operation, that creates the TreeEngine threads, and at the
   * end, performs the final selection from the shared root node.
   *
   * @param root The root node from which the search should start.
   * @param turnlength Time in ms within which the move needs to be decided.
   * @return The best move action that was found by the algorithm during the time of the turn
   *     length.
   */
  public N doSearch(
      N root, long turnlength, ZobristHashing zobrist, int simsPerPlayout, boolean virtualLossOn) {
    Log.set(Log.LEVEL_DEBUG);
    long endTime = System.nanoTime() + turnlength * 1_000_000;
    List<TreeThread> threads = new ArrayList<TreeThread>(threadCount);
    for (int i = 0; i < threadCount; i++) {
      TreeThread th = new TreeThread(root, endTime, simsPerPlayout, virtualLossOn);
      // set the priority of the thread to the highest possible value as, during the
      // search phase, the other threads aren't as important
      th.setPriority(Thread.MAX_PRIORITY);
      threads.add(th);
    }

    for (Thread th : threads) {
      th.start();
    }

    for (Thread th : threads) {
      try {
        long timeRemaining = endTime - System.nanoTime();
        if (timeRemaining < 0) {
          Log.warn(
              "MctsTree",
              "Not all TreeThreads returned in time, continuing with final selection phase");
          break;
        }
        th.join();
      } catch (InterruptedException e) {
        Log.warn(
            "MctsTree",
            "Master thread interrupted while waiting for workers to finish, continuing with final"
                + " selection phase");
      }
    }

    // Set the current thread to the highest possible priority as we want to ensure
    // a move is returned in time
    Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

    N finalChoice = finalSelection.select(root, zobrist);

    // print some statistics via a thread to decrease the time it takes to return
    // the move
    new Thread(
            () -> {
              Thread.currentThread()
                  .setPriority(Thread.MIN_PRIORITY); // Set lower priority for thread as we want to
              // return the move as soon as possible
              logTreeStatistics(root, finalChoice, threads);
            })
        .start();

    return finalChoice;
  }

  /**
   * This method prints some post-search statistics.
   *
   * @param root
   * @param finalChoice
   * @param threads
   */
  public void logTreeStatistics(N root, N finalChoice, List<TreeThread> threads) {

    printTreeOverview(root, 10);
    printPossibleMoves(root, playerId);
    printTree(root, 3);

    Action bestMove = finalChoice.getPrevAction();

    if (bestMove != null) {
      Log.debug(
          "MctsTree",
          "Best action found: "
              + bestMove.toString()
              + " "
              + Math.round(finalChoice.getScore() * 10_000.0) / 100.0
              + "%");
    } else {
      Log.debug("MctsTree", "No best action found.");
    }

    int totalPlayoutCount = 0;
    for (Thread th : threads) {
      int currentThreadPlayoutCount = ((TreeThread) th).getPlayoutCount();
      totalPlayoutCount += currentThreadPlayoutCount;
      Log.debug(
          "Number of playouts over thread with id "
              + th.getId()
              + " is: "
              + currentThreadPlayoutCount);
    }
    Log.debug("Total number of playouts over all threads: " + totalPlayoutCount);
    totalPlayoutsOverGame += totalPlayoutCount;
    numberTurns += 1;
    Log.debug("numTurns: " + numberTurns);
    Log.debug(
        "Average number of playouts per turn to this point in the game: "
            + (totalPlayoutsOverGame / numberTurns));

    Log.debug("Best Move: " + bestMove + "\n===================");
  }

  /**
   * Print the TreeEngine up to the specified depth. This generates excessive printout.
   *
   * @param root The root of the TreeEngine.
   * @param maxDepth Depth at which to cut off the TreeEngine printout.
   */
  public static <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>>
      void printTree(N root, int maxDepth) {
    if (!Log.TRACE) {
      return;
    }
    StringBuilder sb = new StringBuilder();
    sb.append("\n===================");
    sb.append("\nCurrent Search Tree:");
    sb.append("\n===================");

    Queue<N> list = new LinkedList<>();
    list.add(root);
    while (!list.isEmpty()) {
      N n = list.poll();

      if (n.getDepth() >= maxDepth) {
        break;
      }

      sb.append("\n").append(n);
      sb.append("\nDepth: ").append(n.getDepth());

      // print board
      sb.append("\nBoard state:");
      sb.append("\n").append(n.getState(false).getPretty());

      list.addAll(n.getChildren());
    }
    sb.append("\n===================");
    Log.trace("MCTS Tree", sb.toString());
  }

  /**
   * Helper method to nicely print the first level of the game TreeEngine.
   *
   * @param root The root of the TreeEngine. All moves leading out from this TreeEngine will be
   *     printed.
   */
  public static <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>>
      void printPossibleMoves(N root, int playerId) {
    if (Log.DEBUG) {
      root.logPossibleMoves();
    }
  }

  /**
   * Prints an overview over the amount of nodes in the search TreeEngine to the specified depth.
   *
   * @param root The root of the TreeEngine.
   * @param maxDepth Depth at which to cut off the TreeEngine overview.
   */
  public static <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>>
      void printTreeOverview(N root, int maxDepth) {
    if (!Log.DEBUG) {
      return;
    }

    StringBuilder sb = new StringBuilder();

    sb.append("\n===================");
    sb.append("\nCurrent search TreeEngine overview:");
    sb.append("\n===================");

    int[] count = new int[maxDepth];

    Stack<N> stack = new Stack<>();
    stack.add(root);
    while (!stack.isEmpty()) {
      N n = stack.pop();

      if (n.getDepth() >= maxDepth) {
        // don't add children to stack if we're at/past the max depth
        continue;
      }

      count[n.getDepth()]++;
      stack.addAll(n.getChildren());
    }

    for (int i = 0; i < count.length; i++) {
      sb.append("\n\tdepth ").append(i).append(": ").append(count[i]).append(" nodes");
    }

    sb.append("\n===================");

    Log.debug("MCTS Tree Overview", sb.toString());
  }

  /**
   * The TreeEngine thread that performs MCTS on the shared TreeEngine, This forms part of the
   * template operation that drives the TreeEngine parallelisation.
   */
  private class TreeThread extends Thread {
    // root is shared
    private final N myRoot;
    private final long myEndTime;
    private int simsPerPlayout;
    private boolean virtualLossOn;
    int numberOfPlayouts;

    public TreeThread(N root, long endTime, int simsPerPlayout, boolean virtualLossOn) {
      this.myRoot = root;
      this.myEndTime = endTime;
      this.numberOfPlayouts = 0;
      this.simsPerPlayout = simsPerPlayout;
      this.virtualLossOn = virtualLossOn;
    }

    /**
     * @return number of playouts made by this thread
     */
    public int getPlayoutCount() {
      return numberOfPlayouts;
    }

    @Override
    public void run() {
      SimulationTuple resultsTuple;
      double[] results;
      List<Action> moves, fullMovePath;
      while (myEndTime > System.nanoTime()) {
        moves = new ArrayList<>();
        numberOfPlayouts++;
        N currentNode = myRoot;
        currentNode.writeLock();
        while (!logic.isTerminal(currentNode.getState(false))) {
          N previousNode = currentNode;
          currentNode = selection.select(previousNode);
          if (currentNode == null) {
            currentNode = previousNode;
            break;
          } else {
            moves.add(currentNode.getPrevAction());
            if (virtualLossOn) {
              currentNode.applyVirtualLoss();
            }
          }
          currentNode.writeLock();
          previousNode.writeUnlock();
        }
        N addedNode = expansion.expand(currentNode);
        // if the expansion phase returns the same node, the state is terminal/there are
        // no more unexplored
        // actions. In this case, we don't want to apply virtual loss twice.
        if (addedNode != currentNode) {
          moves.add(addedNode.getPrevAction());
          if (virtualLossOn) {
            addedNode.applyVirtualLoss();
          }
        }
        currentNode.writeUnlock();
        for (int sim = 0; sim < simsPerPlayout; sim++) {
          // check if the time has run out
          if (myEndTime < System.nanoTime()) {
            break;
          }
          S state = addedNode.getState(true);
          // set the simulation property if the state has a simulation mode, such as
          // GoBoard
          try {
            Method setSimulation = state.getClass().getMethod("setSimulation");
            try {
              setSimulation.invoke(state);
            } catch (IllegalAccessException
                | IllegalArgumentException
                | InvocationTargetException e) {
              throw new RuntimeException(e);
            }
          } catch (NoSuchMethodException | SecurityException e) {
            // do nothing
          }
          resultsTuple = simulation.simulate(moves, state);
          results = resultsTuple.getScores();
          // get the moves from the simulation
          fullMovePath = resultsTuple.getMoves();
          // verify that no aliasing took place
          if (fullMovePath == moves) {
            Log.error("TreeThread", "MoveList-alias detected in simulation phase");
            throw new RuntimeException("MoveList-alias detected in simulation phase");
          }
          // perform backpropagation step
          currentNode = addedNode;
          while (currentNode != myRoot) { // Pseudo: while (currentNode element of TreeEngine)
            if (sim == simsPerPlayout - 1 && virtualLossOn) {
              currentNode.restoreVirtualLoss();
            }
            for (BackpropagationThreadSafe<N> backpropEnhancement : backpropagationEnhancements) {
              backpropEnhancement.propagate(currentNode, results, fullMovePath);
            }
            currentNode = currentNode.getParent();
          }
          myRoot.incVisitCount();
        }
      }
    }
  }
}
