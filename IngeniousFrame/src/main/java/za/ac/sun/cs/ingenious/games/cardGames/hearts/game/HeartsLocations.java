package za.ac.sun.cs.ingenious.games.cardGames.hearts.game;

/**
 * Class HeartsLocations provides all relevant game locations.
 *
 * @author Joshua Wiebe
 */
public enum HeartsLocations {

  // List players first in order access player locations with index. E. g. for player 0:
  // UnoLocations.values()[index]
  // There are always exactly four players.
  PLAYER0,
  PLAYER1,
  PLAYER2,
  PLAYER3,
  PLAYER4,
  Player5,

  // Other locations
  DRAWPILE, // Initial position for all cards.
  UNKNOWNPLAYER, // If position of a card is unknown.
  TRICKPILE; // Pile where players put a card from their hand.
}
