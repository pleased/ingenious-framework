package za.ac.sun.cs.ingenious.games.mnk.engines.TreeEngine;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.message.MatchSettingsInitGameMessage;
import za.ac.sun.cs.ingenious.games.mnk.MNKFinalEvaluator;
import za.ac.sun.cs.ingenious.games.mnk.MNKLogic;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;

public abstract class MNKMctsEngine extends Engine {

  protected ZobristHashing zobrist;
  protected long currentBoardHash;
  protected MNKFinalEvaluator eval;
  protected MNKState currentState;
  protected MNKLogic logic;

  public MNKMctsEngine(EngineToServerConnection toServer) {
    super(toServer);
    this.logic = new MNKLogic();
    this.currentState = null;

    // super(toServer);
    // board = null;
    // logic = new MNKLogic();
    eval = new MNKFinalEvaluator();
  }

  @Override
  public void setZobrist(ZobristHashing zobristHashing) {
    zobrist = zobristHashing;
  }

  public void setCurrentBoardHash(long hash) {
    currentBoardHash = hash;
  }

  @Override
  public void receiveInitGameMessage(InitGameMessage a) {
    MatchSetting ms = ((MatchSettingsInitGameMessage) a).getSettings();
    int h, w, k;
    boolean p;
    try {
      h = ms.getSettingAsInt("mnk_height");
      w = ms.getSettingAsInt("mnk_width");
      k = ms.getSettingAsInt("mnk_k");
      p = ms.getSettingAsBoolean("perfectInformation");
      this.currentState = new MNKState(h, w, k, p, ms.getNumPlayers());
    } catch (MissingSettingException | IncorrectSettingTypeException e) {
      Log.error(
          "Could not read settings from received MatchSetting object, message: " + e.getMessage());
    }
  }

  @Override
  public void receivePlayedMoveMessage(PlayedMoveMessage a) {
    logic.makeMove(currentState, a.getMove());
  }

  @Override
  public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
    Log.info("Game terminated.");
    Log.info("Final scores for my terminal position:");
    double[] score = eval.getScore(currentState);
    for (int i = 0; i < score.length; i++) {
      Log.info("Player " + i + ": " + score[i]);
    }
    Log.info("Final state as seen by player " + this.playerID + ":");
    currentState.printPretty();
    toServer.closeConnection();
  }
}
