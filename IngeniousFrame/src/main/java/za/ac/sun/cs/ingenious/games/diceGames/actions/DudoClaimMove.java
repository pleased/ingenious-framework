package za.ac.sun.cs.ingenious.games.diceGames.actions;

import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;

/**
 * Wrapper class for a DiscreteAction that allows for the referee to tell the engines how many dice
 * each player has after a round
 */
public class DudoClaimMove extends DiscreteAction {

  private int[] numPlayerDice;

  public DudoClaimMove(DiscreteAction action, int[] numPlayerDice) {
    super(action.getPlayerID(), action.getActionNumber());
    this.numPlayerDice = numPlayerDice;
  }

  public int[] getNumPlayerDice() {
    return numPlayerDice;
  }
}
