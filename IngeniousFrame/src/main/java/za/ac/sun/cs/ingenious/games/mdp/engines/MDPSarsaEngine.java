package za.ac.sun.cs.ingenious.games.mdp.engines;

import java.util.List;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.mdp.MDPEngine;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;
import za.ac.sun.cs.ingenious.search.rl.sarsa.TabularSarsa;

/**
 * Tabular SARSA engine for MDP.
 *
 * @author Steffen Jacobs
 */
public class MDPSarsaEngine extends MDPEngine {
  private MDPState<Long> previousState = null;
  private Action previousAction = null;
  private TabularSarsa alg;

  public MDPSarsaEngine(EngineToServerConnection toServer) {
    super(toServer);
    alg = new TabularSarsa();
  }

  @Override
  public void setZobrist(ZobristHashing zobristHashing) {}

  @Override
  public String engineName() {
    return "MDPSarsaEngine";
  }

  @Override
  public void receivePlayedMoveMessage(PlayedMoveMessage a) {
    Move move = a.getMove();
    int player = move.getPlayerID();
    if (player == this.playerID) {
      previousState = (MDPState<Long>) state.deepCopy();
    }

    logic.makeMove(state, a.getMove());
  }

  @Override
  public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
    Action choice = null;
    List<Action> availableActions = logic.generateActions(state, playerID);

    choice = alg.chooseAction(state, availableActions);

    previousAction = choice;
    return new PlayActionMessage(choice);
  }

  @Override
  public void receiveRewardMessage(RewardMessage a) {
    ScalarReward reward = (ScalarReward) a.getReward();

    List<Action> stateActions = logic.generateActions(state, playerID);

    alg.update(previousState, previousAction, reward, state, stateActions);
  }

  @Override
  public void receiveMatchResetMessage(MatchResetMessage a) {
    super.receiveMatchResetMessage(a);
    alg.endEpisode();
  }

  @Override
  public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
    super.receiveGameTerminatedMessage(a);
    alg.printMetrics();
  }
}
