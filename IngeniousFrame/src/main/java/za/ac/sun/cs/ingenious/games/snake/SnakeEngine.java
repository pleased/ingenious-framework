package za.ac.sun.cs.ingenious.games.snake;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.games.snake.moves.SnakeSyncAction;
import za.ac.sun.cs.ingenious.games.snake.ui.SnakeFrame;

/**
 * Base engine for Snake that handles setting up the UI, moves received from the referee and
 *
 * @author Steffen Jacobs
 */
public abstract class SnakeEngine extends Engine {
  protected SnakeState state;
  protected SnakeLogic logic;
  protected SnakeFinalEvaluator eval;
  protected SnakeFrame frame;
  protected int logStep;

  public SnakeEngine(EngineToServerConnection toServer) {
    super(toServer);

    state = new SnakeState();
    logic = new SnakeLogic();
    eval = new SnakeFinalEvaluator();
    frame = new SnakeFrame();
    frame.getPanel().updateUI(state);
    logStep = 0;
    frame.getPanel().putScreenshotToState(state);
    frame.getPanel().putScreenshotToState(state);
  }

  @Override
  public void receiveMatchResetMessage(MatchResetMessage a) {
    super.receiveMatchResetMessage(a);
    state.reset();
    frame.getPanel().updateUI(state);
    frame.getPanel().putScreenshotToState(state);
    frame.getPanel().putScreenshotToState(state);
  }

  @Override
  public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
    Log.info("Final scores for my terminal position:");
    double[] score = eval.getScore(state);

    for (int i = 0; i < score.length; i++) {
      Log.info("Player " + i + ": " + score[i]);
    }

    toServer.closeConnection();
  }

  @Override
  public void receivePlayedMoveMessage(PlayedMoveMessage a) {
    logic.makeMove(state, a.getMove());
    frame.getPanel().updateUI(state);

    Move move = a.getMove();
    if (move instanceof SnakeSyncAction) {
      if (state.getImageBuffer().size() == 2) {
        // If this is the first state, fill the image buffer with the true initial state
        // screenshots.
        frame.getPanel().replaceAllScreenshotsInState(state);
      } else {
        /*
         * If this is not the first state, replace the most recent screenshot with a new screenshot that takes
         * into account after effects of actions.
         */
        frame.getPanel().replaceScreenshotInState(state);
      }
    } else {
      frame.getPanel().putScreenshotToState(state);
    }
  }
}
