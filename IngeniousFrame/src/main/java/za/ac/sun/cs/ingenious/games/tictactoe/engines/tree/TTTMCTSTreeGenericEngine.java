package za.ac.sun.cs.ingenious.games.tictactoe.engines.tree;

import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage.newBackpropagationAverage;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationRave.newBackpropagationRave;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationSimple.newBackpropagationSimple;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionContextual.newExpansionContextual;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionMast.newExpansionMast;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle.newExpansionSingle;
import static za.ac.sun.cs.ingenious.search.mcts.selection.FinalSelectionUct.newFinalSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionPW.newTreeSelectionProgressiveWidening;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionRave.newTreeSelectionRave;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUct.newTreeSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUctTuned.newTreeSelectionUctTuned;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationContextual.newSimulationContextual;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrFull.newSimulationLgrfFull;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrTree.newSimulationLgrTree;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrfFull.newSimulationLgrFull;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationMast.newSimulationMast;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom.newSimulationRandom;

import com.esotericsoftware.minlog.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.move.ForfeitAction;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.games.tictactoe.engines.TTTMCTSEngine;
import za.ac.sun.cs.ingenious.games.tictactoe.engines.TTTMctsFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.MctsAlgorithm.MctsTree;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.CMCTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.LGR.LGRTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST.MastTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.PW.MctsPWNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE.MctsRaveNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelection;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionFinal;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;

public class TTTMCTSTreeGenericEngine extends TTTMCTSEngine {

  MctsTree<TurnBasedSquareBoard, MctsNodeTreeParallel<TurnBasedSquareBoard>> mcts;

  ArrayList<BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>>>
      backpropagationEnhancements;

  Hashtable<String, MctsNodeExtensionParallelInterface> enhancementExtensionClasses;

  String enhancementConfigName;

  MatchSetting engineConfiguration;

  MastTable mastTable;
  LGRTable lgrTable;
  CMCTable cmcTable;

  protected final int TURN_LENGTH; // in milliseconds
  protected final int THREAD_COUNT;

  boolean recordMoves;

  /**
   * @param toServer An established connection to the GameServer
   */
  public TTTMCTSTreeGenericEngine(
      EngineToServerConnection toServer, String enhancementConfig, int threadCount, int turnLength)
      throws IOException, MissingSettingException, IncorrectSettingTypeException {
    super(toServer);

    backpropagationEnhancements = new ArrayList<>();
    enhancementExtensionClasses = new Hashtable<>();

    THREAD_COUNT = threadCount;
    TURN_LENGTH = turnLength;

    enhancementConfigName =
        enhancementConfig.substring(
            enhancementConfig.indexOf("Choice") + 6, enhancementConfig.length() - 5);

    engineConfiguration = new MatchSetting(enhancementConfig);

    recordMoves = true;

    // Selection strategy object
    TreeSelection<MctsNodeTreeParallel<TurnBasedSquareBoard>> selection = getSelectionClass();

    // Final Selection strategy object
    TreeSelectionFinal<MctsNodeTreeParallel<TurnBasedSquareBoard>> finalSelection =
        newFinalSelectionUct(logic);

    SimulationThreadSafe<TurnBasedSquareBoard> simulation = getSimulationClass();

    // Expansion strategy object
    ExpansionThreadSafe<
            MctsNodeTreeParallel<TurnBasedSquareBoard>, MctsNodeTreeParallel<TurnBasedSquareBoard>>
        expansion = getExpansionClass();

    // Backpropagation strategy objects
    for (String backprop : engineConfiguration.getSettingAsString("Backpropagation").split(",")) {
      BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>>
          backpropagationEnhancement = getBackpropagationClass(backprop);

      if (backpropagationEnhancement != null) {
        backpropagationEnhancements.add(backpropagationEnhancement);
      } else {
        Log.error(
            "TTTMCTSTreeGenericEngine", "Backpropagation class " + backprop + " does not exist");
        throw new RuntimeException("Backpropagation class " + backprop + " does not exist");
      }
    }

    this.mcts =
        new MctsTree<>(
            selection,
            expansion,
            simulation,
            backpropagationEnhancements,
            finalSelection,
            logic,
            THREAD_COUNT,
            playerID);
  }

  @Override
  public String engineName() {
    return "TTTMCTSTreeGenericEngine";
  }

  @Override
  public void receiveMatchResetMessage(MatchResetMessage a) {
    TTTMctsFinalEvaluator eval = new TTTMctsFinalEvaluator();
    Log.info("Game has terminated");
    Log.info("Final scores:");
    double[] score = eval.getScore(currentState);
    for (int i = 0; i < score.length; i++) {
      if (i == playerID) {
        Log.info(enhancementConfigName + ": " + score[i]);
      } else {
        Log.info("Opponent: " + score[i]);
      }
    }
    Log.info("Final state:");
    currentState.printPretty();
  }

  @Override
  /** Use MCTS to compute the best action to play on this player's next move in the game */
  public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {

    resetEnhancementMemory();

    MctsNodeTreeParallel<TurnBasedSquareBoard> root =
        new MctsNodeTreeParallel<TurnBasedSquareBoard>(
            currentState,
            null,
            null,
            new ArrayList<>(),
            logic,
            enhancementExtensionClasses,
            playerID);

    MctsNodeTreeParallel<TurnBasedSquareBoard> finalChoice =
        mcts.doSearch(
            root,
            TURN_LENGTH,
            zobrist,
            engineConfiguration.getSettingAsInt("simsPerPlayout", 1),
            engineConfiguration.getSettingAsBoolean("virtualLossEnabled", true));

    /*
     * commented out to increase playouts/s and reduce load
     * new Thread(() -> {
     * Thread.currentThread().setPriority(Thread.MIN_PRIORITY); // Set lower
     * priority for thread as we want to
     * // return the move as soon as possible
     * logTableStatistics(root);
     * }).start();
     */

    Action action = finalChoice.getPrevAction();
    if (action == null) {
      return new PlayActionMessage(new ForfeitAction((byte) playerID));
    }

    return new PlayActionMessage(action);
  }

  private void resetEnhancementMemory() {
    if (mastTable != null) {
      mastTable.resetVisitedMoves();
    }
    if (lgrTable != null) {
      lgrTable.resetVisitedMoves();
    }
    if (cmcTable != null) {
      cmcTable.resetVisitedMoves();
    }

    for (MctsNodeExtensionParallelInterface extension : enhancementExtensionClasses.values()) {
      extension.resetMemory();
    }

    // drop a hint to the garbage collector
    System.gc();
  }

  private void logTableStatistics(MctsNodeTreeParallel<TurnBasedSquareBoard> root) {
    if (mastTable != null) {
      mastTable.logTableStatistics(root);
    }
    if (lgrTable != null) {
      lgrTable.logTableStatistics(root);
    }
    if (cmcTable != null) {
      cmcTable.logTableStatistics(root);
    }
  }

  /**
   * Create object for the selection class specified in the .json file.
   *
   * @param selectionClass
   * @return
   */
  public TreeSelection<MctsNodeTreeParallel<TurnBasedSquareBoard>> getSelectionClass()
      throws MissingSettingException, IncorrectSettingTypeException {
    String selectionClass = engineConfiguration.getSettingAsString("Selection");
    if (selectionClass.equals("Uct")) {
      return newTreeSelectionUct(
          playerID,
          engineConfiguration.getSettingAsDouble("cValue"),
          engineConfiguration.getSettingAsBoolean("fpu_enabled", false),
          engineConfiguration.getSettingAsDouble("startPriorityValue", -1));
    } else if (selectionClass.equals("UctTuned")) {
      return newTreeSelectionUctTuned(
          playerID,
          engineConfiguration.getSettingAsDouble("cValue"),
          engineConfiguration.getSettingAsBoolean("fpu_enabled", false),
          engineConfiguration.getSettingAsDouble("startPriorityValue", -1));
    } else if (selectionClass.equals("Rave")) {
      return newTreeSelectionRave(
          playerID,
          engineConfiguration.getSettingAsDouble("cValue"),
          engineConfiguration.getSettingAsInt("vValue"),
          engineConfiguration.getSettingAsBoolean("fpu_enabled", false),
          engineConfiguration.getSettingAsDouble("startPriorityValue", -1));
    } else if (selectionClass.equals("PW")) {
      // TODO : Add configurable parameters for PW
      MctsPWNodeExtensionParallel parallelPWNodeExtension = new MctsPWNodeExtensionParallel();
      enhancementExtensionClasses.put("PW", parallelPWNodeExtension);
      return newTreeSelectionProgressiveWidening();
    } else {
      Log.error("Invalid selection strategy defined in the <...>EnhancementChoice.json file.");
      throw new RuntimeException(
          "Invalid selection strategy defined in the <...>EnhancementChoice.json file.");
    }
  }

  /**
   * Create object for the expansion class specified in the .json file.
   *
   * @param expansionClass
   * @param enhancements
   * @return
   */
  public ExpansionThreadSafe<
          MctsNodeTreeParallel<TurnBasedSquareBoard>, MctsNodeTreeParallel<TurnBasedSquareBoard>>
      getExpansionClass() throws MissingSettingException, IncorrectSettingTypeException {
    String expansionClass = engineConfiguration.getSettingAsString("Expansion");
    if (expansionClass.equals("Single")) {
      return newExpansionSingle(logic);
    } else if (expansionClass.equals("Mast")) {
      assert mastTable != null : "Mast table is null";
      return newExpansionMast(logic, mastTable);
    } else if (expansionClass.equals("Contextual")) {
      assert cmcTable != null : "CMC table is null";
      return newExpansionContextual(logic, cmcTable);
    } else {
      Log.error("Invalid expansion strategy defined in the <...>EnhancementChoice.json file.");
      throw new RuntimeException(
          "Invalid expansion strategy defined in the <...>EnhancementChoice.json file.");
    }
  }

  /**
   * Create object for the simulation class specified in the .json file.
   *
   * @param simulationClass
   * @param recordMoves
   * @param enhancements
   * @return
   */
  public SimulationThreadSafe<TurnBasedSquareBoard> getSimulationClass()
      throws MissingSettingException, IncorrectSettingTypeException {
    String simulationClass = engineConfiguration.getSettingAsString("Simulation");
    if (simulationClass.equals("Random")) {
      return newSimulationRandom(
          logic,
          new TTTMctsFinalEvaluator(),
          new PerfectInformationActionSensor<TurnBasedSquareBoard>(),
          recordMoves);
    } else if (simulationClass.equals("Mast")) {
      mastTable =
          new MastTable(
              engineConfiguration.getSettingAsDouble("gammaMast", 1.0),
              engineConfiguration.getSettingAsDouble("tau"));
      return newSimulationMast(
          logic,
          new TTTMctsFinalEvaluator(),
          new PerfectInformationActionSensor<TurnBasedSquareBoard>(),
          recordMoves,
          engineConfiguration.getSettingAsBoolean("treeOnly"),
          mastTable);
    } else if (simulationClass.equals("Contextual")) {
      cmcTable =
          new CMCTable(
              engineConfiguration.getSettingAsDouble("gammaCmc"),
              engineConfiguration.getSettingAsDouble("threshold"),
              engineConfiguration.getSettingAsInt("window"));
      return newSimulationContextual(
          logic,
          new TTTMctsFinalEvaluator(),
          new PerfectInformationActionSensor<TurnBasedSquareBoard>(),
          recordMoves,
          cmcTable);
    } else if (simulationClass.equals("LgrTree")) {
      lgrTable = new LGRTable();
      return newSimulationLgrTree(
          logic,
          new TTTMctsFinalEvaluator(),
          new PerfectInformationActionSensor<TurnBasedSquareBoard>(),
          recordMoves,
          lgrTable);
    } else if (simulationClass.equals("LgrFull")) {
      lgrTable = new LGRTable();
      return newSimulationLgrFull(
          logic,
          new TTTMctsFinalEvaluator(),
          new PerfectInformationActionSensor<TurnBasedSquareBoard>(),
          recordMoves,
          lgrTable);
    } else if (simulationClass.equals("LgrfTree")) {
      lgrTable = new LGRTable();
      return newSimulationLgrTree(
          logic,
          new TTTMctsFinalEvaluator(),
          new PerfectInformationActionSensor<TurnBasedSquareBoard>(),
          recordMoves,
          lgrTable);
    } else if (simulationClass.equals("LgrfFull")) {
      lgrTable = new LGRTable();
      return newSimulationLgrfFull(
          logic,
          new TTTMctsFinalEvaluator(),
          new PerfectInformationActionSensor<TurnBasedSquareBoard>(),
          recordMoves,
          lgrTable);
    } else {
      Log.error("Invalid simulation strategy defined in the <...>EnhancementChoice.json file.");
      throw new RuntimeException(
          "Invalid simulation strategy defined in the <...>EnhancementChoice.json file.");
    }
  }

  /**
   * Create object for the backpropagation class specified in the .json file.
   *
   * @param backpropagationClass
   * @return
   */
  public BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>>
      getBackpropagationClass(String backpropagationClass) {
    if (backpropagationClass.equals("Average")) {
      return newBackpropagationAverage();
    } else if (backpropagationClass.equals("Simple")) {
      return newBackpropagationSimple(playerID);
    } else if (backpropagationClass.equals("Rave")) {
      try {
        MctsRaveNodeExtensionParallel parallelRaveNodeExtension =
            new MctsRaveNodeExtensionParallel(engineConfiguration.getSettingAsInt("vValue"));
        enhancementExtensionClasses.put("Rave", parallelRaveNodeExtension);
        return newBackpropagationRave();
      } catch (MissingSettingException | IncorrectSettingTypeException e) {
        Log.error("Could not retrieve \"vValue\" setting from config file.");
      }
    }
    Log.error("Invalid backpropagation strategy defined in the <...>EnhancementChoice.json file.");
    throw new RuntimeException(
        "Invalid backpropagation strategy defined in the <...>EnhancementChoice.json file.");
  }
}
