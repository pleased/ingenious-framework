package za.ac.sun.cs.ingenious.core.data.structures.dag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import za.ac.sun.cs.ingenious.core.util.misc.Pair;

public class DAG<S, T> {
  static final ArrayList emptyList = new ArrayList();

  protected HashMap<S, DAGNode> nodes;
  protected int numEdges;
  // TODO: Implement a set of source nodes
  private List<S> sortedList;

  public DAG() {
    nodes = new HashMap<S, DAGNode>();
    numEdges = 0;
  }

  public boolean contains(S node) {
    return nodes.containsKey(node);
  }

  public int size() {
    return nodes.size();
  }

  public int getNumEdges() {
    return numEdges;
  }

  public void addNode(S node) {
    if (!nodes.containsKey(node)) {
      nodes.put(node, new DAGNode(node));

      resetIterator();
    }
  }

  static int count = 0;

  // True - Valid DAG, False - Invalid DAG
  public boolean validate() {
    // TODO: Check if there is a cycle

    return true;
  }

  public void addEdge(S parent, S child, T edge) {

    DAGNode parentNode = nodes.get(parent);
    if (parentNode == null) {
      parentNode = new DAGNode(parent);
      nodes.put(parent, parentNode);
    }
    DAGNode childNode = nodes.get(child);
    if (childNode == null) {
      childNode = new DAGNode(child);
      nodes.put(child, childNode);
    }

    parentNode.addChild(childNode, edge);
    numEdges++;

    resetIterator();
  }

  public List<S> getChildren(S parent) {
    DAGNode parentNode = nodes.get(parent);

    if (parentNode == null) {
      return emptyList;
    }

    return parentNode.getChildren().stream()
        .map((dagNode -> dagNode.state))
        .collect(Collectors.toList());
  }

  public List<S> getChildren(S parent, T edge) {
    DAGNode parentNode = nodes.get(parent);
    if (parentNode == null) {
      return emptyList;
    }

    return parentNode.getChildren(edge).stream()
        .map((node -> node.state))
        .collect(Collectors.toList());
  }

  public List<T> getEdges(S parent) {
    DAGNode parentNode = nodes.get(parent);

    if (parentNode == null) {
      return emptyList;
    }

    return parentNode.labeledChildren.stream().map((Pair::getKey)).collect(Collectors.toList());
  }

  // TODO - Issue #296: Store variable with node of whether it is a leaf or not as
  // logic may or may not be able to determine if it is terminal as the
  // information is imperfect
  // TODO - Issue #298: Extend Comparable to make topological sort deterministic.
  protected class DAGNode {
    private final S state;
    protected final List<Pair<T, DAGNode>> labeledChildren;

    private final HashSet<DAGNode> unlabeledChildren;

    protected DAGNode(S state) {
      this.state = state;

      unlabeledChildren = new HashSet<>();
      labeledChildren = new ArrayList<>();
    }

    private Set<DAGNode> getChildren(T edge) {
      Set<DAGNode> children = new HashSet<>();

      for (Pair<T, DAGNode> edgePair : labeledChildren) {
        if (edgePair.getKey().equals(edge)) {
          children.add(edgePair.getValue());
        }
      }

      return children;
    }

    private Set<DAGNode> getChildren() {
      Set<DAGNode> children =
          new HashSet<DAGNode>(unlabeledChildren.size() + labeledChildren.size());

      for (Pair<T, DAGNode> edgePair : labeledChildren) {
        children.add(edgePair.getValue());
      }
      children.addAll(unlabeledChildren);

      return children;
    }

    protected S getState() {
      return state;
    }

    // Adds a child node with either a labeled or unlabeled edge
    protected void addChild(DAGNode child, T edge) {
      if (edge == null) {
        unlabeledChildren.add(child);
      } else {
        unlabeledChildren.remove(child);

        labeledChildren.add(new Pair<T, DAGNode>(edge, child));
      }
    }

    public String toString() {

      StringBuilder stringBuilder = new StringBuilder();

      for (DAGNode child : getChildren()) {
        stringBuilder.append("\t" + child.state.hashCode() + ",");
      }

      return "State: ("
          + state.hashCode()
          + ")\n"
          + state.toString()
          + "children: {"
          + stringBuilder
          + "}\n";
    }
  }

  protected void resetIterator() {
    sortedList = null;
  }

  public List<S> getSortedList() {
    if (sortedList == null) {
      sortedList = _getSortedList();
    }

    return sortedList;
  }

  private List<S> _getSortedList() {
    List<S> sortedStates = new ArrayList<>();
    Map<DAGNode, Integer> inCount = new HashMap<>();
    LinkedList<DAGNode> zeroInStates = new LinkedList<>();

    for (DAGNode node : nodes.values()) {
      if (!inCount.containsKey(node)) {
        inCount.put(node, 0);
        zeroInStates.addFirst(node);
      }

      for (DAGNode child : node.getChildren()) {

        if (zeroInStates.contains(child)) {
          zeroInStates.remove(child);
        }

        if (!inCount.containsKey(child)) {
          inCount.put(child, 1);
        } else {
          inCount.put(child, inCount.get(child) + 1);
        }
      }
    }

    while (!zeroInStates.isEmpty()) {
      DAGNode node = zeroInStates.removeLast();

      for (DAGNode child : node.getChildren()) {
        inCount.put(child, inCount.get(child) - 1);

        if (inCount.get(child) == 0) {
          zeroInStates.addFirst(child);
        }
      }

      sortedStates.add(node.state);
    }

    return sortedStates;
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();

    for (DAGNode node : this.nodes.values()) {
      stringBuilder.append(node + "\n");
    }

    return stringBuilder.toString();
  }
}
