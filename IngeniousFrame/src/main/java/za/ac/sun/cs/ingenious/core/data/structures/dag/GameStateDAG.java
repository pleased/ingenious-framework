package za.ac.sun.cs.ingenious.core.data.structures.dag;

import com.esotericsoftware.minlog.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.exception.dag.StateNotFoundException;
import za.ac.sun.cs.ingenious.core.util.misc.Pair;

public class GameStateDAG<S extends GameState> extends DAG<S, Pair<Action, Double>> {
  public enum NodeType {
    PLAYER,
    CHANCE,
    TERMINAL,
  }

  public List<Integer> getActivePlayers(S node) {
    if (nodes.containsKey(node)) {
      return ((GameStateNode) nodes.get(node)).activePlayers;
    } else {
      return emptyList;
    }
  }

  public NodeType getType(S node) throws StateNotFoundException {
    if (nodes.containsKey(node)) {
      return ((GameStateNode) nodes.get(node)).type;
    } else {
      throw new StateNotFoundException("No node found");
    }
  }

  public void addNode(S node, List<Integer> activePlayers) {
    if (!nodes.containsKey(node)) {
      nodes.put(node, new GameStateNode(node, activePlayers));
    }
  }

  public void addNode(S node, List<Integer> activePlayers, NodeType type) {
    if (!nodes.containsKey(node)) {
      nodes.put(node, new GameStateNode(node, activePlayers, type));
    }
  }

  public void addEdge(S parent, S child, Action a, double prob) throws StateNotFoundException {
    if (nodes.get(parent) == null || nodes.get(child) == null) {
      throw new StateNotFoundException(
          "Nodes must exist in " + "GameStateDag to add action labeled edge");
    }

    this.addEdge(parent, child, new Pair<>(a, prob));
  }

  public List<S> getChildren(S parent, Action action) {
    List<Pair<Action, Double>> edgeList = getEdges(parent);

    if (edgeList.isEmpty()) {
      return emptyList;
    }

    DAGNode parentNode = nodes.get(parent);

    List<S> list = new ArrayList<>();
    for (Pair<Pair<Action, Double>, DAGNode> pair : parentNode.labeledChildren) {
      if (pair.getKey().getKey().equals(action)) {
        list.add(pair.getValue().getState());
      }
    }
    return list;
  }

  public List<Action> getPossibleActions(S state) {
    List<Pair<Action, Double>> edges = getEdges(state);

    return edges.stream().map(Pair::getKey).collect(Collectors.toList());
  }

  /** Implementation does not support non-unique labeled edges */
  public Map<Action, Double> getActionDistribution(S state) throws IllegalStateException {
    List<Pair<Action, Double>> edges = getEdges(state);

    try {
      Map<Action, Double> dist =
          edges.stream().collect(Collectors.toMap(Pair::getKey, Pair::getValue));
      return dist;
    } catch (IllegalStateException e) {
      Log.error(
          "GameStateDAG",
          "GameStateDAG does not support non-determinism. "
              + "Please ensure all labeled edges are unique");
      // e.printStackTrace();
      throw e;
    }
  }

  // TODO - Issue #297: Split into chance nodes and player nodes
  private class GameStateNode extends DAGNode {
    private final List<Integer> activePlayers;
    private NodeType type;

    protected GameStateNode(S state, List<Integer> activePlayers) {
      super(state);

      this.activePlayers = activePlayers;

      type = NodeType.PLAYER;
    }

    protected GameStateNode(S state, List<Integer> activePlayers, NodeType type) {
      super(state);

      this.activePlayers = activePlayers;

      this.type = type;
    }
  }
}
