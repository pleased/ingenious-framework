package za.ac.sun.cs.ingenious.games.ingenious.engines;

import java.util.ArrayList;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousMinMaxBoardInterface;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousRack;

public class TrailingColoursEval extends IngeniousEvaluator {
  public TrailingColoursEval() {}

  @Override
  public double evaluate(
      IngeniousScoreKeeper scores,
      IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> board,
      ArrayList<IngeniousRack> racks,
      int playerId) {
    int playerScore[] = scores.getScore(playerId).getScore();
    int[] newScore = scores.calculateScore(board);

    for (int i = 0; i < playerScore.length; i++) {
      playerScore[i] += newScore[i];
    }
    double ret = 0;
    boolean[] colours = new boolean[playerScore.length];
    for (int i = 0; i < scores.getNumPlayers(); i++) {
      if (i != playerId) {
        int opponentScore[] = scores.getScore(i).getScore();
        for (int j = 0; j < playerScore.length; j++) {
          if (playerScore[j] < opponentScore[j]) {
            colours[j] = true;
          }
        }
      }
    }
    for (int i = 0; i < playerScore.length; i++) {
      if (colours[i]) {
        ret--;
      } else {
        ret++;
      }
    }
    int sumpoints = 0;
    for (int i : newScore) {
      sumpoints += i;
    }

    sumpoints = sumpoints / (18 * board.getNumColours());

    return ret + sumpoints;
  }
}
