package za.ac.sun.cs.ingenious.core.util.search.mcts;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.search.TreeNode;

public interface MctsNode<S extends GameState, C, P> extends TreeNode<S, C, P> {

  public int getVisitCount();

  public void incVisitCount();

  public void decVisitCount();

  public int getChildVisitCount(C child);

  public double getChildValue(C child);

  public boolean unexploredActionsEmpty();

  public Action getPrevAction();

  public C getSelfAsChildType();
}
