package za.ac.sun.cs.ingenious.search.mcts.simulation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST.MastTable;

public class SimulationMast<S extends GameState> extends SimulationThreadSafe<S> {

  private ThreadLocalRandom randomGen = ThreadLocalRandom.current();
  private MastTable mastTable;
  MctsGameFinalEvaluator<S> evaluator;
  ActionSensor<S> obs;
  boolean recordMoves;

  // boolean that determines whether tree-only MAST or full MAST is used
  boolean treeOnly;

  /** constructor used by the game engine which specifies parameters */
  public SimulationMast(
      GameLogic<S> logic,
      MctsGameFinalEvaluator<S> evaluator,
      ActionSensor<S> obs,
      boolean recordMoves,
      boolean treeOnly,
      MastTable mastTable) {
    this.logic = logic;
    this.evaluator = evaluator;
    this.obs = obs;
    this.recordMoves = recordMoves || !treeOnly;
    this.treeOnly = treeOnly;
    this.mastTable = mastTable;
  }

  /**
   * The method that performs random simulation playout.
   *
   * @param state The game state from which to start the simulation playout.
   * @return The result scores
   */
  public SimulationTuple simulate(List<Action> selectionMoves, S state) {
    Action nextAction;
    List<Action> moves = new ArrayList<>(selectionMoves);

    while (!logic.isTerminal(state)) {
      for (int playerId : logic.getCurrentPlayersToAct(state)) {
        nextAction = mastTable.getMastAction(logic, state, playerId, randomGen);
        logic.makeMove(state, obs.fromPointOfView(nextAction, state, playerId));
        if (recordMoves) {
          moves.add(nextAction);
        }
      }
    }

    double[] scores = evaluator.getMctsScore(state);
    if (treeOnly && selectionMoves.size() >= 1) {
      mastTable.updateVisitedMovesTable(selectionMoves, scores);
    } else if (!treeOnly && moves.size() >= 1) {
      mastTable.updateVisitedMovesTable(moves, scores);
    }
    return new SimulationTuple(moves, scores);
  }

  public static <SS extends GameState> SimulationThreadSafe<SS> newSimulationMast(
      GameLogic<SS> logic,
      MctsGameFinalEvaluator<SS> evaluator,
      ActionSensor<SS> obs,
      boolean recordMoves,
      boolean treeOnly,
      MastTable mastTable) {
    return new SimulationMast<SS>(logic, evaluator, obs, recordMoves, treeOnly, mastTable);
  }
}
