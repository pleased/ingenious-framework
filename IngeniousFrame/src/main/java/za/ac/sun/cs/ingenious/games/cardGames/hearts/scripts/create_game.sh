#!/bin/bash

# creates a new Uno game with the default configuration
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar ../../../../../../../../../../../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "HeartsStub.json" -game "Hearts" -lobby "mylobby" -players 4
