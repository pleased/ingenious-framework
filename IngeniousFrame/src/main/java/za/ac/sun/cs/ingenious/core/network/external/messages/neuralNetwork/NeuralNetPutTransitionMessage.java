package za.ac.sun.cs.ingenious.core.network.external.messages.neuralNetwork;

import java.util.HashMap;
import java.util.Map;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalStateMatrix;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

/**
 * Used to communicate a (S,A,R,S') transition to DQN residing in the Python application.
 *
 * @author Steffen Jacobs
 */
public class NeuralNetPutTransitionMessage extends ExternalMessage {
  Map<String, Object> payload;

  public NeuralNetPutTransitionMessage(
      double[][][] state, int action, double reward, double[][][] nextState) {
    super(ExternalMessageType.NEURAL_NET_PUT_TRANSITION);

    payload = new HashMap<>();
    payload.put("state", new ExternalStateMatrix(state));
    payload.put("action", action);
    payload.put("reward", reward);
    payload.put("next_state", new ExternalStateMatrix(nextState));
  }
}
