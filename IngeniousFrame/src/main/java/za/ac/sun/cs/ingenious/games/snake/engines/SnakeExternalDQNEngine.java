package za.ac.sun.cs.ingenious.games.snake.engines;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.snake.SnakeEngine;
import za.ac.sun.cs.ingenious.games.snake.SnakeFeatureExtractor;
import za.ac.sun.cs.ingenious.games.snake.SnakeState;
import za.ac.sun.cs.ingenious.search.rl.qlearning.ExternalDQN;

/**
 * External DQN engine for Snake.
 *
 * @author Steffen Jacobs
 */
public class SnakeExternalDQNEngine extends SnakeEngine {
  private final String NETWORK = "VanillaCNN";
  private final double ALPHA = 0.00025;
  private final double EPSILON = 1.0;
  private final double EPSILON_NO_TRAINING = 0.01;
  private final double EPSILON_STEP = 1.0 / 1000000;
  private final double EPSILON_MIN = 0.01;
  private final double GAMMA = 0.97;
  private final int BUFFER_MEMORY = 50000;
  private final int SAMPLE_SIZE = 32;
  private final int SAMPLE_THRESHOLD = 50000;
  private final int SAMPLE_FREQUENCY = 4;
  private final int TARGET_NETWORK_UPDATE_FREQUENCY = 10000 / 4;
  private final boolean DOUBLE_LEARNING = true;
  private final boolean PRIORITIZED_SAMPLING = true;
  private final double BUFFER_ALPHA = 0.5;

  private final int INSTRUMENTATION_UPDATE_FREQ = 1000;

  private SnakeState previousState = null;
  private Action previousAction = null;
  private ExternalDQN alg;
  private SnakeFeatureExtractor featureExtractor;
  private boolean enableTraining;

  private List<Action> actionsAvailable;

  public SnakeExternalDQNEngine(EngineToServerConnection toServer) {
    super(toServer);

    actionsAvailable =
        Arrays.asList(
            new IdleAction(playerID),
            new CompassDirectionAction(playerID, CompassDirection.N),
            new CompassDirectionAction(playerID, CompassDirection.E),
            new CompassDirectionAction(playerID, CompassDirection.S),
            new CompassDirectionAction(playerID, CompassDirection.W));

    featureExtractor = new SnakeFeatureExtractor();
    enableTraining = false;
  }

  @Override
  public void receiveInitGameMessage(InitGameMessage a) {
    super.receiveInitGameMessage(a);

    if (alg == null) {
      try {
        // Generate sample feature matrices to get dimensions to initialize DQN.
        double[][][] sampleChannelSet = featureExtractor.buildTensor(state, null);

        alg =
            new ExternalDQN(
                "SnakeExternalDQNEngine_" + playerID,
                enableTraining,
                logic,
                featureExtractor,
                NETWORK,
                sampleChannelSet.length,
                sampleChannelSet[0].length,
                sampleChannelSet[0][0].length,
                actionsAvailable,
                ALPHA,
                EPSILON,
                EPSILON_STEP,
                EPSILON_MIN,
                GAMMA,
                BUFFER_MEMORY,
                SAMPLE_SIZE,
                SAMPLE_THRESHOLD,
                SAMPLE_FREQUENCY,
                TARGET_NETWORK_UPDATE_FREQUENCY,
                DOUBLE_LEARNING,
                PRIORITIZED_SAMPLING,
                BUFFER_ALPHA);

        alg.setEpsilonNoTraining(EPSILON_NO_TRAINING);

        alg.setChartUpdateFreq(INSTRUMENTATION_UPDATE_FREQ);
        alg.setLogFileUpdateFreq(INSTRUMENTATION_UPDATE_FREQ);

        alg.displayChart();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void setZobrist(ZobristHashing zobristHashing) {}

  @Override
  public String engineName() {
    return "SnakeExternalDQNEngine";
  }

  @Override
  public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
    Action choice = null;
    List<Action> availableActions = logic.generateActions(state, playerID);

    choice = alg.chooseAction(state, availableActions);

    // If the agent chooses an action that is not applicable to the state, ignore the action choice
    // and use an IdleAction.
    if (!availableActions.contains(choice)) {
      choice = new IdleAction(playerID);
    }

    previousAction = choice.deepCopy();
    previousState = state.deepCopy();

    return new PlayActionMessage(choice);
  }

  @Override
  public void receiveRewardMessage(RewardMessage a) {
    super.receiveRewardMessage(a);
    ScalarReward reward = (ScalarReward) a.getReward();

    alg.update(previousState, previousAction, reward, state);

    alg.notifyScore(state.getScore());

    if (alg.getTotalSteps() % INSTRUMENTATION_UPDATE_FREQ == 0) alg.printMetrics();
  }

  @Override
  public void receiveMatchResetMessage(MatchResetMessage a) {
    super.receiveMatchResetMessage(a);
    alg.endEpisode();
  }

  @Override
  public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
    super.receiveGameTerminatedMessage(a);

    alg.printMetrics();
    alg.close();
  }

  public void setTraining() {
    this.enableTraining = true;
  }
}
