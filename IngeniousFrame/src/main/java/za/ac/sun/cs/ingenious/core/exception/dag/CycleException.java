package za.ac.sun.cs.ingenious.core.exception.dag;

public class CycleException extends Exception {
  public CycleException(String msg) {
    super(msg);
  }
}
