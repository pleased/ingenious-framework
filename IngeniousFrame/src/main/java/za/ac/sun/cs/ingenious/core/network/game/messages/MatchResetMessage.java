package za.ac.sun.cs.ingenious.core.network.game.messages;

import za.ac.sun.cs.ingenious.core.network.Message;

/**
 * This message will be sent by the server to the players after the end of a match. This is useful
 * for engines using algorithms that contain logic between the end of a match and the start of the
 * next.
 *
 * @author Steffen Jacobs
 */
public class MatchResetMessage extends Message {}
