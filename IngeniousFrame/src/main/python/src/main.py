import json
import sys

from src.core.network.ingenious_connection import IngeniousConnection
from src.core.network.message.ingenious_message import IngeniousMessage


def main():
    """
    Main method that initializes the connection with the framework and begins listening for messages.
    """
    port = 9999

    if len(sys.argv) > 1:
        port = int(sys.argv[1])

    connection = IngeniousConnection(port=port)
    connection.listen()


if __name__ == '__main__':
    main()
