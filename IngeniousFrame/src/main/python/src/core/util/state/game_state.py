from src.core.util.state.state_feature_list import StateFeatureList
from src.core.util.state.state_matrix import StateMatrix
from src.core.util.state.state_type import StateType
from src.core.util.state.two_dimensional_board import TwoDimensionalBoard


class GameState:
    """
    Primarily meant as a static class containing helper methods that relate to all game states, such as determining the
    exact type of game state and object represents and creating an instance of this sub type.
    """

    @staticmethod
    def create(raw_state):
        if raw_state is None:
            return None
        if raw_state['type'] == StateType.TWO_DIMENSIONAL_BOARD.value:
            return TwoDimensionalBoard(raw_state['data'])
        elif raw_state['type'] == StateType.STATE_FEATURES.value:
            return StateFeatureList(raw_state['data'])
        elif raw_state['type'] == StateType.STATE_MATRIX.value:
            if 'data' not in raw_state:
                return StateMatrix(None)
            return StateMatrix(raw_state['data'])

        return None
