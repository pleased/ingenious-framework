class StateFeatureList(object):
    """
    Class used for a list of linear feature sets i.e. multiple feature sets that represent multiple states.
    """
    def __init__(self, data: [[float]]):
        self.feature_sets = data
