import enum


class StateType(enum.Enum):
    """
    Constants used to determine the type of game state that came from a payload, in order to wrap it in a class of the
    correct type.
    """
    TWO_DIMENSIONAL_BOARD = "TurnBased2DBoard"
    STATE_FEATURES = "LinearFeatures"
    STATE_MATRIX = "StateMatrix"
