import json


class CustomJsonEncoder(json.JSONEncoder):
    """
    Extends `json.JSONEncoder` with an implementation that encodes using `to_dict` by default.
    """

    def default(self, obj):
        if hasattr(obj, 'to_dict'):
            return obj.to_dict()
        else:
            return json.JSONEncoder.default(self, obj)
