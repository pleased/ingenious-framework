import socket
import time

from src.algorithm.dqn import dqn, DQN
from src.core.network.message.ingenious_message import IngeniousMessage
from src.core.network.message.type.message_type import MessageType
from src.core.network.serializer.json_serializer import JsonSerializer
from src.core.util.state.game_state import GameState
from src.core.util.transition.transition import Transition
from src.policy.dqn.dqn_network import DQNNetwork
from src.policy.linear_methods.linear_function_approximation import linear_function_approximation
from src.policy.tabular.q_table import q_table


class IngeniousConnection:
    """
    Establishes a connection with the Ingenious Framework. Listens on all messages received from the framework and based
    on the message, performs the necessary functions and sends a response back to the framework.
    """

    # Time in milliseconds before timing out when establishing a connection.
    TIME_OUT = 20000

    # Time in milliseconds between connection attempts.
    RETRY_INTERVAL = 1000

    def __init__(self, host='localhost', port=9999):
        self.serializer = JsonSerializer()
        self.host = host
        self.port = port
        # self.buffer_size = 2 ** 12
        self.buffer_size = 2 ** 10

        self.server_connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.__connect()
        print(f"Connected to Ingenious Framework on port {port}.")

    def __connect(self) -> None:
        """
        Attempts to connect to the server framework. This continues until timeout is reached.
        """
        success = False
        current_time = 0
        while current_time < IngeniousConnection.TIME_OUT:
            try:
                self.server_connection.connect((self.host, self.port))
                success = True
                break
            except socket.error as error:
                if isinstance(error, ConnectionRefusedError):
                    print("Retrying connection...")
                    current_time += IngeniousConnection.RETRY_INTERVAL
                    time.sleep(IngeniousConnection.RETRY_INTERVAL / 1000)
                else:
                    raise error

        if not success:
            raise Exception("A connection to the Ingenious Framework could not be made.")

    def __receive_message(self):
        """
        Waits on and receives the next message.
        """
        data = self.server_connection.recv(self.buffer_size)
        data_utf = data.decode(encoding="UTF-8").strip().split("\r\n")

        if len(data_utf) == 1 and data_utf[0] == "":
            return None

        message_raw = data_utf[0].split("::")[1]
        length_left = int(data_utf[0].split("::")[0]) - len(message_raw)

        message_full = message_raw

        while length_left > 0:
            data = self.server_connection.recv(min(self.buffer_size, length_left))
            data_utf = data.decode(encoding="UTF-8").strip().split("\r\n")

            message_raw = data_utf[0]
            message_full = message_full + message_raw
            length_left = length_left - len(message_raw)

        return self.serializer.decode_message(message_full)

    def __send_message(self, message: IngeniousMessage = IngeniousMessage(type=MessageType.ACK)) -> None:
        data = self.serializer.encode_message(message)
        self.server_connection.send(bytes(data + "\r\n", "UTF-8"))

    def listen(self):
        """
        Receives & processes all incoming messages.

        - If an empty message is received, this is indicative of a terminated connection on the other side, so then the
        connection is closed.
        - When receiving an EXIT message from the framework, the connection on this side is also closed.
        - Of the various message types, the necessary logic is executed and a response is sent back to the framework.

        There might be a better way to compartmentalize this logic.

        """
        while True:
            # Blocking call to receive and construct an IngeniousMessage.
            message = self.__receive_message()

            if message is None:
                pass
                # print(
                #     "Received empty message. This might mean the connection is already shut down. Closing connection.")
                # break

            elif message.type == MessageType.EXIT:
                print("Received exit message. Closing connection.")
                break

            # Request for actions with corresponding values given a state in the payload.
            elif message.type == MessageType.REQ_TABLE_STATE_ACTIONS:
                state_features = GameState.create(message.payload)

                actions = q_table.get_state_actions(state_features)

                response = IngeniousMessage(type=MessageType.TABLE_STATE_ACTIONS, payload=actions)
                self.__send_message(response)

            # Request for a specific state-action value, given a state and an action in the payload.
            elif message.type == MessageType.REQ_TABLE_STATE_ACTION_VALUE:
                state_features = GameState.create(message.payload['state'])
                action = message.payload['action']

                value = q_table.get_state_action_value(state_features, action)

                response = IngeniousMessage(type=MessageType.TABLE_STATE_ACTION_VALUE, payload=value)
                self.__send_message(response)

            # New value specified for the given state and action.
            elif message.type == MessageType.SET_TABLE_STATE_ACTION_VALUE:
                state_features = GameState.create(message.payload['state'])
                action = message.payload['action']
                value = message.payload['value']

                q_table.set_state_action_value(state_features, action, value)

                response = IngeniousMessage(type=MessageType.ACK)
                self.__send_message(response)

            elif message.type == MessageType.REQ_LINEAR_FEATURE_VALUES:
                # Map payload states into feature objects.
                states_features = GameState.create(message.payload)

                # Map feature objects into values.
                values = map(linear_function_approximation.get_value, states_features.feature_sets)

                response = IngeniousMessage(type=MessageType.LINEAR_FEATURE_VALUES, payload=list(values))
                self.__send_message(response)

            elif message.type == MessageType.INIT_LINEAR_FEATURE_APPROXIMATION:
                feature_count = message.payload

                linear_function_approximation.init_linear(feature_count)

                response = IngeniousMessage(type=MessageType.ACK)
                self.__send_message(response)

            elif message.type == MessageType.TRAIN_LINEAR_FEATURES:
                state_features = GameState.create(message.payload['state'])
                value = message.payload['value']

                # Map feature objects into values.
                linear_function_approximation.train(state_features.feature_sets[0], value)

                response = IngeniousMessage(type=MessageType.ACK)
                self.__send_message(response)

            elif message.type == MessageType.REQ_NEURAL_NET_ACTION_VALUES:
                state = GameState.create(message.payload)

                q_values_dict = dqn.get_action_values_dict(state.matrix)

                response = IngeniousMessage(type=MessageType.NEURAL_NET_ACTION_VALUES, payload=q_values_dict)
                self.__send_message(response)

            elif message.type == MessageType.INIT_NEURAL_NET:
                player_id = message.payload['player_id']
                network = message.payload['network']
                num_channels = message.payload['num_channels']
                height = message.payload['height']
                width = message.payload['width']
                action_count = message.payload['action_count']
                buffer_memory = message.payload['buffer_memory']
                sample_size = message.payload['sample_size']
                gamma = message.payload['gamma']
                sample_threshold = message.payload['sample_threshold']
                sample_freq = message.payload['sample_freq']
                double_dqn = message.payload['double_dqn']
                prioritized = message.payload['prioritized']
                buffer_alpha = message.payload['buffer_alpha']
                learning_rate = message.payload['learning_rate']
                target_update_freq = message.payload['target_update_freq']

                dqn.init(
                    player_id,
                    network,
                    num_channels,
                    height,
                    width,
                    action_count,
                    buffer_memory,
                    sample_size,
                    gamma,
                    sample_threshold,
                    sample_freq,
                    double_dqn,
                    prioritized,
                    buffer_alpha,
                    learning_rate,
                    target_update_freq,
                )

                response = IngeniousMessage(type=MessageType.ACK)
                self.__send_message(response)

            elif message.type == MessageType.NEURAL_NET_PUT_TRANSITION:
                state = GameState.create(message.payload['state'])
                action = message.payload['action']
                reward = message.payload['reward']
                next_state = GameState.create(message.payload['next_state'])

                # Move the next states to
                if next_state.matrix is not None and next_state.matrix.is_cuda:
                    next_state.matrix = next_state.matrix.cpu()

                dqn.train(Transition(state.matrix, action, reward, next_state.matrix))

                response = IngeniousMessage(type=MessageType.ACK)
                self.__send_message(response)

            elif message.type == MessageType.NEURAL_NET_CREATE_BACKUP:
                dqn.backup_network(message.payload)

                response = IngeniousMessage(type=MessageType.ACK)
                self.__send_message(response)

            else:
                pass

        self.server_connection.close()
        print(q_table.table)
        print("Key count: " + str(len(q_table.table.keys())))
        if linear_function_approximation.model is not None:
            print("LFA Model: ")
            print(str(list(linear_function_approximation.model.parameters())))

        print("Key count: " + str(len(q_table.table.keys())))
        q_table.save()

        if dqn is not None and dqn.net is not None:
            print("Closing summary writer of DQN.")
            dqn.net.summary_writer.close()
