import pickle


class QTable(object):
    """
    Implements a simple Q table that maintains state-action pairs and their corresponding expected reward values.
    """
    default_q_value = 0.0
    file = "q_table.policy"

    def __init__(self):
        try:
            self.table = pickle.load(open(self.file, "rb"))
            print("Loaded existing policy with " + str(len(self.table.keys())) + " states.")
        except:
            print("Could not load " + self.file + ". Creating new.")
            self.table = {}

        pass

    def get_state_actions(self, state):
        return self.table.setdefault(state, {})

    def get_state_action_value(self, state, action: int) -> float:
        return self.table.setdefault(state, {}).setdefault(action, QTable.default_q_value)

    def get_action_values(self, state):
        return self.table.setdefault(state, {})

    def set_state_action_value(self, state, action: int, value: float):
        self.table.setdefault(state, {})[action] = value

    def save(self):
        pickle.dump(self.table, open(self.file, "wb"))


q_table = QTable()
