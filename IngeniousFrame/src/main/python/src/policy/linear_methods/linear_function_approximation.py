import torch
from torch import nn, optim


class LinearFunctionApproximation(object):
    """
    Class that handles training a single layer linear network using PyTorch.
    """

    learning_rate = 0.1
    loss_function = nn.MSELoss(reduction='mean')
    use_bias = False

    def __init__(self):
        self.model: nn.Module | None = None
        self.optimizer = None

    def init_linear(self, feature_count: int):
        self.model = nn.Linear(feature_count, 1, bias=self.use_bias)

        # with torch.no_grad():
        #     self.model.weight.fill_(0.0)

        self.optimizer = optim.SGD(self.model.parameters(), lr=self.learning_rate)

        print("Model initialized.")
        print(self.model)
        print("Optimizer initialized.")
        print(self.optimizer)

    def train(self, features: [float], value: float):
        if self.model is None or self.optimizer is None:
            print("LinearFunctionApproximation: Train was invoked before model initialization.")
            return

        # print("Train. " + str(features) + " Value: " + str(value))

        features_tensor = torch.tensor(features, dtype=torch.float)
        value_tensor = torch.tensor([value], dtype=torch.float)

        self.model.train()

        value_approx = self.model(features_tensor)
        loss = self.loss_function(value_tensor, value_approx)
        loss = torch.clamp(loss, min=-1, max=1)

        # print("Loss: " + str(loss) + " Value: " + str(value_tensor) + " Value Approx: " + str(value_approx))

        loss.backward()
        self.optimizer.step()
        self.optimizer.zero_grad()

        # print(self.model.state_dict())

    def get_value(self, features: [float]):
        features_tensor = torch.tensor(features, dtype=torch.float)
        feature_value = self.model(features_tensor).tolist()[0]

        # print("Returning for: " + str(features) + " value: " + str(feature_value))
        return feature_value


linear_function_approximation = LinearFunctionApproximation()


