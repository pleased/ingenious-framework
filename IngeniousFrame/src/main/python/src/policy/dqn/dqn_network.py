import math
import uuid
import os

import numpy as np
import torch
from torch import nn, optim
from torch.utils.tensorboard import SummaryWriter

from src.core.util.constants import Constants
from src.policy.pytorch_models.grid_world_nn import GridWorldNN
from src.policy.pytorch_models.mnk_nn import MnkNN
from src.policy.pytorch_models.standard_cnn import StandardCNN


class DQNNetwork(object):
    """
    Class that handles the main and target networks, descent steps, forward passes of the network and saving / hard
    syncing the networks.
    """

    loss_function = nn.SmoothL1Loss()
    update_count = 0

    total_loss = 0.0
    loss_log_freq = 1

    default_directory = f"{os.getcwd()}/src/backup_networks/"

    def __init__(
            self,
            player_id: str,
            network: str,
            num_channels: int,
            height: int,
            width: int,
            num_features_out: int,
            learning_rate: float,
            target_update_freq: int,
    ):
        self.player_id = player_id
        self.model: nn.Module | None = None
        self.target_model: nn.Module | None = None
        self.optimizer = None
        self.summary_writer = None
        self.learning_rate = learning_rate
        self.target_update_freq = target_update_freq

        self.init_network(network, num_channels, height, width, num_features_out)
        self.init_tensorboard(player_id)

    def init_tensorboard(self, player_id: str):
        tensorboard_run_name = f"dqn-{str(uuid.uuid4())}-{player_id}"
        self.summary_writer = SummaryWriter(f"{os.getcwd()}/runs/{tensorboard_run_name}")
        print(f"Created TensorBoard run: {tensorboard_run_name}")

    def init_network(self, network: str, num_channels: int, height: int, width: int, num_features_out: int):
        if self.model is None and self.target_model is None:
            print("\nInitializing network with params:")
            print(f"\tnetwork:\t\t\t{network}")
            print(f"\tnum_channels:\t\t{num_channels}")
            print(f"\theight:\t\t\t\t{height}")
            print(f"\twidth:\t\t\t\t{width}")
            print(f"\tnum_features_out:\t{num_features_out}")

            self.model = self.create_network(network, num_channels, height, width, num_features_out)
            self.target_model = self.create_network(network, num_channels, height, width, num_features_out)

            if torch.cuda.is_available():
                print("Using CUDA")
                self.model.cuda()
                self.target_model.cuda()

            try:
                self.model.load_state_dict(torch.load(f"{self.default_directory}{self.player_id}.pt"))
                self.target_model.load_state_dict(self.model.state_dict())
            except FileNotFoundError:
                print("No model found on disk.")

            self.optimizer = optim.RMSprop(self.model.parameters(), lr=self.learning_rate)

            print("Model initialized.")
            print(self.model)
            print("Optimizer initialized.")
            print(self.optimizer)
        else:
            print("Model initialization did not occur: Model was already loaded from file.")

    def create_network(self, network_name: str, num_channels: int, height: int, width: int, num_features_out: int):
        """
        Creates an instance of a PyTorch model, the type of which depending on the specified network name.
        :param network_name:
        :return:
        """

        if network_name == "StandardCNN":
            return StandardCNN(num_channels, height, width, num_features_out)
        if network_name == "GridWorldNN":
            return GridWorldNN(num_channels, height, width, num_features_out)
        if network_name == "MnkNN":
            return MnkNN(num_channels, height, width, num_features_out)

        return StandardCNN(num_channels, height, width, num_features_out)

    def train(self, state_batch, action_number_batch, value_batch):
        """
        [batch_dim, state_dim1, state_dim2, ...]
        :param state_batch:
        :param action_number_batch:
        :param value_batch:
        :return:
        """
        if self.model is None or self.optimizer is None:
            print("Convolutional Network: Train was invoked before model initialization.")
            return

        self.update_count = self.update_count + 1

        features_tensor = state_batch
        action_number_tensor = torch.tensor(
            action_number_batch,
            dtype=torch.int64,
            device=Constants.device
        ).unsqueeze(1)
        value_tensor = torch.tensor(value_batch, dtype=torch.float, device=Constants.device).unsqueeze(1)

        # print(f"features_tensor: {(features_tensor.size())} {str(features_tensor.tolist())}")
        # print(f"action_number_tensor: {(action_number_tensor.size())} {str(action_number_tensor.tolist())}")
        # print(f"value_tensor: {(value_tensor.size())} {str(value_tensor.tolist())}")

        self.model.train()

        value_approx = self.model(features_tensor)

        # print(f"{str(self.update_count)} value_approx: {(value_approx.squeeze().size())} {str(value_approx.squeeze().tolist())}")
        if torch.isnan(value_approx).any():
            print(f"Nan detected for value_approx: "
                  f"{(value_approx.squeeze().size())} {str(value_approx.squeeze().tolist())}")

        action_value_approx = value_approx.squeeze().gather(1, action_number_tensor)

        # print(f"action_value_approx: {(action_value_approx.size())} {str(action_value_approx.tolist())}")
        # print(f"value_tensor: {value_tensor.size()} {str(value_tensor.tolist())}")

        loss = self.loss_function(value_tensor, action_value_approx)

        self.optimizer.zero_grad()
        loss.backward()
        # for param in self.model.parameters():
        #     param.grad.data.clamp_(-1, 1)
        self.optimizer.step()

        error = loss.item()
        self.total_loss += error

        if self.update_count % self.target_update_freq == 0:
            print("Network sync")
            print(
                f"{str(self.update_count)} value_approx: {(value_approx.squeeze().size())} {str(value_approx.squeeze().tolist())}")
            self.target_model.load_state_dict(self.model.state_dict())
            self.backup_network(self.player_id)

        # Log the average loss of the last `loss_log_freq` updates to TensorBoard.
        if self.update_count % self.loss_log_freq == 0:
            self.summary_writer.add_scalar('total_loss', self.total_loss / self.loss_log_freq, self.update_count)
            self.total_loss = 0

        return error

    def backup_network(self, filename):
        torch.save(self.model.state_dict(), f"{self.default_directory}{filename}.pt")
        print("Model saved.")

    def get_action_values(self, feature_sets):
        action_values = self.model(feature_sets.to(Constants.device))

        return action_values.squeeze().tolist()

    def get_target_action_values(self, feature_sets):
        action_values = self.target_model(feature_sets.to(Constants.device))

        return action_values.squeeze().tolist()
