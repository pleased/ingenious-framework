#/bin/bash
# Here we start two clients bob and alice
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client

# bob & alice are random engines.
# josh & michael are rule based engines.

#!/bin/bash
java --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
        -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client \
          -config "Uno.json" \
          -username "bob" &
java --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
        -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client \
          -config "Uno.json" \
          -username "alice"
#java --add-opens java.base/java.util=ALL-UNNAMED \
#        --add-opens java.desktop/java.awt=ALL-UNNAMED \
#        -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client \
#          -config "Uno.json" \
#          -username "josh" &
#java --add-opens java.base/java.util=ALL-UNNAMED \
#        --add-opens java.desktop/java.awt=ALL-UNNAMED \
#        -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client \
#          -config "Uno.json" \
#          -username "michael"
