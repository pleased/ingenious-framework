# Dudo

For the rules of Dudo, please see [this paper](http://cs.gettysburg.edu/~tneller/modelai/2013/cfr/cfr.pdf).

## Training scripts:

`train_cfr` - Learn a strategy profile for vanilla CFR or chance sampling CFR implementation

 - Use: `train_cfr.sh <iterations> [OPTION]...`
 - Optional arguments:
    + `[-s | --sides] <sides>`: Number of sides of a dice. Default = 6.
    + `[-d | --dice] <dice>`: Number of die each player receives. Default = 1.
    + `[-c | --chance] <samples>`: Number of chance samples considered at chance nodes. Results in chance sampling CFR. If not specified, vanilla CFR is used.
    + `[-e | --exploit] <file>`: Where to save exploitability per training iteration. If not specified, exploitability is not calculated.
    + `[-t | --time] <file>`: Where to save training iteration time.
    + `--load <file>`: Load pre-trained strategy profile.
    + `--save <file`: Where to save trained strategy profile.
    + `--print1`: Print player one's normalized strategy profile after training
    + `--print2`: Print player two's normalized strategy profile after training
    
<br>

`train_fsicfr` - Learn a strategy profile for FSICFR implementation

 - Use: `train_fsicfr.sh <iterations> [OPTION]...`
 - Optional arguments:
     + `[-s | --sides] <sides>`: Number of sides of a dice. Default = 6.
     + `[-d | --dice] <dice>`: Number of die each player receives. Default = 1.
     + `[-e | --exploit] <file>`: Where to save exploitability per training iteration. If not specified, exploitability is not calculated.
     + `[-t | --time] <file>`: Where to save training iteration time.
     + `[-m | --memory] <memory>`: Number of previous moves a player can recall.
     + `--load <file>`: Load pre-trained strategy profile.
     + `--save <file`: Where to save trained strategy profile.
     + `--print1`: Print player one's normalized strategy profile after training
     + `--print2`: Print player two's normalized strategy profile after training

## Ingenious framework game play scripts:

`start_server.sh` - Start the ingenious framework server

`create_game.sh default.json` - Start a game of Dudo. Game setup specified in `default.json`.

`connect_random.sh <name>` - Connect a random player to the server with a given name.

`connect_human.sh <name>` - Connect a human player to the server with a given name.

`connect_cfr.sh <name> [-enhancement <file>]` - Connect a CFR engine to the server with a given name. Training parameters can be specified in an enhancement file, e.g. `CFREnhancement.json`.

`connect_fsicfr.sh <name> [-enhancement <file>]` - Connect a FSICFR engine to the server with a given name. Training parameters can be specified in an enhancement file, e.g. `FSICFREnhancement.json`.