#!/bin/bash

if [ "$#" -eq 2 ]; 
then
    java --add-opens java.base/java.util=ALL-UNNAMED \
            --add-opens java.desktop/java.awt=ALL-UNNAMED \
                -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -game "DudoReferee" -engine "za.ac.sun.cs.ingenious.games.diceGames.dudo.engines.DudoCFREngine" -username $1 -enhancement "$2"
else
    java --add-opens java.base/java.util=ALL-UNNAMED \
            --add-opens java.desktop/java.awt=ALL-UNNAMED \
                -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -game "DudoReferee" -engine "za.ac.sun.cs.ingenious.games.diceGames.dudo.engines.DudoCFREngine" -username $1
fi
