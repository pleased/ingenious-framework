#!/bin/bash

java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar ../../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "gameboard.json" -game "mnk" -lobby "mylobby" -players 2 -port $1

