#!/bin/bash
# Here we start two clients bob and alice
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "bob" -engine "za.ac.sun.cs.ingenious.games.shadowtictactoe.STTTSOISMCTSEngine" -game "shadowtictactoe" -hostname localhost -port 61234 &
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "alice" -engine "za.ac.sun.cs.ingenious.games.shadowtictactoe.STTTRandomEngine" -game "shadowtictactoe" -hostname 127.0.0.1 -port 61234 &
