import json
import sys
import os

def main():
    with open("Othello.json", "r") as f: 
        data = json.load(f)

    filename = f"Othello_{int(sys.argv[1])}.json"

    directory = "gameConfigs"

    if not os.path.exists(directory):
        os.makedirs(directory)

    if not os.path.exists(filename):
        data["boardSize"] = int(sys.argv[1])
        with open(f"{directory}/{filename}", "w") as f:
            json.dump(data, f, indent=4)

if __name__ == '__main__':
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    main()