#!/bin/bash

# THIS SCRIPT IS USED IN MULTIPLE PYTHON SCRIPTS / SHELL SCRIPTS -- DO NOT EDIT
# 1 : number of game instances
# 2 : board size
# 3 : number of matches
# 4 : number of threads per player
# 5 : turnlength in milliseconds
# 6 : path to enhancement.json that's being tested
# 7 : name of the enhancement
# 8 : script id (required if multiple calls to this script occur concurrently to prevent overwriting file contents)

python3 updateBoardSize.py $2

# export JVM variable to prevent JVM from crashing
export LD_BIND_NOW=1

# Initialise games
for i in `seq 1 $1`
do
       if java --add-opens java.base/java.util=ALL-UNNAMED \
            --add-opens java.desktop/java.awt=ALL-UNNAMED \
             -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "gameConfigs/Othello_$2.json"\
              -lobby "OthelloLobby$i.$7.$8"\
              -numMatches $3 \
              -turnLength $5 ; then

       		java --add-opens java.base/java.util=ALL-UNNAMED \
            		--add-opens java.desktop/java.awt=ALL-UNNAMED \
            		-jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -config "gameConfigs/Othello_$2.json"\
              		-username "player1.$i.$7.$8" \
              		-threadCount $4 \
              		-turnLength $5 \
              		-enhancement "$6" \
              		-lobby "OthelloLobby$i.$7.$8" > playerLogs/$i.$7.$8.player1.log && echo "$i done" & \
			sleep 0.2s
       		java --add-opens java.base/java.util=ALL-UNNAMED \
            		--add-opens java.desktop/java.awt=ALL-UNNAMED \
            		-jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -config "gameConfigs/Othello_$2.json"\
              		-username "player2.$i.$7.$8" \
              		-threadCount 1 \
              		-turnLength $5 \
              		-enhancement "../MctsEnhancementConfigs/EnhancementChoiceVanilla.json" \
              		-lobby "OthelloLobby$i.$7.$8" > playerLogs/$i.$7.$8.player2.log && echo "$i done" &
	fi
	sleep 0.5s
done

wait

> results/OthelloResults$7.$8.txt

for i in `seq 1 $1`
do
       grep -A 2 "Final score" playerLogs/$i.$7.$8.player1.log >> results/OthelloResults$7.$8.txt
done

wait

python3 results.py results/OthelloResults$7.$8.txt $6 Opponent

