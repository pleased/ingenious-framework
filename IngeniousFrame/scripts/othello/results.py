import sys
import os

def main():
    filepath = sys.argv[1]

    if not os.path.isfile(filepath):
        print("File path {} does not exist. Exiting...".format(filepath))
        sys.exit()

    player1 = sys.argv[2]
    player1 = player1[player1.index("Choice") + 6 : -5]
    player2 = sys.argv[3]

    f = open(filepath)
    line = f.readline()
    p1_wins = 0.0
    game_count = 0
    while line:
        if "Final scores" in line:
            line = f.readline()
            if player1 in line:
                p1_score = float(line.split(f"{player1}: ")[-1])
                p2_score = float(f.readline().split(f"{player2}: ")[-1])
            else:
                p2_score = float(line.split(f"{player2}: ")[-1])
                p1_score = float(f.readline().split(f"{player1}: ")[-1])
            if p1_score > p2_score:
                p1_wins+=1
            elif p1_score == p2_score:
                p1_wins+=0.5
            game_count+=1
        line = f.readline()
    f.close()

    p2_wins = game_count - p1_wins

    print("RESULTS:")
    print(f"{player1} {p1_wins}/{game_count} {p1_wins*100/game_count}%")
    print(f"{player2} {p2_wins}/{game_count} {p2_wins*100/game_count}%")

if __name__ == '__main__':
    main()