import json
import subprocess
import os
import itertools

boardSizes = [7]
instanceCount = [24]
turnLengths = [1000]
enhancements = ["FPU"]
numMatches = 1

def update_json(field, value):
    with open('playoutStatistics.json', 'r') as f:
        data = json.load(f)
    data[field] = value
    with open('playoutStatistics.json', 'w') as f:
        json.dump(data, f, indent=4)

def json_contains_field(field):
    if (not os.path.exists('playoutStatistics.json')):
        with open('playoutStatistics.json', 'w') as f:
            json.dump({}, f, indent=4)
    with open('playoutStatistics.json', 'r') as f:
        data = json.load(f)
    return field in data.keys()

def main():
    for boardSize, instances, turnLength, enhancement in itertools.product(boardSizes, instanceCount, turnLengths, enhancements):
        playout(boardSize, instances, turnLength, enhancement)

def playout(boardSize, instances, turnLength, enhancement):

    if (json_contains_field(f'{enhancement}_{boardSize}_{instances}_{turnLength}')):
        print(f'{enhancement}_{boardSize}_{instances}_{turnLength} already exists')
        # ask if we should overwrite
        y = input("Overwrite? (y/n): ")
        if (y == 'y'):
            print("Overwriting")
        else:
            print("Skipping")
            return

    command = f'./test.sh {instances} {boardSize} {numMatches} 1 {turnLength} ../MctsEnhancementConfigs/EnhancementChoice{enhancement}.json {enhancement}'

    subprocess.run(command, shell=True)

    moveCount = 0
    totalPlayouts = 0
    for i in range(1, instances + 1):

        command = f'cat playerLogs/{i}.{enhancement}..player2.log | grep "Total number of playouts over all threads"'
        lines = subprocess.run(command, shell = True, capture_output = True).stdout.decode('utf-8').strip().split('\n')

        if (len(lines) <= 1):
            print(f"ERROR : instance {i}\n{command}")
            continue

        command = f'cat playerLogs/{i}.{enhancement}..player1.log | grep "Total number of playouts over all threads"'
        lines = subprocess.run(command, shell = True, capture_output = True).stdout.decode('utf-8').strip().split('\n')

        if (len(lines) <= 1):
            print(f"ERROR : instance {i}\n{command}")
            continue

        playouts = 0
        try:
            for j in range(len(lines)//2):
                playouts += int(lines[j].split(' ')[-1])
        except:
            print(f"ERROR : instance {i}\n{command}")
            continue

        moveCount += len(lines)//2

        print(f"Instance {i} : {playouts/(len(lines)//2)} average playouts per move.")

        totalPlayouts += playouts

    print(f"average = {totalPlayouts/moveCount}")
    update_json(f'{enhancement}_{boardSize}_{instances}_{turnLength}', totalPlayouts/moveCount)

if __name__ == '__main__':
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    main()
    

