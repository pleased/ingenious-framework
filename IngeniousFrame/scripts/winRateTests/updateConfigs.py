import json
import os
import sys

GO_TUNED_C = 0.2883582836414371
OTHELLLO_TUNED_C = 0.2883582836414371

configs = {
    ######################### GO #########################
    "go": {
        "Vanilla" : {
            "Selection": "Uct",
            "Expansion": "Single",
            "Simulation": "Random",
            "Backpropagation": "Simple",
            "cValue": GO_TUNED_C
        },
        "Contextual" : {
            "Selection": "Uct",
            "Expansion": "Single",
            "Simulation": "Contextual",
            "Backpropagation": "Simple",
            "cValue": 0.122,
            "gammaCmc": 0.292,
            "threshold": 0.787,
            "window": -1
        },
        "FPU" : {
            "Selection": "Uct",
            "Expansion": "Single",
            "Simulation": "Random",
            "Backpropagation": "Simple",
            "fpu_enabled": True,
            "cValue": 0.214,
            "startPriorityValue": 3.876
        },
        "Rave" : {
            "Selection": "Rave",
            "Expansion": "Single",
            "Simulation": "Random",
            "Backpropagation": "Simple,Rave",
            "cValue": 0.291,
            "vValue": 7727
        },
        "MastExpansion" : {
            "Selection": "Uct",
            "Expansion": "Mast",
            "Simulation": "Mast",
            "Backpropagation": "Simple",
            "treeOnly": False,
            "Backpropagation": "Simple",
            "cValue": 0.273,
            "tau": 0.0
        },        
        "ContextualTuned" : {
            "Selection": "Uct",
            "Expansion": "Single",
            "Simulation": "Contextual",
            "Backpropagation": "Simple",
            "cValue": GO_TUNED_C,
            "gammaCmc": 0.860,
            "threshold": 0.0,
            "window": -1
        },
        "FPUTuned" : {
            "Selection": "Uct",
            "Expansion": "Single",
            "Simulation": "Random",
            "Backpropagation": "Simple",
            "fpu_enabled": True,
            "cValue": GO_TUNED_C,
            "startPriorityValue": 1.965
        },
        "RaveTuned" : {
            "Selection": "Rave",
            "Expansion": "Single",
            "Simulation": "Random",
            "Backpropagation": "Simple,Rave",
            "cValue": GO_TUNED_C,
            "vValue": 6676
        },
        "MastExpansionTuned" : {
            "Selection": "Uct",
            "Expansion": "Mast",
            "Simulation": "Mast",
            "Backpropagation": "Simple",
            "treeOnly": False,
            "cValue": GO_TUNED_C,
            "tau": 0.0
        }
    },
    ######################### OTHELLO #########################
    "othello" : {
        "Vanilla" : {
            "Selection": "Uct",
            "Expansion": "Single",
            "Simulation": "Random",
            "Backpropagation": "Simple",
            "cValue": OTHELLLO_TUNED_C
        },
        "Contextual" : {
            "Selection": "Uct",
            "Expansion": "Single",
            "Simulation": "Contextual",
            "Backpropagation": "Simple",
            "cValue": 0.45,
            "gammaCmc": 0.489,
            "threshold": 0.0,
            "window": -1
        },
        "FPU" : {
            "Selection": "Uct",
            "Expansion": "Single",
            "Simulation": "Random",
            "Backpropagation": "Simple",
            "fpu_enabled": True,
            "cValue": 0.222,
            "startPriorityValue": 2.424
        },
        "Rave" : {
            "Selection": "Rave",
            "Expansion": "Single",
            "Simulation": "Random",
            "Backpropagation": "Simple,Rave",
            "cValue": 0.085,
            "vValue": 9652
        },
        "MastExpansion" : {
            "Selection": "Uct",
            "Expansion": "Mast",
            "Simulation": "Mast",
            "Backpropagation": "Simple",
            "treeOnly": False,
            "cValue": 1.0,
            "tau": 36.1
        },        
        "ContextualTuned" : {
            "Selection": "Uct",
            "Expansion": "Single",
            "Simulation": "Contextual",
            "Backpropagation": "Simple",
            "cValue": OTHELLLO_TUNED_C,
            "gammaCmc": 0.536,
            "threshold": 0.205,
            "window": -1
        },
        "FPUTuned" : {
            "Selection": "Uct",
            "Expansion": "Single",
            "Simulation": "Random",
            "Backpropagation": "Simple",
            "fpu_enabled": True,
            "cValue": OTHELLLO_TUNED_C,
            "startPriorityValue": 1.67
        },
        "RaveTuned" : {
            "Selection": "Rave",
            "Expansion": "Single",
            "Simulation": "Random",
            "Backpropagation": "Simple,Rave",
            "cValue": OTHELLLO_TUNED_C,
            "vValue": 9510
        },
        "MastExpansionTuned" : {
            "Selection": "Uct",
            "Expansion": "Mast",
            "Simulation": "Mast",
            "Backpropagation": "Simple",
            "treeOnly": False,
            "cValue": OTHELLLO_TUNED_C,
            "tau": 59.09
        }
    }
}

def updateConfigs(game):
    if game == "go" or game == "othello":
        for config in configs[game]:
            with open(f"../MctsEnhancementConfigs/EnhancementChoice{config}.json", "w") as f:
                json.dump(configs[game][config], f, indent=4)
    else:
        print("ERROR: Please provide valid game name")

if __name__ == "__main__":
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    updateConfigs(sys.argv[1])