#!/bin/bash
trap "kill 0" SIGINT

root=$(pwd)

# Build JAR
gradle shadowJar -p "$root/../../"

# Create lobby
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar "$root/../../build/libs/IngeniousFrame-all-0.0.4.jar create -config" "$root/gridWorld.json" -game "GridWorldReferee" -lobby "mylobby" -numMatches 1 &

wait

# Create client
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar "$root/../../build/libs/IngeniousFrame-all-0.0.4.jar" client -username "p1" -engine "za.ac.sun.cs.ingenious.games.gridWorld.engines.GridWorldPolicyIterationEngine" -game "GridWorldReferee" -config "$root/gridWorld.json" &

wait

cd "$root"

echo "Done"
