# Gridworld

## Game Description

The following description is taken from Appendix B of the project report for "Ingenious Reinforcement Learning".

### Rules

Gridworld is a game in which an agent needs to navigate a grid environment to reach an objective.
Each cell in the grid represents a different state that the agent may find itself in.
The agent can move in four directions from each cell: up, right, down and left.
Upon reaching a terminal state, the agent is reset to the initial position and receives a reward that can be either 
positive or negative, depending on whether the position represents the completion or failure of the task.

Extensions to these rules are including unreachable cells as obstacles as well as wind.
Wind introduces stochastic elements to an otherwise deterministic environment.
After each action, the position of the agent is moved some distance in the direction of the wind.
In the case of stochastic wind, the intensity of the wind randomly fluctuates. This results in different magnitudes of 
the shift being experienced by the agent.

### Level file

Gridworld levels are defined in level files that are parsed to determine the level dynamics. 
These files provide a declarative manner to describe the Gridworld environment.
A level file can define three sections: reward definitions, wind dynamics and the grid layout.

Examples of the contents of a level file is provided in Figure B.2 and Figure B.3 of the report.

#### Rewards

The section for reward definitions is indicated by a `Reward:` line.
Rewards are defined as single-character tokens containing a reward value and a flag indicating whether the token represents a terminal position.
These tokens can then be placed in the grid to represent these reward positions.
The following syntax is used for reward definitions: `<token> = <reward_value> <is_terminal>`.
The characters `X`, `S` and `P` are reserved and cannot be used.

#### Wind

The wind section, indicated by `Wind:`, provides definitions for the wind dynamics in the grid.
Wind definitions consist of a direction, index, magnitude and stochastic flag.
The direction of the wind can either be `Row` or `Column`.
The exact column or row of the wind is specified by the index.
Magnitude specifies how many positions an agent is shifted when landing within an affected row or column.
The stochastic flag, when true, causes the wind magnitude to randomly fluctuate between the `magnitude`, `magnitude + 1` and `magnitude - 1`.
The magnitude is `magnitude` with a 50% probability, `magnitude + 1` with a 25% probability and `magnitude - 1` with a 25% probability.
The following syntax is used to define wind: `<direction> <index> <magnitude> <is_stochastic>`.

#### Grid

The grid definition is indicated by `Board:`.
The step reward, received when moving to non-terminal states, and action directions are specified before the grid.
These use the following syntax: `<step_reward> <allowed_directions>`.
Allowed directions values of `0`, `1` and `2` indicate cardinal, ordinal and principal directions respectively.

The grid environment is defined as a grid of tokens representing the different cells.
Each token is separated by a single space, resulting in more accurate proportions of the grid's textual representation.
An agent's starting cell is indicated with the `S` token.
The `X` token represents a boundary or wall that the agent is unable to move through.
Tokens used from those defined in the rewards section represent cells of the grid that are terminal and in which the agent receives rewards for reaching.

## Scripts Usage

Detailed documentation on executing these scripts can be found [in the RL readme](IngeniousFrame/src/main/java/za/ac/sun/cs/ingenious/search/rl/readme.md).

### Level selection

Sample levels that can be used by scripts are: 
- `cliff.gridworld`
- `cliff_small.gridworld`
- `cliff_medium.gridworld`
- `large.gridworld`
- `small.gridworld`
- `symmetrical.gridworld`
- `windyCliff.gridworld`

The `gridworld.json` configuration should specify one of these files (or any new level files added in the future).
Note that this only specifies the level used for training agents in the the `run_*` scripts. The `train_*` scripts take 
an optional parameter to specify this. The reason for this is due to the training scripts executing the Training Sandbox
that circumvents the client-server flow.

### Script summary

The scripts below require first executing `start_server.sh`.

* `./run_dqn.sh` - Runs a trained DQN agent from `python/src/trained_models`. Note that this requires a few steps to set 
up the python environment. These can be found in the RL readme.
* `./run_policy_iteration.sh` - Runs the game with a policy iteration agent. This agent requires no training as it knows 
all of the game's dynamics.
* `./run_tabular_qlearning.sh` - Runs a trained tabular Q-learning agent (loaded from a .alg file). To train an agent 
see `./train_tabular_qlearning.sh`.
* `./run_value_iteration.sh` - Runs the game with a value iteration agent. This agent requires no training as it knows 
all of the game's dynamics.
* `./start_server.sh` - Runs the game server.
* `./train_lfa_q_learning.sh`, `./train_off_policy_monte_carlo.sh`, `./train_on_policy_monte_carlo.sh`,
`./train_tabular_qlearning.sh` and `./train_tabular_sarsa.sh` trains linear function approximation, off-policy Monte 
Carlo, on-policy MonteCarlo, tabular Q-learning and tabular SARSA agents respectively. These agents get stored in `.alg` 
files. Each of these scripts take two parameters: the number of matches to run and the file name of the level.
* `./train_dqn.sh` - Trains a DQN agent that is then stored in `python/src/trained_models`. Note that this will 
circumvent the standard client-server flow in order to speed up training.  This script takes the same two parameters
as the others (number of matches, file name of level).

### Manual testing

To run the game as a human player to test manually, first start the server.
 
```
./start_server.sh
```

Build the framework.
```
gradle build jarFat -p "./../../"
```

Start the lobby.
```
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar "./../../build/libs/IngeniousFrame-all-0.0.4.jar" create -config "./gridWorld.json" -game "GridWorldReferee" -lobby "mylobby" -numMatches 3
```

Run the game.
```
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar "./../../build/libs/IngeniousFrame-all-0.0.4.jar" client -username "p1" -engine "za.ac.sun.cs.ingenious.games.gridWorld.engines.GridWorldHumanPlayerEngine" -game "GridWorldReferee" -config "./gridWorld.json"
```
