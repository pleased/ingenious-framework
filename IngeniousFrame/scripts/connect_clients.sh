#!/bin/bash
# Here we start two clients bob and alice
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar ../build/libs/IngeniousFrame-all-0.0.4.jar client -username "alice" -engine "za.ac.sun.cs.ingenious.games.bomberman.engines.BMEngineBFS" -game "bomberman" > alice.log &
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar ../build/libs/IngeniousFrame-all-0.0.4.jar client -username "bob" -engine "za.ac.sun.cs.ingenious.games.bomberman.engines.BMEngineRandom" -game "bomberman" > bob.log &
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar ../build/libs/IngeniousFrame-all-0.0.4.jar client -username "carol" -engine "za.ac.sun.cs.ingenious.games.bomberman.engines.BMEngineMCTS" -game "bomberman" > carol.log &
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar ../build/libs/IngeniousFrame-all-0.0.4.jar client -username "dave" -engine "za.ac.sun.cs.ingenious.games.bomberman.engines.BMEngineMCTS" -game "bomberman" > dave.log &
# java  --add-opens java.base/java.util=ALL-UNNAMED \
#         --add-opens java.desktop/java.awt=ALL-UNNAMED \
#             -jar ../build/libs/IngeniousFrame-all-0.0.4.jar client -username "alice" -engine "za.ac.sun.cs.ingenious.games.bomberman.engines.BMEngineBFS" -game "bomberman" -hostname 127.0.0.1 -port 61234 > alice.log &
# java  --add-opens java.base/java.util=ALL-UNNAMED \
#         --add-opens java.desktop/java.awt=ALL-UNNAMED \
#             -jar ../build/libs/IngeniousFrame-all-0.0.4.jar client -username "bob" -engine "za.ac.sun.cs.ingenious.games.bomberman.engines.BMEngineRandom" -game "bomberman" -hostname localhost -port 61234 > bob.log &
# java  --add-opens java.base/java.util=ALL-UNNAMED \
#         --add-opens java.desktop/java.awt=ALL-UNNAMED \
#             -jar ../build/libs/IngeniousFrame-all-0.0.4.jar client -username "carol" -engine "za.ac.sun.cs.ingenious.games.bomberman.engines.BMEngineMCTS" -game "bomberman" -hostname 127.0.0.1 -port 61234 > carol.log &
# java  --add-opens java.base/java.util=ALL-UNNAMED \
#         --add-opens java.desktop/java.awt=ALL-UNNAMED \
#             -jar ../build/libs/IngeniousFrame-all-0.0.4.jar client -username "dave" -engine "za.ac.sun.cs.ingenious.games.bomberman.engines.BMEngineMCTS" -game "bomberman" -hostname 127.0.0.1 -port 61234 > dave.log &

