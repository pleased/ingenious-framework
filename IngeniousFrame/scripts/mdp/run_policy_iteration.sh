#!/bin/bash
trap "kill 0" SIGINT

root=$(pwd)

# Build JAR
gradle shadowJar -p "$root/../../"

# Create lobby
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar "$root/../../build/libs/IngeniousFrame-all-0.0.4.jar" create -config "$root/mdp.json" -game "MDPReferee" -lobby "mylobby" -numMatches 10 &

wait

# Create client
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar "$root/../../build/libs/IngeniousFrame-all-0.0.4.jar" client -username "p1" -engine "za.ac.sun.cs.ingenious.games.mdp.engines.MDPPolicyIterationEngine" -game "MDPReferee" -config "$root/mdp.json" &

wait

cd $root

echo "Done"