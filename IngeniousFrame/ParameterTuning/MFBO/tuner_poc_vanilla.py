import os

import numpy as np

from mctsConfigBuilder import ConfigTree
from sbo.utils import proper_round

from base_mcts_tuner import MctsTuner

class Tuner(MctsTuner):
    def __init__(
        self,
        game: str,
        board_sizes: np.ndarray,
        duration_bounds: np.ndarray,
        evals_bounds: np.ndarray,
        max_threads: int,
        db_name: str,
        num_configs_to_sample: int,
        num_cores: int = -1,
        smoke_test: bool = False,
        load_from_file: bool = True,
        target_fidelity_sets: list[list[float]] = [[1.0, 1.0, 1.0]],
    ):
        super().__init__(
            log_domain_dims=[],
            board_sizes=board_sizes,
            duration_bounds=duration_bounds,
            evals_bounds=evals_bounds,
            max_threads=max_threads,
            num_configs_to_sample=num_configs_to_sample,
            db_name=db_name,
            num_cores=num_cores,
            smoke_test=smoke_test,
            load_from_file=load_from_file,
            target_fidelity_sets=target_fidelity_sets,
        )
        self._game = game

    def _map_parameter_values_and_config(
        self, point: np.ndarray, point_index: int
    ) -> tuple[dict[str, float], dict[str, int]]:
        point_dict = {
            "Selection": "Uct",
            "Expansion": "Single",
            "Simulation": "Random",
            "Backpropagation": "Simple",
        }

        def add_key_value(key: str, index: int):
            value = self._all_relevant_parameters[key]
            if isinstance(value[0], int):
                point_dict[key] = proper_round(
                    value[0] + (value[1] - value[0]) * point[index]
                )
            else:
                point_dict[key] = value[0] + (value[1] - value[0]) * point[index]

        add_key_value("cValue", 0)

        enhancement_name = "Vanilla"

        config_dict = {
            "game": self._game,
            "board_size": proper_round((point[-3] * self._max_num_tiles) ** 0.5),
            "turn_duration": proper_round(
                point[-2] * (self._duration_bounds[1] - self._duration_bounds[0])
                + self._duration_bounds[0]
            ),
            "thread_count": proper_round(point[1]*(self._max_threads - 1) + 0.5),
            "enhancement_name": enhancement_name,
        }

        assert config_dict["thread_count"] in range(1, self._max_threads + 1)

        config_dict["point_index"] = point_index

        return point_dict, config_dict

    def get_filtered_configurations(self) -> list[dict[str, any]]:
        cft = ConfigTree()
        configs = cft.get_all_configs()
        filtered_configs = [
            c
            for c in configs
            if not c["fpu_enabled"]
            and c["Selection"] == "Uct"
            and c["Expansion"] == "Single"
            and c["Simulation"] == "Random"
            and c["Backpropagation"] == "Simple"
        ]

        return filtered_configs

if __name__ == "__main__":
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    tuner = Tuner(
        game="go",
        board_sizes=np.array([9, 13, 19]),
        duration_bounds=np.array([500, 3000]),
        evals_bounds=np.array([10, 50]),
        max_threads=8,
        db_name="vanilla",
        num_configs_to_sample=-1,
        num_cores=18,
        smoke_test=False,
    )
    tuner.run(
        iterations=76,
        targets_per_iteration=1,
        points_per_target=1,
        initial_points_per_configuration=8,
        max_initial_points=24,
        kernel_kwargs={
            "kernel_option": 0,
            "mode": 0,
        },
        af_method="mfkg",
    )
