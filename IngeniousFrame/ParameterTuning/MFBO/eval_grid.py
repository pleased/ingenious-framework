import asyncio
import itertools
import json
import logging
import os

import threading

import numpy as np
from mctsConfigBuilder import ConfigTree
from mctsEvaluator import eval

# set up logging
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(message)s",
    handlers=[
        logging.FileHandler("eval_grid.log"),
        logging.StreamHandler(),
    ],
)

PER_GAME_OVERHEAD_LOAD = 1.5
GAME_LOAD_RATIO = 0.8
FLOAT_PRECISION = 1e-8


class GridEvaluator:
    def __init__(self) -> None:
        self.cft = ConfigTree()
        self.config = self.cft.get_all_configs()
        self.config = [
            c
            for c in self.config
            if not c["fpu_enabled"]
            and c["Selection"] == "Rave"
            and c["Expansion"] == "Single"
            and c["Simulation"] == "Random"
            and c["Backpropagation"] == "Simple,Rave"
        ][0]
        # create an empty file "gs_results.csv" if it doesn't exist
        if not os.path.isfile("gs_results.csv"):
            with open("gs_results.csv", "w") as f:
                f.write("")

        self.paused = True
        th = threading.Thread(target=self.pause_resume)
        th.start()

    def pause_resume(self):
        logging.info("Press any key to start")
        input()
        while True:
            logging.info("Starting...")
            self.paused = False
            input()
            self.paused = True
            logging.info("Paused. Press any key to start.")
            input()

    def eval_grid(
        self,
        game: str,
        turn_duration: int,
        board_size: int,
        thread_count: int,
        grid_size: int,
        num_evals: int,
    ):
        asyncio.run(
            self._eval_grid(
                game,
                turn_duration,
                board_size,
                thread_count,
                grid_size,
                num_evals,
            )
        )

    async def _eval_grid(
        self,
        game: str,
        turn_duration: int,
        board_size: int,
        thread_count: int,
        grid_size: int,
        num_evals: int,
    ):
        X, Y = await self._run_batch(
            game,
            turn_duration,
            board_size,
            thread_count,
            grid_size,
            num_evals,
        )
        data = {
            "X": X.tolist(),
            "Y": Y.tolist(),
        }
        with open(
            f"{game}_{board_size}_{thread_count}_{turn_duration}_{grid_size}_{num_evals}.json",
            "w",
        ) as f:
            json.dump(data, f)

    async def _run_batch(
        self,
        game: str,
        turn_duration: int,
        board_size: int,
        thread_count: int,
        grid_size: int,
        num_evals: int,
    ):
        coroutine_data = []
        x1 = np.linspace(0, 1, grid_size)
        x2 = np.linspace(0, 1, grid_size)
        point_index = 0
        X = []
        for _x1, _x2 in itertools.product(x1, x2):
            config_dict = {
                "game": game,
                "board_size": board_size,
                "turn_duration": turn_duration,
                "thread_count": thread_count,
                "enhancement_name": "Rave",
                "point_index": point_index,
            }
            point_index += 1
            point_dict = self.config.copy()
            point_dict["cValue"] = np.sqrt(2.0)
            point_dict["vValue"] = (
                point_dict["vValue"][1] - point_dict["vValue"][0]
            ) * _x2 + point_dict["vValue"][0]
            X.append([_x1, _x2])

            point_data = (point_dict, config_dict)
            for _ in range(num_evals):
                coroutine_data.append(point_data)

        logging.info(f"Num points in grid = {len(coroutine_data)}")

        # construct tuples containing the coroutine, load and turn duration
        coroutine_load_TT_tuples = [
            (
                eval(
                    configuration=coroutine_data_set[0],
                    load=(
                        coroutine_data_set[1]["thread_count"] + PER_GAME_OVERHEAD_LOAD
                    )
                    * GAME_LOAD_RATIO,
                    id=id,
                    **coroutine_data_set[1],
                ),
                (coroutine_data_set[1]["thread_count"] + PER_GAME_OVERHEAD_LOAD)
                * GAME_LOAD_RATIO,
                coroutine_data_set[1]["turn_duration"],
            )
            for id, coroutine_data_set in enumerate(coroutine_data)
        ]

        coroutine_queue = asyncio.Queue()
        result_queue = asyncio.Queue()

        num_workers = os.cpu_count()
        workers = [
            asyncio.create_task(self._process_coroutines(coroutine_queue, result_queue))
            for _ in range(num_workers)
        ]

        current_load = 0

        X_result = [None for _ in range(len(coroutine_data))]
        y_result = [[] for _ in range(len(coroutine_data))]
        time_taken = [[] for _ in range(len(coroutine_data))]

        previous_staggered = False

        num_done = 0
        num_todo = len(coroutine_data)

        while coroutine_load_TT_tuples or current_load > FLOAT_PRECISION:
            i = 0
            while i < len(coroutine_load_TT_tuples) or current_load > FLOAT_PRECISION:
                while self.paused:
                    await asyncio.sleep(1)
                if i < len(coroutine_load_TT_tuples):
                    (
                        next_coroutine,
                        next_load,
                        turn_duration,
                    ) = coroutine_load_TT_tuples[i]
                    if current_load + next_load <= os.cpu_count():
                        coroutine_load_TT_tuples.pop(i)
                        current_load += next_load
                        await coroutine_queue.put(next_coroutine)
                        # Stagger the start of the next coroutine to avoid overloading the system. As all agents play
                        # against single-threaded vanilla mcts with the same turn duration, we can stagger the start of
                        # the next coroutine by the turn duration to have fewer non-vanilla agents playing at the same
                        # time. This reduces computational overhead as the non-vanilla agents log statistics via threads
                        # and require a larger amount of garbage collection, which increases overall system load.
                        if previous_staggered:
                            await asyncio.sleep(turn_duration / 1000)
                        previous_staggered = not previous_staggered
                        continue
                if (
                    i == len(coroutine_load_TT_tuples) - 1
                    or len(coroutine_load_TT_tuples) == 0
                ):
                    score, load, point_index, elapsed_time = await result_queue.get()
                    if score is not None:
                        X_result[point_index] = X[point_index]
                        y_result[point_index].append(score)
                        time_taken[point_index].append(elapsed_time)
                        with open("gs_results.csv", "a") as f:
                            f.write(
                                f"{X_result[point_index][0]},{X_result[point_index][1]},{X_result[point_index][2]},{score},{elapsed_time}\n"
                            )
                    current_load -= load
                    num_done += 1
                    logging.info(
                        f"Done {num_done}/{num_todo} ({num_done / num_todo * 100:.2f}%)"
                    )
                    result_queue.task_done()
                i = (
                    (i + 1) % len(coroutine_load_TT_tuples)
                    if coroutine_load_TT_tuples
                    else 1
                )

        for worker in workers:
            worker.cancel()

        # remove any points that failed to evaluate
        i = 0
        while i < len(X_result):
            if X_result[i] is None:
                X_result.pop(i)
                y_result.pop(i)
                time_taken.pop(i)
            else:
                i += 1

        num_successful_evaluations = [len(y) for y in y_result]
        time_taken = [np.mean(t) for t in time_taken]

        X_result = np.array(X_result)
        y_result = np.array([np.mean(y) for y in y_result]).reshape(-1, 1)

        return X_result, y_result

    async def _process_coroutines(
        self, coroutine_queue: asyncio.Queue, result_queue: asyncio.Queue
    ):
        while True:
            coroutine = await coroutine_queue.get()
            try:
                result = await coroutine
                await result_queue.put(result)
            except asyncio.CancelledError:
                break
            finally:
                coroutine_queue.task_done()


if __name__ == "__main__":
    ge = GridEvaluator()
    ge.eval_grid(
        game="go",
        turn_duration=3000,
        board_size=9,
        thread_count=1,
        grid_size=11,
        num_evals=100,
    )
