import asyncio
import logging
import os
import time
import json

from typing import Dict

import numpy as np
import torch

from mctsEvaluator import eval
from sbo.agents.mfbo import MultiFidelityBayesianOptimiser as MFBO
from sbo.agents.mfbo_utils.costModels import ProductAffineFidelityCostModel
from sbo.utils import proper_round
from tinydb import TinyDB

logging.basicConfig(
    format="%(asctime)s %(levelname)s %(message)s",
    datefmt="%H:%M:%S",
    level=logging.INFO,
)

PER_GAME_OVERHEAD_LOAD = 1.5
GAME_LOAD_RATIO = 0.7
FLOAT_PRECISION = 1e-8


class MctsTuner:
    def __init__(
        self,
        log_domain_dims: list[int],
        board_sizes: np.ndarray,
        duration_bounds: np.ndarray,
        evals_bounds: np.ndarray,
        max_threads: int,
        db_name: str,
        num_configs_to_sample: int,
        num_cores: int = -1,
        smoke_test: bool = False,
        load_from_file: bool = True,
        target_fidelity_sets: list[list[float]] = [[1.0, 1.0, 1.0]],
    ):
        self._board_sizes = board_sizes
        self._duration_bounds = duration_bounds
        self._evals_bounds = evals_bounds
        self._max_threads = max_threads
        self._load_from_file = load_from_file

        if num_cores == -1:
            self._num_cores = os.cpu_count()
        else:
            self._num_cores = num_cores

        filtered_configs = self.get_filtered_configurations()

        self._all_relevant_parameters = self.get_all_relevant_parameters(
            filtered_configs
        )

        num_tiles = board_sizes**2
        self._max_num_tiles = np.max(num_tiles)
        num_tiles = num_tiles / self._max_num_tiles

        discrete_fidelity_sets = [num_tiles]

        num_continous_dims = len(self._all_relevant_parameters)

        fixed_problem_dims = 0

        num_continous_fidelity_dims = 2
        num_discrete_fidelity_dims = len(discrete_fidelity_sets)
        total_dims = num_continous_dims + fixed_problem_dims + num_continous_fidelity_dims + num_discrete_fidelity_dims + 1
        self._mfbo = MFBO(
            # add 1 for threadcount dimension
            num_problem_dimensions=num_continous_dims + fixed_problem_dims + 1,
            num_discrete_fidelity_dims=num_discrete_fidelity_dims,
            discrete_fidelity_sets=discrete_fidelity_sets,
            num_continuous_fidelity_dims=num_continous_fidelity_dims,
            log_domain_dims=log_domain_dims,
            target_fidelity_sets=target_fidelity_sets,
            nu=2.5,
            y_scale=1e5,
            num_configs_to_sample=num_configs_to_sample,
            smoke_test=smoke_test,
            cost_model=ProductAffineFidelityCostModel(
                tot_dims=total_dims,
                fidelity_weights={
                    num_continous_dims: self._max_threads,
                    -3: self._max_num_tiles,
                    -2: 1.0,
                    -1: evals_bounds[1],
                },
                fidelity_weights_minima={
                    num_continous_dims: 1/self._max_threads,
                    -2: duration_bounds[0] / duration_bounds[1],
                    -1: evals_bounds[0] / evals_bounds[1],
                },
            ),
            maximise=True,
        )

        self._smoke_test = smoke_test

        # create database and table to store results
        self.db = TinyDB(f"{db_name}.json")
        self.table = self.db.table("evaluations")

    def _add_evaluation(
        self,
        X: np.ndarray,
        y: float,
        avg_time_taken: float,
        num_evaluations,
        iteration: int,
    ):
        self.table.insert(
            {
                "X": X.tolist(),
                "y": y,
                "avg_time_taken": avg_time_taken,
                "num_evaluations": num_evaluations,
                "iteration": iteration,
            }
        )

    def _load_evaluations(self):
        X = []
        y = []
        cur_iteration = 0
        for evaluation in self.table.all():
            X.append(np.array(evaluation["X"]))
            y.append(evaluation["y"])
            if evaluation["iteration"] > cur_iteration:
                cur_iteration = evaluation["iteration"]
        return np.array(X), np.array(y).reshape(-1, 1), cur_iteration + 1

    def run(
        self,
        iterations: int,
        targets_per_iteration: int,
        points_per_target: int,
        initial_points_per_configuration: int = 1,
        max_initial_points: int = 20,
        kernel_kwargs: Dict[str, float] = {},
        af_method: str = "",
    ):
        asyncio.run(
            self._run(
                iterations=iterations,
                targets_per_iteration=targets_per_iteration,
                points_per_target=points_per_target,
                initial_points_per_configuration=initial_points_per_configuration,
                max_initial_points=max_initial_points,
                kernel_kwargs=kernel_kwargs,
                af_method=af_method,
            )
        )

    async def _run(
        self,
        iterations: int,
        targets_per_iteration: int,
        points_per_target: int,
        initial_points_per_configuration: int = 1,
        max_initial_points: int = 20,
        kernel_kwargs: Dict[str, float] = {},
        af_method: str = "",
    ):
        cur_iteration = 1

        if self._load_from_file:
            logging.info("Loading historic evaluations from file...")
            X, Y, cur_iteration = self._load_evaluations()
            logging.info(f"Loaded {len(X)} evaluations.")
        if not self._load_from_file or len(X) == 0:
            logging.info("No historic evaluations found. Running initial batch...")
            X_init = self._mfbo.get_random_initial_points_per_config(
                initial_points_per_configuration
            )
            if len(X_init) > max_initial_points:
                X_init = X_init[
                    np.random.choice(len(X_init), max_initial_points, replace=False)
                ]
            start = time.time()
            X, Y = await self._run_batch(X_init, iteration=0)
            logging.info(
                f"Time to run initial batch: {self._get_time_string(time.time() - start)}"
            )

        start = time.time()

        self._mfbo.fit(X, Y, kernel_kwargs=kernel_kwargs)

        logging.info(
            f"Time for initial fit: {self._get_time_string(time.time() - start)}"
        )

        i = cur_iteration
        while i < iterations + 1:
            try:
                iter_start = time.time()
                logging.info(f"Running iteration {i} with {len(X)} points...")
                start = time.time()
                X_new = self._mfbo.get_next_points(
                    num_targets=targets_per_iteration,
                    points_per_target=points_per_target,
                    af_method=af_method,
                )
                logging.info(
                    f"{i} : Time to get next points: {self._get_time_string(time.time() - start)}"
                )
                start = time.time()
                X_new, Y_new = await self._run_batch(X_new, iteration=i)
                logging.info(
                    f"{i} : Time to run batch: {self._get_time_string(time.time() - start)}"
                )
                X = np.vstack((X, X_new))
                Y = np.vstack((Y, Y_new))
                start = time.time()
                self._mfbo.fit(X, Y, kernel_kwargs=kernel_kwargs)
                logging.info(
                    f"{i} : Time to fit: {self._get_time_string(time.time() - start)}"
                )
                logging.info(
                    f"{i} : Total time taken: {self._get_time_string(time.time() - iter_start)}"
                )
            except torch.cuda.OutOfMemoryError as e:
                logging.error(
                    f"{i} : GPU ran out of memory after {self._get_time_string(time.time() - iter_start)}."
                )
                if self._mfbo.halve_batch_size():
                    logging.info(f"{i} : Halving batch sizes and retrying...")
                else:
                    logging.info(
                        f"{i} : Batch sizes could not be decreased. Switching to CPU and increasing batch size to number of cores."
                    )
                    self._mfbo.set_device_and_bs(
                        "cpu", self._num_cores, self._num_cores
                    )
                    self._mfbo.free_cuda_memory()
                    self._mfbo.fit(X, Y)
                continue
            i += 1

    async def _smoke_test_eval(self, configuration, load, id, point_index, enhancement_name, thread_count):
        configuration["thread_count"] = thread_count
        directory = f"enhancementConfigurations/{id}/"
        if not os.path.exists(directory):
            os.makedirs(directory)
        # dump the json as an aux json
        json.dump(
            configuration,
            open(f"{directory}EnhancementChoice{enhancement_name}.json", "w"),
            indent=4,
        )
        return np.random.rand(), load, point_index, np.random.rand()

    def _get_time_string(self, seconds: float) -> str:
        hours = int(seconds // 3600)
        minutes = int((seconds % 3600) // 60)
        seconds = int(seconds % 60)
        str = ""
        if hours > 0:
            str += f"{hours}h "
        else:
            str += "0h "
        if minutes > 0:
            str += f"{minutes}m "
        else:
            str += "0m "
        if seconds > 0:
            str += f"{seconds}s"
        else:
            str += "0s"
        return str

    async def _run_batch(self, X_train: np.ndarray, iteration: int):
        coroutine_data = []
        for i, x in enumerate(X_train):
            point_data = self._map_parameter_values_and_config(x, i)
            for _ in range(
                proper_round(
                    x[-1] * (self._evals_bounds[1] - self._evals_bounds[0])
                    + self._evals_bounds[0]
                )
            ):
                coroutine_data.append(point_data)

        # sort the coroutines by the number of threads they use
        coroutine_data.sort(key=lambda x: x[1]["thread_count"], reverse=True)
        # sort the coroutines by the (board size)**2 * turn_duration they use, as larger boards take longer to evaluate
        coroutine_data.sort(
            key=lambda x: x[1]["board_size"] ** 2 * x[1]["turn_duration"], reverse=True
        )

        # construct tuples containing the coroutine, load and turn duration
        coroutine_load_TT_tuples = [
            (
                eval(
                    configuration=coroutine_data_set[0],
                    load=(
                        coroutine_data_set[1]["thread_count"] + PER_GAME_OVERHEAD_LOAD
                    )
                    * GAME_LOAD_RATIO,
                    id=id,
                    **coroutine_data_set[1],
                ) if not self._smoke_test else self._smoke_test_eval(
                    configuration=coroutine_data_set[0],
                    load=(
                        coroutine_data_set[1]["thread_count"] + PER_GAME_OVERHEAD_LOAD
                    )
                    * GAME_LOAD_RATIO,
                    id=id,
                    point_index=coroutine_data_set[1]["point_index"],
                    enhancement_name=coroutine_data_set[1]["enhancement_name"],
                    thread_count=coroutine_data_set[1]["thread_count"],
                ),
                (coroutine_data_set[1]["thread_count"] + PER_GAME_OVERHEAD_LOAD)
                * GAME_LOAD_RATIO,
                coroutine_data_set[1]["turn_duration"],
            )
            for id, coroutine_data_set in enumerate(coroutine_data)
        ]

        coroutine_queue = asyncio.Queue()
        result_queue = asyncio.Queue()

        num_workers = self._num_cores
        workers = [
            asyncio.create_task(self._process_coroutines(coroutine_queue, result_queue))
            for _ in range(num_workers)
        ]

        current_load = 0

        X_result = [None for _ in range(X_train.shape[0])]
        y_result = [[] for _ in range(X_train.shape[0])]
        time_taken = [[] for _ in range(X_train.shape[0])]

        previous_staggered = False

        while coroutine_load_TT_tuples or current_load > FLOAT_PRECISION:
            i = 0

            while i < len(coroutine_load_TT_tuples) or current_load > FLOAT_PRECISION:
                if i < len(coroutine_load_TT_tuples):
                    (
                        next_coroutine,
                        next_load,
                        turn_duration,
                    ) = coroutine_load_TT_tuples[i]
                    if current_load + next_load <= self._num_cores:
                        coroutine_load_TT_tuples.pop(i)
                        current_load += next_load
                        await coroutine_queue.put(next_coroutine)
                        # Stagger the start of the next coroutine to avoid overloading the system. As all agents play
                        # against single-threaded vanilla mcts with the same turn duration, we can stagger the start of
                        # the next coroutine by the turn duration to have fewer non-vanilla agents playing at the same
                        # time. This reduces computational overhead as the non-vanilla agents log statistics via threads
                        # and require a larger amount of garbage collection, which increases overall system load.
                        if previous_staggered and not self._smoke_test:
                            await asyncio.sleep(turn_duration / 1000)
                        previous_staggered = not previous_staggered
                        continue
                if (
                    i == len(coroutine_load_TT_tuples) - 1
                    or len(coroutine_load_TT_tuples) == 0
                ):
                    score, load, point_index, elapsed_time = await result_queue.get()
                    if score is not None:
                        X_result[point_index] = X_train[point_index]
                        y_result[point_index].append(score)
                        time_taken[point_index].append(elapsed_time)
                    current_load -= load
                    result_queue.task_done()
                i = (
                    (i + 1) % len(coroutine_load_TT_tuples)
                    if coroutine_load_TT_tuples
                    else 1
                )

        for worker in workers:
            worker.cancel()

        # remove any points that failed to evaluate
        i = 0
        while i < len(X_result):
            if X_result[i] is None:
                X_result.pop(i)
                y_result.pop(i)
                time_taken.pop(i)
            else:
                i += 1

        num_successful_evaluations = [len(y) for y in y_result]
        time_taken = [np.mean(t) for t in time_taken]

        X_result = np.array(X_result)
        y_result = np.array([np.mean(y) for y in y_result]).reshape(-1, 1)

        for i, x in enumerate(X_result):
            self._add_evaluation(
                X=X_result[i],
                y=y_result[i][0],
                avg_time_taken=time_taken[i],
                num_evaluations=num_successful_evaluations[i],
                iteration=iteration,
            )

        return X_result, y_result

    async def _process_coroutines(self, coroutine_queue, result_queue):
        while True:
            coroutine = await coroutine_queue.get()
            try:
                result = await coroutine
                await result_queue.put(result)
            except asyncio.CancelledError:
                break
            finally:
                coroutine_queue.task_done()

    def _map_parameter_values_and_config(
        self, point: np.ndarray, point_index: int
    ) -> tuple[dict[str, float], dict[str, int]]:
        raise NotImplementedError

    def get_filtered_configurations(self) -> list[dict[str, any]]:
        raise NotImplementedError

    def get_all_relevant_parameters(
        self, configs: list[dict[str, any]]
    ) -> dict[str, tuple]:
        all_relevant_parameters = {}
        for config in configs:
            relevant_parameters = self.get_relevant_parameters(config)
            for key, value in relevant_parameters.items():
                if key not in all_relevant_parameters.keys():
                    all_relevant_parameters[key] = value
        return all_relevant_parameters

    def get_relevant_parameters(self, config: dict) -> dict[str, tuple]:
        relevant_parameters = {}
        for key, value in config.items():
            if isinstance(value, tuple):
                relevant_parameters[key] = value
        return relevant_parameters
