#!/usr/bin/env python3
# Copyright (c) Meta Platforms, Inc. and affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

r"""
Cost models to be used with multi-fidelity optimization.

Cost are useful for defining known cost functions when the cost of an evaluation
is heterogeneous in fidelity. For a full worked example, see the
`tutorial <https://botorch.org/tutorials/multi_fidelity_bo>`_ on continuous
multi-fidelity Bayesian Optimization.
"""

from __future__ import annotations

from typing import Dict, List, Optional

import numpy as np

import torch
from botorch.models.deterministic import DeterministicModel
from torch import Tensor


class MctsProductAffineFidelityCostModel(DeterministicModel):
    def __init__(
        self,
        fidelity_weights: List[float],
        fidelity_dims: List[int],
        game_to_game_cost_map: Dict[str, float],
        game_to_board_size_map: Dict[str, np.ndarray],
        game_cost_dim: int,
        num_problem_dims: int,
        board_size_dim: int,
        fixed_cost: float = 0.01,
        scaling_factor: float = 1.0,
    ) -> None:
        super().__init__()
        self.num_fidelity_dims = len(fidelity_dims)
        zero_indices = [i for i, w in enumerate(fidelity_weights) if w == 0]
        nonzero_indices = [i for i, w in enumerate(fidelity_weights) if w != 0]
        offset = 0
        for idx in zero_indices:
            if idx <= board_size_dim + offset:
                board_size_dim -= 1
                offset += 1
        self.fidelity_dims = [fidelity_dims[i] for i in nonzero_indices]
        self.fixed_cost = fixed_cost
        self.scaling_factor = scaling_factor
        self.weight_data = {
            game: {
                "weight": game_to_game_cost_map[game],
                "squared_max_board_size": game_to_board_size_map[game].max() ** 2,
            }
            for game in game_to_game_cost_map
        }
        self.weight_data.update(
            {
                "game_cost_dim": game_cost_dim + num_problem_dims,
                "board_size_dim": board_size_dim,
            }
        )
        weights = torch.tensor(fidelity_weights)[nonzero_indices]
        self.register_buffer("weights", weights)
        self._num_outputs = 1

    def forward(self, X: Tensor) -> Tensor:
        r"""Evaluate the cost on a candidate set X.

        Computes a cost of the form

            cost = fixed_cost + prod(sum_j weights[j] * X[fidelity_dims[j]])

        for each element of the q-batch

        Args:
            X: A `batch_shape x q x d'`-dim tensor of candidate points.

        Returns:
            A `batch_shape x q x 1`-dim tensor of costs.
        """
        game_cost_dim = self.weight_data["game_cost_dim"]
        board_size_dim = self.weight_data["board_size_dim"]
        weight_dim = self.fidelity_dims[0]
        # TODO : remove assertion
        assert game_cost_dim == weight_dim
        target_game_weight = X[tuple([0] * (X.dim() - 1))][game_cost_dim].item()
        for game in self.weight_data:
            if game in ["board_size_dim", "game_cost_dim"]:
                continue
            if self.weight_data[game]["weight"] == target_game_weight:
                self.weights[board_size_dim] = self.weight_data[game][
                    "squared_max_board_size"
                ]
                break
        scaled_X = X[..., self.fidelity_dims] * self.weights.to(X)[None, None, :]
        mult_cost = torch.prod(scaled_X, dim=-1)
        return self.fixed_cost + torch.log(
            self.scaling_factor * mult_cost.unsqueeze(-1) + 1.0
        )
