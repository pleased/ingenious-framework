import numpy as np
import torch


def floatEquals(a, b) -> bool:
    """
    Returns whether the two floats are within a small epsilon of each other. Handles numpy arrays, torch tensors and
    regular floats.

    Parameters:
        a (float|torch.Tensor|np.ndarray): The first float to compare
        b (float|torch.Tensor|np.ndarray): The second float to compare

    Returns:
        bool: Whether the two floats are within a small epsilon of each other
    """
    if isinstance(a, torch.Tensor) and isinstance(b, torch.Tensor):
        return torch.allclose(a, b)
    return np.allclose(a, b)


def proper_round(num):
    """
    Rounds a number to the nearest integer
    """
    str_num = str(num)
    first_dec = str_num[: str_num.index(".") + 2]
    if first_dec[-1] >= "5":
        return int(str_num[: str_num.index(".")]) + 1
    return int(str_num[: str_num.index(".")])
