#!/usr/bin/env python3
# Copyright (c) Meta Platforms, Inc. and affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in ingenious-framework/IngeniousFrame/ParameterTuning/MFBO/sbo/LICESNSE

from __future__ import annotations

import itertools

from typing import Any, List, Optional

import torch
from botorch.exceptions import UnsupportedError
from gpytorch.constraints import Interval, Positive
from gpytorch.kernels import Kernel
from gpytorch.kernels.matern_kernel import MaternKernel
from gpytorch.priors import Prior
from gpytorch.priors.torch_priors import GammaPrior
from torch import Tensor


class LinearTruncatedFidelityKernel(Kernel):
    r"""GPyTorch Linear Truncated Fidelity Kernel.

    Computes a covariance matrix based on the Linear truncated kernel between
    inputs `x_1` and `x_2` for up to two fidelity parmeters:

        K(x_1, x_2) = k_0 + c_1(x_1, x_2)k_1 + c_2(x_1,x_2)k_2 + c_3(x_1,x_2)k_3

    where

    - `k_i(i=0,1,2,3)` are Matern kernels calculated between non-fidelity
        parameters of `x_1` and `x_2` with different priors.
    - `c_1=(1 - x_1[f_1])(1 - x_2[f_1]))(1 + x_1[f_1] x_2[f_1])^p` is the kernel
        of the the bias term, which can be decomposed into a deterministic part
        and a polynomial kernel. Here `f_1` is the first fidelity dimension and
        `p` is the order of the polynomial kernel.
    - `c_3` is the same as `c_1` but is calculated for the second fidelity
        dimension `f_2`.
    - `c_2` is the interaction term with four deterministic terms and the
        polynomial kernel between `x_1[..., [f_1, f_2]]` and
        `x_2[..., [f_1, f_2]]`.

    Example:
        >>> x = torch.randn(10, 5)
        >>> # Non-batch: Simple option
        >>> covar_module = LinearTruncatedFidelityKernel()
        >>> covar = covar_module(x)  # Output: LinearOperator of size (10 x 10)
        >>>
        >>> batch_x = torch.randn(2, 10, 5)
        >>> # Batch: Simple option
        >>> covar_module = LinearTruncatedFidelityKernel(batch_shape = torch.Size([2]))
        >>> covar = covar_module(x)  # Output: LinearOperator of size (2 x 10 x 10)
    """

    def __init__(  # noqa C901
        self,
        fidelity_dims: List[int],
        dimension: Optional[int] = None,
        power_prior: Optional[Prior] = None,
        power_constraint: Optional[Interval] = None,
        nu: float = 2.5,
        lengthscale_prior_unbiased: Optional[Prior] = None,
        lengthscale_prior_biased: Optional[Prior] = None,
        lengthscale_constraint_unbiased: Optional[Interval] = None,
        lengthscale_constraint_biased: Optional[Interval] = None,
        covar_module_unbiased: Optional[Kernel] = None,
        covar_modules_biased: Optional[Kernel] = None,
        **kwargs: Any,
    ) -> None:
        """
        Args:
            fidelity_dims: A list containing either one or two indices specifying
                the fidelity parameters of the input.
            dimension: The dimension of `x`. Unused if `active_dims` is specified.
            power_prior: Prior for the power parameter of the polynomial kernel.
                Default is `None`.
            power_constraint: Constraint on the power parameter of the polynomial
                kernel. Default is `Positive`.
            nu: The smoothness parameter for the Matern kernel: either 1/2, 3/2,
                or 5/2. Unused if both `covar_module_unbiased` and
                `covar_module_biased` are specified.
            lengthscale_prior_unbiased: Prior on the lengthscale parameter of Matern
                kernel `k_0`. Default is `Gamma(1.1, 1/20)`.
            lengthscale_constraint_unbiased: Constraint on the lengthscale parameter
                of the Matern kernel `k_0`. Default is `Positive`.
            lengthscale_prior_biased: Prior on the lengthscale parameter of Matern
                kernels `k_i(i>0)`. Default is `Gamma(5, 1/20)`.
            lengthscale_constraint_biased: Constraint on the lengthscale parameter
                of the Matern kernels `k_i(i>0)`. Default is `Positive`.
            covar_module_unbiased: Specify a custom kernel for `k_0`. If omitted,
                use a `MaternKernel`.
            covar_module_biased: Specify a custom kernel for the biased parts
                `k_i(i>0)`. If omitted, use a `MaternKernel`.
            batch_shape: If specified, use a separate lengthscale for each batch of
                input data. If `x1` is a `batch_shape x n x d` tensor, this should
                be `batch_shape`.
            active_dims: Compute the covariance of a subset of input dimensions. The
                numbers correspond to the indices of the dimensions.
        """
        if dimension is None and kwargs.get("active_dims") is None:
            raise UnsupportedError(
                "Must specify dimension when not specifying active_dims."
            )
        n_fidelity = len(fidelity_dims)
        if len(set(fidelity_dims)) != n_fidelity:
            raise ValueError("fidelity_dims must not have repeated elements")
        if nu not in {0.5, 1.5, 2.5}:
            raise ValueError("nu must be one of 0.5, 1.5, or 2.5")

        super().__init__(**kwargs)
        self.fidelity_dims = fidelity_dims
        if power_constraint is None:
            power_constraint = Positive()

        if lengthscale_prior_unbiased is None:
            lengthscale_prior_unbiased = GammaPrior(3, 6)

        if lengthscale_prior_biased is None:
            lengthscale_prior_biased = GammaPrior(6, 2)

        if power_prior is not None:
            self.register_prior(
                "power_prior",
                power_prior,
                lambda m: m.power,
                lambda m, v: m._set_power(v),
            )

        if self.active_dims is not None:
            dimension = len(self.active_dims)

        if covar_module_unbiased is None:
            covar_module_unbiased = MaternKernel(
                nu=nu,
                batch_shape=self.batch_shape,
                lengthscale_prior=lengthscale_prior_unbiased,
                ard_num_dims=dimension - len(fidelity_dims),
                lengthscale_constraint=lengthscale_constraint_unbiased,
            )

        covar_modules_biased = []
        for _ in range(n_fidelity):
            covar_modules_biased.append(
                MaternKernel(
                    nu=nu,
                    batch_shape=self.batch_shape,
                    lengthscale_prior=lengthscale_prior_biased,
                    ard_num_dims=dimension - len(fidelity_dims),
                    lengthscale_constraint=lengthscale_constraint_biased,
                )
            )

        self.register_parameter(
            name="raw_power",
            parameter=torch.nn.Parameter(
                torch.zeros(*self.batch_shape, len(covar_modules_biased))
            ),
        )
        self.register_constraint("raw_power", power_constraint)

        self.covar_module_unbiased = covar_module_unbiased
        self.covar_modules_biased = torch.nn.ModuleList(covar_modules_biased)

    @property
    def power(self) -> Tensor:
        return self.raw_power_constraint.transform(self.raw_power)

    @power.setter
    def power(self, value: Tensor) -> None:
        self._set_power(value)

    def _set_power(self, value: Tensor) -> None:
        if not torch.is_tensor(value):
            value = torch.as_tensor(value).to(self.raw_power)
        self.initialize(raw_power=self.raw_power_constraint.inverse_transform(value))

    def forward(self, x1: Tensor, x2: Tensor, diag: bool = False, **params) -> Tensor:
        if params.get("last_dim_is_batch", False):
            raise NotImplementedError(
                "last_dim_is_batch not yet supported by LinearTruncatedFidelityKernel"
            )

        power = self.power.view(*self.batch_shape, len(self.covar_modules_biased), 1)
        power_idx = 0
        active_dimsM = torch.tensor(
            [i for i in range(x1.size(-1)) if i not in self.fidelity_dims],
            device=x1.device,
        )
        if len(active_dimsM) == 0:
            raise RuntimeError(
                "Input to LinearTruncatedFidelityKernel must have at least one "
                "non-fidelity dimension."
            )
        x1_ = x1.index_select(dim=-1, index=active_dimsM)
        x2_ = x2.index_select(dim=-1, index=active_dimsM)
        covar_unbiased = self.covar_module_unbiased(x1_, x2_, diag=diag)

        covar_biased_iter = [
            self.covar_modules_biased[i](x1_, x2_, diag=diag)
            for i in range(len(self.covar_modules_biased))
        ]

        covar_biased_iter = itertools.cycle(covar_biased_iter)

        spatial_bias_factor = None
        interaction_bias_factors = []

        for i in range(len(self.fidelity_dims)):
            fd_outer_idx = self.fidelity_dims[i]
            fd_idxr = torch.full((1,), fd_outer_idx, dtype=torch.long, device=x1.device)
            x1i_ = x1.index_select(dim=-1, index=fd_idxr).clamp(0, 1)
            x2it_ = x2.index_select(dim=-1, index=fd_idxr).clamp(0, 1)
            if not diag:
                x2it_ = x2it_.transpose(-1, -2)
            cross_term_i = (1 - x1i_) * (1 - x2it_)
            covar_biased = next(covar_biased_iter)
            _bias_factor = cross_term_i * (1 + x1i_ * x2it_).pow(
                power[..., power_idx, :]
            )
            power_idx += 1
            if diag:
                _bias_factor = _bias_factor.view(covar_biased.shape)

            if spatial_bias_factor is None:
                spatial_bias_factor = _bias_factor * covar_biased
            else:
                spatial_bias_factor += _bias_factor * covar_biased

            for j in range(i + 1, len(self.fidelity_dims)):
                fd_inner_idx = self.fidelity_dims[j]
                fd_idxr = torch.full(
                    (1,), fd_inner_idx, dtype=torch.long, device=x1.device
                )
                x1j_ = x1.index_select(dim=-1, index=fd_idxr).clamp(0, 1)
                x2jt_ = x2.index_select(dim=-1, index=fd_idxr).clamp(0, 1)
                x1ij_ = torch.cat([x1i_, x1j_], dim=-1)
                if diag:
                    x2jbt_ = torch.cat([x2it_, x2jt_], dim=-1)
                    k = (1 + (x1ij_ * x2jbt_).sum(dim=-1, keepdim=True)).pow(
                        power[..., power_idx, :]
                    )
                else:
                    x2jt_ = x2jt_.transpose(-1, -2)
                    x2jbt_ = torch.cat([x2it_, x2jt_], dim=-2)
                    k = (1 + (x1ij_ @ x2jbt_)).pow(power[..., power_idx, :])

                cross_term_j = (1 - x1j_) * (1 - x2jt_)
                interaction_bias_factors.append(
                    cross_term_i * cross_term_j * k * covar_biased
                )

        if diag:
            for i in range(len(interaction_bias_factors)):
                interaction_bias_factors[i] = interaction_bias_factors[i].view(
                    covar_biased.shape
                )

        interaction_bias_factor = None
        for i in range(len(interaction_bias_factors)):
            if interaction_bias_factor is None:
                interaction_bias_factor = interaction_bias_factors[i]
            else:
                interaction_bias_factor += interaction_bias_factors[i]

        if interaction_bias_factor:
            module = covar_unbiased + spatial_bias_factor + interaction_bias_factor
        else:
            module = covar_unbiased + spatial_bias_factor

        return module
