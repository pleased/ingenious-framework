#!/usr/bin/env python3
# Copyright (c) Meta Platforms, Inc. and affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in ingenious-framework/IngeniousFrame/ParameterTuning/MFBO/sbo/LICESNSE

r"""
Cost models to be used with multi-fidelity optimization.

Cost are useful for defining known cost functions when the cost of an evaluation
is heterogeneous in fidelity. For a full worked example, see the
`tutorial <https://botorch.org/tutorials/multi_fidelity_bo>`_ on continuous
multi-fidelity Bayesian Optimization.
"""

from __future__ import annotations

from typing import Dict, Optional

import torch
from botorch.models.deterministic import DeterministicModel
from torch import Tensor


class ProductAffineFidelityCostModel(DeterministicModel):
    def __init__(
        self,
        tot_dims: int,
        fidelity_weights: Optional[Dict[int, float]] = None,
        fidelity_weights_minima: Optional[Dict[int, float]] = None,
        fixed_cost: float = 0.01,
        scaling_factor: float = 1.0,
    ) -> None:
        r"""
        Args:
            tot_dims: The total number of dimensions of the input space; including the
                fidelity parameters.
            fidelity_weights: A dictionary mapping a subset of columns of `X`
                (the fidelity parameters) to its associated weight in the
                affine cost expression. If omitted, assumes that the last
                column of `X` is the fidelity parameter with a weight of 1.0.
            fixed_cost: The fixed cost of running a single candidate point (i.e.
                an element of a q-batch).
        """
        if fidelity_weights is None:
            fidelity_weights = {-1: 1.0}
        super().__init__()
        weights_dict = {
            k if k >= 0 else tot_dims + k: v
            for k, v in (fidelity_weights or {}).items()
        }
        # sort the weights by key
        sorted_weights = sorted(weights_dict.items(), key=lambda x: x[0])
        self.fidelity_dims = torch.tensor([x[0] for x in sorted_weights])
        weights = torch.tensor([x[1] for x in sorted_weights])

        weight_minima_dict = {
            k if k >= 0 else tot_dims + k: v
            for k, v in (fidelity_weights_minima or {}).items()
        }
        self.fidelity_weights_minima = weight_minima_dict

        self.fixed_cost = fixed_cost
        self.scaling_factor = scaling_factor

        self.register_buffer("weights", weights)
        self._num_outputs = 1

    def forward(self, X: Tensor) -> Tensor:
        r"""Evaluate the cost on a candidate set X.

        Computes a cost of the form

            cost = fixed_cost + prod(sum_j weights[j] * X[fidelity_dims[j]])

        for each element of the q-batch

        Args:
            X: A `batch_shape x q x d'`-dim tensor of candidate points.

        Returns:
            A `batch_shape x q x 1`-dim tensor of costs.
        """
        X_cp = X.clone()
        if self.fidelity_weights_minima is not None:
            for key, val in self.fidelity_weights_minima.items():
                X_cp[..., key] = X_cp[..., key] * (1 - val) + val
        scaled_X = X_cp[..., self.fidelity_dims] * self.weights.to(X_cp)[None, None, :]
        return self.fixed_cost + self.scaling_factor * torch.prod(
            scaled_X, dim=-1
        ).unsqueeze(-1)
