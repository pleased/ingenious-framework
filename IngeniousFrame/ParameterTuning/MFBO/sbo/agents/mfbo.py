import itertools
import logging
from typing import Callable, Dict, Generator, List, Optional, Tuple

import numpy as np

import sobol
import torch
from botorch import fit_gpytorch_mll

from botorch.acquisition import PosteriorMean
from botorch.acquisition.cost_aware import InverseCostWeightedUtility
from botorch.acquisition.fixed_feature import FixedFeatureAcquisitionFunction
from botorch.acquisition.objective import ScalarizedPosteriorTransform
from botorch.acquisition.utils import project_to_target_fidelity
from botorch.models.deterministic import DeterministicModel

from botorch.models.transforms.outcome import Standardize
from botorch.optim.fit import fit_gpytorch_mll_torch
from botorch.optim.optimize import optimize_acqf, optimize_acqf_mixed

from gpytorch.mlls.exact_marginal_log_likelihood import ExactMarginalLogLikelihood

from .mfbo_utils.customAcquisition import (
    qMultiTargetMultiFidelityKnowledgeGradient,
    qMultiTargetMultiFidelityLowerBoundMaxValueEntropy,
    qMultiTargetMultiFidelityMaxValueEntropy,
)

from .mfbo_utils.customMFGP import FixedNoiseMultiFidelityGP, SingleTaskMultiFidelityGP

torch.set_printoptions(precision=3, sci_mode=False)

FLOAT_PRECISION = 1e-8

logger = logging.getLogger(__name__)


class MultiFidelityBayesianOptimiser:
    _fixed_discrete_features_sets: List[Dict[int, float]]
    """A dictionary containing all possible combinations of fixed discrete features."""
    _all_fixed_features_sets: List[Dict[int, float]]
    """A dictionary containing all possible combinations of fixed features, i.e. including the fixed fidelity
    features."""
    _target_fidelity_sets_gen: Generator[Dict[int, float], None, None]
    """A generator of dictionaries containing the indices of fidelity parameters and their corresponding target
    fidelities"""
    _target_fidelity_set: Dict[int, float]
    """The current target fidelity set, represented by a dictionary of dimension indices and their corresponding target
    fidelity values"""
    _maximise: bool
    """Whether to maximise or minimise the function"""
    _num_problem_dimensions: int
    """The number of dimensions of the function"""
    _total_dimensions: int
    """The total number of dimensions of the problem, including the fidelity parameters"""
    _bounds: torch.Tensor
    """The bounds of the problem, specified as [[L1, L2, ...], [U1, U2, ...]]"""
    _model: SingleTaskMultiFidelityGP
    """The model used to predict the next point"""
    _nu: float
    """The nu parameter of the model, which controls the smoothness of the Matern kernel. Valid values are 1/2, 3/2 and
    5/2"""
    _num_configs_to_sample: int
    """The number of fixed configurations (including discrete and fidelity variables) to sample per iteration. This
    linearly increases the time it takes to acquire each point. If -1 is passed in, all possible configurations are
    sampled"""
    _biasing_temperature: float
    """The temperature to use while sampling `num_configs_to_sample` from a Gibbs distribution, where the probability
    of sampling a configuration is proportional to the cosine similarity between the configuration and the current
    target fidelity set"""
    _cost_model: DeterministicModel
    """The cost model"""
    _cost_aware_utility: InverseCostWeightedUtility
    """The cost aware utility function"""
    _acqf_num_restarts: int
    """The number of instances of the acquisition function optimisation process to run from different initialisations in
    order to avoid local minima"""
    _acqf_num_raw_samples: int
    """The number of Sobol samples to draw from the domain for evaluation via the posterior mean during acquisition
    function optimisation"""
    _acqf_batch_limit: int
    """The maximum number of raw samples to consider per batch during acquisition function optimisation"""
    _acqf_num_iterations: int
    """The number of iterations to run the acquisition function optimisation step for"""
    _kg_num_fantasies: int
    """The number of fantasy points to use to model the relationship between the different levels of fidelity during the
    knowledge gradient computation"""
    _kg_acqf_num_restarts: int
    """The number of instances of the knowledge gradient acquisition function optimisation process to run from different
    initialisations in order to avoid local minima"""
    _kg_acqf_num_raw_samples: int
    """The number of Sobol samples to draw from the domain for evaluation via the knowledge gradient during acquisition
    function optimisation"""
    _kg_acqf_batch_limit: int
    """The maximum number of raw samples to consider per batch during knowledge gradient acquisition function 
    optimisation"""
    _kg_acqf_num_iterations: int
    """The number of iterations to run the knowledge gradient acquisition function optimisation step for"""
    _linear_truncated: bool
    """Whether to use a linear truncated kernel or a standard kernel"""
    _tkwargs: dict
    """The keyword arguments to pass to torch.tensor"""

    def __init__(
        self,
        target_fidelity_sets: List[List[float]],
        num_problem_dimensions: int,
        cost_model: DeterministicModel,
        num_discrete_fidelity_dims: int = 0,
        discrete_fidelity_sets: List[List[float]] = [],
        num_continuous_fidelity_dims: int = 0,
        log_domain_dims: List[int] = [],
        log_severity: int | List[int] = 80,
        discretes_feature_values: Optional[Dict[int, List[float]]] = None,
        target_discrete_feature_sets: Optional[List[Dict[int, float]]] = None,
        maximise: bool = True,
        noiseless: bool = False,
        nu: float = 2.5,
        num_configs_to_sample: int = 8,
        biasing_temperature: float = 0.2,
        acqf_num_restarts: int = 20,
        acqf_num_raw_samples: int = 1024,
        acqf_batch_limit: int = 10,
        acqf_num_iterations: int = 200,
        kg_num_fantasies: int = 128,
        kg_acqf_num_restarts: int = 20,
        kg_acqf_num_raw_samples: int = 1024,
        kg_acqf_batch_limit: int = 10,
        kg_acqf_num_iterations: int = 200,
        mve_num_fantasies: int = 128,
        mve_num_candidates: int = 256,
        mve_num_mv_samples: int = 10,
        mve_num_y_samples: int = 128,
        y_scale: float = 1e0,
        linear_truncated: bool = True,
        seed: Optional[int] = None,
        smoke_test: bool = False,
        tkwargs: Optional[dict] = None,
    ) -> None:
        self._y_scale = y_scale

        num_fidelity_dims = 0
        if num_continuous_fidelity_dims > 0:
            num_fidelity_dims += num_continuous_fidelity_dims
        if num_discrete_fidelity_dims > 0:
            num_fidelity_dims += num_discrete_fidelity_dims
        if num_fidelity_dims == 0:
            raise Exception(
                "At least one of the number of continuous or discrete fidelity dimensions must be greater than 0"
            )

        if len(log_domain_dims) > 0:
            if isinstance(log_severity, list):
                assert len(log_severity) == len(
                    log_domain_dims
                ), "Either a single log severity or a log severity for each log-domain dimension should be specified"

        self._num_discrete_fidelity_dims = num_discrete_fidelity_dims
        self._num_continuous_fidelity_dims = num_continuous_fidelity_dims

        self._log_domain_dims = log_domain_dims
        self._log_severity = log_severity

        assert num_discrete_fidelity_dims == len(
            discrete_fidelity_sets
        ), "The number of discrete fidelity dims must match the number of discrete fidelity sets"
        for i in range(len(target_fidelity_sets)):
            assert (
                len(target_fidelity_sets[i]) == num_fidelity_dims
            ), "Each target fidelity set must have the same number of dimensions as the number of fidelity weights"
        if tkwargs is None:
            self._tkwargs = {
                "dtype": torch.double,
                "device": torch.device("cuda" if torch.cuda.is_available() else "cpu"),
            }
        else:
            self._tkwargs = tkwargs
        if seed is not None:
            torch.manual_seed(seed)
            np.random.seed(seed)
        # construct the target fidelity sets
        target_fidelity_set_dicts = [
            {
                j + num_problem_dimensions: target_fidelity_sets[i][j]
                for j in range(len(target_fidelity_sets[i]))
            }
            for i in range(len(target_fidelity_sets))
        ]
        # shuffle the target fidelity sets
        np.random.shuffle(target_fidelity_set_dicts)
        # construct the generator of target fidelity sets
        self._target_fidelity_sets_gen = itertools.cycle(target_fidelity_set_dicts)
        self._target_fidelity_set = next(self._target_fidelity_sets_gen)
        self._maximise = maximise
        self._noiseless = noiseless
        self._num_configs_to_sample = num_configs_to_sample if not smoke_test else 1
        self._biasing_temperature = biasing_temperature
        # construct the list of fixed fidelity features
        discrete_fidelity_permutations = itertools.product(*discrete_fidelity_sets) if len(discrete_fidelity_sets) > 0 else []
        fixed_fidelity_features_sets = []
        for fidelities in discrete_fidelity_permutations:
            fixed_features = {
                i + num_problem_dimensions: fidelities[i]
                for i in range(len(fidelities))
            }
            fixed_fidelity_features_sets.append(fixed_features)
        # construct the list of fixed discrete features
        if discretes_feature_values is not None:
            self._fixed_discrete_features_sets = []
            keys = list(discretes_feature_values.keys())
            assert (num_discrete := max(keys) - min(keys) + 1) == len(
                keys
            ), "The keys of `discrete_feature_values` must be contiguous and must start at the index after the continuous problem dimensions"
            assert (
                min(keys) == num_problem_dimensions - num_discrete
            ), "The keys of `discrete_feature_values` must start at the index after the number of continuous problem dimensions"
            assert all(
                [key < num_problem_dimensions for key in keys]
            ), "All discrete feature indices must be in the range of the problem dimensions"
            self._num_discrete_dims = num_discrete
            for values in itertools.product(
                *[discretes_feature_values[key] for key in keys]
            ):
                fixed_features = {keys[i]: values[i] for i in range(len(values))}
                self._fixed_discrete_features_sets.append(fixed_features)
            # construct the list of all fixed features
            all_fixed_feature_values = discretes_feature_values.copy()
            for i in range(num_discrete_fidelity_dims):
                all_fixed_feature_values[
                    i + num_problem_dimensions
                ] = discrete_fidelity_sets[i]
            self._all_fixed_features_sets = []
            keys = list(all_fixed_feature_values.keys())
            for values in itertools.product(
                *[all_fixed_feature_values[key] for key in keys]
            ):
                fixed_features = {keys[i]: values[i] for i in range(len(values))}
                self._all_fixed_features_sets.append(fixed_features)
            # assert that all target feature sets are in the list of all fixed feature sets
            for tdfs in target_discrete_feature_sets:
                assert any(
                    [
                        all(
                            [
                                tdfs[key] == self._all_fixed_features_sets[i][key]
                                for key in tdfs.keys()
                            ]
                        )
                        for i in range(len(self._all_fixed_features_sets))
                    ]
                ), "All target discrete feature sets must conform to the values specified in `discrete_feature_values`"
            self._target_discrete_feature_sets = target_discrete_feature_sets
        else:
            self._num_discrete_dims = 0
            self._fixed_discrete_features_sets = []
            self._target_discrete_feature_sets = []
            self._all_fixed_features_sets = fixed_fidelity_features_sets

        self._num_problem_dimensions = num_problem_dimensions
        self._total_dimensions = (
            num_problem_dimensions
            + num_continuous_fidelity_dims
            + num_discrete_fidelity_dims
        )
        # NOTE: the bounds must be from 0.0 to 1.0 as the model indiscriminately removes the bounds of the blocked
        # (fixed) features, i.e. if the bounds are not the same, the dimensions aren't aligned and the model will
        # exhibit undefined behaviour
        self._bounds = torch.tensor(
            [[0.0] * self._total_dimensions, [1.0] * self._total_dimensions],
            **self._tkwargs,
        )
        # set the Matern kernel's parameter
        self._nu = nu
        # construct the cost model
        self._cost_model = cost_model
        # construct the cost model and cost aware utility function
        self._cost_aware_utility = InverseCostWeightedUtility(
            cost_model=self._cost_model
        )
        # Set acquisition function optimisation parameters and knowledge gradient parameters
        self._acqf_num_restarts = acqf_num_restarts if not smoke_test else 1
        self._acqf_num_raw_samples = acqf_num_raw_samples if not smoke_test else 4
        self._acqf_batch_limit = acqf_batch_limit if not smoke_test else 1
        self._acqf_num_iterations = acqf_num_iterations if not smoke_test else 2
        self._kg_num_fantasies = kg_num_fantasies if not smoke_test else 4
        self._kg_acqf_num_restarts = kg_acqf_num_restarts if not smoke_test else 1
        self._kg_acqf_num_raw_samples = kg_acqf_num_raw_samples if not smoke_test else 4
        self._kg_acqf_batch_limit = kg_acqf_batch_limit if not smoke_test else 1
        self._kg_acqf_num_iterations = kg_acqf_num_iterations if not smoke_test else 2
        self._mve_num_candidates = mve_num_candidates
        self._linear_truncated = linear_truncated
        self._mve_num_fantasies = mve_num_fantasies
        self._mve_num_mv_samples = mve_num_mv_samples
        self._mve_num_y_samples = mve_num_y_samples
        # See https://stats.stackexchange.com/questions/126251/how-do-i-force-the-l-bfgs-b-to-not-stop-early-projected-gradient-is-zero
        self._optimize_options = {
            "gtol": 1e-5,  # (default 1e-5)
            "nonnegative": False,  # (default False)
            "iprint": -1,
        }

    def _map_to_log(self, x: np.ndarray | torch.Tensor, log_severity: int):
        """
        Maps the given x to the log space
        """
        return np.log(log_severity * x + 1) / np.log(log_severity + 1)

    def _map_from_log(self, x: np.ndarray | torch.Tensor, log_severity: int):
        """
        Maps the given x from the log space
        """
        return ((log_severity + 1) ** x - 1) / log_severity

    def _scale_y(self, scores):
        """
        Scales the given scores by the y_scale parameter
        """
        return scores * self._y_scale

    def _inv_scale_y(self, scores):
        """
        Inverse scales the given scores by the y_scale parameter
        """
        return scores / self._y_scale

    def _get_biased_subset_of_list(
        self, lst: List[Dict[int, float]]
    ) -> List[Dict[int, float]]:
        """
        Returns a random biased subset of the given list of dictionaries. Biases are based
        on the cosine similarity between each vector and the current target fidelity set.

        Parameters:
            lst (List[Dict[int, float]]): The list of dictionaries to get a subset of

        Returns:
            List[Dict[int, float]]: A random subset of the given list of dictionaries
        """
        subset_size = self._num_configs_to_sample
        if subset_size > len(lst) or len(lst) == 0:
            return lst

        # If there are no discrete fidelity dimensions, we can just return a random subset as the Gibbs distribution will result in equal probabilities for all configurations
        if self._num_discrete_fidelity_dims == 0:
            return lst[np.random.choice(len(lst), size=subset_size, replace=False)]

        lst_cpy = []
        x = np.zeros((self._total_dimensions))
        discrete_fid_indices = list(
            range(
                self._num_problem_dimensions,
                self._num_problem_dimensions + self._num_discrete_fidelity_dims,
            )
        )
        discrete_target_fid_values = np.array(
            [self._target_fidelity_set[k] for k in discrete_fid_indices]
        )
        x[list(self._target_fidelity_set.keys())] = list(
            self._target_fidelity_set.values()
        )
        target_cost = self._cost_model(torch.tensor(x, **self._tkwargs)).item()
        # filter out higher cost configurations
        for config in lst:
            if any([key in discrete_fid_indices for key in config.keys()]):
                x[list(config.keys())] = list(config.values())
                cost = self._cost_model(torch.tensor(x, **self._tkwargs)).item()
                if cost <= target_cost:
                    lst_cpy.append(config)
            else:
                # if the config does not contain any fidelity parameters, add it to the list as we then consider it without the cost
                lst_cpy.append(config)
        if subset_size == -1 or len(lst_cpy) <= subset_size:
            return lst_cpy
        cosine_similarity = np.empty((len(lst_cpy)))
        for i in range(len(lst_cpy)):
            # if the config does not contain any fidelity parameters, set the cosine similarity to 1.0
            if not any([key in discrete_fid_indices for key in lst_cpy[i].keys()]):
                cosine_similarity[i] = 1.0
                continue
            # otherwise, calculate the cosine similarity based on the discrete fidelity parameters
            conf_i = np.array([lst_cpy[i][k] for k in discrete_fid_indices])
            if (conf_i_norm := np.linalg.norm(conf_i)) == 0.0:
                cosine_similarity[i] = 0.0
                continue
            cosine_similarity[i] = np.dot(
                discrete_target_fid_values,
                conf_i,
            ) / (np.linalg.norm(discrete_target_fid_values) * conf_i_norm)
        # create biased weights based on the cosine similarity
        weights = np.exp(cosine_similarity / self._biasing_temperature)
        # get the index of the highest cosine similarity
        max_index = np.argmax(cosine_similarity)
        max_cosine_similarity = cosine_similarity[max_index]
        # ensure that the highest cosine similarity is sampled, as we always want to include it as a baseline
        weights[max_index] = 0.0
        # normalise the weights
        weights /= np.sum(weights)
        # get the indices of the subset
        subset_indices = np.random.choice(
            np.arange(len(lst_cpy)), size=subset_size - 1, replace=False, p=weights
        )
        # add the index of the highest cosine similarity
        subset_indices = np.append(subset_indices, max_index)
        # restore the highest cosine similarity
        weights[max_index] = max_cosine_similarity
        # sort the weights in ascending order and sort the order of the corresponding indices
        for i in range(len(subset_indices)):
            for j in range(i + 1, len(subset_indices)):
                if weights[subset_indices[i]] < weights[subset_indices[j]]:
                    temp = subset_indices[i]
                    subset_indices[i] = subset_indices[j]
                    subset_indices[j] = temp
        # return the biased subset
        return [lst_cpy[i] for i in subset_indices]

    def _generate_projection_functions(
        self,
    ) -> List[Callable[[torch.Tensor], torch.Tensor]]:
        """
        Generates a list of projection functions for each target set

        Returns:
            List[Callable[[torch.Tensor], torch.Tensor]]: The list of projection functions
        """

        def _helper(
            fidelity_set: Dict[int, float]
        ) -> Callable[[torch.Tensor], torch.Tensor]:
            """
            Helper function for generating a projection function for the given fidelity set

            Parameters:
                fidelity_set (Dict[int, float]): The fidelity set to generate a projection function for

            Returns:
                Callable[[torch.Tensor], torch.Tensor]: The generated projection function
            """

            def _project(X: torch.Tensor) -> torch.Tensor:
                """
                Projects the given points to the specified level of fidelity

                Parameters:
                    X (torch.Tensor): The points to project

                Returns:
                    torch.Tensor: The projected points
                """
                return project_to_target_fidelity(X=X, target_fidelities=fidelity_set)

            return _project

        return [
            _helper(
                {
                    **tdfs,
                    **self._target_fidelity_set,
                }
            )
            for tdfs in (self._target_discrete_feature_sets or [{}])
        ]

    def _get_mfkg(
        self, pending_points: torch.Tensor
    ) -> qMultiTargetMultiFidelityKnowledgeGradient:
        """
        Constructs and returns a Multi-Fidelity Knowledge Gradient acquisition function

        Parameters:
            pending_points (torch.Tensor): The points that are currently being evaluated num_points (int): The number of
            points to return

        Returns:
            qMultiFidelityKnowledgeGradient: The constructed Multi-Fidelity Knowledge Gradient acquisition function
        """
        # FixedFeatureAcquisitionFunction effectively reduces the dimensionality of the problem.
        logger.debug(f"Current target fidelity set: {self._target_fidelity_set}\n")
        curr_val_acqf = FixedFeatureAcquisitionFunction(
            # Set the Posterior Mean of the given model as the acquisition function
            acq_function=PosteriorMean(self._model, maximize=self._maximise),
            # The expected dimension of the function, i.e. the number of problem dimensions + the number of fidelities
            d=self._total_dimensions,
            # Indices of columns in X that should be fixed to the given values
            columns=list(self._target_fidelity_set.keys()),
            # The values to which the above columns should be fixed
            values=list(self._target_fidelity_set.values()),
        )
        if len(pending_points) > 0:
            # set the currently pending points of the acquisition function
            curr_val_acqf.set_X_pending(
                pending_points[:, : self._num_problem_dimensions]
            )
        # For a good explanation on how the `optimize_acqf` function works, see
        # https://github.com/pytorch/botorch/discussions/1877 (`optimize_acqf` gets
        # called by `optimize_acqf_mixed`)
        if len(self._fixed_discrete_features_sets) == 0:
            # If there are no fixed discrete features, we use the standard `optimize_acqf` function
            acqf_optimizer = optimize_acqf
            kwargs = {}
        else:
            # If there are fixed discrete features, we use the `optimize_acqf_mixed` function
            acqf_optimizer = optimize_acqf_mixed
            kwargs = {
                "fixed_features_list": self._get_biased_subset_of_list(
                    self._fixed_discrete_features_sets
                )
            }
        # get the current highest value of the acquisition function
        _, current_value = acqf_optimizer(
            # the above-created acquisition function
            acq_function=curr_val_acqf,
            # As we are using a fixed feature acquisition function, we need to pass in the bounds of dimensions
            # pertaining to the problem, i.e. not the dimensions of the fidelities
            bounds=self._bounds[:, : self._num_problem_dimensions],
            # The number of candidates to generate
            q=1,
            # Draw `raw_samples` number of Sobol samples from the domain for evaluation via the posterior mean
            raw_samples=self._kg_acqf_num_raw_samples,
            # batch_limit = max number of raw samples to consider at a time maxiter = max number of iterations to run
            # the optimisation process for
            options={
                "batch_limit": self._kg_acqf_batch_limit,
                "maxiter": self._kg_acqf_num_iterations,
                **self._optimize_options,
            },
            # The `raw_samples` are evaluated in batches of size `batch_limit`, then `num_restarts` samples are sampled
            # with Boltzmann sampling. For each of these samples, gradient-based optimization is run by means of the
            # L-BFGS-B optimiser, for a maximum of `maxiter` iterations. The best `q` point(s) from each of the
            # `num_restarts` samples are then returned.
            num_restarts=self._kg_acqf_num_restarts,
            **kwargs,
        )
        logger.debug(
            f"Current base acqf value: {self._inv_scale_y(current_value.item())}"
        )
        return qMultiTargetMultiFidelityKnowledgeGradient(
            # The model that was passed in
            model=self._model,
            # Number of points to use to model the relationship between the different levels of fidelity Using more
            # points better explores the correlations between the different fidelity levels and increases the accuracy
            # of the acquisition function
            num_fantasies=self._kg_num_fantasies,
            # pending points are the points that are currently being evaluated
            X_pending=pending_points if len(pending_points) > 0 else None,
            # above calculated value(s)
            current_value=current_value,
            # above constructed cost_aware_utility
            cost_aware_utility=self._cost_aware_utility,
            # required function for projecting the points to the specified level of fidelity
            projections=self._generate_projection_functions(),
            posterior_transform=None
            if self._maximise
            else ScalarizedPosteriorTransform(torch.tensor([-1.0], **self._tkwargs)),
        )

    def _get_mve_candidates(self) -> torch.Tensor:
        """
        Returns a set of candidates to evaluate the MVE acquisition function on
        """
        candidate_set = torch.empty(
            self._mve_num_candidates, self._total_dimensions, **self._tkwargs
        )
        fixed_features = np.array(
            [
                list(self._all_fixed_features_sets[i].values())
                for i in range(len(self._all_fixed_features_sets))
            ]
        )
        if self._num_problem_dimensions != self._num_discrete_dims:
            candidate_set[
                :, : self._num_problem_dimensions - self._num_discrete_dims
            ] = torch.rand(
                self._mve_num_candidates,
                self._num_problem_dimensions - self._num_discrete_dims,
                **self._tkwargs,
            )
        if self._num_discrete_dims > 0 or self._num_continuous_fidelity_dims > 0:
            candidate_set[
                :,
                self._num_problem_dimensions
                - self._num_discrete_dims : self._num_problem_dimensions
                + self._num_discrete_fidelity_dims,
            ] = torch.tensor(
                fixed_features[
                    np.random.choice(
                        len(self._all_fixed_features_sets),
                        size=self._mve_num_candidates,
                        replace=True,
                    )
                ],
                **self._tkwargs,
            )
        if self._num_continuous_fidelity_dims > 0:
            candidate_set[:, -self._num_continuous_fidelity_dims :] = torch.rand(
                self._mve_num_candidates,
                self._num_continuous_fidelity_dims,
                **self._tkwargs,
            )
        return candidate_set

    def _get_mfmve(
        self, pending_points: torch.Tensor
    ) -> qMultiTargetMultiFidelityMaxValueEntropy:
        """
        Constructs and returns a Multi-Fidelity Max Value Entropy acquisition function

        Parameters:
            pending_points (torch.Tensor): The points that are currently being evaluated num_points (int): The number of
            points to return

        Returns:
            qMultiTargetMultiFidelityMaxValueEntropy: The constructed Multi-Fidelity Max Value Entropy acquisition function
        """
        logger.debug(f"Current target fidelity set: {self._target_fidelity_set}\n")

        return qMultiTargetMultiFidelityMaxValueEntropy(
            # The model that was passed in
            model=self._model,
            candidate_set=self._get_mve_candidates(),
            num_fantasies=self._mve_num_fantasies,
            num_mv_samples=self._mve_num_mv_samples,
            num_y_samples=self._mve_num_y_samples,
            maximize=self._maximise,
            # pending points are the points that are currently being evaluated
            X_pending=pending_points if len(pending_points) > 0 else None,
            # above constructed cost_aware_utility
            cost_aware_utility=self._cost_aware_utility,
            # required function for projecting the points to the specified level of fidelity
            projections=self._generate_projection_functions(),
        )

    def _get_mflbmve(
        self, pending_points: torch.Tensor
    ) -> qMultiTargetMultiFidelityLowerBoundMaxValueEntropy:
        """
        Constructs and returns a Multi-Fidelity Lower Bound Max Value Entropy acquisition function

        Parameters:
            pending_points (torch.Tensor): The points that are currently being evaluated num_points (int): The number of
            points to return

        Returns:
            qMultiTargetMultiFidelityLowerBoundMaxValueEntropy: The constructed Multi-Fidelity Lower Bound Max Value Entropy
            acquisition function
        """
        logger.debug(f"Current target fidelity set: {self._target_fidelity_set}\n")

        return qMultiTargetMultiFidelityLowerBoundMaxValueEntropy(
            # The model that was passed in
            model=self._model,
            candidate_set=self._get_mve_candidates(),
            num_fantasies=self._mve_num_fantasies,
            num_mv_samples=self._mve_num_mv_samples,
            num_y_samples=self._mve_num_y_samples,
            maximize=self._maximise,
            # pending points are the points that are currently being evaluated
            X_pending=pending_points if len(pending_points) > 0 else None,
            # above constructed cost_aware_utility
            cost_aware_utility=self._cost_aware_utility,
            # required function for projecting the points to the specified level of fidelity
            projections=self._generate_projection_functions(),
        )

    def _optimize_af_and_get_points(
        self,
        num_points: int,
        acqf: qMultiTargetMultiFidelityKnowledgeGradient
        | qMultiTargetMultiFidelityMaxValueEntropy
        | qMultiTargetMultiFidelityLowerBoundMaxValueEntropy,
    ) -> np.ndarray:
        """
        Optimises the given Multi-Fidelity Knowledge Gradient acquisition function and returns the next point(s) to
        evaluate

        Parameters:
            num_points (int): The number of points to return
            acqf (qMultiFidelityKnowledgeGradient | qMultiTargetMultiFidelityMaxValueEntropy |
            qMultiTargetMultiFidelityLowerBoundMaxValueEntropy): The acquisition function to optimise

        Returns:
            np.ndarray: The next point(s) to evaluate
        """
        target_fidelity_list = np.round(
            list(self._target_fidelity_set.values()), 3
        ).tolist()
        logger.info(f"Current target fidelity set: {target_fidelity_list}")

        ff_candidate_list, ff_acq_value_list = [], []
        
        fixed_features_list = self._get_biased_subset_of_list(
            self._all_fixed_features_sets
        ) if len(self._all_fixed_features_sets) > 0 else [None]

        num_negative_acqf_values = 0
        for ff, fixed_features in enumerate(fixed_features_list):
            candidate, acq_value = optimize_acqf(
                acq_function=acqf,
                bounds=self._bounds,
                q=1,
                num_restarts=self._acqf_num_restarts,
                raw_samples=self._acqf_num_raw_samples,
                options={
                    "batch_limit": self._acqf_batch_limit,
                    "maxiter": self._acqf_num_iterations,
                    **self._optimize_options,
                },
                fixed_features=fixed_features,
                return_best_only=True,
            )

            if acq_value < 0.0:
                num_negative_acqf_values += 1
            ff_candidate_list.append(candidate)
            ff_acq_value_list.append(acq_value)

            point = np.round(candidate.cpu().numpy().flatten(), 3)
            logger.debug(
                f"{ff + 1}/{len(fixed_features_list)} -> point:"
                f"\n\t{point[:self._num_problem_dimensions]}"
                f"\n\twith acqf value: {self._inv_scale_y(acq_value.item())}"
                f"\n\twith fidelity set: {point[self._num_problem_dimensions:]}"
                f"\n\tand cost: {self._cost_model(candidate).item()}\n"
            )

        if num_negative_acqf_values > len(fixed_features_list) / 2:
            warn_str = "More than half of the acquisition function evaluations resulted in negative values."
            if num_negative_acqf_values == len(fixed_features_list):
                warn_str = "All acquisition function values were negative."
            logger.warning(
                warn_str + " This is likely due to numerical instability. Consider:"
                "\n  -  scaling the objective function"
                "\n  -  increasing the number of fantasy points"
                "\n  -  increasing the budget for optimising the acquisition function"
            )

        ff_candidate_list = torch.stack(ff_candidate_list)
        ff_acq_values = torch.stack(ff_acq_value_list)

        # get the best `num_points` points
        best_points_indices = torch.argsort(ff_acq_values, descending=True)[:num_points]

        candidate_points, acqf_values = (
            ff_candidate_list[best_points_indices],
            ff_acq_values[best_points_indices],
        )

        costs = self._cost_model(candidate_points).detach().cpu().numpy().flatten()
        candidate_points = candidate_points.squeeze(1)
        points = np.round(candidate_points.cpu().numpy(), 3)
        acqf_values = self._inv_scale_y(acqf_values.detach().cpu().numpy())

        logger.info(
            f"Added {points.shape[0]} points:"
            f"\n\tproblem-dims:\n{points[:, :self._num_problem_dimensions]}"
            f"\n\tfidelity-dims:\n{points[:, self._num_problem_dimensions:]}"
            f"\n\twith acqf values:\n{acqf_values},"
            f"\n\twith costs:\n{costs}\n"
        )

        ff_candidate_list.detach()
        ff_acq_values.detach()

        return candidate_points

    def get_next_points(
        self,
        num_targets: int,
        af_method: str,
        points_per_target: int = 1,
        pending_points: List[np.ndarray] | np.ndarray = None,
        target_fidelity_set: Optional[List[float]] = None,
    ) -> np.ndarray:
        """
        Returns the next point(s) to evaluate for the next target fidelity set. If there are no more target fidelity
        sets, the next point(s) to evaluate for the first target fidelity set are returned, then the second, etc.

        Parameters:
            num_targets (int): The number of target fidelity sets to return points for
            points_per_target (int): The number of points to return per target fidelity set
            pending_points (List[np.ndarray]|np.ndarray): The points that are currently being evaluated
            af_method (str): The acquisition function to use. Can be one of the following: mfkg (Multi-Fidelity
            Knowledge Gradient), mfmve (Multi-Fidelity Max Value Entropy), mflbmve (Multi-Fidelity Lower Bound Max
            Value Entropy)
            target_fidelity_set (Optional[List[float]]): A specific target fidelity set to use. The default and
            recommended behaviour iterates over the target fidelity sets per call to this function.

        Returns:
            np.ndarray: The next point(s) to evaluate
        """
        if target_fidelity_set is not None:
            assert (
                len(target_fidelity_set)
                == self._num_discrete_fidelity_dims + self._num_continuous_fidelity_dims
            ), "The target fidelity set must have the same number of dimensions as the number of fidelity dimensions"
        if pending_points is not None and len(pending_points) != 0:
            if isinstance(pending_points, np.ndarray):
                stacked_points = pending_points
            else:
                stacked_points = (
                    np.vstack(pending_points)
                    if len(pending_points) > 0
                    else np.array([])
                )
            pending_points_tensor = torch.tensor(stacked_points, **self._tkwargs)
        else:
            pending_points_tensor = torch.tensor([], **self._tkwargs)

        points = torch.tensor([], **self._tkwargs)

        af = None
        for _ in range(num_targets):
            # get the acquisition function
            if af_method == "mfkg":
                af = self._get_mfkg(pending_points_tensor)
            elif af_method == "mfmve":
                af = self._get_mfmve(pending_points_tensor)
            elif af_method == "mflbmve":
                af = self._get_mflbmve(pending_points_tensor)
            else:
                raise ValueError(
                    f"Invalid acquisition function method: {af_method}. "
                    "Must be one of the following: mfkg, mfmve, mflbmve"
                )
            # optimise the acquisition function and get the next points to evaluate
            new_points = self._optimize_af_and_get_points(points_per_target, af)
            points = torch.cat((points, new_points), dim=0)
            pending_points_tensor = torch.cat(
                (pending_points_tensor, new_points), dim=0
            )
            # update the target fidelity set to the next one
            self._target_fidelity_set = next(self._target_fidelity_sets_gen)
            logger.debug(
                f"Target fidelity set updated to {self._target_fidelity_set}\n"
            )

        pending_points_tensor.detach()
        points = points.detach().cpu().numpy()

        # map the points from the log space
        for i, dim in enumerate(self._log_domain_dims):
            if isinstance(self._log_severity, list):
                points[:, dim] = self._map_from_log(
                    points[:, dim], self._log_severity[i]
                )
            else:
                points[:, dim] = self._map_from_log(points[:, dim], self._log_severity)

        return points

    def get_next_point(
        self, pending_points: List[np.ndarray] | np.ndarray
    ) -> np.ndarray:
        """
        Returns the next point to evaluate

        Params:
            pending_points (List[np.ndarray]|np.ndarray): The points that are currently being evaluated

        Returns:
            np.ndarray: The next point to evaluate
        """
        return self.get_next_points(1, pending_points)

    def constrain_fixed_feature_sets(
        self,
        lhs: List[Tuple[float, int]],
        operator: str,
        rhs: List[Tuple[float, int]] = [],
        lhs_constant: float = 0.0,
        rhs_constant: float = 0.0,
    ) -> None:
        """
        Constrains the internal list of fixed feature sets to optimise for by removing fixed feature combinations where
        the given expression evaluates to True. For example:

        lhs = [(1.0, 2), (2.0, 3)]

        operator = "<"

        rhs = [(1.5, 2), (1.5, 3)]

        constrain_fixed_feature_sets(lhs, operator, rhs)

        would filter the internal list of fixed feature sets to optimise for to only include those where the the value
        of (the fixed feature at index 2)*1.0 + (the fixed feature at index 3)*2.0 >= (the fixed feature at index 2)*1.5
        \+ (the fixed feature at index 3)*1.5.

        Parameters:
            lhs (List[Tuple[float, int]]): The left hand side of the expression operator (str): The operator to use for
            the expression. Can be one of the following: <, <=, >, >=, ==, != rhs (List[Tuple[float, int]]): The right
            hand side of the expression lhs_constant (float): The constant to add to the left hand side of the
            expression rhs_constant (float): The constant to add to the right hand side of the expression
        """
        assert operator in [
            "<",
            "<=",
            ">",
            ">=",
            "==",
            "!=",
        ], "The operator must be one of the following: <, <=, >, >=, ==, !="

        def _constrain_fixed_feature_sets_helper(
            dicts_to_filter: List[Dict[int, float]]
        ):
            i = 0
            while i < len(dicts_to_filter):
                lhs_value = lhs_constant
                rhs_value = rhs_constant
                for j in range(len(lhs)):
                    lhs_value += lhs[j][0] * dicts_to_filter[i][lhs[j][1]]
                for j in range(len(rhs)):
                    rhs_value += rhs[j][0] * dicts_to_filter[i][rhs[j][1]]
                if operator == "<":
                    if lhs_value - rhs_value < FLOAT_PRECISION:
                        dicts_to_filter.pop(i)
                        continue
                elif operator == "<=":
                    if lhs_value - rhs_value <= FLOAT_PRECISION:
                        dicts_to_filter.pop(i)
                        continue
                elif operator == ">":
                    if lhs_value - rhs_value > FLOAT_PRECISION:
                        dicts_to_filter.pop(i)
                        continue
                elif operator == ">=":
                    if lhs_value - rhs_value >= FLOAT_PRECISION:
                        dicts_to_filter.pop(i)
                        continue
                elif operator == "==":
                    if abs(lhs_value - rhs_value) <= FLOAT_PRECISION:
                        dicts_to_filter.pop(i)
                        continue
                elif operator == "!=":
                    if abs(lhs_value - rhs_value) > FLOAT_PRECISION:
                        dicts_to_filter.pop(i)
                        continue
                i += 1

        _constrain_fixed_feature_sets_helper(self._fixed_discrete_features_sets)
        _constrain_fixed_feature_sets_helper(self._all_fixed_features_sets)

    def filter_fixed_feature_sets(
        self, cases_to_remove: List[Dict[int, float | int]]
    ) -> None:
        """
        For each specified set of discrete feature index-value pairs, removes all fixed features to optimise for where
        the fixed features contain the specified set of discrete feature index-value pairs. For example:

        cases_to_remove = [{1: 2, 2: 3}, {1: 3, 2: 4}]

        filter_fixed_feature_sets(cases_to_remove)

        would filter the internal list of fixed feature sets to optimise for to only include those where the fixed
        features do not contain the set of discrete feature index-value pairs {1: 2, 2: 3} or {1: 3, 2: 4}.

        Parameters:
            cases_to_remove (List[Dict[int, float|int]]): The list of discrete feature index-value pairs to remove
        """

        def _filter_fixed_feature_sets_helper(dicts_to_filter: List[Dict[int, float]]):
            i = 0
            while i < len(dicts_to_filter):
                for case in cases_to_remove:
                    if all(
                        [
                            abs(dicts_to_filter[i][key] - case[key]) < FLOAT_PRECISION
                            for key in case
                        ]
                    ):
                        dicts_to_filter.pop(i)
                        break
                else:
                    i += 1

        _filter_fixed_feature_sets_helper(self._fixed_discrete_features_sets)
        _filter_fixed_feature_sets_helper(self._all_fixed_features_sets)

    def set_device_and_bs(
        self, device: str, acqf_batch_limit: int, acqf_kg_batch_limit: int
    ):
        """
        Sets the device to use for the model as well as the batch limit for the acquisition function optimisation
        processes

        Parameters:
            device (str): The device to use for the model
            acqf_batch_limit (int): The batch limit for the acquisition function optimisation processes
            kg_acqf_batch_limit (int): The batch limit for the knowledge gradient acquisition function optimisation
        """
        self._tkwargs["device"] = torch.device(device)
        self._bounds = self._bounds.to(self._tkwargs["device"])
        self._acqf_batch_limit = acqf_batch_limit
        self._kg_acqf_batch_limit = acqf_kg_batch_limit

    def get_random_initial_points_per_config(
        self, points_per_fixed_configuration: int
    ) -> np.ndarray:
        """
        Returns a list of random points to evaluate for each fixed configuration

        Parameters:
            points_per_fixed_configuration (int): The number of points to return for each fixed configuration

        Returns:
            np.ndarray: The list of random points to evaluate for each fixed configuration. If there are no fixed
            configurations, returns a list of `points_per_fixed_configuration` random points. If there are no continuous
            dimensions, returns a list of all fixed feature configurations.
        """
        assert (
            points_per_fixed_configuration > 0
        ), "The number of points per fixed configuration must be greater than 0"
        num_continuous_dims = (
            self._num_problem_dimensions
            - self._num_discrete_dims
            + self._num_continuous_fidelity_dims
        )
        if num_continuous_dims > 0:
            gen = sobol.generator(num_continuous_dims)
        else:
            # if there are no continuous dimensions, return the list of all fixed feature configurations
            return np.array(
                [
                    list(fixed_set.values())
                    for fixed_set in self._all_fixed_features_sets
                ]
            )
        # if there are no fixed features, return a list of `points_per_fixed_configuration` random points
        if len(self._all_fixed_features_sets) == 0:
            return np.array([next(gen) for _ in range(points_per_fixed_configuration)])
        # initialise the array of points
        points = np.empty(
            (
                points_per_fixed_configuration * len(self._all_fixed_features_sets),
                self._total_dimensions,
            )
        )
        for i in range(len(self._all_fixed_features_sets)):
            sobol_points = np.array(
                [next(gen) for _ in range(points_per_fixed_configuration)]
            )
            # map the appropriate dimensions of the Sobol points to the continuous problem dimensions, if there are any
            if self._num_problem_dimensions - self._num_discrete_dims > 0:
                points[
                    i
                    * points_per_fixed_configuration : (i + 1)
                    * points_per_fixed_configuration,
                    : self._num_problem_dimensions - self._num_discrete_dims,
                ] = sobol_points[
                    :, : self._num_problem_dimensions - self._num_discrete_dims
                ].reshape(
                    points_per_fixed_configuration, -1
                )
            # map the appropriate dimensions of the Sobol points to the continuous fidelity dimensions, if there are any
            if self._num_continuous_fidelity_dims > 0:
                points[
                    i
                    * points_per_fixed_configuration : (i + 1)
                    * points_per_fixed_configuration,
                    -self._num_continuous_fidelity_dims :,
                ] = sobol_points[:, -self._num_continuous_fidelity_dims :].reshape(
                    points_per_fixed_configuration, -1
                )
            # tile the fixed features
            fixed_features = np.array(list(self._all_fixed_features_sets[i].values()))
            points[
                i
                * points_per_fixed_configuration : (i + 1)
                * points_per_fixed_configuration,
                self._num_problem_dimensions
                - self._num_discrete_dims : self._num_problem_dimensions
                + self._num_discrete_fidelity_dims,
            ] = np.tile(fixed_features, (points_per_fixed_configuration, 1))

        # map the points from the log space
        for i, dim in enumerate(self._log_domain_dims):
            if isinstance(self._log_severity, list):
                points[:, dim] = self._map_from_log(
                    points[:, dim], self._log_severity[i]
                )
            else:
                points[:, dim] = self._map_from_log(points[:, dim], self._log_severity)

        return points

    def fit(self, points: np.ndarray, scores: np.ndarray, kernel_kwargs={}) -> None:
        """
        Fits the model to the given points and scores

        Parameters:
            points (np.ndarray): The points to fit the model to
            scores (np.ndarray): The scores to fit the model to
        """
        # make a copy to prevent aliasing
        points = points.copy()
        # if scores is one-dimensional, reshape it to be two-dimensional
        if len(scores.shape) == 1:
            scores = scores.reshape(-1, 1)
        # map the points to the log space
        for i, dim in enumerate(self._log_domain_dims):
            if isinstance(self._log_severity, list):
                points[:, dim] = self._map_to_log(points[:, dim], self._log_severity[i])
            else:
                points[:, dim] = self._map_to_log(points[:, dim], self._log_severity)
        # convert the points to tensors for use with the model
        points_tensor = torch.tensor(points, **self._tkwargs)
        scores_tensor = torch.tensor(self._scale_y(scores), **self._tkwargs)
        # fit the model to the points and scores
        if not self._noiseless:
            self._model = SingleTaskMultiFidelityGP(
                train_X=points_tensor,
                train_Y=scores_tensor,
                nu=self._nu,
                linear_truncated=self._linear_truncated,
                outcome_transform=Standardize(m=1),
                data_fidelities=list(self._target_fidelity_set.keys()),
                kernel_kwargs=kernel_kwargs,
            )
        else:
            self._model = FixedNoiseMultiFidelityGP(
                train_X=points_tensor,
                train_Y=scores_tensor,
                train_Yvar=torch.full_like(scores_tensor, 1e-6),
                nu=self._nu,
                linear_truncated=self._linear_truncated,
                outcome_transform=Standardize(m=1),
                data_fidelities=list(self._target_fidelity_set.keys()),
                kernel_kwargs=kernel_kwargs,
            )
        # optimise the model's hyperparameters
        mll = ExactMarginalLogLikelihood(self._model.likelihood, self._model)

        try:
            fit_gpytorch_mll(mll)
        except:
            fit_gpytorch_mll(mll, optimizer=fit_gpytorch_mll_torch)

        if mll.training:
            logger.error(f"mll failed to fit")

        # free the memory used by the previously created tensors
        points_tensor.detach()
        scores_tensor.detach()

    def predict(
        self, points: np.ndarray | torch.Tensor
    ) -> Tuple[np.ndarray, np.ndarray]:
        """
        Predicts the mean and variance of the function at the given point(s)

        Parameters:
            points (np.ndarray): The points at which to predict the function

        Returns:
            Tuple[np.ndarray, np.ndarray]: The mean and variance of the function at the given point(s)
        """
        # make a copy to prevent aliasing
        points = points.copy()
        # map the points to the log space
        for i, dim in enumerate(self._log_domain_dims):
            if isinstance(self._log_severity, list):
                points[:, dim] = self._map_to_log(points[:, dim], self._log_severity[i])
            else:
                points[:, dim] = self._map_to_log(points[:, dim], self._log_severity)

        return self._predict(points)

    def _predict(
        self, points: np.ndarray | torch.Tensor
    ) -> Tuple[np.ndarray, np.ndarray]:
        """
        Predicts the mean and variance of the function at the given point(s) without mapping the points to the log space

        Parameters:
            points (np.ndarray): The points at which to predict the function

        Returns:
            Tuple[np.ndarray, np.ndarray]: The mean and variance of the function at the given point(s)
        """
        # get the mean and variance of the function at the given point
        if isinstance(points, np.ndarray):
            assert len(points.shape) == 2, "The point must be a 2D array"
            points = points.reshape(points.shape[0], 1, points.shape[1])
            point_tensor = torch.tensor(points, **self._tkwargs)
            posterior = self._model.posterior(point_tensor)
        else:
            posterior = self._model.posterior(points)
        return (
            self._inv_scale_y(
                posterior.mean.flatten().detach().cpu().numpy().reshape(-1)
            ),
            self._inv_scale_y(
                self._inv_scale_y(
                    posterior.variance.flatten().detach().cpu().numpy().reshape(-1)
                )
            ),
        )

    def get_best_prediction(
        self, fidelities: np.ndarray
    ) -> Tuple[np.ndarray, float, float]:
        """
        Returns the best predicted point and corresponding mean and variance of the model at the given fidelities

        Parameters:
            fidelities (np.ndarray): The fidelities at which to estimate the best prediction

        Returns:
            Tuple[np.ndarray, float, float]: The point, mean and variance of the best prediction
        """
        assert len(fidelities) == len(
            self._target_fidelity_set
        ), "The number of fidelities specified must match the number of target fidelities"
        # if any of the fidelity dimensions are in the log space, map them to the log space
        for i, dim in enumerate(self._log_domain_dims):
            if dim >= self._num_problem_dimensions:
                index = dim - self._num_problem_dimensions
                if isinstance(self._log_severity, list):
                    fidelities[index] = self._map_to_log(
                        fidelities[index], self._log_severity[i]
                    )
                else:
                    fidelities[index] = self._map_to_log(
                        fidelities[index], self._log_severity
                    )
        # FixedFeatureAcquisitionFunction effectively reduces the dimensionality of the problem.
        rec_acqf = FixedFeatureAcquisitionFunction(
            acq_function=PosteriorMean(self._model, maximize=self._maximise),
            d=self._total_dimensions,
            columns=list(self._target_fidelity_set.keys()),
            values=fidelities.tolist(),
        )
        if len(self._fixed_discrete_features_sets) == 0:
            # If there are no fixed discrete features, we use the standard `optimize_acqf` function
            acqf_optimizer = optimize_acqf
            kwargs = {}
        else:
            # If there are fixed discrete features, we use the `optimize_acqf_mixed` function
            acqf_optimizer = optimize_acqf_mixed
            kwargs = {"fixed_features_list": self._fixed_discrete_features_sets}
        # get the best prediction
        best_point, _ = acqf_optimizer(
            acq_function=rec_acqf,
            bounds=self._bounds[:, : self._num_problem_dimensions],
            q=1,
            num_restarts=self._acqf_num_restarts,
            raw_samples=self._acqf_num_raw_samples,
            options={
                "batch_limit": self._acqf_batch_limit,
                "maxiter": self._acqf_num_iterations,
                **self._optimize_options,
            },
            **kwargs,
        )
        # construct the best point
        projected_point = rec_acqf._construct_X_full(best_point).detach().cpu().numpy()
        # get the posterior mean and variance at the constructed point
        mean, variance = self._predict(projected_point.reshape(1, -1))
        # map the point from the log space
        for i, dim in enumerate(self._log_domain_dims):
            if isinstance(self._log_severity, list):
                projected_point[0][dim] = self._map_to_log(
                    projected_point[0][dim], self._log_severity[i]
                )
            else:
                projected_point[0][dim] = self._map_to_log(
                    projected_point[0][dim], self._log_severity
                )
        return projected_point[0], mean[0], variance[0]

    def halve_batch_size(self) -> bool:
        """
        Attempt to reduce memory usage by halving the batch size of the acquisition function optimisation process.

        Returns:
            bool: Whether the batch size was successfully halved
        """
        if self._acqf_batch_limit == self._kg_acqf_batch_limit == 1:
            return False
        self._acqf_batch_limit = max(1, self._acqf_batch_limit // 2)
        self._kg_acqf_batch_limit = max(1, self._kg_acqf_batch_limit // 2)
        logger.info(
            f"Batch size halved to {self._acqf_batch_limit} for acquisition function optimisation and to {self._kg_acqf_batch_limit} for knowledge gradient acquisition function optimisation"
        )
        return True

    def free_cuda_memory(self):
        """
        Frees any unused GPU memory allocated to CUDA
        """
        torch.cuda.empty_cache()
