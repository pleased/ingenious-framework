from collections.abc import Sequence

import matplotlib.pyplot as plt
import numpy as np

BOUNDS = [0.0, 1.0]


def forrester_hf(x: np.ndarray) -> np.ndarray:
    return (6 * x - 2) ** 2 * np.sin(12 * x - 4)


class ForresterSuite(Sequence):
    def __init__(
        self,
        num_funcs: int,
        lower_bound_mean: float,
        lower_bound_fluctuation: float,
        upper_bound_mean: float,
        upper_bound_fluctuation: float,
        jitter: float = 0.0,
    ) -> None:
        self.num_funcs = num_funcs
        funcs = []
        for _ in range(self.num_funcs):
            a = np.random.uniform(-1.5, 2.5)
            b = np.random.uniform(0, 20)
            c = 0
            funcs.append(
                ForresterFunc(
                    a,
                    b,
                    c,
                    lower_bound_mean,
                    lower_bound_fluctuation,
                    upper_bound_mean,
                    upper_bound_fluctuation,
                    np.random.uniform(-jitter, jitter),
                )
            )
        self.funcs = funcs

    def __getitem__(self, index: int) -> "ForresterFunc":
        return self.funcs[index]

    def __len__(self) -> int:
        return self.num_funcs


class ForresterFunc:
    def __init__(
        self,
        a: float,
        b: float,
        c: float,
        lower_bound_mean: float,
        lower_bound_fluctuation: float,
        upper_bound_mean: float,
        upper_bound_fluctuation: float,
        jitter: float = 0.0,
    ) -> None:
        self.a = a
        self.b = b
        self.c = c
        self.jitter = jitter
        self.min = self._find_min()
        self.max = self._find_max()
        self.new_lower = np.random.uniform(
            lower_bound_mean - lower_bound_fluctuation,
            lower_bound_mean + lower_bound_fluctuation,
        )
        self.new_upper = np.random.uniform(
            upper_bound_mean - upper_bound_fluctuation,
            upper_bound_mean + upper_bound_fluctuation,
        )

    def __call__(self, x: np.ndarray) -> np.ndarray:
        return self._scale(self._forrester(x))

    def _forrester(self, x: np.ndarray) -> np.ndarray:
        return (
            self.a * forrester_hf(x + self.jitter)
            + self.b * (x + self.jitter - 0.5)
            + self.c
        )

    def _find_max(self) -> float:
        x = np.linspace(BOUNDS[0], BOUNDS[1], 1000)
        y = self._forrester(x)
        return y.max()

    def _find_min(self) -> float:
        x = np.linspace(BOUNDS[0], BOUNDS[1], 1000)
        y = self._forrester(x)
        return y.min()

    def _scale(self, y: np.ndarray) -> np.ndarray:
        scaled = (y - self.min) / (self.max - self.min)
        scaled = self.new_lower + scaled * (self.new_upper - self.new_lower)
        return scaled


if __name__ == "__main__":
    x = np.linspace(BOUNDS[0], BOUNDS[1], 100)
    funcs = ForresterSuite(20, 0.2, 0.15, 0.8, 0.15, 0.1)
    plt.figure()
    hf_forrester = ForresterFunc(1, 0, 0, 0.2, 0.0, 0.8, 0.0)
    true_y = hf_forrester(x)
    plt.plot(x, true_y, label="Forrester", linewidth=3)
    approx_ys = []
    correlations = []
    for i in range(len(funcs)):
        approx_ys.append(funcs[i](x))
        correlations.append(np.corrcoef(true_y, approx_ys[-1])[0, 1])

    # sort by correlation
    approx_ys = np.array(approx_ys)
    correlations = np.array(correlations)
    sort_idx = np.argsort(correlations)[::-1]
    approx_ys = approx_ys[sort_idx]
    correlations = correlations[sort_idx]

    for i in range(len(funcs)):
        plt.plot(x, approx_ys[i], "--", label=f"corr={correlations[i]:.2f}")

    plt.legend()
    plt.show()
