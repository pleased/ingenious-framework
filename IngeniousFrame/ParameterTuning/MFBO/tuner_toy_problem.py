import itertools
import json
import logging
import os
import time
import traceback
import warnings

import gpytorch

import matplotlib.pyplot as plt

import numpy as np
import torch

from botorch.exceptions.warnings import BadInitialCandidatesWarning
from forrester import ForresterFunc, ForresterSuite
from sbo.agents.mfbo import MultiFidelityBayesianOptimiser as MFBO
from sbo.agents.mfbo_utils.costModels import ProductAffineFidelityCostModel
from sbo.utils import proper_round

logging.basicConfig(
    format="%(asctime)s %(levelname)s %(message)s",
    datefmt="%H:%M:%S",
    level=logging.INFO,
)

# warnings.filterwarnings("ignore", category=BadInitialCandidatesWarning)
# warnings.filterwarnings("ignore", category=FutureWarning)


class Tuner:
    def __init__(
        self,
        num_funcs: int,
        min_evals: int,
        max_evals: int,
        num_configs_to_sample: int,
        noisy: bool,
    ):
        self.fs = ForresterSuite(num_funcs - 1, 0.2, 0.15, 0.8, 0.15, 0.1)

        self.max_evals = max_evals
        self.noisy = noisy

        self.true_func = ForresterFunc(1, 0, 0, 0.2, 0.0, 0.8, 0.0)

        x = np.linspace(0, 1, 1000)
        true_y = self.true_func(x)

        approx_ys = []
        correlations = []
        for i in range(len(self.fs)):
            approx_ys.append(self.fs[i](x))
            correlations.append(np.corrcoef(true_y, approx_ys[-1])[0, 1])

        approx_ys = np.array(approx_ys)
        correlations = (np.array(correlations) + 1) / 2
        sort_idx = np.argsort(correlations)[::-1]
        approx_ys = approx_ys[sort_idx]
        self.correlations = correlations[sort_idx]
        self.fs = [
            _fs for _, _fs in sorted(zip(correlations, self.fs), key=lambda x: x[0])
        ]

        correlations = correlations.copy().tolist()
        correlations.append(1.0)

        discrete_fidelity_sets = [correlations]

        self.min_evals = min_evals
        self.num_fidelity_dims = 2

        self._mfbo = MFBO(
            target_fidelity_sets=[[1.0, 1.0]],
            num_problem_dimensions=1,
            num_discrete_fidelity_dims=1,
            num_continuous_fidelity_dims=1,
            log_domain_dims=[0],
            discrete_fidelity_sets=discrete_fidelity_sets,
            nu=2.5,
            y_scale=1e5,
            cost_model=ProductAffineFidelityCostModel(
                fidelity_weights={-2: 100.0, -1: max_evals},
                fidelity_weights_minima={-1: self.min_evals / max_evals},
                fixed_cost=10.0,
                scaling_factor=0.1,
            ),
            num_configs_to_sample=num_configs_to_sample,
            maximise=False,
            kg_num_fantasies=32,  # 64,
            kg_acqf_num_restarts=10,  # 20,
            kg_acqf_num_raw_samples=128,  # 512,
            kg_acqf_batch_limit=128,  # 512,
            kg_acqf_num_iterations=100,  # 200,
            mve_num_fantasies=32,  # 64,
            mve_num_candidates=128,  # 512,
            mve_num_mv_samples=10,
            mve_num_y_samples=32,
            acqf_num_restarts=10,
            acqf_num_iterations=100,
            acqf_num_raw_samples=128,
            acqf_batch_limit=128,
        )

    def run(
        self,
        iterations: int,
        max_cost: int,
        af_method: str,
        targets_per_iteration: int = 1,
        points_per_target: int = 1,
        initial_points_per_configuration: int = 1,
        max_initial_points: int = 5,
        kernel_kwargs={},
    ):
        X_init = self._mfbo.get_random_initial_points_per_config(
            initial_points_per_configuration
        )
        if len(X_init) > max_initial_points:
            X_init = X_init[
                np.random.choice(len(X_init), max_initial_points, replace=False)
            ]

        X = X_init
        Y = self.run_batch(X)

        self._mfbo.fit(X, Y, kernel_kwargs)

        fig, ax = plt.subplots()

        x_plots = np.linspace(0, 1, 1000)
        y_plots = [self.true_func(x_plots)]
        labels = ["Forrester"]
        y_plots.extend([self.fs[i](x_plots) for i in range(len(self.fs))])
        labels.extend([f"corr={c:.2f}" for c in self.correlations])

        colors = itertools.cycle([np.random.rand(3) for _ in range(len(labels) - 1)])

        average_mses = []
        target_mses = []
        diffs_at_optimum = []
        cumulative_cost = [
            self._mfbo._cost_model(torch.tensor(X_init, **self._mfbo._tkwargs))
            .sum()
            .item()
        ]

        iter = 1
        while iter <= iterations + 1:
            tot_mse = 0.0

            ax.clear()
            for i in range(len(labels)):
                if i == 0:
                    color = "k"
                    plt.plot(
                        x_plots, y_plots[i], label=labels[i], linewidth=3, color=color
                    )
                    target_mse, diff_at_optimum = self.plot_func(
                        1.0, y_plots[i], ax, color
                    )
                    logging.info(f"MSE for target: {target_mse}")
                    tot_mse += target_mse
                else:
                    color = next(colors)
                    plt.plot(x_plots, y_plots[i], "--", label=labels[i], color=color)
                    mse, _ = self.plot_func(
                        self.correlations[i - 1], y_plots[i], ax, color
                    )
                    tot_mse += mse
                scatter_indices = []
                for j in range(len(X)):
                    x = X[j]
                    if isinstance(color, list | np.ndarray):
                        continue
                    elif (
                        color != "k"
                        and np.allclose(x[-2], self.correlations[i - 1])
                        and proper_round(x[-1] * self.max_evals) == self.max_evals
                    ):
                        scatter_indices.append(j)
                    elif color == "k" and np.allclose(
                        x[-self.num_fidelity_dims :], [1.0] * self.num_fidelity_dims
                    ):
                        scatter_indices.append(j)
                if len(scatter_indices) > 0:
                    plt.scatter(X[scatter_indices, 0], Y[scatter_indices], color=color)
            ax.set_xlabel("x")
            ax.set_ylabel("y")
            ax.set_ylim(0, 1)
            ax.legend()

            logging.info(f"Average MSE: {(avg_mse := tot_mse/len(labels))}")

            # plt.pause(update_interval)
            # plt.show()
            plt.tight_layout()
            plt.savefig(kernel_kwargs["filepath"])

            if iter == iterations + 1 or cumulative_cost[-1] > max_cost:
                average_mses.append(avg_mse)
                target_mses.append(target_mse)
                diffs_at_optimum.append(diff_at_optimum)
                break

            failed_to_fit = False

            try:
                iter_start = time.time()
                logging.info(f"Running iteration {iter} with {len(X)} points...")
                start = time.time()
                X_new = self._mfbo.get_next_points(
                    num_targets=targets_per_iteration,
                    points_per_target=points_per_target,
                    af_method=af_method,
                )
            except torch.cuda.OutOfMemoryError as e:
                logging.error(
                    f"{iter} : GPU ran out of memory after {self._get_time_string(time.time() - iter_start)}."
                )
                if self._mfbo.halve_batch_size():
                    logging.info(f"{iter} : Halving batch sizes and retrying...")
                else:
                    logging.info(
                        f"{iter} : Batch sizes could not be decreased. Switching to CPU and increasing batch size to number of cores."
                    )
                    self._mfbo.set_device_and_bs(
                        "cpu", self._num_cores, self._num_cores
                    )
                    self._mfbo.free_cuda_memory()
                    self._mfbo.fit(X, Y)
                continue

            average_mses.append(avg_mse)
            target_mses.append(target_mse)
            diffs_at_optimum.append(diff_at_optimum)
            cumulative_cost.append(
                (
                    cumulative_cost[-1]
                    + self._mfbo._cost_model(torch.tensor(X_new, **self._mfbo._tkwargs))
                    .sum()
                    .detach()
                    .cpu()
                    .numpy()
                    .squeeze()
                )
            )

            logging.info(f"{iter} : Cumulative cost: {cumulative_cost[-1]}")

            logging.info(
                f"{iter} : Time to get next points: {self._get_time_string(time.time() - start)}"
            )
            start = time.time()
            Y_new = self.run_batch(X_new)
            X = np.vstack((X, X_new))
            Y = np.vstack((Y, Y_new))
            start = time.time()
            if not failed_to_fit:
                try:
                    self._mfbo.fit(X, Y, kernel_kwargs=kernel_kwargs)
                except:
                    logging.error(
                        f"{iter} : Cholesky fit failed, swapping to Lanczos/CG..."
                    )
                    cholesky_iter = iter
                    failed_to_fit = True
            if failed_to_fit:
                logging.info(f"Using Lanczos/CG to fit since iter {cholesky_iter}")
                with gpytorch.settings.max_cholesky_size(X.shape[0] - X_new.shape[0]):
                    self._mfbo.fit(X, Y, kernel_kwargs=kernel_kwargs)
            logging.info(
                f"{iter} : Time to fit: {self._get_time_string(time.time() - start)}"
            )
            logging.info(
                f"{iter} : Total time taken: {self._get_time_string(time.time() - iter_start)}"
            )

            iter += 1

        return X, Y, average_mses, target_mses, diffs_at_optimum, cumulative_cost

    def plot_func(self, corr: float, true_f: np.ndarray, ax, color):
        x = np.empty((1000, 3))
        x[:, 0] = np.linspace(0, 1, 1000)
        x[:, 1] = corr
        x[:, 2] = 1.0

        mean, var = self._mfbo.predict(x)

        std = np.sqrt(var)

        ax.fill_between(x[:, 0], mean - 2 * std, mean + 2 * std, alpha=0.2, color=color)

        ax.plot(x[:, 0], mean, ":", color=color)

        mse = np.mean((mean - true_f) ** 2)

        true_min_idx = np.argmin(true_f)
        diff_at_optimum = np.abs(mean[true_min_idx] - true_f[true_min_idx])

        return mse, diff_at_optimum

    def run_batch(self, X):
        infosource = None
        y = np.empty(X.shape[0])
        for i in range(X.shape[0]):
            x = X[i]
            if np.isclose(x[-self.num_fidelity_dims], 1.0):
                infosource = self.true_func
            else:
                for j in range(len(self.fs)):
                    if np.isclose(x[-self.num_fidelity_dims], self.correlations[j]):
                        infosource = self.fs[j]
                        break
            if infosource is None:
                logging.error("INFO SOURCE IS NONE")
            y[i] = infosource(x[0])
            if self.noisy:
                y[i] = np.mean(
                    np.random.rand(
                        proper_round(
                            self.max_evals
                            * (
                                x[-self.num_fidelity_dims + 1]
                                * (1 - self.min_evals / self.max_evals)
                                + self.min_evals / self.max_evals
                            )
                        )
                    )
                    < y[i]
                )

        return y.reshape(-1, 1)

    def _get_time_string(self, seconds: float) -> str:
        hours = int(seconds // 3600)
        minutes = int((seconds % 3600) // 60)
        seconds = int(seconds % 60)
        str = ""
        if hours > 0:
            str += f"{hours}h "
        else:
            str += "0h "
        if minutes > 0:
            str += f"{minutes}m "
        else:
            str += "0m "
        if seconds > 0:
            str += f"{seconds}s"
        else:
            str += "0s"
        return str


if __name__ == "__main__":
    index = 0
    avg_mses = []
    target_mses = []
    diffs_at_optimums = []
    cum_costs = []
    labels = []
    config_map = {}
    for kernel_option, mode, af_method in itertools.product(
        (0, 1, 2), (2,), ("mfmve", "mflbmve", "mfkg")
    ):
        directory = f"plots/test_torch_mll_10/{af_method}"
        if not os.path.exists(directory):
            os.makedirs(directory)
        index += 1
        config_map[index] = {
            "kernel_option": kernel_option,
            "mode": mode,
            "af_method": af_method,
        }
        labels.append(
            f"{index}_{kernel_option}_{mode}_{af_method}"
        )
        kernel_kwargs = {
            "filepath": f"{directory}/plot_{index}.pdf",
            "kernel_option": kernel_option,
            "mode": mode,
        }
        failed = False
        attempts = 1
        for attempt in range(attempts):
            np.random.seed(24)
            tuner = Tuner(
                num_funcs=3,
                num_configs_to_sample=3,
                min_evals=12,
                max_evals=48,
                noisy=True,
            )
            try:
                X, y, avg_mse, target_mse, diffs_at_optimum, cum_cost = tuner.run(
                    points_per_target=1,
                    iterations=100,
                    max_cost=1e6,
                    initial_points_per_configuration=5,
                    max_initial_points=50,
                    af_method=af_method,
                    kernel_kwargs=kernel_kwargs,
                )
                X = X.tolist()
                y = y.tolist()

            except Exception as e:
                logging.error(e)
                if attempt == attempts - 1:
                    logging.error("Run failed. Continuing...")
                    failed = True
                else:
                    logging.error("Run failed. Re-atempting...")
                continue
            break
        if failed:
            config_map[index]["error"] = True
            continue
        config_map[index]["X"] = X
        config_map[index]["y"] = y
        config_map[index]["final_avg_mse"] = avg_mse[-1]
        config_map[index]["final_target_mse"] = target_mse[-1]
        config_map[index]["final_diff_at_optimum"] = diffs_at_optimum[-1]
        config_map[index]["final_cum_cost"] = cum_cost[-1]
        config_map[index]["avg_mse"] = avg_mse
        config_map[index]["target_mse"] = target_mse
        config_map[index]["diff_at_optimum"] = diffs_at_optimum
        config_map[index]["cum_cost"] = cum_cost
        avg_mses.append(avg_mse)
        target_mses.append(target_mse)
        diffs_at_optimums.append(diffs_at_optimum)
        cum_costs.append(cum_cost)
        plt.clf()
        plt.figure()
        plt.semilogy(cum_cost, avg_mse, label="Average MSE")
        plt.semilogy(cum_cost, target_mse, label="Target MSE")
        plt.semilogy(
            cum_cost,
            diffs_at_optimum,
            label="Diff at optimum",
        )
        plt.xlabel("Cost")
        plt.ylabel("MSE")
        plt.legend()
        plt.tight_layout()
        plt.savefig(
            f"{directory}/plot_{index}_{kernel_option}_{mode}.pdf"
        )
        plt.clf()
        plt.figure()
        plt.plot(list(range(len(cum_cost))), cum_cost, label="Cumulative cost")
        plt.xlabel("Iteration")
        plt.ylabel("Cost")
        plt.legend()
        plt.tight_layout()
        plt.savefig(f"{directory}/plot_{index}_cost.pdf")
        plt.clf()
    directory = "/".join(directory.split("/")[:-1])
    # save config map
    with open(f"{directory}/config_map.json", "w") as f:
        json.dump(config_map, f, indent=4)
    plt.figure(figsize=(15, 15))
    for i in range(len(avg_mses)):
        plt.semilogy(cum_costs[i], avg_mses[i], label=labels[i])
    plt.xlabel("Cost")
    plt.ylabel("Average MSE")
    plt.legend()
    plt.tight_layout()
    plt.savefig(f"{directory}/avg_mses.pdf")
    plt.clf()
    plt.figure(figsize=(15, 15))
    for i in range(len(target_mses)):
        plt.semilogy(cum_costs[i], target_mses[i], label=labels[i])
    plt.xlabel("Cost")
    plt.ylabel("Target MSE")
    plt.legend()
    plt.tight_layout()
    plt.savefig(f"{directory}/target_mses.pdf")
    plt.clf()
    plt.figure(figsize=(15, 15))
    for i in range(len(diffs_at_optimums)):
        plt.semilogy(
            cum_costs[i],
            diffs_at_optimums[i],
            label=labels[i],
        )
    plt.xlabel("Cost")
    plt.ylabel("Diff at optimum")
    plt.legend()
    plt.tight_layout()
    plt.savefig(f"{directory}/diffs_at_optimum.pdf")
    plt.clf()
