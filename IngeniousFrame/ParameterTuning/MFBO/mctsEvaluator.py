import asyncio
import json
import logging
import os
import time

logger = logging.getLogger(__name__)

AUX_JSON_PREFIX = f"enhancementConfigurations/"
PATH_TO_SCRIPT_PREFIX = "../../scripts/"
PATH_FROM_SCRIPT = "../../ParameterTuning/MFBO/"
SCRIPT_NAME = "test.sh"


async def eval(
    enhancement_name: str,
    game: str,
    board_size: int,
    thread_count: int,
    turn_duration: int,
    configuration: dict[str, str | float | int],
    point_index: int,
    load: float,
    id: str | int,
) -> tuple[float, float, float, float]:
    """
    Evaluates the given configuration for the specified game.

    Parameters
        enhancement_name : str
            The name of the enhancement to evaluate the configuration for.
        game : str
            The game to evaluate the configuration for.
        board_size : int
            The board size to evaluate the configuration for.
        thread_count : int
            The number of threads to evaluate the configuration for.
        turn_duration : int
            The turn duration to evaluate the configuration for.
        configuration : dict[str, str|float|int]
            The configuration to evaluate.
        point_index : int
            The index of the point-representation of this configuration.
        load : float
            The load of the coroutine doing the evaluation.
        id : str
            ID of the coroutine doing the evaluation.

    Returns
        score : float
            The score of the configuration.
        load : float
            The load of the coroutine doing the evaluation.
        point_index : int
            The index of the point-representation of this configuration.
        time : float
            The time it took to evaluate the configuration in seconds.
    """
    try:
        directory = f"{AUX_JSON_PREFIX}{id}/"
        if not os.path.exists(directory):
            os.makedirs(directory)
        # dump the json as an aux json
        json.dump(
            configuration,
            open(f"{directory}EnhancementChoice{enhancement_name}.json", "w"),
        )

        cur_time = time.time()

        command = (
            f"cd {PATH_TO_SCRIPT_PREFIX}{game} ; "
            + f"./{SCRIPT_NAME} 1 {board_size} 1 {thread_count} {turn_duration} "
            + f"{PATH_FROM_SCRIPT}{directory}EnhancementChoice{enhancement_name}.json "
            + f"{enhancement_name} {enhancement_name}_{id}_{cur_time}"
        )

        start = time.time()

        # run the script
        result = await asyncio.create_subprocess_shell(
            command,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, _ = await result.communicate()

        end = time.time()

        processed_stdout = stdout.decode("utf-8").strip().split("\n")[-3:]

        # get the score from the output
        score = None
        for line in processed_stdout:
            if line.split(" ", 1)[0] == enhancement_name:
                score = float(line.strip().split(" ")[-1].replace("%", "")) / 100.0
                break
        if score == None:
            logger.warning(
                f"An error occurred for coroutine with id={enhancement_name}_{id}_{cur_time}, point_index={point_index} and command:\n\t{command}"
            )
        return score, load, point_index, end - start
    except Exception as e:
        return None, load, point_index, None
