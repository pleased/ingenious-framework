import json
from copy import deepcopy

selection = ["Uct", "UctTuned", "Rave"]
expansion = ["Single"]
simulation = ["Random", "Mast", "Contextual"]
backpropagation = ["Simple"]

parameter_mappings = {
    "Uct": {"cValue": (0.0, 2.0)},
    "UctTuned": {"cValue": (0.0, 2.0)},
    "Rave": {"cValue": (0.0, 2.0), "vValue": (0, 100_000)},
    "Mast": {
        "gammaMast": (0.0, 1.0),
        "tau": (0.0001, 10.0),
    },
    "Contextual": {"gammaCmc": (0.0, 1.0), "threshold": (0.0, 1.0), "window": (0.0, 1.0)},
    "FPU": {"startPriorityValue": (0.0, 5.0), "simsPerPlayout": (1, 10)},
}

hamming_fields = [
    "Selection",
    "Expansion",
    "Simulation",
    "Backpropagation",
    "fpu_enabled",
]

conditional_hamming_fields = {"Mast": "treeOnly", "Contextual": "window"}


class ConfigTree:
    rootNodes: list["ConfigNode"]

    def __init__(self) -> None:
        self.rootNodes = []
        # build node structure
        for s in selection:
            selection_node = ConfigNode()
            for e in expansion:
                expansion_node = ConfigNode()
                for sim in simulation:
                    simulation_node = ConfigNode()
                    for b in backpropagation:
                        simulation_node.add_child(
                            ConfigNode(
                                {
                                    "Selection": s,
                                    "Expansion": e,
                                    "Simulation": sim,
                                    "Backpropagation": b,
                                }
                            )
                        )
                    expansion_node.add_child(simulation_node)
                selection_node.add_child(expansion_node)
            self.rootNodes.append(selection_node)

    def get_max_depth(self):
        # -1 as there is no root node
        max_depth = -1
        for node in self.rootNodes:
            max_depth = max(max_depth, self._get_max_depth_from_node(node))
        return max_depth

    def _get_max_depth_from_node(self, node: "ConfigNode"):
        if not node.get_children():
            return 1
        max_depth = 0
        for child in node.get_children():
            max_depth = max(max_depth, self._get_max_depth_from_node(child))
        return max_depth + 1

    def get_all_configs(self):
        configs = []
        for node in self.rootNodes:
            configs.extend(self._get_all_configs_from_node(node))
        return configs

    def _get_all_configs_from_node(self, node: "ConfigNode"):
        configs = []
        if not node.get_children():
            return [node.config]
        for child in node.get_children():
            configs.extend(self._get_all_configs_from_node(child))
        return configs

    # TODO: move out
    def get_hamming_score(self, config1: dict, config2: dict):
        distance = 0
        equal = 0
        for field in hamming_fields:
            if field in config1.keys() and field in config2.keys():
                if config1[field] != config2[field]:
                    distance += 1
                else:
                    equal += 1
            elif field in config1.keys() or field in config2.keys():
                distance += 1
        for key, value in conditional_hamming_fields.items():
            if key in config1.values() and key in config2.values():
                if config1[value] != config2[value]:
                    distance += 1
                else:
                    equal += 1
            elif key in config1.values() or key in config2.values():
                distance += 1
        comparisons = distance + equal
        return (comparisons - distance) / comparisons


def get_copy_with_key_value(config, key, value):
    new_config = deepcopy(config)
    new_config[key] = value
    return new_config


def get_copy_with_keys_values(config, keys_values: dict):
    new_config = deepcopy(config)
    for key, value in keys_values.items():
        new_config[key] = value
    return new_config


class ConfigNode:
    config: dict
    visits: int
    children: list["ConfigNode"]

    def __init__(self, config: dict = None) -> None:
        self.children = []
        self.visits = 0
        if not config:
            return
        if "fpu_enabled" not in config.keys():
            # no fpu
            self.add_child(
                ConfigNode(get_copy_with_key_value(config, "fpu_enabled", False))
            )
            # with fpu
            fpu_config = get_copy_with_key_value(config, "fpu_enabled", True)
            self.add_child(
                ConfigNode(
                    get_copy_with_keys_values(fpu_config, parameter_mappings["FPU"])
                )
            )
        elif config["Simulation"] == "Mast" and "treeOnly" not in config.keys():
            original_mast_config = get_copy_with_keys_values(
                config, parameter_mappings["Mast"]
            )
            general_mast_config = get_copy_with_key_value(
                original_mast_config, "gammaMast", 1.0
            )
            for mast_config in [original_mast_config, general_mast_config]:
                # tree only
                tree_only = get_copy_with_key_value(mast_config, "treeOnly", True)
                # full tree
                full_tree = get_copy_with_key_value(mast_config, "treeOnly", False)
                # tree only expansion
                tree_only_expansion = get_copy_with_key_value(
                    tree_only, "Expansion", "Mast"
                )
                # full tree expansion
                full_tree_expansion = get_copy_with_key_value(
                    full_tree, "Expansion", "Mast"
                )
                # add children
                mast_configs = [
                    tree_only,
                    full_tree,
                    tree_only_expansion,
                    full_tree_expansion,
                ]
                for mast_config in mast_configs:
                    self.add_child(ConfigNode(mast_config))
        elif config["Simulation"] == "Contextual" and "window" not in config.keys():
            contextual_config = get_copy_with_keys_values(
                config, parameter_mappings["Contextual"]
            )
            # window simulation
            window_sim = contextual_config
            # window simulation expansion
            window_sim_expansion = get_copy_with_key_value(
                window_sim, "Expansion", "Contextual"
            )
            # full simulation
            full_sim = get_copy_with_key_value(contextual_config, "window", -1)
            # full simulation expansion
            full_sim_expansion = get_copy_with_key_value(
                full_sim, "Expansion", "Contextual"
            )
            # add children
            contextual_configs = [
                window_sim,
                window_sim_expansion,
                full_sim,
                full_sim_expansion,
            ]
            for contextual_config in contextual_configs:
                self.add_child(ConfigNode(contextual_config))
        else:
            if config["Selection"] == "Rave":
                config = get_copy_with_keys_values(config, parameter_mappings["Rave"])
                config["Backpropagation"] = config["Backpropagation"] + ",Rave"
            else:
                config = get_copy_with_keys_values(
                    config, parameter_mappings[config["Selection"]]
                )
            self.config = config

    def add_child(self, child):
        self.children.append(child)

    def get_children(self):
        return self.children


if __name__ == "__main__":
    tree = ConfigTree()
    configs = tree.get_all_configs()
    filtered_configs = [
        c
        for c in configs
        if c["Selection"] == "Uct"
        and c["Expansion"] == "Contextual"
        and c["Simulation"] == "Contextual"
        and c["Backpropagation"] == "Simple"
        and c["window"] != -1
        and not c["fpu_enabled"]
    ]

    print(len(filtered_configs))
    print(json.dumps(filtered_configs, indent=4))
