import itertools
import json
import logging
import os
import time

from typing import Dict, List, Tuple

import matplotlib.pyplot as plt
import numpy as np

import sobol
import torch
from botorch import fit_gpytorch_mll
from botorch.acquisition import PosteriorMean
from botorch.acquisition.fixed_feature import FixedFeatureAcquisitionFunction
from botorch.models.gp_regression import FixedNoiseGP, SingleTaskGP
from botorch.models.gp_regression_mixed import MixedSingleTaskGP
from botorch.models.transforms import Standardize
from botorch.optim.fit import fit_gpytorch_mll_torch

from botorch.optim.optimize import optimize_acqf

from gpytorch.constraints.constraints import GreaterThan, Interval
from gpytorch.kernels import ScaleKernel

from gpytorch.kernels.matern_kernel import MaternKernel
from gpytorch.kernels.rbf_kernel import RBFKernel
from gpytorch.likelihoods import BernoulliLikelihood, BetaLikelihood, GaussianLikelihood
from gpytorch.mlls import (
    PredictiveLogLikelihood,
    VariationalELBO,
    VariationalMarginalLogLikelihood,
)
from gpytorch.mlls.exact_marginal_log_likelihood import ExactMarginalLogLikelihood
from gpytorch.priors.torch_priors import GammaPrior

from matplotlib.axes import Axes
from mpl_toolkits.mplot3d.axes3d import Axes3D
from sbo.agents.mfbo_utils.customMFGP import SingleTaskMultiFidelityGP
from tinydb import TinyDB

# plt.rcParams["text.usetex"] = True

logging.basicConfig(
    format="%(asctime)s %(levelname)s %(message)s",
    datefmt="%H:%M:%S",
    level=logging.INFO,
)

NUM_RESTARTS = 50
BATCH_LIMIT = 50
MAXITER = 200
NUM_RAW_SAMPLES = 4096

NU = 2.5

torch.set_num_threads(os.cpu_count())
torch.set_num_interop_threads(os.cpu_count())


class PlotGenerator:
    def __init__(
        self,
        db_path: str,
        problem_dims: int,
        tot_dims: int,
        y_scale: float = 1e5,
        num_data_points: int = None,
        log_domain_dims=[],
    ) -> None:
        self.db = TinyDB(db_path)
        self.table = self.db.table("evaluations")
        self.tkwargs = {
            "device": "cuda" if torch.cuda.is_available() else "cpu",
            "dtype": torch.float64,
        }
        self.problem_dims = problem_dims
        self.tot_dims = tot_dims
        self.y_scale = y_scale
        self.num_plotted = 0
        self.acq_values = []
        self.nonnegative = False
        self.y_scale = 1e5
        self.num_data_points = num_data_points
        self._log_severity = 80
        self._log_domain_dims = log_domain_dims

    def new_figure(
        self, num_rows: int, num_cols: int, x_scale: float = 6, y_scale: float = 3.25
    ) -> None:
        self.fig = plt.figure(figsize=(num_cols * x_scale, num_rows * y_scale))
        self.num_plotted = 0
        self.num_rows = num_rows
        self.num_cols = num_cols

    def _load_evaluations(self):
        X = []
        y = []
        for evaluation in self.table.all():
            X.append(np.array(evaluation["X"]))
            y.append(evaluation["y"])
        X = np.array(X)
        y = np.array(y)

        if self.num_data_points:
            X = X[: self.num_data_points]
            y = y[: self.num_data_points]

        self.X = X
        self.y = y

        print(f"X shape = {X.shape}")

        return X, y.reshape(-1, 1)

    def _map_to_log(self, x: np.ndarray | torch.Tensor):
        return np.log(self._log_severity * x + 1) / np.log(self._log_severity + 1)

    def _map_from_log(self, x: np.ndarray | torch.Tensor):
        return ((self._log_severity + 1) ** x - 1) / self._log_severity

    def _scale_y(self, scores):
        return scores * self.y_scale

    def _inv_scale_y(self, scores):
        return scores / self.y_scale

    def fit(self, kernel_kwargs):
        # load the evaluations from the database
        points, scores = self._load_evaluations()

        points = points.copy()

        if len(self._log_domain_dims) > 0:
            points[:, self._log_domain_dims] = self._map_to_log(
                points[:, self._log_domain_dims]
            )

        # scale scores
        scores = self._scale_y(scores)

        # convert the points to tensors for use with the model
        points_tensor = torch.tensor(points, **self.tkwargs)
        scores_tensor = torch.tensor(scores, **self.tkwargs)

        scores_tensor = scores_tensor + 0.5
        scores_tensor = torch.floor(scores_tensor)

        # fit the model to the points and scores
        self.model = SingleTaskMultiFidelityGP(
            train_X=points_tensor,
            train_Y=scores_tensor,
            nu=2.5,
            linear_truncated=True,
            outcome_transform=Standardize(m=1),
            data_fidelities=list(range(self.problem_dims, self.tot_dims)),
            kernel_kwargs=kernel_kwargs,
        )

        # optimise the model's hyperparameters
        mll = ExactMarginalLogLikelihood(
            likelihood=self.model.likelihood,
            model=self.model,
        )

        fit_gpytorch_mll(
            mll,
            # optimizer=fit_gpytorch_mll_torch,
            # optimizer_kwargs={
            #     # TODO : Make parameter at init if used
            #     "step_limit": 500,
            #     "options": {"maxiter": 100}
            # }
        )
        # free the memory used by the previously created tensors
        points_tensor.detach()
        scores_tensor.detach()

    def plot_samples_1d(
        self,
        ax: Axes,
        target_index: int,
        axes_label: str,
        bounds: Tuple[float, float],
        fidelity_set: np.ndarray,
        samples: int,
        color: str,
        seed: int = 1,
    ):
        if self.problem_dims == 1:
            X = np.empty((samples, self.tot_dims))
            x_vals = np.linspace(0, 1, samples)
            assert target_index == 0
            X[:, target_index] = x_vals
            X[:, self.problem_dims :] = fidelity_set

            X_tensor = torch.tensor(X, **self.tkwargs)

            with torch.no_grad():
                posterior = self.model.posterior(X_tensor)
                mean = posterior.mean.detach().cpu().numpy().squeeze()
                var = posterior.variance.detach().cpu().numpy().squeeze()

            if len(self._log_domain_dims) > 0:
                X[:, self._log_domain_dims] = self._map_from_log(
                    X[:, self._log_domain_dims]
                )

            X[:, target_index] = (
                X[:, target_index] * (bounds[1] - bounds[0]) + bounds[0]
            )

            x_vals = X[:, target_index]

            y_mean = self._inv_scale_y(mean)

            std = np.sqrt(var)

            ax.plot(x_vals, y_mean, color=color)
            ax.fill_between(
                x_vals,
                y_mean - 2 * self._inv_scale_y(std),
                y_mean + 2 * self._inv_scale_y(std),
                alpha=0.2,
                color=color,
            )
            ax.scatter(x_vals[y_mean.argmax()], y_mean.max(), color=color)

        else:
            grid = np.array(
                np.meshgrid(
                    *[np.linspace(0, 1, samples) for _ in range(self.problem_dims - 1)]
                )
            ).T.reshape(-1, self.problem_dims - 1)

            X = np.empty((samples, self.tot_dims))

            for i in range(grid.shape[0]):
                X[:, target_index] = np.linspace(0, 1, samples)
                if target_index == 0:
                    X[:, target_index + 1 : self.problem_dims] = grid[i]
                elif target_index == self.problem_dims - 1:
                    X[:, :target_index] = grid[i]
                else:
                    X[:, 0:target_index] = grid[i, :target_index]
                    X[:, target_index + 1 : self.problem_dims] = grid[i, target_index:]
                X[:, self.problem_dims :] = fidelity_set

                X_tensor = torch.tensor(X, **self.tkwargs)

                with torch.no_grad():
                    posterior = self.model.posterior(X_tensor)
                    mean = posterior.mean.detach().cpu().numpy()
                    var = posterior.variance.detach().cpu().numpy()

                if len(self._log_domain_dims) > 0:
                    X[:, self._log_domain_dims] = self._map_from_log(
                        X[:, self._log_domain_dims]
                    )

                X[:, target_index] = (
                    X[:, target_index] * (bounds[1] - bounds[0]) + bounds[0]
                )

                x_vals = X[:, target_index]
                y_mean = mean
                # ax.fill_between(
                #     x_vals,
                #     self._map_from_log(y_mean) - self._map_from_log(std),
                #     self._map_from_log(y_mean) + self._map_from_log(std),
                #     alpha=0.2,
                #     color=color,
                # )
                y_mean = self._inv_scale_y(y_mean)
                ax.plot(x_vals, y_mean, color=color)
                ax.scatter(x_vals[y_mean.argmax()], y_mean.max(), color=color)

        hf_indices = []
        lf_indices = []
        for i in range(self.X.shape[0]):
            if np.allclose(self.X[i, self.problem_dims :], fidelity_set):
                hf_indices.append(i)
            else:
                lf_indices.append(i)
        hf_X = (
            self.X[hf_indices][:, target_index].reshape(-1) * (bounds[1] - bounds[0])
            + bounds[0]
        )
        hf_y = self.y[hf_indices]
        lf_X = (
            self.X[lf_indices][:, target_index].reshape(-1) * (bounds[1] - bounds[0])
            + bounds[0]
        )
        lf_y = self.y[lf_indices]
        ax.scatter(
            hf_X,
            hf_y,
            color=color,
            marker="x",
        )
        ax.scatter(
            lf_X,
            lf_y,
            color="grey",
            marker="x",
        )
        ax.set_xlabel(axes_label)
        ax.set_ylabel("Performance")
        # ax.set_yticks([])

    def plot_marginalised_2d(
        self,
        ax: Axes3D,
        target_indices: Tuple[int, int],
        related_indices: List[int],
        axes_labels: Tuple[str, str],
        bounds: Tuple[Tuple[float, float], Tuple[float, float]],
        fidelity_set: Dict[str, float | bool],
        samples: int,
        repetitions: int,
        plot_uncertainty: bool = False,
        ax_titles: List[str] = None,
        seed: int = 1,
    ):
        problem_indices = [
            i
            for i in range(self.problem_dims)
            if i not in target_indices and i not in related_indices
        ]
        fidelity_indices = list(range(self.problem_dims, self.tot_dims))
        if len(problem_indices) > 0:
            sob = sobol.generator(len(problem_indices))

        X = np.empty((repetitions, samples**2, self.tot_dims))
        x_vals = np.linspace(0, 1, samples)
        y_vals = np.linspace(0, 1, samples)
        x_mesh, y_mesh = np.meshgrid(x_vals, y_vals)
        X[:, :, target_indices[0]] = x_mesh.flatten()
        X[:, :, target_indices[1]] = y_mesh.flatten()

        mean_acqf = PosteriorMean(self.model)
        acqf = FixedFeatureAcquisitionFunction(
            acq_function=mean_acqf,
            d=self.tot_dims,
            columns=list(range(self.problem_dims, self.tot_dims)),
            values=fidelity_set.tolist(),
        )
        candidate, acq_value = optimize_acqf(
            acq_function=acqf,
            bounds=torch.tensor(
                [[0.0] * self.problem_dims, [1.0] * self.problem_dims], **self.tkwargs
            ),
            q=1,
            num_restarts=NUM_RESTARTS,
            raw_samples=NUM_RAW_SAMPLES,
            options={
                "batch_limit": BATCH_LIMIT,
                "maxiter": MAXITER,
                "nonnegative": self.nonnegative,
                "sample_around_best": True,
                "seed": seed,
            },
        )
        candidate = acqf._construct_X_full(candidate).detach().cpu().numpy().reshape(-1)
        logging.info(
            f"Candidate = {candidate[target_indices+related_indices]} with acq_value = {self._inv_scale_y(acq_value.item())}"
        )
        self.acq_values.append(acq_value.item())
        X[:, :, related_indices] = candidate[related_indices]

        if len(problem_indices) > 0:
            for i in range(repetitions):
                X[i, :, problem_indices] = next(sob).reshape(-1, 1)
        X[:, :, fidelity_indices] = fidelity_set.reshape(1, -1)

        X_tensor = torch.tensor(X, **self.tkwargs)

        with torch.no_grad():
            posterior = self.model.posterior(X_tensor)
            mean = self._inv_scale_y(
                np.mean(posterior.mean.detach().cpu().numpy(), axis=0)
            )
            std = self._inv_scale_y(
                np.sqrt(np.mean(posterior.variance.detach().cpu().numpy(), axis=0))
            )

        z_mean = mean.reshape(samples, samples)

        x_mesh = x_mesh * (bounds[0][1] - bounds[0][0]) + bounds[0][0]
        y_mesh = y_mesh * (bounds[1][1] - bounds[1][0]) + bounds[1][0]

        ax.plot_surface(x_mesh, y_mesh, z_mean, cmap="viridis")

        ax.set_xlabel(axes_labels[0])
        ax.set_ylabel(axes_labels[1])
        ax.set_zlabel("Performance")
        ax.set_title(
            f"Marginalised Posterior Mean for {axes_labels[0]} and {axes_labels[1]}"
        )
        # ax.set_zticks([])
        if ax_titles is not None:
            ax.set_title(ax_titles[0])

        if plot_uncertainty:
            ax = self.get_next_3D_axes()
            std = std.reshape(samples, samples)
            ax.plot_surface(x_mesh, y_mesh, std, cmap="viridis")
            ax.set_xlabel(axes_labels[0])
            ax.set_ylabel(axes_labels[1])
            ax.set_zlabel("Uncertainty")
            ax.set_title(
                f"Marginalised Posterior Uncertainty for {axes_labels[0]} and {axes_labels[1]}"
            )
            # ax.set_zticks([])
            if ax_titles is not None:
                ax.set_title(ax_titles[1])

    def get_next_3D_axes(self) -> Axes3D:
        self.num_plotted += 1
        return self.fig.add_subplot(
            self.num_rows, self.num_cols, self.num_plotted, projection="3d"
        )

    def get_next_2D_axes(self) -> Axes:
        self.num_plotted += 1
        return self.fig.add_subplot(self.num_rows, self.num_cols, self.num_plotted)

    def plot_fidelity_dim(
        self,
        ax: Axes,
        target_fidelity: int,
        axes_label: str,
        legend_label: str,
        samples: int,
        color: str,
        seed: int = 1,
    ):
        problem_indices = list(range(self.problem_dims))

        X = np.empty((samples, self.tot_dims))
        x_vals = np.linspace(0, 1, samples)
        X[:, target_fidelity] = x_vals

        fixed_fidelity_indices = list(
            set(range(self.problem_dims, self.tot_dims)) - set([target_fidelity])
        )

        mean_acqf = PosteriorMean(self.model)
        acqf = FixedFeatureAcquisitionFunction(
            acq_function=mean_acqf,
            d=self.tot_dims,
            columns=fixed_fidelity_indices,
            values=[1.0] * len(fixed_fidelity_indices),
        )
        candidate, acq_value = optimize_acqf(
            acq_function=acqf,
            bounds=torch.tensor(
                [[0.0] * (self.problem_dims + 1), [1.0] * (self.problem_dims + 1)],
                **self.tkwargs,
            ),
            q=1,
            num_restarts=NUM_RESTARTS,
            raw_samples=NUM_RAW_SAMPLES,
            options={
                "batch_limit": BATCH_LIMIT,
                "maxiter": MAXITER,
                "nonnegative": self.nonnegative,
                "sample_around_best": True,
                "seed": seed,
            },
        )
        candidate = candidate.detach().cpu().numpy().reshape(-1)
        logging.info(
            f"Candidate = {candidate[problem_indices + [len(problem_indices)]]} with acq_value = {self._inv_scale_y(acq_value.item())}"
        )
        self.acq_values.append(acq_value.item())
        X[:, problem_indices] = candidate[problem_indices]
        X[:, fixed_fidelity_indices] = 1.0

        X_tensor = torch.tensor(X, **self.tkwargs)

        with torch.no_grad():
            posterior = self.model.posterior(X_tensor)
            mean = posterior.mean.detach().cpu().numpy()
            var = posterior.variance.detach().cpu().numpy()

        y_mean = mean.squeeze()
        var = var.squeeze()
        std = np.sqrt(var)

        ax.fill_between(
            x_vals,
            self._inv_scale_y(y_mean - std * 2),
            self._inv_scale_y(y_mean + std * 2),
            # self._map_from_log(y_mean) - self._map_from_log(std),
            # self._map_from_log(y_mean) + self._map_from_log(std),
            alpha=0.2,
            color=color,
        )
        y_mean = self._inv_scale_y(y_mean)
        ax.plot(x_vals, y_mean, label=legend_label, color=color)
        ax.scatter(x_vals[y_mean.argmax()], y_mean.max(), color=color)
        ax.set_xlabel(axes_label)
        ax.set_ylabel("Performance")
        # ax.set_yticks([])


def make_plots_poc(kernel_kwargs, combo: str, num_data_points=None, log_domain_dims=[]):
    if combo == "rave_cmc":
        db_path = "data/evaluation_data_rave_cmc.json"
        param_names = ["V", "gamma", "threshold"]
        bounds = [(0, 10_000), (0.0, 1.0), (0.0, 1.0)]
    elif combo == "mast":
        db_path = "data/evaluation_data_mast.json"
        param_names = ["C", "gamma", "tau"]
        bounds = [(0.0, 2.0), (0.0, 1.0), (0.0001, 10.0)]
    elif combo == "rave":
        db_path = "data/evaluation_data_rave.json"
        param_names = ["V"]
        bounds = [(0, 10_000)]
    elif combo == "rave010":
        db_path = "data/evaluation_data_rave_0_10k.json"
        param_names = ["V"]
        bounds = [(0, 10_000)]
    elif combo == "rave110":
        db_path = "data/evaluation_data_rave_1_10k.json"
        param_names = ["V"]
        bounds = [(1, 10_000)]
    else:
        raise

    pg = PlotGenerator(
        db_path=db_path,
        problem_dims=len(bounds),
        tot_dims=len(bounds) + 3,
        num_data_points=num_data_points,
        log_domain_dims=log_domain_dims,
    )

    start = time.time()
    pg.fit(kernel_kwargs=kernel_kwargs)

    logging.info(f"Time to fit = {(time.time() - start)}s")

    colors = itertools.cycle(["b", "k", "g", "m"])
    letters = itertools.cycle([chr(i) for i in range(65, 77)])

    if len(bounds) == 1:
        pg.new_figure(1, 1, 5, 4)
        samples = 51
    elif len(bounds) == 2:
        pg.new_figure(1, 2, 9, 4)
        samples = 31
    elif len(bounds) == 3:
        pg.new_figure(2, 3, 5, 4)
        samples = 21
    else:
        raise

    for i in range(len(bounds)):
        ax = pg.get_next_2D_axes()
        pg.plot_samples_1d(
            ax=ax,
            target_index=i,
            axes_label=(param_names[i]),
            bounds=bounds[i],
            samples=samples,
            fidelity_set=np.array([1.0, 1.0, 1.0]),
            color=next(colors),
        )
        ax.set_title(f"{param_names[i]}")
        plt.tight_layout()

    for i in range(len(bounds)):
        for j in range(i + 1, len(bounds)):
            ax = pg.get_next_3D_axes()
            pg.plot_marginalised_2d(
                ax=ax,
                target_indices=[i, j],
                related_indices=[k for k in range(len(bounds)) if k not in [i, j]],
                axes_labels=(param_names[i], param_names[j]),
                bounds=(bounds[i], bounds[j]),
                samples=50,
                repetitions=1,
                fidelity_set=np.array([1.0, 1.0, 1.0]),
            )
            ax.set_title(f"{param_names[i]} and {param_names[j]}")
            plt.tight_layout()

    # make temp_plots_largedata dir
    if not os.path.exists("temp_plots"):
        os.makedirs("temp_plots")

    plt.savefig(
        f"temp_plots/{combo}_{kernel_kwargs['kernel_option']}_{kernel_kwargs['mode']}.pdf"
    )


def plot_rave_true(turn_duration, legend_labels=[]):
    assert turn_duration in [1000, 3000]
    db_path = f"data/rave_go_9_1_{turn_duration}_51_100.json"
    data = json.load(open(db_path, "r"))
    x = np.array(data["X"]).squeeze() * 10_000
    y = np.array(data["Y"]).squeeze()

    plt.plot(x, y, "r")
    legend_labels.append(f"Rave {turn_duration}ms")


if __name__ == "__main__":
    # for kernel_option, mode in itertools.product(
    #     (0, 1, 2, 3, 4, 5),
    #     (0, 1, 2)
    # ):
    #     make_plots_poc({
    #         "kernel_option": kernel_option,
    #         "mode": mode,
    #     })

    # plot_rave_with_stgp()

    for num_points in [25, 50, 75, 100, 155]:
        make_plots_poc(
            {
                "kernel_option": 0,
                "mode": 0,
            },
            "rave",
            num_points,
            [0],
        )

        legend_labels = ["GP", "GP Uncertainty", "Max", "hf evals", "lf evals", "GS 3s"]
        plot_rave_true(3000, legend_labels)
        # plot_rave_true(1000, legend_labels)

        plt.title(f"Rave with {num_points} points")

        plt.legend(legend_labels)
        manager = plt.get_current_fig_manager()
        manager.window.maximize()
        plt.show()
